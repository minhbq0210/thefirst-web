import path from 'path'
import express from 'express'
import compression from 'compression'
import favicon from 'serve-favicon'

import React from 'react'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { matchRoutes } from 'react-router-config'

import AppContainer from './src/app'
import routes from './src/routes'
import fetch from 'isomorphic-fetch'

// Add these imports - Step 1
import { Provider } from 'react-redux'
import { store } from './src/redux'

const cookieParser = require('cookie-parser')
const app = express()
app.use(cookieParser())
app.use(compression({ filter: shouldCompress }))

// process.env.DEVELOPMENT ? 'view' : 'build'
const viewPath = 'build'

// Set view engine & serve static assets
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, viewPath))
app.use(favicon(path.join(__dirname, 'build', 'favicon.ico'), { maxAge: 2592000000 }))
app.use(express.static(path.join(__dirname, 'build'), { maxAge: 2592000000 }))

// Always return the main index.html, so react-router render the route in the client
app.get('*', (req, res) => {
  const branch = matchRoutes(routes, req.url)
  const promises = []
  const authorization = '&authorization=' + req.cookies.authorize

  let slug = ''
  branch.forEach(({ route, match }) => {
    switch (route.loadData) {
      case 'api_home':
        promises.push(fetch(global.rxu.config.api_home + '?' + authorization).then(res => res.json())); break

      case 'api_site_product':
        slug = req.params[0].substring('/product/'.length)
        promises.push(fetch(global.rxu.config.api_site_product + '?' + '&slug=' + slug).then(res => res.json())); break

      case 'api_site_productcate':
        slug = req.params[0].substring('/cate/'.length)
        promises.push(fetch(global.rxu.config.api_site_productcate + '?' + '&slug=' + slug).then(res => res.json())); break
    }
    console.log(req.params, '000')
  })

  Promise.all(promises).then(data => {
    if (typeof (data[0]) !== 'undefined' && data[0].success === -2) {
      res.writeHead(301, { Location: '/admin/login' })
      res.end()
    }

    const context = data.reduce((context, data) => {
      return Object.assign(context, data.data)
    }, {})

    // const serializedState = JSON.stringify(store.getState())
    const serializedContext = JSON.stringify(context)
    const html = renderToString(
      <Provider store={store}>
        <StaticRouter location={req.url} context={context} >
          <AppContainer />
        </StaticRouter>
      </Provider>
    )

    if (context.url) {
      res.writeHead(301, { Location: context.url })
      res.end()
    } else {
      let tempScriptTag = `<script type="text/javascript"> window.__STATIC_CONTEXT__  = ${serializedContext}</script>`
      return res.render('index', { html, serializedContext })
    }
  })
})

// Run server
const port = process.env.PORT || global.rxu.config.web_port
app.listen(port, err => {
  if (err) return console.error(err)
  console.log(`[RXRUNNING] Server listening at http://localhost:${port}`)
})

function shouldCompress (req, res) {
  // don't compress responses with this request header
  if (req.headers['x-no-compression'] && !req.headers['x-no-compression']) {
    return false
  }

  // fallback to standard filter function
  return compression.filter(req, res)
}
