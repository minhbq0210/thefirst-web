/**
 * About the webpack-dev-server,
 * please visit: https://webpack.js.org/configuration/dev-server/#devserver-openpage
 */
const path = require("path");
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");


// Create multiple instances
const extractHNM = new ExtractTextPlugin({ filename: 'static/css/[name]_hangnhapmy.css' })
const extractMLK = new ExtractTextPlugin({ filename: 'static/css/[name]_mualaket.css' })

module.exports = {
	devtool: 'source-map',
	/* devServer: { inline: true, hot: false, port: 3000, contentBase: path.join(__dirname, "public"), open: false, openPage: '' }, */
	entry: [
		'./src/index.js',
	],
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: 'static/js/[name].js',
		chunkFilename: 'static/js/[name].chunk.js',
		publicPath: '/',
	},
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        uglifyOptions: {
          compress: false,
          ecma: 6,
          mangle: true
        },
        sourceMap: true
      })
    ],
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendor",
          chunks: "initial",
          enforce: true
        }
      }
    }
  },
	module: {
		strictExportPresence: true,
		rules: [
			{
				test: /\.(js|jsx)$/,
				include: path.resolve(__dirname, 'src'),
				loader: 'babel-loader',
				options: {
					compact: true,
					cacheDirectory: true,
				}
			},
			{
        test:/\.css$/,
        include:[path.resolve(__dirname, './src/themes/hangnhapmy')],
        use: extractHNM.extract({
          fallback: 'style-loader',
          use: [ { loader: 'css-loader', options: { importLoaders: 1, minimize: true, sourceMap: false } } ]
        })
      },
      {
        test:/\.css$/,
        include:[path.resolve(__dirname, "./src/themes/mualaket")],
        use: extractMLK.extract({
          fallback: 'style-loader',
          use: [ { loader: 'css-loader', options: { importLoaders: 1, minimize: true, sourceMap: false } } ]
        })
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }]
      }
		] // rules
	},
	plugins: [
		new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),   
    new webpack.NoEmitOnErrorsPlugin(),
    new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.js$|\.css$|\.html$/,
        threshold: 10240,
        minRatio: 0
    }),
		extractHNM,
		extractMLK
	],
	node: {
		dgram: 'empty',
		fs: 'empty',
		net: 'empty',
		tls: 'empty',
	},
}