import path from 'path'
import express from 'express'
import compression from 'compression'
import favicon from 'serve-favicon'

import React from 'react'
import ReactDOMServer, { renderToString, renderToNodeStream } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { matchRoutes } from 'react-router-config'

import AppContainer from './src/app'
import routes from './src/routes'
import fetch from 'isomorphic-fetch'

// Add these imports - Step 1
import { Provider } from 'react-redux'
import { store } from './src/redux'

const cookieParser = require('cookie-parser')
const memcache = require('memory-cache');
const rxStatic = require('./src/classes/rxStatic')
const app = express()
app.use(cookieParser())
app.use(compression({ filter: shouldCompress }))

// process.env.DEVELOPMENT ? 'view' : 'build'
const viewPath = 'build'

// Set view engine & serve static assets
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, viewPath))
app.use(favicon(path.join(__dirname, 'build', 'favicon.ico'), { maxAge: 2592000000 }))
app.use(express.static(path.join(__dirname, 'build'), { maxAge: 2592000000 }))
// app.use("/css", express.static(path.join(__dirname, './build/static/css')));

let SEO_title = 'Anna Wholesale | Cung cấp hàng Úc Chính Hãng, Chất Lượng, Giá Tốt Nhất'
let SEO_desc = 'Dễ dàng mua sắm các sản phẩm cho mẹ và bé, vitamin, thực phẩm chức năng, bò, bào ngư với giá cả tốt nhất cùng nguồn gốc đảm bảo và chất lượng chuẩn Úc, được phân phối trực tiếp từ Anna Store Sydney Wholesale site.'

// Always return the main index.html, so react-router render the route in the client
app.get('*', (req, res) => {
  global.window = {document: {createElementNS: () => {return {}} }};
  let isStatic = rxStatic.serverStaticFiles(req, res)
  if (!isStatic) {
    const branch = matchRoutes(routes, req.url)
    const promises = []
    const authorization = '&authorization=' + req.cookies.authorize

    let slug = ''
    let strSEO = {
      slug: '',
      title: '',
      desc: '',
      canonical: ''
    }
    
    try {
      if (req.params[0]) {
        if (req.params[0] == '/') {
          strSEO = {
            name: 'homepage',
            slug: '',
            title: 'Anna Wholesale | Cung cấp hàng Úc Chính Hãng, Chất Lượng, Giá Tốt Nhất',
            desc: 'Dễ dàng mua sắm các sản phẩm cho mẹ và bé, vitamin, thực phẩm chức năng, bò, bào ngư với giá cả tốt nhất cùng nguồn gốc đảm bảo và chất lượng chuẩn Úc, được phân phối trực tiếp từ Anna Store Sydney Wholesale site.',
            canonical: global.rxu.config.web_domain
          }
        }
        if (req.params[0].indexOf('product') != -1) {
          let slug = req.params[0].substring('/product/'.length)
          if (slug.length > 0) {
            strSEO = {
              name: 'product',
              slug: slug,
              title: '',
              desc: ''
            }
          }
        }
        if (req.params[0].indexOf('news') != -1) {
          let slug = req.params[0].substring('/news/'.length)
          if (slug.length > 0) {
            strSEO = {
              name: 'news',
              slug: slug,
              title: '',
              desc: ''
            }
          } else {
            strSEO = {
              name: 'news',
              canonical: global.rxu.config.web_domain + '/news/',
              title: SEO_title,
              description: SEO_desc
            }
          }
        }
        if (req.params[0].indexOf('search') != -1) {
          let slug = req.params[0].substring('/search/'.length)
          strSEO = {
            name: 'search',
            slug: '',
            title: SEO_title,
            desc: SEO_desc,
            canonical: global.rxu.config.web_domain + '/search/' + slug
          }
        }
        if (req.params[0].indexOf('customer') != -1 ) {
          let slug = req.params[0].substring('/customer/'.length)
          strSEO = {
            name: 'customer',
            slug: '',
            title: 'Thông tin cá nhân',
            description: SEO_desc,
            canonical: global.rxu.config.web_domain + '/customer/' + slug
          }
        }
        if (req.params[0].indexOf('payment') != -1 ) {
          strSEO = {
            name: 'payment',
            slug: '',
            title: SEO_title,
            description: SEO_desc,
            canonical: global.rxu.config.web_domain
          }
        }
      }
    } catch(e) {
      console.log(e.message)
    }
    let key = '__express__'+req.originalUrl || req.url
    let cachedBody = memcache.get(key)
    let cachedContext = memcache.get(key+'_Context')
    let cachedSEO = memcache.get(key+'_SEO')
    let strSEO_tmp = cachedSEO ? JSON.parse(cachedSEO) : {title: '', description: ''}
    // if (cachedBody) {
      // let html = cachedBody
      // let serializedContext = cachedContext
      // let title = strSEO_tmp.title
      // let description = strSEO_tmp.description
      // let canonical = strSEO_tmp.canonical
      // return res.render('index', {html, serializedContext, title, description, canonical})
    // } else {
      branch.forEach(({ route, match }) => {
        switch (route.loadData) {
          case 'home_get':
            promises.push(fetch(global.rxu.config.api_home + '?' + authorization).then(res => res.json())); break

          case 'home_get_productdetail':
            slug = req.params[0].substring('/product/'.length)
            promises.push(fetch(global.rxu.config.api_home_productdetail + '?' + '&slug=' + slug).then(res => res.json())); break

          case 'home_get_newsdetail':
            slug = req.params[0].substring('/news/'.length)
            promises.push(fetch(global.rxu.config.api_home_newsdetail + '?' + '&slug=' + slug).then(res => res.json())); break
        }
      })

      // console.log(req.params[0], 'params')

      Promise.all(promises).then(async (data) => {
        if (typeof(data[0]) != 'undefined' && data[0].success == -2) {
          res.writeHead(301, {Location: '/'})
          res.end()
        }
        // console.log(data, 'data')
        const context = data.reduce((context, data) => {
          return Object.assign(context, data.data)
        }, {})
        // console.log(context,  'context')
        // const arrcontext = []
        // console.log(context, 'context')
        // var keys = Object.keys(context);
        // keys.forEach(function(key){
        //   arrcontext.push(context[key]);
        // });
        // console.log(arrcontext, 'arrcontext')

        // const serializedState = JSON.stringify(store.getState())
        const serializedContext = JSON.stringify(context)
        // const html = renderToString(
        //   <Provider store={store}>
        //     <StaticRouter location={req.url} context={context} >
        //       <AppContainer />
        //     </StaticRouter>
        //   </Provider>
        // )
        const htmlProvider = []
        await new Promise((resolve, reject) => {
            let bodyStream = renderToNodeStream(
                <Provider store={store}>
                  <StaticRouter location={req.url} context={context} >
                    <AppContainer />
                  </StaticRouter>
                </Provider>
            );
            bodyStream.on('data', (chunk) => {
                htmlProvider.push(chunk.toString());
            });
            bodyStream.on('error', (error) => {
                reject(error);
            });
            bodyStream.on('end', () => {
                resolve(htmlProvider.join(''));
            });
        });

        if (strSEO.name == 'homepage') {
          // console.log('11111')
          strSEO.description = 'Dễ dàng mua sắm các sản phẩm cho mẹ và bé, vitamin, thực phẩm chức năng, bò, bào ngư với giá cả tốt nhất cùng nguồn gốc đảm bảo và chất lượng chuẩn Úc, được phân phối trực tiếp từ Anna Store Sydney Wholesale site.';
        }
        if (strSEO.name == 'product') {
          strSEO.title = (context && context.product && context.product.seotitle) ? context.product.seotitle : SEO_title
          strSEO.description = (context && context.product && context.product.seometa) ? context.product.seometa : SEO_desc
          strSEO.canonical = global.rxu.config.web_domain + '/product/' + ((context && context.product && context.product.slug) ? context.product.slug : '')
        }
        if (strSEO.name == 'news') {
          strSEO.title = (context && context.news && context.news.seotitle) ? context.news.seotitle : SEO_title
          strSEO.description = (context && context.news && context.news.seometa) ? context.news.seometa : SEO_desc
          strSEO.canonical = global.rxu.config.web_domain + '/news/' + ((context && context.news && context.news.slug) ? context.news.slug : '')
        }
        if (strSEO.name == 'search') {
          strSEO.title = SEO_title
          strSEO.description = SEO_desc
        }

        const html = `<!DOCTYPE html>
            <html lang="vi">
              <head>
                <link rel="canonical" href="${strSEO.canonical}" />
                <link rel="stylesheet" type="text/css" href="${global.rxu.config.web_domain}/static/css/main.css">
                <meta charset="utf-8" />
                <meta name="google-site-verification" content="8dgT24iCf-0rK-pOT4W4q-rgYIZk_L201ZBumRTRDFE" />
                <meta name='viewport' content='width=device-width,minimum-scale=1,initial-scale=1' />
                <title>${strSEO.title}</title>
                <meta name="description" content="${strSEO.description}">
                <style>body {width: 100% !important;}</style>
              </head>
              <body>
                <h1 style='display: none'>${strSEO.title.slice(0, 70)}</h1>
                ${htmlProvider}
                <script src="${global.rxu.config.web_domain}/lazysizes.min.js" async></script>
              </body>
            </html>
          `
        // memcache.put(key, html, 60000);
        // memcache.put(key+'_Context', serializedContext, 60000);
        // memcache.put(key+'_SEO', JSON.stringify(strSEO), 60000);
        if (context.url) {
          res.writeHead(301, { Location: context.url })
          res.end()
        } else {
          let title = strSEO.title.slice(0, 60)
          let description = strSEO.description
          let canonical = strSEO.canonical
          return res.render('index', {html, serializedContext, title, description, canonical})
        }
      })
    // }
  }
})

// Run server
const port = process.env.PORT || global.rxu.config.web_port
app.listen(port, err => {
  if (err) return console.error(err)
  console.log(`[RXRUNNING] Server listening at http://localhost:${port}`)
})

function shouldCompress (req, res) {
  // don't compress responses with this request header
  if (req.headers['x-no-compression'] && !req.headers['x-no-compression']) {
    return false
  }

  // fallback to standard filter function
  return compression.filter(req, res)
}
