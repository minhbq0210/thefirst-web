import fs from 'fs'
import path from 'path'
import express from 'express'
import compression from 'compression'
import favicon from 'serve-favicon'

import React from 'react'
import ReactDOMServer, { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { matchRoutes } from 'react-router-config'

import AppContainer from './src/app'
import routes from './src/routes'
import fetch from 'isomorphic-fetch'
import { rxconfig } from './src/classes/ulti'

let cookieParser = require('cookie-parser')
const app = express()
app.use(cookieParser())
app.use(compression({filter: shouldCompress}))

const config = rxconfig()
// const viewPath = process.env.DEVELOPMENT ? 'view' : 'build'
const viewPath = 'build'

// Add these imports - Step 1
import { Provider } from 'react-redux'
import { store } from './src/redux'

// Set view engine & serve static assets
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, viewPath))
app.use(favicon(path.join(__dirname, 'build', 'favicon.ico'), { maxAge: 2592000000 }))
app.use(express.static(path.join(__dirname, 'build'), { maxAge: 2592000000 }))

// Always return the main index.html, so react-router render the route in the client
app.get('*', (req, res) => {

	const branch = matchRoutes(routes, req.url)
	const promises = []	
	const authorization = '&authorization=' + req.cookies.authorize	
	let slug = ''

	branch.forEach( ({route, match}) => {
		switch(route.loadData) {
			case 'admin_product_get':
				promises.push(fetch(config.api_product + '?' + authorization).then(res => res.json())); break;	

			case 'home_get':
				promises.push(fetch(config.api_home + '?' + authorization).then(res => res.json())); break;

			case 'home_get_product':				
				promises.push(fetch(config.api_home_product + '?' + authorization + '&st_full=created_at:desc&st_col=created_at&st_type=-1&pg_page=1&pg_size=12').then(res => res.json())); break;

			case 'home_get_productdetail':
				slug = req.params[0].substring('/product/'.length)
				promises.push(fetch(config.api_home_productdetail + '?' + authorization + '&slug=' + slug).then(res => res.json())); break;

			case 'home_get_newsdetail':
				slug = req.params[0].substring('/news/'.length)
				promises.push(fetch(config.api_home_newsdetail + '?' + authorization + '&slug=' + slug).then(res => res.json())); break;

			case 'category':
				promises.push(fetch(config.api_category + '?' + authorization).then(res => res.json())); break;
		}		
	})

	Promise.all(promises).then(data => {
		// data will be an array[] of datas returned by each promises.
		// Redirect to login when not login yet
		if (typeof(data[0]) != 'undefined' && data[0].success == -2) {
			res.writeHead(301, {Location: '/admin/login'})
			res.end()
		}

		const context = data.reduce((context, data) => {			
			return Object.assign(context, data.data)
		}, {})

		// const serializedState = JSON.stringify(store.getState())
		const serializedContext = JSON.stringify(context)
		const html = renderToString(			
			<Provider store={store}>
				<StaticRouter location={req.url} context={context} >
					<AppContainer />
				</StaticRouter>
			</Provider>			
		)

		if(context.url) {			
			res.writeHead(301, {Location: context.url})
			res.end()
		} else {
			return res.render('index', {html, serializedContext})
		}		
	})
})

// Run server
const port = process.env.PORT || config.web_port
app.listen(port, err => {
	if (err) return console.error(err)
	console.log(`Server listening at http://localhost:${port}`)
})

function shouldCompress (req, res) {
  if (req.headers['x-no-compression'] && false) {
    // don't compress responses with this request header
    return false
  }

  // fallback to standard filter function
  return compression.filter(req, res)
}