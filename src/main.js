import React from 'react'
import { Switch } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
// import { I18nextProvider } from 'react-i18next'

// import i18n from './i18n'
import routes from './routes'

const Main = () => (
	<main className='container main-container'>
		<Switch>
			{renderRoutes(routes)}
		</Switch>
	</main>
)

export default Main