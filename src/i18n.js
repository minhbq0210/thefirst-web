import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import viet from './i18n/viet.json';
import eng from './i18n/eng.json';


i18n
  .use(LanguageDetector)
  .init({
    // we init with resources
    resources: {
      vn: {
        translations: viet
      },
      en: {
        translations: eng
      }
    },
    fallbackLng: 'vn',

    // have a common namespace used around the full app
    ns: ['translations'],
    defaultNS: 'translations',

    keySeparator: false, // we use content as keys

    interpolation: {
      escapeValue: false, // not needed for react!!
      formatSeparator: ','
    },

    react: {
      wait: true
    }
  });

export default i18n;