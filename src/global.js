import * as ramda from 'ramda'
import jQuery from 'jquery'
import Chart from 'chart.js'
// import toastr from 'reactjs-toastr'
// import 'reactjs-toastr/lib/toast.css'
import dataLocal from './dataLocal.json'
// import DatePicker from 'react-date-picker'
// import 'react-datepicker/dist/react-datepicker.css'

import { rxsecret } from './classes/secret'

let secret = rxsecret()
let theme = secret.theme || 'mualaket'

let rxu = require('./classes/ulti').default

global.rxu = rxu
global.Chart = Chart
global.$ = global.jquery = jQuery
global.rxi = ramda
global.currentTheme = theme
// global.toastr = toastr
// global.DatePicker = DatePicker
global.rootRequire = function(name) {
  return require(__dirname + '/' + name)
}

global.rootRequiretheme = function(name) {    
  return require(__dirname + '/themes/' + theme + '/' + name)
}

// global ulti
global.ultiStaticContext = function(context) {
  if (typeof window !== 'undefined') {
    context = context || window.__STATIC_CONTEXT__    
  }

  return context
}
global.SEOtitle = 'Anna Wholesale | Cung cấp hàng Úc Chính Hãng, Chất Lượng, Giá Tốt Nhất'
global.SEOdesc = "Dễ dàng mua sắm các sản phẩm cho mẹ và bé, vitamin, thực phẩm chức năng, bò, bào ngư với giá cả tốt nhất cùng nguồn gốc đảm bảo và chất lượng chuẩn Úc, được phân phối trực tiếp từ Anna Store Sydney Wholesale site."

// Jquery addon and jquery global stuff
global.global_init = global.global_init || 'notyet'
if (global.global_init === 'notyet' && typeof window !== 'undefined') {
  
  // <script type="text/javascript">
  document.documentElement.addEventListener('touchstart', function (event) {
    if (event.touches.length > 1) {
        event.preventDefault();
    }
  }, false);
  var lastTouchEnd = 0;
  document.addEventListener('touchend', function (event) {
    var now = (new Date()).getTime();
    if (now - lastTouchEnd <= 300) {
      event.preventDefault();
    }
    lastTouchEnd = now;
  }, false);


  // Plugin
  ;(function($) {
    let uniqueCntr = 0
    $.fn.scrolled = function (waitTime, fn) {
      if (typeof waitTime === "function") {
          fn = waitTime
          waitTime = 20
      }
      var tag = "scrollTimer" + uniqueCntr++;
      this.scroll(function () {
        var self = $(this)
        var timer = self.data(tag)
        if (timer) {
            clearTimeout(timer)
        }
        timer = setTimeout(function () {
            self.removeData(tag)
            fn.call(self[0])
        }, waitTime)
        self.data(tag, timer)
      })
    }
  })(global.$)

  // Function
  let $ = global.$
  $(window).scrolled(function() {   
    let sticky = global.$('.rx-sticky'), scroll = $(window).scrollTop()
    
    if($( window ).width() > 480){
      if (scroll >= 500) {
        sticky.addClass('fixed')
      }
      else {
        sticky.removeClass('fixed')
      }
    }
    if($( window ).width() <= 480) {
      if (scroll >= 700) {
        sticky.addClass('fixed')
      }
      else {
        sticky.removeClass('fixed')
      }
    }
    if (typeof sticky.offset() !== 'undefined') {
      let sticky_bottom = sticky.offset().top + sticky.outerHeight(true) + 100
      sticky.find('.product-content-nav-item').each(function() {
        let ele = $(this)
        let theClass = ele.attr('class').match(/rxid[\w-]*\b/)[0]

        // Check what section currently      
        let block = $('#' + theClass.replace('rxid',''))      
        if (typeof block.offset() !== 'undefined' && sticky_bottom <= block.offset().top + block.outerHeight(true) && sticky_bottom >= block.offset().top) {        
          $('.product-content-nav-item').removeClass('rx-active')
          $('.' + theClass).addClass('rx-active')
        }
      })
    }    
  })
}

global.getDataLocal = function() {
  let data = []
  Object.keys(dataLocal).forEach(function(key) {
    data.push(dataLocal[key])
  })
  return data
}

let RxGlobal = {}
export default RxGlobal