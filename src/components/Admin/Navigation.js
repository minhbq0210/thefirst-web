import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'

polyfill()

class AdminNavigation extends Component {
  constructor(props) {
    super(props)
    this.state = global.ultiStaticContext(this.props.staticContext) || {}
  }

  componentDidMount() {    
  }

  render() {
    return (
      <div className='admin-cardblock'>        
        <div className='admin-cardblock-head'><div className='title'>Điều hướng</div><div className='description'>Quản lý điều hướng</div></div>
        <div className='admin-cardblock-body'>          
        </div>
      </div>
    )
  }
}

export default AdminNavigation