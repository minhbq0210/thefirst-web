import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import { rxcurrencyVnd, rxgetdatetime } from './../../classes/ulti'
import * as Datetime from 'react-datetime';
// import $ from 'jquery'


polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config

class AdminDiscount extends Component {
  constructor(props) {
    super(props)
    this.state = { 
      data: [],
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      editingData: {useboth: false, programpromotion: 1} 
    }
    this.handleDatestarted = this.handleDatestarted.bind(this);
    this.handleDateended = this.handleDateended.bind(this);
  }

  componentDidMount() {
    this.fetchAlldata()
  }

  fetchAlldata() {
    this.fetchData()
  }

  fetchData() {
    rxget(config.api_discount, this.state.paging, {
      '1': (json) => { this.setState({ data: json.data }) }
    })
  }

  //////////////
  // Discount //
  //////////////
  onClickSort(e, stcol) {
    let paging = this.state.paging                  
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ paging: paging }, () => { this.fetchData() })    
  }

  onClickData(e, perdata) {
  }

  onClickDataEdit(e, perdata) {
    let clone = JSON.parse(JSON.stringify(perdata))
    this.setState({ editingData: clone })
  }

  onClickDataInActiveNew(e, perdata) {    
    e.stopPropagation()
    rxget(config.api_discount_inactive, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataActiveNew(e, perdata) {    
    e.stopPropagation()
    rxget(config.api_discount_active, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataDeleteNew(e, perdata) {    
    e.stopPropagation()
    rxget(config.api_discount_delete, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataRestore(e, perdata) {
    e.stopPropagation()
    rxget(config.api_discount_restore, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataUpdateSubmit(e, perdata) {
    rxpost(config.api_discount_edit, this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataCreateSubmit(e, perdata) {
    let editingData = this.state.editingData
    if (editingData['metadiscount']['discountvalue']) {editingData['metadiscount']['discountvalue'] = Number(editingData['metadiscount']['discountvalue'].replace(',',''))}
    if (editingData['startdate']) {editingData['startdate'] = editingData['startdate']}
    if (editingData['enddate']) {editingData['enddate'] = editingData['enddate']}
    rxpost(config.api_discount, this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataTrash(e, isdeleted) {
    let paging = this.state.paging
    paging['search_is_deleted'] = isdeleted
    this.setState({ paging: paging }, () => {
      this.fetchData()
    }) 
  }

  onBlurDatafilter(e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging
    paging['search_' + name] = e.target.value
    this.setState({ paging: paging })

    this.timerDatafilter = setTimeout((e, name) => {
      this.fetchData()
    }, WAIT_INTERVAL)    
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onBlurDataKey(e, name, key) {
    let editingData = this.state.editingData
    if (typeof(editingData[key]) === 'undefined') {editingData[key] = {}}
    editingData[key][name] = e.target.value
    if (['discountvalue', 'startprice'].indexOf(name) !== -1) {
      let strnumber = editingData[key][name].toString().replace(/(,)/g,'')
      // editingData[key][name] = rxcurrencyVnd(strnumber)
      editingData[key][name] = strnumber
      if (editingData[key][name] === '0') {
        delete(editingData[key][name])
      }
    }
    this.setState({ editingData: editingData })    
  }

  handleChange(e, name, key) {
    let editingData = this.state.editingData
    if (key) {
      if (typeof(editingData[key]) === 'undefined') {editingData[key] = {}}
      editingData[key][name] = (!isNaN(parseFloat(e.target.value)) && isFinite(e.target.value)) ? Number(e.target.value) : e.target.value
    } else {
      editingData[name] = (!isNaN(parseFloat(e.target.value)) && isFinite(e.target.value)) ? Number(e.target.value) : e.target.value
    }
    this.setState({ editingData: editingData })   
  }

  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  toggleOne(e, name) {
    let editingData = this.state.editingData
    editingData[name] = !editingData[name]
    if (editingData['uselimit'] === true) {
      delete (editingData['amountdiscount'])
    }
    this.setState({ editingData: editingData })
  }

  // Callback upload
  callbackUpload(e) {
    this.onBlurData({target: {value: e.images}}, 'img_landscape')
  }

  // callbackUploadDetail(e) {    
  //   this.onBlurData({target: {value: e.images}}, 'img_detail')
  // }  

  // Help show functions
  helpProductcat(cats) {    
    let result = ''
    for(let i = 0; i < cats.length; i++) { result += cats[i].name+ ', ' }
    return result
  }

  helpSortClass(stcol, extraclass) {
    extraclass = extraclass || []

    let result = ''    
    if (this.state.paging.st_col === stcol) {
      result = this.state.paging.st_type === 1 ? 'rx-sort-asc' : 'rx-sort-desc'
    }

    for(let i = 0; i < extraclass.length; i++) {      
      result += ' ' + extraclass[i]
    }

    return result 
  }

  formatDate(strdate) {
    let myDate=strdate.split('/')
    let newDate=myDate[1]+'/'+myDate[0]+'/'+myDate[2]
    return (new Date(newDate).getTime() / 1000)
  }

  gencode() {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    for (var i = 0; i < 12; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  onClickGenCode(e) {
    let editingData = this.state.editingData
    editingData.code = this.gencode()
    this.setState({editingData: editingData})
  }

  onClickDiscountNew(e) {
    let clone = { name: '', created_at: 1, is_deleted: 0, is_active: 1, programpromotion: 1, useboth: false, metadiscount: {typepromotion: 1, typeapply: 0, discountvalue: '0'}, startdate: '', enddate: '' }
    this.setState({ editingData: clone })
  }

  rederSelect(arr, name, key) {
    let arrresult = []
    if (arr.constructor === Array) {
      arrresult = [<select onChange={(e) => this.handleChange(e, name, key)} key={0}>
        {arr.map(perdata => (<option value={perdata.id} key={perdata.id}>{perdata.value}</option>))}
      </select>]
    }
    return arrresult
  }

  findInArrObj(objArray, value) {
    var obj = objArray.find(function (obj) { return obj.id === value; })
    var strvalue = ''
    if (obj) {strvalue = obj.value.toLowerCase()}
    return strvalue
  }

  handleDatestarted(event) {
    // $('.rdtPicker').css('display', 'none');  
    let editingData = this.state.editingData
    editingData['startdate'] = event
    this.setState({editingData: editingData});
  }

  handleDateended(event) {
    let editingData = this.state.editingData
    editingData['enddate'] = event
    this.setState({editingData: editingData}); 
  }

  render() {
    let arrprogrampromotion = [{id: 1, value: 'Mã khuyến mãi (Coupon)'},{id: 2, value: 'Chương trình khuyến mãi'}]
    let arrtypepromotion = [{id: 1, value: 'AUD'},{id: 2, value: '% Giảm'},{id: 3, value: 'Miễn phí vận chuyển'}]
    let arrtypeapply = [{id: 0, value: 'Tất cả đơn hàng'},{id: 1, value: 'Trị giá đơn hàng từ'},{id: 2, value: 'Nhóm sản phẩm'}]//,{id: 3, value: 'Sản phẩm'},{id: 4, value: 'Nhóm khách hàng'},{id: 5, value: 'Khách hàng'},{id: 6, value: 'Biến thể'}]
    let arrapplyforcity = [{id: 0, value:'Tất cả tỉnh thành'}, {id: 50, value:'Hồ Chí Minh'}, {id: 1, value:'Hà Nội'}, {id: 32, value:'Đà Nẵng'}, {id: 57, value:'An Giang'}, {id: 49, value:'Bà Rịa - Vũng Tàu'}, {id: 47, value:'Bình Dương'}, {id: 45, value:'Bình Phước'}, {id: 39, value:'Bình Thuận'}, {id: 35, value:'Bình Định'}, {id: 62, value:'Bạc Liêu'}, {id: 15, value:'Bắc Giang'}, {id: 4, value:'Bắc Kạn'}, {id: 18, value:'Bắc Ninh'}, {id: 53, value:'Bến Tre'}, {id: 3, value:'Cao Bằng'}, {id: 63, value:'Cà Mau'}, {id: 59, value:'Cần Thơ'}, {id: 41, value:'Gia Lai'}, {id: 2, value:'Hà Giang'}, {id: 23, value:'Hà Nam'}, {id: 28, value:'Hà Tĩnh'}, {id: 11, value:'Hòa Bình'}, {id: 21, value:'Hưng Yên'}, {id: 19, value:'Hải Dương'}, {id: 20, value:'Hải Phòng'}, {id: 60, value:'Hậu Giang'}, {id: 37, value:'Khánh Hòa'}, {id: 58, value:'Kiên Giang'}, {id: 40, value:'Kon Tum'}, {id: 8, value:'Lai Châu'}, {id: 51, value:'Long An'}, {id: 6, value:'Lào Cai'}, {id: 44, value:'Lâm Đồng'}, {id: 13, value:'Lạng Sơn'}, {id: 24, value:'Nam Định'}, {id: 27, value:'Nghệ An'}, {id: 25, value:'Ninh Bình'}, {id: 38, value:'Ninh Thuận'}, {id: 16, value:'Phú Thọ'}, {id: 36, value:'Phú Yên'}, {id: 29, value:'Quảng Bình'}, {id: 33, value:'Quảng Nam'}, {id: 34, value:'Quảng Ngãi'}, {id: 14, value:'Quảng Ninh'}, {id: 30, value:'Quảng Trị'}, {id: 61, value:'Sóc Trăng'}, {id: 9, value:'Sơn La'}, {id: 26, value:'Thanh Hóa'}, {id: 22, value:'Thái Bình'}, {id: 12, value:'Thái Nguyên'}, {id: 31, value:'Thừa Thiên Huế'}, {id: 52, value:'Tiền Giang'}, {id: 54, value:'Trà Vinh'}, {id: 5, value:'Tuyên Quang'}, {id: 46, value:'Tây Ninh'}, {id: 55, value:'Vĩnh Long'}, {id: 17, value:'Vĩnh Phúc'}, {id: 10, value:'Yên Bái'}, {id: 7, value:'Điện Biên'}, {id: 42, value:'Đắk Lắk'}, {id: 43, value:'Đắk Nông'}, {id: 48, value:'Đồng Nai'}, {id: 56, value:'Đồng Tháp'}]
    let arrapplygroupproduct = [{id:0, value:'Chọn nhóm sản phẩm'}, {id:1, value:'Sản phẩm nổi bật'}, {id:2, value:'Sản phẩm khuyến mãi'}, {id:3, value:'Trang chủ'}]
    let arrapplyfororder = [{id:1, value:'Một lần trên một đơn đặt hàng'}, {id:2, value:'Cho từng mặt hàng trong giỏ hàng'}]
    // let arrapplyproduct = [{id:1, value:'Tai nghe Xiaomi Piston'}, {id:2, value:'Tai nghe Sony MDR'}, {id:3, value:'Tai nghe Sennheiser HD'}, {id:4, value:'Tai nghe Sennheiser CX'}]
    // let arrapplygroupcustomer = [{id:0, value:'Chọn nhóm khách hàng...'}, {id:1, value:'Tất cả khách hàng'}, {id:2, value:'Nhận email quảng cáo'}, {id:3, value:'Khách hàng thân thiết'}, {id:4, value:'Khách hàng tiềm năng'}, {id:5, value:'Trong nước'}]
    // let arrapplycustomer = [{id:0, value:'Chọn khách hàng...'}, {id:1, value:'nglong18101994@gmail.com'}, {id:2, value:'nglong1994@gmail.com'}, {id:3, value:'vung@gmail.com'}, {id:4, value:'nguyenlong@gmail.com'}]

    // Data tables
    let data = this.state.data.map(perdata => (
      <tr key={perdata._id}>
        <td>
          <div className='lk-dropdown'>
            <div className={(perdata.is_active === 1) ? 'lk-discount' : 'lk-discount lk-inactive'} style={(perdata.programpromotion === 1 && perdata.is_active === 1) ? {background:'#1c8aa6'} : {} }>
              <div className='lk-discount-inner'>
                {perdata.programpromotion === 1 && <p className='lk-discount-code'>MÃ KHUYẾN MÃI: <b>{perdata.code}</b></p>}
                {perdata.programpromotion !== 1 && <p className='lk-discount-code'>CHƯƠNG TRÌNH KHUYẾN MÃI: <b>{perdata.name}</b></p>}
                <p className='lk-discount-desc'>{perdata.metadiscount.typepromotion !== 3 && 'Giảm'}{perdata.metadiscount.typepromotion === 3 && 'Miễn phí vận chuyển'} {rxcurrencyVnd(perdata.metadiscount.discountvalue)}{perdata.metadiscount.typepromotion === 2 && '%'}{perdata.metadiscount.typepromotion !== 2 && '' } cho {this.findInArrObj(arrtypeapply, perdata.metadiscount.typeapply)} {perdata.programpromotion !== 1 && '(khi mua '+perdata.metadiscount.quantity+' sản phẩm trở lên)'}</p>
                {(perdata.programpromotion === 1 && !perdata.useboth) && <p>(Mã khuyến mãi <b>không thể sử dụng</b> chung với chương trình khuyến mãi)</p>}
                {(perdata.programpromotion === 1 && perdata.useboth) && <p>(Mã khuyến mãi <b>được sử dụng</b> chung với chương trình khuyến mãi)</p>}
              </div>
            </div>
          </div>
        </td>
        {perdata.programpromotion !== 1 && <td></td>}
        {perdata.programpromotion === 1 && !perdata.uselimit && <td>{perdata.amountdiscountused} / {perdata.amountdiscount}</td>}
        {perdata.programpromotion === 1 && perdata.uselimit && <td>Không giới hạn</td>}
        <td>{rxgetdatetime(perdata.startdate)}</td>
        <td>{rxgetdatetime(perdata.enddate)}</td>
        <td>
          { this.state.paging.search_is_deleted !== 1 &&
            <div> {(perdata.is_active !== 0) && 
                  <span className='admin-table-on' onClick={(e) => this.onClickDataInActiveNew(e, perdata)}>Ngừng</span>}{(perdata.is_active === 0) && <span className='admin-table-off' onClick={(e) => this.onClickDataActiveNew(e, perdata)}>Sử dụng</span>}
                  <span className='rx-action-edit' onClick={(e) => this.onClickDataEdit(e, perdata)}>Sửa</span>
                  <span className='rx-action-delete' onClick={(e) => this.onClickDataDeleteNew(e, perdata)}>Xoá</span> </div> }
          { this.state.paging.search_is_deleted === 1 && 
            <div> <span className='rx-action-restore' onClick={(e) => this.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}
        </td></tr>))

    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Khuyến mãi</div><div className='description'>Quản lý khuyến mãi</div></div>
          <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>   
            <div className='col-lg-12 col-sm-12'>
              <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurDatafilter(e, 'name')} /></span>
              <i className='icon-plus rx-recycle' onClick={(e) => this.onClickDiscountNew(e)}></i>
              { this.state.paging.search_is_deleted !== 1 && <i className='icon-trash rx-recycle' onClick={(e) => this.onClickDataTrash(e, 1)}></i> }              
              { this.state.paging.search_is_deleted === 1 && <i className='icon-list rx-recycle' onClick={(e) => this.onClickDataTrash(e, 0)}></i> }

              <div className='admin-table-pagination admin-pagin-right'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>

              <div className='admin-table-wrap'><table className='admin-table-product preset'>
                <thead>
                  <tr>
                    <th>Chi tiết</th>
                    <th>Số lần sử dụng</th>
                    <th>Thời gian bắt đầu</th>
                    <th>Thời gian kết thúc</th>
                    <th>Thao tác</th>
                  </tr>
                </thead>
                <tbody>
                  {data}
                </tbody>
              </table></div>
              <div className='admin-table-pagination'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
            </div>
          </div>
          <div className='clearfix-martop'></div>          
        </div>
        
        { this.state.editingData.created_at && <div className='admin-cardblock-form'>
          <div className='admin-cardblock-formname'>Tạo khuyến mãi
            <div className='admin-cardblock-formbtns'>
              <div className='admin-cardblock-btns'>
                <a tabIndex='5' className='btn-cancel' onClick={(e) => this.onClickDataEdit(e, {})} onKeyPress={(e) => this.onClickDataEdit(e, {})}>Trở về</a>
                {this.state.editingData.created_at !== 1 && <a tabIndex='6' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>}
                <a tabIndex='7' className='btn-clone' onClick={(e) => this.onClickDataCreateSubmit(e)} onKeyPress={(e) => this.onClickDataCreateSubmit(e)}>Tạo mới</a>
              </div>
            </div>
          </div>

          <div className='row admin-cardblock-formbody'>
            <div className='admin-card admin-cardblock-formbody-left col-lg-8 col-md-8 col-sm-12 clearfix'>
            <div className='admin-cardblock-body'>
              <div className='row fullwidth-form-body'>
                {this.state.editingData.programpromotion !== 2 && <div className='lk-box-main-option-discount'>
                  <div className='fullwidth-label'>Tạo mã khuyến mãi</div>
                  <div className='lk-box-main-option-discount'>
                    <input tabIndex='8' type='text' value={this.state.editingData.code || ''} onChange={(e) => this.onBlurData(e, 'code')} className='fullwidth-input lk-input-text-code' />
                    <button className='lk-button-gen-code' onClick={(e) => this.onClickGenCode(e)}>Tạo mã tự động</button>
                  </div>
                  <div>
                    <div>Khách hàng sẽ nhập mã giảm giá này lúc thực hiện thanh toán.</div>
                  </div>
                </div>}

                {this.state.editingData.programpromotion === 2 && <div className='lk-box-main-option-discount'>
                  <div className='fullwidth-label'>Tạo chương trình khuyến mãi</div>
                  <input tabIndex='8' type='text' value={this.state.editingData.name || ''} onChange={(e) => this.onBlurData(e, 'name')} className='fullwidth-input' />
                </div>}
            
                <div className='lk-line-box'></div>
                <div>
                  <div className='fullwidth-label'>Chọn chương trình khuyến mãi</div>
                  <div className='lk-select-form'>
                    {this.rederSelect(arrprogrampromotion, 'programpromotion')}
                  </div>
                 
                  {this.state.editingData.programpromotion !== 2 && <div>
                    <label className='lk-checbox-main'>Cho phép sử dụng chung với chương trình khuyến mãi
                      <input type='checkbox' className='checkbox' onClick={(e) => this.toggleOne(e, 'useboth')}/>
                      <span className='checkmark'></span>
                    </label>
                    <div className='fullwidth-label'>Nhập số lần sử dụng của mã khuyến mãi?</div>
                    <div>
                      {!this.state.editingData.uselimit && <input type='number' className='lk-input-amount-discount'value={this.state.editingData.amountdiscount || ''} onChange={(e) => this.onBlurData(e, 'amountdiscount')} />}
                      {this.state.editingData.uselimit && <input type='number' value='∞' className='lk-input-amount-discount' disabled/>}

                      <div className='lk-box-check-limit-discount'>
                        <div className='rs-checkbox-group-body'>
                          <label className='lk-checbox-main'>Không giới hạn
                            <input type='checkbox' className='checkbox' onClick={(e) => this.toggleOne(e, 'uselimit')}/>
                            <span className='checkmark'></span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>}
                </div>

                <div className='lk-line-box'></div>
                <div className='lk-box-main-type-option-discount'>
                  <div className='fullwidth-label'>Loại khuyến mãi</div>
                    <div className='lk-wrapper-row'>
                      
                      <div>
                        <div className='lk-text-title-type-discount'></div>
                        <div className='lk-select-form'>
                          {this.rederSelect(arrtypepromotion, 'typepromotion', 'metadiscount')}
                        </div>
                      </div>
                      
                      {(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typepromotion !== 3) && <div>
                        <div className='lk-text-title-type-discount'>Giảm</div>
                        <div className='next-input--stylized'>
                          <input tabIndex='8' type='text' value={this.state.editingData.metadiscount.discountvalue || ''} onChange={(e) => this.onBlurDataKey(e, 'discountvalue', 'metadiscount')} className='next-input next-input--invisible fullwidth-input' placeholder='0' />
                          {this.state.editingData.metadiscount.typepromotion === 2 && <span className='next-input-add-on next-input__add-on--after'>%</span>}
                          {this.state.editingData.metadiscount.typepromotion !== 2 && <span className='next-input-add-on next-input__add-on--after'></span>}
                        </div>
                      </div>}

                      {(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typepromotion !== 3) && <div>
                        <div className='lk-text-title-type-discount'>Áp dụng cho</div> 
                        <div className='lk-select-form'>
                          {this.rederSelect(arrtypeapply, 'typeapply', 'metadiscount')}
                        </div>
                      </div>}

                      {(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typepromotion === 3) && <div>
                        <div className='lk-text-title-type-discount'>Đối với mức phí vận chuyển nhỏ hơn hoặc bằng</div>
                        <div className='next-input--stylized'>
                          <input tabIndex='8' type='text' value={this.state.editingData.metadiscount.discountvalue || ''} onChange={(e) => this.onBlurDataKey(e, 'discountvalue', 'metadiscount')} className='next-input next-input--invisible fullwidth-input' placeholder='0' />
                          <span className='next-input-add-on next-input__add-on--after'></span>
                        </div>
                      </div>}
                      
                      {(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typepromotion === 3) && <div>
                        <div className='lk-text-title-type-discount'>Áp dụng cho</div>
                        <div className='lk-select-form'>
                          {this.rederSelect(arrapplyforcity, 'applyforcity', 'metadiscount')}
                        </div>
                      </div>}
                      
                      {(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typeapply === 1) && <div>
                        <div className='next-input--stylized'>
                          <input tabIndex='8' type='text' value={this.state.editingData.metadiscount.startprice || ''} onChange={(e) => this.onBlurDataKey(e, 'startprice', 'metadiscount')} className='next-input next-input--invisible fullwidth-input' placeholder='0' />
                          <span className='next-input-add-on next-input__add-on--after'></span>
                        </div>
                      </div>}

                      {(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typeapply === 2) && <div>
                        <div className='lk-select-form'>
                          {this.rederSelect(arrapplygroupproduct, 'applygroupproduct', 'metadiscount')}
                        </div>
                      </div>}

                      {(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typeapply === 2) && <div>
                        <div className='lk-select-form'>
                          {this.rederSelect(arrapplyfororder, 'applyfororder', 'metadiscount')}
                        </div>
                      </div>}

                      {/*(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typeapply === 3) && <div>
                        <div className='lk-select-form'>
                          {this.rederSelect(arrapplyproduct, 'applyproduct', 'metadiscount')}
                        </div>
                      </div>*/}

                      {(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typeapply === 3) && <div>
                        <div className='lk-select-form'>
                          {this.rederSelect(arrapplyfororder, 'applyfororder', 'metadiscount')}
                        </div>
                      </div>}

                      {/*(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typeapply === 4) && <div>
                        <div className='lk-select-form'>
                          {this.rederSelect(arrapplygroupcustomer, 'applygroupcustomer', 'metadiscount')}
                        </div>
                      </div>*/}

                      {/*(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typeapply === 5) && <div>
                        <div className='lk-select-form'>
                          {this.rederSelect(arrapplycustomer, 'applycustomer', 'metadiscount')}
                        </div>
                      </div>*/}

                      {(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typeapply === 6) && <div>
                        <div className='next-input--stylized'>
                          <input tabIndex='8' type='text' value={this.state.editingData.metadiscount.distortionproduct || ''} onChange={(e) => this.onBlurDataKey(e, 'distortionproduct', 'metadiscount')} className='next-input next-input--invisible fullwidth-input' placeholder='' />
                        </div>
                      </div>}

                      {(this.state.editingData.metadiscount && this.state.editingData.metadiscount.typeapply === 6) && <div>
                        <div className='lk-select-form'>
                          {this.rederSelect(arrapplyfororder, 'applyfororder', 'metadiscount')}
                        </div>
                      </div>}
                    </div>

                    <div className='lk-wrapper-row'>
                      {this.state.editingData.programpromotion === 2 && <div>
                        <div className='fullwidth-label'>Số lượng sản phẩm áp dụng:</div>
                        <input tabIndex='8' type='number' value={this.state.editingData.metadiscount.quantity || ''} onChange={(e) => this.onBlurDataKey(e, 'quantity', 'metadiscount')} className='fullwidth-input' />
                      </div>}
                    </div>

                  </div>
                </div>
              </div>
            </div>

            <div className='admin-card admin-cardblock-formbody-right col-lg-4 col-md-4 col-sm-12 clearfix'>
              <div className='admin-cardblock-body'>
                <div className='fullwidth-form-body'>
                  <div className='fullwidth-label'>Thời gian áp dụng</div>  
                  <div className='lk-line-box'></div>
                  <div className='fullwidth-label'>Bắt đầu khuyến mãi</div>
                    {/*<input tabIndex='8' type='text' placeholder='DD/MM/YYYY' value={this.state.editingData.startdate || ''} onChange={this.handleDatestarted} className='fullwidth-input' />*/}
                    <Datetime dateFormat='DD/MM/YYYY' closeOnSelect='true' timeformat='hh-mm-ss' id='startdate' value={this.state.editingData.startdate || ''} onChange={(e) => this.handleDatestarted(e)} className='fullwidth-input'/>
                  <div className='fullwidth-label'>Hết hạn khuyến mãi</div>
                    {/*<input tabIndex='8' type='text' placeholder='DD/MM/YYYY' value={this.state.editingData.enddate || ''} onChange={this.handleDateended} className='fullwidth-input' />*/}
                    <Datetime dateFormat='DD/MM/YYYY' closeOnSelect='true' timeformat='hh-mm-ss' value={this.state.editingData.enddate || ''} onChange={(e) => this.handleDateended(e)} className='fullwidth-input'/>
                  <label className='lk-checbox-main'>Không bao giờ hết hạn
                    <input type='checkbox' className='checkbox' onClick={(e) => this.toggleOne(e, 'checkenddate')}/>
                    <span className='checkmark'></span>
                  </label>
                </div>        
              </div>
            </div>
            <div className='admin-cardblock-btns'>
              <a tabIndex='19' className='btn-cancel' onClick={(e) => this.onClickDataEdit(e, {})} onKeyPress={(e) => this.onClickDataEdit(e, {})}>Trở về</a>
              {this.state.editingData.created_at !== 1 && <a tabIndex='20' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>}
              <a tabIndex='21' className='btn-clone' onClick={(e) => this.onClickDataCreateSubmit(e)} onKeyPress={(e) => this.onClickDataCreateSubmit(e)}>Tạo mới</a>
            </div>
          </div>
        </div> }
      </div>
    )
  }
}

export default AdminDiscount