import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'

import RxCrud from './../Shares/RxCrud'

polyfill()
class AdminSettingFilter extends Component {  

  constructor(props) {
    super(props)    
    this.state = {          
      flagUpdateCat: false, curCat: 0
    }

    this.onClickCatItem = this.onClickCatItem.bind(this)
    this.runUpdateCat = this.runUpdateCat.bind(this)
  }

  componentDidMount() {
  }
  
  onClickCatItem(e) {
    this.setState({flagUpdateCat: !this.state.flagUpdateCat, curCat: e._id})
  }

  runUpdateCat(e) {
    let paging = (this.state.curCat)? { searchid_appdist: this.state.curCat, st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 } : { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 }
    e.inthis.setState({paging: paging}, () => { e.inthis.fetchData() })    
  }
  
  // R E N D E R S
  renderHead(inthis) {
    return (<tr>
      <th className={inthis.helpSortClass('name')} onClick={(e) => inthis.onClickSort(e, 'name')}>Tên</th>
      <th className={inthis.helpSortClass('type')} onClick={(e) => inthis.onClickSort(e, 'type')}>Loại</th>  
      <th>Thao tác</th>
    </tr>)
  }

  renderBody(inthis) {
    return inthis.state.data.map(perdata => (<tr key={perdata._id}>
      <td>{perdata.name}</td> 
      <td>{perdata.type}</td> 
      <td>
        { inthis.state.paging.search_is_deleted !== 1 &&
          <div>{(perdata.is_active !== 0) && <span className='admin-table-on'>On</span>}{(perdata.is_active === 0) && <span className='admin-table-off'>Off</span>}                  
                <span className='rx-action-edit' onClick={(e) => inthis.onClickDataEdit(e, perdata)}>Sửa</span>
                <span className='rx-action-delete' onClick={(e) => inthis.onClickDataDelete(e, perdata)}>Xoá</span> </div> }
        { inthis.state.paging.search_is_deleted === 1 && 
          <div> <span className='rx-action-restore' onClick={(e) => inthis.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}
      </td>
    </tr>))
  }

  render() {        
    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Cấu hình bộ lọc</div><div className='description'>Quản lý bộ lọc khi tìm kiếm</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            <div className='col-lg-12 col-md-12 col-sm-12'>
              <RxCrud renderHead={this.renderHead} flagUpdate={this.state.flagUpdateCat} parentUpdate={this.runUpdateCat} renderBody={this.renderBody} api={'api_setoption'} form={
              [ {name: 'Tên bộ lọc', func: (inthis) => (<input tabIndex='1' type='text' value={inthis.state.editingData.name} onChange={(e) => inthis.onBlurData(e, 'name')} className='fullwidth-input' />)},
                {name: 'Loại thuộc tính', func: (inthis) => (<div><div>VD: Nếu bộ lọc là màu sắc loại là "color", Kích thước là "size", Vật liệu là "physical", Kiểu dáng là "design", Thương hiệu là "trademark" </div><input tabIndex='2' type='text' value={inthis.state.editingData.type} onChange={(e) => inthis.onBlurData(e, 'type')} className='fullwidth-input' /></div>)},
                {name: 'Thuộc tính', func: (inthis) => (<div>
                    <div className='row'>
                      <div className='bootstrap-tagsinput'> 
                          {(inthis.state.editingData && inthis.state.editingData.value) && inthis.state.editingData.value.map((opttext, indextext) => (<span className='tag label label-tags' key={indextext}>{opttext.value}<span className='remove-property' onClick={e => inthis.onClickDelProperty(e, indextext)} ></span></span> ))}
                          <input type='text' placeholder='Thêm thuộc tính (nhấn Enter để thêm thuộc tính)' value={inthis.state.valuetext} onKeyPress={e => inthis.handleKeyPress(e, 'addproperty')} onChange={e => inthis.updateInputValue(e, 'value')}/>
                      </div>
                    </div>
                  </div>)},
              ]} />
            </div>                      
          </div>
          <div className='clearfix-martop'></div>          
        </div>    
      </div>
    )
  }
}

export default AdminSettingFilter