import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import RxUpload from './../Shares/RxUpload'
import { WithContext as ReactTags } from 'react-tag-input'
import CKEditor from 'react-ckeditor-component'

polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config

class AdminCateProduct extends Component {
  constructor(props) {
    super(props)

    this.state = { 
      data: [], cate: [], brand:[], tag: [], cateall: [], brandall:[], option: [], arrcate: [],
      allcate: {_id: 0, name: 'Tất cả'}, 
      curobj: {cate: 0}, 
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      pagingcat: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      pagingbrand: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      editingCate: {},
      editingBrand: {},
      editingData: {},
      msg: '',
      tags: [],
      suggestions: [],
    }
    this.state.quantity = this.state.quantity || 0 
    this.onChangeContentCKE = this.onChangeContentCKE.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleAddition = this.handleAddition.bind(this)
    this.handleDrag = this.handleDrag.bind(this)
  }

  componentDidMount() {
    this.fetchAlldata()
    this.timerCatefilter = null
    this.timerDatafilter = null
  }

  fetchAlldata() {
    this.fetchCate()
    this.fetchAllCate()
    this.setState({ allcate: {_id: 0, name: 'Tất cả'} })
  }
  fetchAllCate() {
    rxget(config.api_category,{ pg_size: 1000 }, {
      '1': (json) => { this.setState({ cateall: this.sortName(json.data.cate) }) }
    })
  }
  
  fetchCate() {    
    rxget(config.api_category, this.state.pagingcat, {
      '1': (json) => { this.setState({ cate: json.data.cate, quantity: json.data.count}) }
    })
  }

  //////////////
  // CATEGORY //
  //////////////

  onClickCateEdit(e, perdata) {
    let clone = JSON.parse(JSON.stringify(perdata))
    this.setState({ editingCate: clone })
  }

  onClickCateDelete(e, perdata) {
    e.stopPropagation()
    rxget(config.api_category_delete, perdata, {
      '1': (json) => { this.fetchCate(); }
    })
  }

  onClickCateRestore(e, perdata) {
    e.stopPropagation()
    rxget(config.api_category_restore, perdata, {
      '1': (json) => { this.fetchCate() }
    })
  }

  onClickCateUpdateSubmit(e, perdata) {
    rxpost(config.api_category_edit, this.state.editingCate, {
      '1': (json) => { 
        this.fetchCate()
        this.onClickCateEdit({}, {}) 
        this.setState({ msg: '' })
      },
      '-2': (json) => { this.setState({ msg: 'Fields must not be empty' })}
    })
  }

  onClickCateCreateSubmit(e, perdata) {
    rxpost(config.api_category, this.state.editingCate, {
      '1': (json) => { 
        this.fetchCate()
        this.onClickCateEdit({}, {}) 
        this.setState({ msg: '' })
      },
      '-2': (json) => { this.setState({ msg: 'Fields must not be empty' })}
    })
  }

  onClickCateTrash(e, isdeleted) {
    let pagingcat = this.state.pagingcat
    pagingcat['search_is_deleted'] = isdeleted
    this.setState({ pagingcat: pagingcat }, () => {
      this.fetchCate()
    }) 
  }

  onClickCateNew(e) {
    let timeStr = Date.now().toString()
    let clone = { name: 'Cate_'+ timeStr.substr(timeStr.length - 5), desc: '', created_at: 1, is_deleted: 0, is_active: 1, parent_id: 0, is_brand:0, tags:[] }
    this.setState({ editingCate: clone })
  }

  onBlurCatefilter(e, name) {
    clearTimeout(this.timerCatefilter)
    let pagingcat = this.state.pagingcat
    pagingcat['search_' + name] = e.target.value  
    this.setState({ pagingcat: pagingcat })

    this.timerCatefilter = setTimeout(() => { this.fetchCate() }, WAIT_INTERVAL)   
  }

  onBlurCate(e, name) {
    let editingCate = this.state.editingCate
    editingCate[name] = e.target.value
    if (name === 'priority') {
      editingCate[name] = Number(e.target.value)  
    } 
    this.setState({ editingCate: editingCate })    
  }
  onBlurDataValue(value, name) {
    let editingData = this.state.editingData
    editingData[name] = value 
    this.setState({editingData: editingData})
  }
  // Pagin
  onClickPaginCatBack(e) {
    let pagingcat = this.state.pagingcat
    pagingcat.pg_page = (pagingcat.pg_page > 1)? (pagingcat.pg_page - 1) : pagingcat.pg_page
    this.setState({ pagingcat: pagingcat }, () => { this.fetchCate() })
  }

  onClickPaginCatNext(e) {
    let pagingcat = this.state.pagingcat
    pagingcat.pg_page += 1
    this.setState({ pagingcat: pagingcat }, () => { this.fetchCate() })
  }

  onChangeContentCKE(evt) {
    let editingCate = this.state.editingCate
    let newContent = evt.editor.getData()  
    editingCate.desc = newContent
    this.setState({ editingCate: editingCate })
  } 

  // Callback upload
  callbackUploadCate(e) {
    this.onBlurCate({target: {value: e.images}}, 'img_landscape')
  }

  handleChangeCateParent(e, name) {
    let editingCate = this.state.editingCate
    editingCate[name] = e.target.value
    let objCate = this.state.cateall.find(function (obj) { return obj._id === e.target.value })
    if (objCate) {
      editingCate['trace'] = (editingCate['_id']) ? objCate.trace + '-' + editingCate['_id'] : objCate.trace
    } else {
      editingCate['trace'] = ''
    }
    this.setState({ editingCate: editingCate })   
  }

  sortName(obj){
    var byName = obj.slice(0);
    byName.sort(function(a,b) {
        var x = a.name.toLowerCase();
        var y = b.name.toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
    });
    return byName;
  }
  onClickSort(e, stcol) {
    let paging = this.state.pagingcat                 
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ pagingcat: paging }, () => { this.fetchCate() })    
  }

  onBlurCatefind(e) {
    let cateall = this.state.cateall
    let idcat = ''
    for(let i in cateall){
      if(e === cateall[i].name){
        idcat = cateall[i]._id
      }
    }
    return idcat
  }

  handleDelete(i) {
    let tags = this.state.editingCate.tags || []
    tags.splice(i, 1)
    this.setState({tags: tags})
  }

  handleAddition(tag) {
    let tags = this.state.editingCate.tags || []
    tags.push({
        id: tags.length + 1 || 0,
        text: tag
    })
    this.setState({tags: tags})
  }

  handleDrag(tag, currPos, newPos) {
    let tags = this.state.editingCate.tags || []

    // mutate array
    tags.splice(currPos, 1)
    tags.splice(newPos, 0, tag)

    // re-render
    this.setState({ tags: tags })
  }
  helpProductcat(cats) { 
    let result = ''
    if (cats) {
      for(let i = 0; i < cats.length; i++) { 
        result += cats[i].text + ', '
      }
      return result.slice(0, -2)
    }    
  }

  render() {
    let suggestions = []
    let tags = this.state.editingCate.tags || []
    //total page
    let totalpage = 0
    if(this.state.quantity && this.state.quantity > 0){
      totalpage = Math.ceil(this.state.quantity / this.state.pagingcat.pg_size)
    }
    let cate = this.state.cate.map(perdata => (
      <tr className={(perdata._id === this.state.curobj.cate) && 'rx-active'} key={perdata._id}>
        <td className='table-lowpadding'><img className='admin-table-product-img lazyload' alt={perdata.name} data-src={config.base_api + '/upload/image/' + (perdata.img_landscape || 'ico_app_default.png')} /></td>
        <td><span>{perdata.name}</span></td>
        <td><span>{perdata.nameEng ? perdata.nameEng : ''}</span></td>
        <td>{this.helpProductcat(perdata.tags)}</td>
        <td>{perdata.priority}</td>
        <td>
          {perdata.desc}
        </td>
        <td>
          { this.state.pagingcat.search_is_deleted !== 1 &&
            <div> <span className='rx-action-edit' onClick={(e) => this.onClickCateEdit(e, perdata)}>Sửa</span>
                  <span className='rx-action-delete' onClick={(e) => this.onClickCateDelete(e, perdata)}>Xoá</span></div>}
          { this.state.pagingcat.search_is_deleted === 1 &&
            <div><span className='rx-action-restore' onClick={(e) => this.onClickCateRestore(e, perdata)}>Khôi phục</span></div>}
        </td>
      </tr>))
    
    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Danh mục ưa thích</div><div className='description'>Quản lý danh mục ưa thích</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            <div className='col-lg-12 col-md-12 col-sm-12'>
            <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurCatefilter(e, 'name')} /></span>
            <i className='icon-plus rx-recycle' onClick={(e) => this.onClickCateNew(e)}></i>
            { this.state.pagingcat.search_is_deleted !== 1 && <i className='icon-trash rx-recycle' onClick={(e) => this.onClickCateTrash(e, 1)}></i> }              
            { this.state.pagingcat.search_is_deleted === 1 && <i className='icon-list rx-recycle' onClick={(e) => this.onClickCateTrash(e, 0)}></i> }              
            <div className='admin-totalpage admin-pagin-right'> Total page: {totalpage}</div>
            <div className='admin-table-wrap'><table className='admin-table-cate preset'>                            
              <thead>
                <tr>
                  <th>Hình ảnh</th>
                  <th onClick={(e) => this.onClickSort(e, 'name')}>Tên danh mục (Viet ver)</th>
                  <th onClick={(e) => this.onClickSort(e, 'nameEng')}>Tên danh mục (Eng ver)</th>
                  <th >Keywords</th>
                  <th onClick={(e) => this.onClickSort(e, 'priority')}>Độ ưu tiên</th>
                  <th>Mô tả</th>
                  <th style={{width: 100}}>Thao tác</th>
                </tr>
              </thead>
              <tbody>
                {cate}
              </tbody>
            </table></div>
            <div className='admin-table-pagination'>
              {(this.state.pagingcat.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginCatBack(e)}><i className='icon-arrow-left'></i></div>}
              <div className='pagin-curr'>{this.state.pagingcat.pg_page}</div>
              {(this.state.cate.length >= this.state.pagingcat.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginCatNext(e)}><i className='icon-arrow-right'></i></div>}
            </div>
          </div>
          <div className='clearfix-martop'></div>          
        </div>

        { this.state.editingCate.created_at && <div className='admin-cardblock-form'>
            <div className='admin-cardblock-formname'>Sửa danh mục <b>{this.state.editingCate.name}</b><div className='admin-cardblock-formclose' onClick={(e) => this.onClickCateEdit(e, {})}>x</div></div>
            <div className='admin-cardblock-formbody'><div className='col-sm-5 clearfix'>
              { this.state.msg && <div className='cart-page-error' >{this.state.msg}</div> }
              <div className='col-md-3 col-sm-3 img-detail'>
                <div className='fullwidth-label'>Hình ảnh</div>
                <RxUpload callback={(e) => this.callbackUploadCate(e)} images={this.state.editingCate.img_landscape} />                
              </div>
              <div className='fullwidth-label'>Tên danh mục (Viet ver)</div><input tabIndex='1' type="text" value={this.state.editingCate.name} onChange={(e) => this.onBlurCate(e, 'name')} className='fullwidth-input' />
              <div className='fullwidth-label'>Tên danh mục (Eng ver)</div><input tabIndex='2' type="text" value={this.state.editingCate.nameEng} onChange={(e) => this.onBlurCate(e, 'nameEng')} className='fullwidth-input' />
              <div className='fullwidth-label'>Mô tả danh mục</div>{/*<input tabIndex='3' type="text" value={this.state.editingCate.desc} onChange={(e) => this.onBlurCate(e, 'desc')} className='fullwidth-input' />*/}
              <CKEditor activeClass='p10' content={this.state.editingCate.desc} events={{ 'change': this.onChangeContentCKE }} />
              {/*<div className='fullwidth-label'>Chọn danh mục cha</div>
              <div className='lk-select-form'>
                <select tabIndex='4' onChange={(e) => this.handleChangeCateParent(e, 'parent_id')} value={this.state.editingCate.parent_id} >
                  <option value={'0'}>Không có danh mục cha</option>
                  {this.state.cateall.map(perdata => (
                    perdata.is_brand === 0 && <option value={perdata._id} key={perdata._id}>{perdata.name}</option>))}
                </select>
              </div>*/}
              <div className='fullwidth-form-body'>
                <div className='fullwidth-label'>SEO keywords</div>
                <div className='rxselectbox-wrap'>
                  <div className='rxselectbox-result clearfix' tabIndex='22'>
                    <ReactTags tags={tags} suggestions={suggestions} handleDelete={this.handleDelete} handleAddition={this.handleAddition} autocomplete={1} minQueryLength={1} 
                      placeholder='Nhập từ khoá, enter để kết thúc 1 từ khoá'
                      classNames={{ tagInput: 'tagsinput', selected: '', tag: 'tags', remove: 'tagsremove', suggestions: 'tagsuggest', }} />
                  </div>
                </div> 
              </div>
              <div className='fullwidth-label'>Chọn ưu tiên sắp sếp menu danh mục </div><input tabIndex='5' type='number' value={this.state.editingCate.priority || 0} onChange={(e) => this.onBlurCate(e, 'priority')} className='fullwidth-input' />
              <div className='admin-cardblock-btns'>
                <a tabIndex='6' className='btn-cancel' onClick={(e) => this.onClickCateEdit(e, {})} onKeyPress={(e) => this.onClickCateEdit(e, {})}>Trở về</a>
                {this.state.editingCate.created_at !== 1 && <a tabIndex='7' className='btn-edit' onClick={(e) => this.onClickCateUpdateSubmit(e)} onKeyPress={(e) => this.onClickCateUpdateSubmit(e)}>Cập nhật</a>}
                <a tabIndex='8' className='btn-clone' onClick={(e) => this.onClickCateCreateSubmit(e)} onKeyPress={(e) => this.onClickCateCreateSubmit(e)}>Tạo mới</a>
              </div>
            </div></div>
          </div> }
        </div>
      </div>
    )
  }
}

export default AdminCateProduct