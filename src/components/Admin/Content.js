import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxgetdate } from './../../classes/ulti'

import CKEditor from 'react-ckeditor-component'

import RxUpload from './../Shares/RxUpload'
import RxExpandable from './../Shares/RxExpandable'
import RxSelectbox from './../Shares/RxSelectbox'
import RxCrud from './../Shares/RxCrud'
import RxToggle from './../Shares/RxToggle'

import { rxget } from './../../classes/request'

polyfill()
const config = global.rxu.config
class AdminContent extends Component {  

  constructor(props) {
    super(props)    
    this.state = {          
      flagUpdateCat: false, curCat: 0,
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 100 }
    }

    this.onClickCatItem = this.onClickCatItem.bind(this)
    this.runUpdateCat = this.runUpdateCat.bind(this)
  }

  componentDidMount() {
    // this.fetchData()
  }

  fetchData() {  
    rxget(config.api_contentcate, this.state.paging, {
      '1': (json) => { this.setState({ contentcate: json.data }) }
    })
  }
  
  onClickCatItem(e) {
    this.setState({flagUpdateCat: !this.state.flagUpdateCat, curCat: e._id})
  }

  runUpdateCat(e) {
    let paging = (this.state.curCat)? { searchid_appdist: this.state.curCat, st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 } : { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 }
    e.inthis.setState({paging: paging}, () => { e.inthis.fetchData() })    
  }

  // R E N D E R S
  renderCatHead(inthis) {
    return(<tr><th>Danh mục</th><th>Thao tác</th></tr>)
  }

  renderCatBody(inthis) {    
    let tempData = inthis.state.data.slice(0)
    tempData.unshift({_id: 0, name: 'Xem tất cả'})
    return tempData.map((perdata, index) => (
      <tr key={perdata._id} onClick={(e) => inthis.run('onClickCatItem', perdata)} ><td>{perdata.name}</td>
        { perdata._id !== 0 ? <td>{ inthis.state.paging.search_is_deleted !== 1 && <div><span className='rx-action-edit' onClick={(e) => inthis.onClickDataEdit(e, perdata)}>Sửa</span> <span className='rx-action-delete' onClick={(e) => inthis.onClickDataDelete(e, perdata)}>Xoá</span></div>}
                                  { inthis.state.paging.search_is_deleted === 1 && <div><span className='rx-action-restore' onClick={(e) => inthis.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}</td> : <td></td> }
      </tr>))    
  }

  renderHead(inthis) {
    return (<tr>
      <th>Ảnh</th>
      <th className={inthis.helpSortClass('name', ['rx-th-width-220'])} onClick={(e) => inthis.onClickSort(e, 'name')} >Nội dung</th>                  
      <th className={inthis.helpSortClass('created_at')} onClick={(e) => inthis.onClickSort(e, 'created_at')}>Ngày</th>                  
      <th className='rx-th-width-100' >Danh mục</th>
      <th>Thao tác</th>
    </tr>)
  }

  renderBody(inthis) {
    return inthis.state.data.map(perdata => (<tr key={perdata._id}>
      <td className='table-lowpadding'><img className='admin-table-product-img lazyload' alt={perdata.name} data-src={config.base_api + '/upload/image/' + (perdata.img_landscape || 'ico_app_default.png')} /></td>
      <td>{perdata.name}</td>        
      <td><small>{rxgetdate(perdata.created_at)}</small></td>        
      <td><small>{inthis.helpProductcat(perdata.appdistobj)}</small></td>
      <td>
        { inthis.state.paging.search_is_deleted !== 1 &&
          <div> {(perdata.is_hot !== 0) && <span className='admin-table-hot'><span className='icon-fire'></span>Hot</span>}
                {(perdata.is_active !== 0) && <span className='admin-table-on'>On</span>}{(perdata.is_active === 0) && <span className='admin-table-off'>Off</span>}                  
                <span className='rx-action-edit' onClick={(e) => inthis.onClickDataEdit(e, perdata)}>Sửa</span>
                <span className='rx-action-delete' onClick={(e) => inthis.onClickDataDelete(e, perdata)}>Xoá</span> </div> }
        { inthis.state.paging.search_is_deleted === 1 && 
          <div> <span className='rx-action-restore' onClick={(e) => inthis.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}
      </td>
    </tr>))
  }

  render() {      
    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Nội dung</div><div className='description'>Quản lý nội dung trên trang</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            <div className='col-lg-3 col-md-4 col-sm-12 rx-padright-15'>            
              <RxCrud renderHead={this.renderCatHead} renderBody={this.renderCatBody} onClickCatItem={this.onClickCatItem} api={'api_contentcate'} setData={(result) => { this.setState({ contentcate: result }) }} form={
              [{name: 'Mã danh mục', func: (inthis) => (<div className='fullwidth-info'>{inthis.state.editingData._id || ''}</div>)},
              {name: 'Tên danh mục', func: (inthis) => (<input tabIndex='1' type="text" value={inthis.state.editingData.name} onChange={(e) => inthis.onBlurData(e, 'name')} className='fullwidth-input' />)}]} />
            </div>

            <div className='col-lg-9 col-md-8 col-sm-12'>
              <RxCrud renderHead={this.renderHead} flagUpdate={this.state.flagUpdateCat} parentUpdate={this.runUpdateCat} renderBody={this.renderBody} api={'api_content'} form={
              [{name: 'Hình ảnh', func: (inthis) => (<RxUpload callback={(e) => inthis.callbackUpload(e)} images={inthis.state.editingData.img_landscape} />)},
              {name: 'Nổi bật', func: (inthis) => (<RxToggle value={inthis.state.editingData.is_hot} onToggle={(newValue) => inthis.onBlurDataValue(newValue, 'is_hot')}></RxToggle>)},
              // {name: 'Hình chi tiết', func: (inthis) => {
              //   let img_detail = (typeof(inthis.state.editingData.img_detail) !== 'string')? inthis.state.editingData.img_detail : inthis.state.editingData.img_detail.split(',').filter(v=>v !== '')
              //   return  (<RxUpload callback={(e) => inthis.callbackUploadDetail(e)} images={img_detail} single='0' />)
              // }},
              {name: 'Tên nội dung', func: (inthis) => (<input tabIndex='1' type='text' value={inthis.state.editingData.name} onChange={(e) => inthis.onBlurData(e, 'name')} className='fullwidth-input' />)},
              {name: 'Mô tả', func: (inthis) => (<input tabIndex='2' type='text' value={inthis.state.editingData.desc} onChange={(e) => inthis.onBlurData(e, 'desc')} className='fullwidth-input' />)},
              {name: 'Danh mục', func: (inthis) => (<RxSelectbox options={this.state.contentcate} results={inthis.state.editingData.appdist || ''} onChange={(result) => {inthis.onBlurDataValue(result, 'appdist')}} name='content' />)},
              {name: 'Nội dung', func: (inthis) => (<CKEditor activeClass='p10' content={inthis.state.editingData.content} events={{ 'change': inthis.onChangeContentCKE }} />)},
              {name: 'Thay đổi SEO', func: (inthis) => (
                <RxExpandable minHeight='120px'>
                  <div className='rxcontent-adminseo-wrap'>
                    <div className='fullwidth-label rxcontent-adminseo-demo'>
                      <div className='rxcontent-adminseo-seotitle'>{inthis.state.editingData.seotitle}</div>
                      <div className='rxcontent-adminseo-seoslug'>{inthis.state.editingData.slug}</div>
                      <div className='rxcontent-adminseo-seometa'>{inthis.state.editingData.seometa}</div>
                    </div>
                    <div className='fullwidth-label'>SEO title</div><input tabIndex='2' type='text' value={inthis.state.editingData.seotitle} onChange={(e) => inthis.onBlurData(e, 'seotitle')} className='fullwidth-input' />
                    <div className='fullwidth-label'>SEO slug</div><input tabIndex='2' type='text' value={inthis.state.editingData.slug} onChange={(e) => inthis.onBlurData(e, 'slug')} className='fullwidth-input' />
                    <div className='fullwidth-label'>SEO meta</div><input tabIndex='2' type='text' value={inthis.state.editingData.seometa} onChange={(e) => inthis.onBlurData(e, 'seometa')} className='fullwidth-input' />
                  </div>
                </RxExpandable>
              )}]} />
            </div> 
                   
          </div>
          <div className='clearfix-martop'></div>          
        </div>    
      </div>
    )
  }
}

export default AdminContent