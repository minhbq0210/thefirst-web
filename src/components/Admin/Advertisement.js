import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxgetdate } from './../../classes/ulti'

import CKEditor from 'react-ckeditor-component'

import RxUpload from './../Shares/RxUpload'
import RxExpandable from './../Shares/RxExpandable'
// import RxSelectbox from './../Shares/RxSelectbox'
import RxCrud from './../Shares/RxCrud'
// import RxToggle from './../Shares/RxToggle'
import RxSelectTableProduct from './../Shares/RxSelectTableProduct'

import { rxget } from './../../classes/request'

polyfill()
const config = global.rxu.config
class AdminContent extends Component {  

  constructor(props) {
    super(props)    
    this.state = {          
      flagUpdateCat: false, curCat: 0,
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 100 },
      pagingCat: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 }
    }

    this.onClickCatItem = this.onClickCatItem.bind(this)
    this.runUpdateCat = this.runUpdateCat.bind(this)
  }

  componentDidMount() {
    // this.fetchData()
    this.fetchDataCate()
  }

  fetchData(results) {  
    rxget(config.api_category_advertisementcate, this.state.paging, {
      '1': (json) => { this.setState({ contentcate: json.data.cate }) }
    })
  }

  fetchDataCate() {
    rxget(config.api_category_option_all, this.state.pagingCat, { '1': (json) => {
      if(json.data) { this.setState({cates: json.data.cates, count: json.data.totals})}
    }})
  }
  
  onClickCatItem(e) {
    this.setState({flagUpdateCat: !this.state.flagUpdateCat, curCat: e._id})
  }

  runUpdateCat(e) {
    let paging = (this.state.curCat)? { searchid_appdist: this.state.curCat, st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 } : { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 }
    e.inthis.setState({paging: paging}, () => { e.inthis.fetchData() })    
  }

  // R E N D E R S
  renderHead(inthis) {
    return(<tr><th>Ảnh</th><th>Danh mục</th><th>Thao tác</th></tr>)
  }

  renderBody(inthis) {    
    // let tempData = inthis.state.data.slice(0)
    // tempData.unshift({_id: 0, name: 'Xem tất cả'})
    return inthis.state.data.map((perdata, index) => (
      <tr key={perdata._id} >
        <td className='table-lowpadding'><img className='admin-table-product-img' alt={perdata.name} src={config.base_api + '/upload/image/' + (perdata.img_landscape || 'ico_app_default.png')} /></td>
        <td>{perdata.name}</td>
        { perdata._id !== 0 ? <td>{ inthis.state.paging.search_is_deleted !== 1 && <div><span className='rx-action-edit' onClick={(e) => inthis.onClickDataEdit(e, perdata)}>Sửa</span> <span className='rx-action-delete' onClick={(e) => inthis.onClickDataDelete(e, perdata)}>Xoá</span></div>}
                                  { inthis.state.paging.search_is_deleted === 1 && <div><span className='rx-action-restore' onClick={(e) => inthis.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}</td> : <td></td> }
      </tr>))    
  }

  renderCatHead(inthis) {
    return (<tr>
      <th>Ảnh</th>
      <th className={inthis.helpSortClass('name', ['rx-th-width-220'])} onClick={(e) => inthis.onClickSort(e, 'name')} >Tiêu đề</th>                  
      <th className={inthis.helpSortClass('created_at')} onClick={(e) => inthis.onClickSort(e, 'created_at')}>Ngày tạo</th>                  
      <th className={inthis.helpSortClass('created_at')} onClick={(e) => inthis.onClickSort(e, 'created_at')}>Cập nhật</th>                  
      {/*<th className='rx-th-width-100' >Danh mục</th>*/}
      <th>Thao tác</th>
    </tr>)
  }

  renderCatBody(inthis) {
    let tempData = inthis.state.data.slice(0)
    return tempData.map(perdata => (<tr key={perdata._id} onClick={(e) => inthis.run('onClickCatItem', perdata)}>
      <td className='table-lowpadding'><img className='admin-table-product-img' alt={perdata.name} src={config.base_api + '/upload/image/' + (perdata.img_landscape || 'ico_app_default.png')} /></td>
      <td style={{width: '60%'}}>{perdata.name}</td>        
      <td><small>{perdata._id !== 0 && rxgetdate(perdata.created_at)}</small></td>        
      <td><small>{perdata._id !== 0 && rxgetdate(perdata.updated_at)}</small></td>        
      {/*<td><small>{inthis.helpProductcat(perdata.appdistobj)}</small></td>*/}
      <td>
        { inthis.state.paging.search_is_deleted !== 1 && perdata._id !== 0 &&
          <div> <span className='rx-action-edit' onClick={(e) => inthis.onClickDataEdit(e, perdata)}>Sửa</span>
                <span className='rx-action-delete' onClick={(e) => inthis.onClickDataDelete(e, perdata)}>Xoá</span> </div> }
        { inthis.state.paging.search_is_deleted === 1 && 
          <div> <span className='rx-action-restore' onClick={(e) => inthis.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}
      </td>
    </tr>))
  }

  render() {      
    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Advertisement</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            <div className='col-lg-12 col-md-12 col-sm-12 rx-padright-15'>            
              <RxCrud renderHead={this.renderCatHead} renderBody={this.renderCatBody} onClickCatItem={this.onClickCatItem} api={'api_advertisement'} setData={(result) => { this.setState({ contentcate: result }) }} form={
               [{name: 'Hình ảnh', func: (inthis) => (<RxUpload callback={(e) => inthis.callbackUpload(e)} images={inthis.state.editingData.img_landscape} />)},
                // {name: 'Nổi bật', func: (inthis) => (<RxToggle value={inthis.state.editingData.is_hot} onToggle={(newValue) => inthis.onBlurDataValue(newValue, 'is_hot')}></RxToggle>)},
                // {name: 'Hình chi tiết', func: (inthis) => {
                //   let img_detail = (typeof(inthis.state.editingData.img_detail) !== 'string')? inthis.state.editingData.img_detail : inthis.state.editingData.img_detail.split(',').filter(v=>v !== '')
                //   return  (<RxUpload callback={(e) => inthis.callbackUploadDetail(e)} images={img_detail} single='0' />)
                // }},
                {name: 'Tiêu đề', func: (inthis) => (<input tabIndex='1' type='text' value={inthis.state.editingData.name} onChange={(e) => inthis.onBlurData(e, 'name')} className='fullwidth-input' />)},
                // {name: 'Vị trí', func: (inthis) => (<div className='lk-select-form'>
                //   <select onChange={(e) => inthis.onBlurData(e, 'position')} value={inthis.state.editingData.position}>
                //     <option value={1}>Top</option>
                //     <option value={2}>Bottom</option>
                //   </select>
                // </div>)},
                {name: 'Nội dung', func: (inthis) => (<CKEditor activeClass='p10' content={inthis.state.editingData.desc} events={{ 'change': inthis.onChangeContentCKE1 }} />)},
                {name: 'Danh mục con', func: (inthis) => (<div className='lk-cart-add-product advertisement'> 
                  <RxSelectTableProduct options={this.state.cates || ''} count={this.state.count} results={inthis.state.editingData.appdist || ''} onChange={(result) => { inthis.onBlurDataCates(result, 'cates') }} type='ads' />
                    <div className='adtable__main list-line-item'>
                      <table className='adtable__inner table-line-item'>
                        <thead><tr>
                            <th className='rxwidth--150'>STT</th>
                            <th className='rxwidth--150'>Hình ảnh</th>
                            <th>Tên danh mục</th>
                            <th className='align-center'>Xoá</th>
                        </tr></thead>
                        <tbody className='line_item_rows'>
                        {inthis.state.editingData.appdistobj && inthis.state.editingData.appdistobj.map((item, k) => {
                          return (<tr key={item._id} className='orderLineItems-2'>
                            <td className='rxwidth--150'>{k+1}</td>
                            <td className='rxwidth--150'><img className='admin-table-product-img' alt={item.name} src={(config.base_api + '/upload/image/' + item.img_landscape || 'ico_app_default.png')} /></td>
                            <td>{item.name}</td>
                            <td className='rxwidth--150 align-center'><span className='adtable__btndelete' onClick={(e) => inthis.onClickDataDeleteItem(e, item, 'cates')}><i className='icon-close' /></span></td>
                          </tr>)
                          })}
                        </tbody>
                      </table>
                    </div>
                </div> )},
                {name: 'Thay đổi SEO', func: (inthis) => (
                  <RxExpandable minHeight='120px'>
                    <div className='rxcontent-adminseo-wrap'>
                      <div className='fullwidth-label rxcontent-adminseo-demo'>
                        <div className='rxcontent-adminseo-seotitle'>{inthis.state.editingData.seotitle}</div>
                        <div className='rxcontent-adminseo-seoslug'>{inthis.state.editingData.slug}</div>
                        <div className='rxcontent-adminseo-seometa'>{inthis.state.editingData.seometa}</div>
                      </div>
                      <div className='fullwidth-label'>SEO title</div><input tabIndex='2' type='text' value={inthis.state.editingData.seotitle} onChange={(e) => inthis.onBlurData(e, 'seotitle')} className='fullwidth-input' />
                      <div className='fullwidth-label'>SEO slug</div><input tabIndex='2' type='text' value={inthis.state.editingData.slug} onChange={(e) => inthis.onBlurData(e, 'slug')} className='fullwidth-input' />
                      <div className='fullwidth-label'>SEO meta</div><input tabIndex='2' type='text' value={inthis.state.editingData.seometa} onChange={(e) => inthis.onBlurData(e, 'seometa')} className='fullwidth-input' />
                    </div>
                  </RxExpandable>
                )}]}/>
            </div>

            {/*<div className='col-lg-8 col-md-8 col-sm-12'>
              <RxCrud renderHead={this.renderHead} flagUpdate={this.state.flagUpdateCat} parentUpdate={this.runUpdateCat} renderBody={this.renderBody} api={'api_advertisement'} form={
              [{name: 'Advertisement ID', func: (inthis) => (<div className='fullwidth-info'>{inthis.state.editingData._id || ''}</div>)},
              {name: 'Hình ảnh', func: (inthis) => (<RxUpload callback={(e) => inthis.callbackUpload(e)} images={inthis.state.editingData.img_landscape} />)},
              {name: 'Tiêu đề', func: (inthis) => (<input tabIndex='1' type="text" value={inthis.state.editingData.name} onChange={(e) => inthis.onBlurData(e, 'name')} className='fullwidth-input' />)},
              {name: 'Danh mục cha', func: (inthis) => (<RxSelectbox options={this.state.contentcate} results={inthis.state.editingData.appdist || ''} onChange={(result) => {inthis.onBlurDataValue(result, 'appdist')}} name='content' />)},
              ]} />
            </div> */}
                   
          </div>
          <div className='clearfix-martop'></div>          
        </div>    
      </div>
    )
  }
}

export default AdminContent