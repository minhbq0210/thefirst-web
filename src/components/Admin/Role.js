import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import { rxgetdate } from './../../classes/ulti'

polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config

class AdminRole extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [], cate: [], 
      curobj: {cate: 0}, 
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      pagingcat: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      editingData: {},
      dataPermission: [],
      permissionArray: [],
      selected_array: [],
      showselected_array: [],
      isPermission: false
    }
  }

  componentDidMount() {
    this.fetchAlldata()
    this.timerDatafilter = null
  }

  fetchAlldata() {
    this.fetchData()
    this.fetchData_permission()
  }

  fetchData() {    
    rxget(config.api_role, this.state.paging, {
      '1': (json) => { 
        this.setState({ data: json.data }) 
      }
    })
  }

  fetchData_permission() {
    rxget(config.api_permissionall, {}, {
      '1': (json) => { 
        this.permission_array = [];
        // Making data 
        this.setState({ dataPermission: json.data }) 
        var per_array = this.groupBy(this.state.dataPermission);
        var array_parse = this.parse_obj_toarray(per_array);

        let keys = [];
        for (let key in array_parse) {
          keys.push(array_parse[key]);
        } 
        this.setState({ permissionArray: keys }) 
      }
    })
  }

  ////////////////
  // Role ////////
  ////////////////
  onClickSort(e, stcol) {
    let paging = this.state.paging                  
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ paging: paging }, () => { this.fetchData() })    
  }

  onClickDataDeleteNew(e, perdata) {    
    e.stopPropagation()
    rxget(config.api_role_delete, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataRestore(e, perdata) {
    e.stopPropagation()
    rxget(config.api_role_restore, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickRoleUpdateSubmit(e, perdata) {
    rxpost(config.api_role_edit, this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickRoleEdit({}, {})
  }

  onClickDataTrash(e, isdeleted) {
    let paging = this.state.paging
    paging['search_is_deleted'] = isdeleted
    this.setState({ paging: paging }, () => {
      this.fetchData()
    }) 
  }

  onClickRoleNew(e) {
    let timeStr = Date.now().toString()
    let clone = { name: '', code: 'Role_'+ timeStr.substr(timeStr.length - 5), desc: '', created_at: 1, is_deleted: 0, is_active: 1, price: 100000, stock: 100,  }
    this.initialPermission(clone)
    this.setState({ editingData: clone })
  }

  onBlurDatafilter(e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging
    paging['search_' + name] = e.target.value
    this.setState({ paging: paging })

    this.timerDatafilter = setTimeout((e, name) => {
      this.fetchData()
    }, WAIT_INTERVAL)    
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onClickRoleEdit(e, perdata) {
    let clone = JSON.parse(JSON.stringify(perdata))
    let init_per = this.initialPermission(clone)
    this.checkPermission(init_per)
    this.show_permission(init_per)
    this.setState({ editingData: clone })
  }

  addPermission() {
    this.setState({ isPermission: true })
    this.checkPermission(this.state.selected_array)
  }

  addClosePermission() {
    this.setState({ editingData: this.state.editingData, isPermission: false })
  }

  checkPermission(permission_checked) {
    let tempPermission = this.state.dataPermission

    if ( permission_checked !== null) {
      for (let key_per in tempPermission) {
        if (permission_checked.indexOf(tempPermission[key_per].id) !== -1){          
          tempPermission[key_per].checked = true
        }
      }
    }

    this.setState({dataPermission: tempPermission})
    return this.state.dataPermission
  }

  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  helpSortClass(stcol, extraclass) {
    extraclass = extraclass || []

    let result = ''    
    if (this.state.paging.st_col === stcol) {
      result = this.state.paging.st_type === 1 ? 'rx-sort-asc' : 'rx-sort-desc'
    }

    for(let i = 0; i < extraclass.length; i++) {      
      result += ' ' + extraclass[i]
    }

    return result 
  }

  checkedAll(data) {
    var check_list = true;
    for (let key in data.value) {
      if (data.value[key].checked !== true){
        check_list = check_list && false;
      } else {
        check_list = check_list && true;
      }
    }
    return check_list;
  }

  checkedExist(data) {
    var arrchecked = data.value.map((x) => x.checked)
    if (arrchecked.indexOf(true) === -1) {
      return false
    } else {
      return true
    }
  }

  toggleAll(e, index) {
    let arrpermission = this.state.permissionArray
    let data = arrpermission[index]
    let arrselected = this.state.selected_array
    if (this.checkedAll(data)){
      for (let key in data.value) {
        let index1 = arrselected.indexOf(data.value[key].id)
        arrselected.splice(index1, 1)
        data.value[key].checked = false;
      }
    } else {
      for (let key in data.value) {
        if (arrselected.indexOf(data.value[key].id) === -1) {
          arrselected.push(data.value[key].id)
          data.value[key].checked = true
          arrpermission[index] = data
        }
      }
    }
    
    this.setState({permissionArray : arrpermission, selected_array: arrselected})
  }

  toggleOne(e, index, perindex, checked) {
    let arrselected = this.state.selected_array
    let arrpermission = this.state.permissionArray
    let data = this.state.permissionArray[index]['value'][perindex]
    data['checked'] = checked
    if (arrselected.indexOf(data.id) === -1) {
      if (checked) {
        arrselected.push(data.id);
      }
    } else {
      if (!checked) {
        let index = arrselected.indexOf(data.id)
        arrselected.splice(index, 1)
      }
    }
    this.setState({permissionArray : arrpermission, selected_array: arrselected})
  }

  onClickRoleCreateSubmit(e, perdata) {
    rxpost(config.api_role, this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickRoleEdit({}, {})
  }

  addResetPermission() {    
    this.fetchData_permission()
    this.setState({selected_array: this.initialPermission(this.state.editingData)})    
    this.show_permission(this.state.selected_array)
    this.setState({ editingData: this.state.editingData, isPermission: false })
  }

  addSavePermission() {
    let editingData = this.state.editingData
    this.show_permission(this.state.selected_array)
    editingData.permission = this.state.selected_array
    this.setState({editingData: editingData, isPermission: false})
  }

  initialPermission(perdata){
    if (perdata && typeof(perdata.permission) === 'string' && perdata.permission !== null) {
      var array = perdata.permission.split(',').map(Number)
      this.setState({selected_array: array})
      return array
    } else {
      this.setState({selected_array: []})
      return []
    }
  }

  show_permission(data) {
    var dataper_selected = [];
    for (let key_per in this.state.dataPermission) {
      if (data.indexOf(this.state.dataPermission[key_per].id) !== -1){
        dataper_selected.push(this.state.dataPermission[key_per]);
      }
    }
    var dataper_tmp = this.groupBy(dataper_selected);
    this.showselected_array = this.parse_obj_toarray(dataper_tmp);
    this.setState({showselected_array: this.showselected_array})
  }

  parse_obj_toarray(value) {
    if (!value) {
      return value;
    } 
    let keys = [];
    for (let key in value) {
      keys.push({key: key, value: (value[key])});
    } 
    return keys;
  } 

  groupBy(permissionArr) {
    var groups = {};
    permissionArr.forEach(function(permission) {
      permission.checked = false;          
      groups[permission.controller] = groups[permission.controller] || [];
      groups[permission.controller].push(permission);
    })
    return groups;
  } 

  render() {
    // Data tables
    let data = this.state.data.map(perdata => (
      <tr key={perdata._id}>
        <td>{perdata.name}</td>
        <td>{perdata.code}</td>
        <td>{perdata.desc}</td>
        <td>{rxgetdate(perdata.created_at)}</td>                  
        <td>
          { this.state.paging.search_is_deleted !== 1 &&
            <div> {(perdata.is_active !== 0) && <span className='admin-table-on'>On</span>}{(perdata.is_active === 0) && <span className='admin-table-off'>Off</span>}
                  <span className='rx-action-edit' onClick={(e) => this.onClickRoleEdit(e, perdata)}>Sửa</span>
                  <span className='rx-action-delete' onClick={(e) => this.onClickDataDeleteNew(e, perdata)}>Xoá</span> </div> }
          { this.state.paging.search_is_deleted === 1 && 
            <div> <span className='rx-action-restore' onClick={(e) => this.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}
        </td>
      </tr>
    ))

    let arrayPermission = this.state.permissionArray.map((perdata, index) => (
      <div className='col-sm-4 col-xs-6 rs-box-group' key={index}>
        <div className='rs-checkbox-group'>
          <div className='rs-checkbox-group-name' >
            <span className={this.checkedAll(perdata) ? 'rs-checkbox-group-all lk_checkall' : this.checkedExist(perdata) ? 'rs-checkbox-group-all lk_checkexist' : 'rs-checkbox-group-all'} onClick={(e) => this.toggleAll(e, index)}></span> 
            <span className='rs-checkbox-group-nametext'><b>{perdata.key}</b></span>
          </div>
          <div className='rs-checkbox-group-body' >
            {perdata.value.map((permission, perindex) => (
              <div key={permission._id} className='rs-checkbox-group-child clearfix'>
                <span className={permission.checked ? 'rs-checkbox-group-small lk_checkone' : 'rs-checkbox-group-small'} onClick={(e) => this.toggleOne(e, index, perindex, !permission.checked )}></span>
                <span className='rs-checkbox-group-nametext'>{permission.action}</span>
              </div>
            ))}
          </div>
        </div>
      </div>
    ))
    
    let arrayShowPermission = this.state.showselected_array.map((permissiongroup, index) => (
      <div className='rx-permission-group clearfix' key={index} >            
        <b>{permissiongroup.key}</b>
        <div>
          {permissiongroup.value.map((permission, perindex) => (<span className='rx-permission-perele' key={perindex}>{permission.action}</span>))}
        </div>
      </div>
    ))
     
    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Vai trò</div><div className='description'>Quản lý vai trò</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            
            <div className='col-lg-12 col-sm-12'>
              <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurDatafilter(e, 'name')} /></span>
              <i className='icon-plus rx-recycle' onClick={(e) => this.onClickRoleNew(e)}></i>
              { this.state.paging.search_is_deleted !== 1 && <i className='icon-trash rx-recycle' onClick={(e) => this.onClickDataTrash(e, 1)}></i> }              
              { this.state.paging.search_is_deleted === 1 && <i className='icon-list rx-recycle' onClick={(e) => this.onClickDataTrash(e, 0)}></i> }

              <div className='admin-table-pagination admin-pagin-right'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>

              <div className='admin-table-wrap'><table className='admin-table-product preset'>
                <thead>
                  <tr>
                    <th className={this.helpSortClass('name', ['rx-th-width-220'])} onClick={(e) => this.onClickSort(e, 'name')}>Tên vai trò</th>
                    <th className={this.helpSortClass('code', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'id')}>Mã vai trò</th>
                    <th className={this.helpSortClass('desc', ['rx-th-width-220'])} onClick={(e) => this.onClickSort(e, 'controller')}>Mô tả</th>
                    <th className={this.helpSortClass('created_at', ['rx-th-width-220'])} onClick={(e) => this.onClickSort(e, 'created_at')}>Ngày tạo</th>                  
                    <th>Thao tác</th>
                  </tr>
                </thead>
                <tbody>{data}</tbody>
              </table></div>
              <div className='admin-table-pagination'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
            </div>
          </div>
          <div className='clearfix-martop'></div>          
        </div>

        { this.state.editingData.created_at && <div className='admin-cardblock-form'>
            <div className='admin-cardblock-formname'>{(this.state.editingData.created_at !== 1) ? 'Sửa vai trò' : 'Tạo vai trò'} <b>{(this.state.editingData.created_at !== 1) ? this.state.editingData.name : ''}</b><div className='admin-cardblock-formclose' onClick={(e) => this.onClickRoleEdit(e, {})}>x</div></div>
            <div className='admin-cardblock-formbody'><div className='col-sm-5 clearfix'>
              <div className='fullwidth-label'>Tên vai trò</div><input tabIndex='1' type='text' name='name' value={this.state.editingData.name || ''} onChange={(e) => this.onBlurData(e, 'name')} className='fullwidth-input' />
              <div className='fullwidth-label'>Mã vai trò</div><input tabIndex='2' type='text' name='code' value={this.state.editingData.code || ''} onChange={(e) => this.onBlurData(e, 'code')} className='fullwidth-input' />
              <div className='fullwidth-label'>Mô tả</div><input tabIndex='3' type='text' name='desc' value={this.state.editingData.desc || ''} onChange={(e) => this.onBlurData(e, 'desc')} className='fullwidth-input' />
              
              {this.state.editingData.created_at !== 1 && <button className='btn btn-default lk-size_button' type='submit' onClick={(e) => this.addPermission()}>Sửa chi tiết vai trò</button>}
              {this.state.editingData.created_at === 1 && <button className='btn btn-default lk-size_button' type='submit' onClick={(e) => this.addPermission()}>Thêm chi tiết vai trò</button>}

              <div className='rx-permission-show'>{arrayShowPermission}</div>    
              <div className='admin-cardblock-btns'>
                <a tabIndex='2' className='btn-cancel' onClick={(e) => this.onClickRoleEdit(e, {})} onKeyPress={(e) => this.onClickRoleEdit(e, {})}>Trở về</a>
                {this.state.editingData.created_at !== 1 && <a tabIndex='3' className='btn-edit' onClick={(e) => this.onClickRoleUpdateSubmit(e)} onKeyPress={(e) => this.onClickRoleUpdateSubmit(e)}>Cập nhật</a>}
                {this.state.editingData.created_at === 1 && <a tabIndex='4' className='btn-clone' onClick={(e) => this.onClickRoleCreateSubmit(e)} onKeyPress={(e) => this.onClickRoleCreateSubmit(e)}>Tạo mới</a>}
              </div>
            </div></div>
          </div> 
        }
        { this.state.isPermission && <div className='admin-cardblock-form'>
            <div className='admin-cardblock-formname'>Sửa quyền  <b>{this.state.editingData.name}</b><div className='admin-cardblock-formclose' onClick={(e) => this.addClosePermission(e)}>x</div></div>
            <div className='admin-cardblock-formbody'><div className='clearfix'>
            <div className='row'>{arrayPermission}</div>
            <div className='card-footer'>
              <button className='btn btn-sm btn-primary' type='submit' onClick={(e) => this.addSavePermission(e)}><i className='fa fa-dot-circle-o'></i> Cập nhật</button>        
              <button className='btn btn-sm btn-danger' type='reset' onClick={(e) => this.addResetPermission(e)}><i className='fa fa-ban'></i> Huỷ bỏ</button>
            </div>
            </div></div>
          </div> 
        }

      </div>
    )
  }
}

export default AdminRole