import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import RxSelectbox from './../Shares/RxSelectbox'
import RxUpload from './../Shares/RxUpload'

polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config

class AdminCateBrand extends Component {
  constructor(props) {
    super(props)
    this.state = { 
      brand:[], brandall:[],
      curobj: {cate: 0}, 
      pagingbrand: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      editingBrand: {},
      msg: ''
    }
    this.state.quantity = this.state.quantity || 0 
  }

  componentDidMount() {
    this.fetchAlldata()
    this.timerCatefilter = null
    this.timerDatafilter = null
  }

  fetchAlldata() {
    this.fetchBrand()
    // this.fetchAllBrand()
    this.fetchAllCate()
    this.setState({ allcate: {_id: 0, name: 'Tất cả'} })
  }

  fetchAllCate() {
    rxget(config.api_category,{ pg_size: 1000 }, {
      '1': (json) => { this.setState({ cateall: this.sortName(json.data.cate) }) }
    })
  }

  fetchAllBrand() {
    rxget(config.api_category_brand,{ pg_size: 1000 }, {
      '1': (json) => { this.setState({ brandall: this.sortName(json.data.brand) }) }
    })
  }
  fetchBrand() {   
    rxget(config.api_category_brand, this.state.pagingbrand, {
      '1': (json) => { this.setState({ brand: json.data.brand, quantity: json.data.count }) }
    })
  }
  //////////////
  // BRAND   //
  //////////////
  onClickSort(e, stcol) {
    let paging = this.state.pagingbrand                 
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ pagingbrand: paging }, () => { this.fetchBrand() })    
  }

  onBrandClick(e, perdata) {
    let curobj = this.state.curobj; curobj.cate = perdata._id
    let paging = (perdata._id)? { searchid_brandlist: perdata._id,  st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 } : { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 }

    this.setState({ curobj: curobj, paging: paging }, () => { this.fetchBrand() })    
  }

  onClickBrandEdit(e, perdata) {
    let clone = JSON.parse(JSON.stringify(perdata))
    this.setState({ editingBrand: clone })
  }
  onClickBrandDelete(e, perdata) {
    e.stopPropagation()
    rxget(config.api_category_delete, perdata, {
      '1': (json) => { this.fetchBrand(); }
    })
  }
  onClickBrandRestore(e, perdata) {
    e.stopPropagation()
    rxget(config.api_category_restore, perdata, {
      '1': (json) => { this.fetchBrand() }
    })
  }
  onClickBrandUpdateSubmit(e, perdata) {
    rxpost(config.api_category_edit, this.state.editingBrand, {
      '1': (json) => { 
        this.fetchBrand()
        this.onClickBrandEdit({}, {}) 
        this.setState({ msg: '' })
      },
      '-2': (json) => { this.setState({ msg: 'Fields must not be empty' })}
    })
  }

  onClickBrandCreateSubmit(e, perdata) {
    rxpost(config.api_category, this.state.editingBrand, {
      '1': (json) => { 
        this.fetchBrand()
        this.onClickBrandEdit({}, {}) 
        this.setState({ msg: '' })
      },
      '-2': (json) => { this.setState({ msg: 'Fields must not be empty' })}
    })
  }
  onClickBrandTrash(e, isdeleted) {
    let pagingbrand = this.state.pagingbrand
    pagingbrand['search_is_deleted'] = isdeleted
    this.setState({ pagingbrand: pagingbrand }, () => {
      this.fetchBrand()
    }) 
  }
  onClickBrandNew(e) {
    let timeStr = Date.now().toString()
    let clone = { name: 'Brand_'+ timeStr.substr(timeStr.length - 5), desc: '', created_at: 1, is_deleted: 0, is_active: 1, parent_id: 0, is_brand:1 }
    this.setState({ editingBrand: clone })
  }
  onBlurBrandfilter(e, name) {
    clearTimeout(this.timerCatefilter)
    let pagingbrand = this.state.pagingbrand
    pagingbrand['search_' + name] = e.target.value  
    this.setState({ pagingbrand: pagingbrand })

    this.timerCatefilter = setTimeout(() => { this.fetchBrand() }, WAIT_INTERVAL)   
  }
  onBlurBrand(e, name) {
    let editingBrand = this.state.editingBrand
    editingBrand[name] = e.target.value
    if (name === 'priority') {
      editingBrand[name] = Number(e.target.value)  
    } 
    this.setState({ editingBrand: editingBrand })    
  }

  onBlurBrandValue(value, name) {
    let editingBrand = this.state.editingBrand
    editingBrand[name] = value 
    this.setState({editingBrand: editingBrand})
  }
  // Pagin
  onClickPaginBrandBack(e) {
    let pagingbrand = this.state.pagingbrand
    pagingbrand.pg_page = (pagingbrand.pg_page > 1)? (pagingbrand.pg_page - 1) : pagingbrand.pg_page
    this.setState({ pagingbrand: pagingbrand }, () => { this.fetchBrand() })
  }

  onClickPaginBrandNext(e) {
    let pagingbrand = this.state.pagingbrand
    pagingbrand.pg_page += 1
    this.setState({ pagingbrand: pagingbrand }, () => { this.fetchBrand() })
  }
  callbackUploadBrand(e) {
    this.onBlurBrand({target: {value: e.images}}, 'img_landscape')
  }

  handleChangeBrandParent(e, name) {
    let editingBrand = this.state.editingBrand
    editingBrand[name] = e.target.value
    let objCate = this.state.cateall.find(function (obj) { return obj._id === e.target.value })
    if (objCate) {
      editingBrand['trace'] = (editingBrand['_id']) ? objCate.trace + '-' + editingBrand['_id'] : objCate.trace
    } else {
      editingBrand['trace'] = ''
    }
    this.setState({ editingBrand: editingBrand })   
  }

  handleChangeCateBrand(e, name) {
    let editingCate = this.state.editingCate
    editingCate[name] = e.target.value
    this.setState({ editingCate: editingCate })   
  }

  sortName(obj){
    var byName = obj.slice(0);
    byName.sort(function(a,b) {
        var x = a.name.toLowerCase();
        var y = b.name.toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
    });
    return byName;
  }

  render() {
    //total page
    let totalpage = 0
    if(this.state.quantity && this.state.quantity > 0){
      totalpage = Math.ceil(this.state.quantity / this.state.pagingbrand.pg_size)
    }

    let brand = this.state.brand.map(perdata => (
      <tr className={(perdata._id === this.state.curobj.cate) && 'rx-active'} key={perdata._id} onClick={(e) => this.onBrandClick(e, perdata)}>
        <td className='table-lowpadding'><img className='admin-table-product-img lazyload' alt={perdata.name} data-src={config.base_api + '/upload/image/' + (perdata.img_landscape || 'ico_app_default.png')} /></td>
        <td><span>{perdata.name}</span></td>
        <td>{perdata.priority}</td>
        <td>
          {perdata.desc}
        </td>
        <td>
          { this.state.pagingbrand.search_is_deleted !== 1 &&
            <div> <span className='rx-action-edit' onClick={(e) => this.onClickBrandEdit(e, perdata)}>Sửa</span>
                  <span className='rx-action-delete' onClick={(e) => this.onClickBrandDelete(e, perdata)}>Xoá</span></div>}
          { this.state.pagingbrand.search_is_deleted === 1 &&
            <div><span className='rx-action-restore' onClick={(e) => this.onClickBrandRestore(e, perdata)}>Khôi phục</span></div>}
        </td>
      </tr>))

    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Thương hiệu ưa thích</div><div className='description'>Quản lý thương hiệu ưa thích</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            <div className='col-lg-12 col-md-12 col-sm-12'>
              <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurBrandfilter(e, 'name')} /></span>
              <i className='icon-plus rx-recycle' onClick={(e) => this.onClickBrandNew(e)}></i>
              { this.state.pagingbrand.search_is_deleted !== 1 && <i className='icon-trash rx-recycle' onClick={(e) => this.onClickBrandTrash(e, 1)}></i> }              
              { this.state.pagingbrand.search_is_deleted === 1 && <i className='icon-list rx-recycle' onClick={(e) => this.onClickBrandTrash(e, 0)}></i> }              
              <div className='admin-totalpage admin-pagin-right'> Total page: {totalpage}</div>
              <div className='admin-table-wrap'><table className='admin-table-cate preset'>                            
                <thead>
                  <tr>
                    <th>Hình ảnh</th>
                    <th onClick={(e) => this.onClickSort(e, 'name')}>Tên hương hiệu</th>
                    <th onClick={(e) => this.onClickSort(e, 'priority')}>Độ ưu tiên</th>
                    <th>Mô tả</th>
                    <th style={{width: 100}}>Thao tác</th>
                  </tr>
                </thead>
                <tbody>
                  {brand}
                </tbody>
              </table></div>
              <div className='admin-table-pagination'>
                {(this.state.pagingbrand.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBrandBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.pagingbrand.pg_page}</div>
                {(this.state.brand.length >= this.state.pagingbrand.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginBrandNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
            </div>
          </div>
          </div>
          <div className='clearfix-martop'></div>                     

        { this.state.editingBrand.created_at && <div className='admin-cardblock-form'>
          <div className='admin-cardblock-formname'>Sửa thương hiệu <b>{this.state.editingBrand.name}</b><div className='admin-cardblock-formclose' onClick={(e) => this.onClickBrandEdit(e, {})}>x</div></div>
          <div className='admin-cardblock-formbody'><div className='col-sm-5 clearfix'>
            { this.state.msg && <div className='cart-page-error' >{this.state.msg}</div> }
            <div className='col-md-3 col-sm-3 img-detail'>
              <div className='fullwidth-label'>Hình ảnh</div>
              <RxUpload callback={(e) => this.callbackUploadBrand(e)} images={this.state.editingBrand.img_landscape} />                
            </div>
            <div className='fullwidth-label'>Tên thương hiệu (Viet ver)</div><input tabIndex='1' type="text" value={this.state.editingBrand.name} onChange={(e) => this.onBlurBrand(e, 'name')} className='fullwidth-input' />
            <div className='fullwidth-label'>Mô tả thương hiệu</div><input tabIndex='3' type="text" value={this.state.editingBrand.desc} onChange={(e) => this.onBlurBrand(e, 'desc')} className='fullwidth-input' />
            <div className='fullwidth-label'>Chọn danh mục cha</div>
            <RxSelectbox options={this.state.cateall} results={this.state.editingBrand.appdist || ''} onChange={(result) => {console.log(result); this.onBlurBrandValue(result, 'appdist')}} />
            <div className='fullwidth-label'>Chọn ưu tiên sắp sếp thương hiệu </div><input tabIndex='4' type='number' value={this.state.editingBrand.priority || 0} onChange={(e) => this.onBlurBrand(e, 'priority')} className='fullwidth-input' />
            <div className='admin-cardblock-btns'>
              <a tabIndex='5' className='btn-cancel' onClick={(e) => this.onClickBrandEdit(e, {})} onKeyPress={(e) => this.onClickBrandEdit(e, {})}>Trở về</a>
              {this.state.editingBrand.created_at !== 1 && <a tabIndex='6' className='btn-edit' onClick={(e) => this.onClickBrandUpdateSubmit(e)} onKeyPress={(e) => this.onClickBrandUpdateSubmit(e)}>Cập nhật</a>}
              <a tabIndex='7' className='btn-clone' onClick={(e) => this.onClickBrandCreateSubmit(e)} onKeyPress={(e) => this.onClickBrandCreateSubmit(e)}>Tạo mới</a>
            </div>
          </div></div>
        </div> }
      </div>
    )
  }
}

export default AdminCateBrand