import React, { Component } from 'react'
// import { Link } from 'react-router-dom'
import { polyfill }  from 'es6-promise'
import { rxget } from './../../classes/request'

const config = global.rxu.config

polyfill()

class AdminReport extends Component {
  constructor(props) {
    super(props)
    this.handleSelect = this.handleSelect.bind(this)
    this.mouseOverHandler = this.mouseOverHandler.bind(this)
    this.mouseOutHandler = this.mouseOutHandler.bind(this)
    this.mouseMoveHandler = this.mouseMoveHandler.bind(this)
    this.state = {   
      key: 1,   
      rxsize:  this.props.rxsize  || 4,
      rxpagin: this.props.rxpagin || '',
      checktab: {today: true,yesterday: false,lastweek: false, thismonth: false, lastmonth: false},
      chart: {today: {},yesterday: {},lastweek: {}, thismonth: {}, lastmonth: {} },
      products: [],
      arrproducts: [],
      x: `${10}px`,
      y: `${10}px`,
      top: '',
      left: ''
    }
  }

  handleSelect(key) {
    this.setState({key })
  }

  componentDidMount() {
    this._ismounted = true
    this.fetchData()
  }

  componentWillUnmount() {
    this._ismounted = false
  }

  fetchData() {
    rxget(config.api_dashboard, this.state.paging, {
      '1': (json) => { if (this._ismounted) { this.setState({chart: json.data.chart[0], products: json.data.products}); this.countDiscountArr(this.state.chart.today) } }
    })
  }

  openCity(e, name) {
    let checktab = this.state.checktab
    checktab = {}
    checktab[name] = true
    this.setState({checktab: checktab})
  }

  convertDate(timestamp) {
    let u = new Date(timestamp * 1000)  
    let tempstr  =  ('0' + u.getUTCDate()).slice(-2) +'/'+ ('0' + (u.getUTCMonth() + 1)).slice(-2)
    return tempstr
  }

  mouseOverHandler(d, e) {
    this.setState({
      showToolTip: true,
      top: `${e.screenY + 150}px`,
      left: `${e.screenX - 250}px`,
      y: d.y,
      x: d.x});
  }

  mouseMoveHandler(d, e) {
    if (this.state.showToolTip) {
      this.setState({top: `${e.y + 150}px`, left: `${e.x - 250}px`});
    }
  }

  mouseOutHandler() {
    this.setState({showToolTip: false});
  }

  // barChart(data) {
  //   let arrchart = []
  //   if (data && data.chart && data.chart.constructor === Array) {
  //     let arrdata = []
  //     data.chart.sort((a, b) => parseFloat(a.date) - parseFloat(b.date))
  //     for (let i in data.chart) {
  //       if (typeof(data.chart[i].date) !== 'undefined' && typeof(data.chart[i].rev) !== 'undefined') {
  //         arrdata.push({x: this.convertDate(data.chart[i].date + 86400 ), y: data.chart[i].rev, color: '#a6c4ea'})  
  //       } 
  //     }
  //     if (arrdata.length > 0) {
  //       arrchart = [<BarChart
  //         key={0}
  //         axisLabels={{x: 'My x Axis', y: 'My y Axis'}}
  //         axes
  //         barWidth={10}
  //         height={410}
  //         width={650}
  //         margin={{top: 10, right: 100, bottom: 50, left: 90}}
  //         data={arrdata}
  //         mouseOverHandler={this.mouseOverHandler}
  //         mouseOutHandler={this.mouseOutHandler}
  //         mouseMoveHandler={this.mouseMoveHandler}
  //       />]
  //     }
  //   } 
  //   return arrchart
  // }

  countDiscountArr(arr) {
    let arrresult = []
    if (arr && arr.arrproduct) {
      let arrproduct = arr.arrproduct
      let arrtmp = {}
      let arrsort = arrproduct.sort()
      let count = 1
      for(let i=0; i<arrsort.length; i++)
      {
        if (!arrtmp[arrsort[i]]) {arrtmp[arrsort[i]] = 1}
        if(arrsort[i] === arrsort[i+1]) 
        { 
          count++;
          arrtmp[arrsort[i]] = count
        }
        else {
          count = 1
        }
      }

      let keysSorted = Object.keys(arrtmp).sort(function(a,b){return arrtmp[b]-arrtmp[a]}).slice(0, 5)
      for (let i in keysSorted) {
        var obj = this.state.products.find(function (obj) { return obj._id === keysSorted[i]; });
        if (obj) {
          arrresult.push(obj)
        }
      }
    }
    this.setState({arrproducts: arrresult})
  }

  render() {
    
    return (
      <div className='admin-cardblock'>        
        <div className='admin-cardblock-head'><div className='title'>Báo cáo</div><div className='description'>Báo cáo Bán hàng</div></div>
        <div className='row admin-cardblock-formbody'> 
          <div className='col-md-6 lk-box-admin-report'>
            <div className='fullwidth-form-body'>
              <div className='lk-head-report-text'>Báo cáo Bán hàng</div>
              <div className='lk-body-report-text'>Thống kê theo thời gian đặt hàng</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Tháng hiện tại</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Theo ngày giờ</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Theo sản phẩm</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Sản phẩm SKU</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Sản phẩm - Nhà sản xuất</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Kênh bán hàng</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Kênh bán hàng POS</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Kênh bán hàng là Facebook</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Nhân viên bán hàng</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Nhân viên bán hàng - Theo ngày</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Theo Traffic Source</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Bán theo vùng</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Theo khách hàng</div>
            </div>
          </div>
          <div className='col-md-6'>
            <div className='fullwidth-form-body'>
              <div className='lk-head-report-text'>Báo cáo Tài chính
                <div>Báo cáo dòng tiền thu chi theo Ngày, theo Tháng, theo Quý,...</div>
              </div>
              <div className='lk-body-report-text'>Tài chính tổng quan</div>
              <div className='lk-line-box'></div>
              <div className='lk-body-report-text'>Theo doanh thu thuần</div>
            </div>

            <div className='fullwidth-form-body'>
              <div className='lk-head-report-text'>Các bảng báo cáo đã tạo</div>
              <div className='lk-head-report-text'>Danh sách</div>
              <div className='lk-body-report-text'>Báo cáo doanh thu bán hàng - 27/4/2018</div>
              <div className='lk-line-box'></div>
            </div>
          </div>
        

        </div>
      </div>
    )
  }
}

export default AdminReport