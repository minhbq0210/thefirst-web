import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
// import { rxconfig, rxcurrencyVnd } from './../../classes/ulti'
// import { connect } from 'react-redux'
// import { rxnavToggle, rxnavClose } from './../../redux'

polyfill()
const config = global.rxu.config

class AdminConfig extends Component {
  constructor(props) {
    super(props)
    this.state = global.ultiStaticContext(this.props.staticContext) || {}
    this.state.editingData = {bank: [{name: ''}], currency: [{name: ''}], shipping: [{name: ''}]} 
  }

  componentDidMount() {   
    this.fetchData() 
  }

  onBlurDataArr(e, nameparent, index, name, indextemp) {
    let editingData = this.state.editingData
    if (nameparent === 'shipping' && (name === 'weighttotal' || name === 'exchangerate')) {
      if (editingData[nameparent][index] && editingData[nameparent][index]['config'] && editingData[nameparent][index]['config'].constructor === Array) {
        editingData[nameparent][index]['config'][indextemp][name] = e.target.value
      }
    } else {
      editingData[nameparent][index][name] = e.target.value
    }
    this.setState({ editingData: editingData })
  }

  onClickAddConfig(parentname, data, name) {
    let editingData = this.state.editingData
    if (editingData[parentname] && editingData[parentname].constructor === Array) {
      for (var i = 0; i < editingData[parentname].length; i++) {
        if (editingData[parentname][i].name === data.name) {
          if (editingData[parentname][i]['config'] && editingData[parentname][i]['config'].constructor === Array) {
            editingData[parentname][i]['config'].push({})
          } else {
            editingData[parentname][i]['config'] = []
          }
        }
      }
    }
    this.setState({editingData: editingData})
  }

  onClickAddBank(name) {
    let editingData = this.state.editingData
    if (editingData[name] && editingData[name].constructor === Array) {
      editingData[name].push({})  
    } else {
      editingData[name] = [{}]
    }
    this.setState({editingData: editingData})
  }

  onClickDelBank(type, index) {
    let editingData = this.state.editingData
    if (editingData && editingData[type] && editingData[type].constructor === Array) {
      if (editingData[type].length > 0) {
        delete editingData[type][index]
      }
      if (editingData[type].constructor === Array) {
        editingData[type] = editingData[type].filter((e) => {return e}) 
      }
    }
    this.setState({editingData: editingData})
  }

  onClickSave(e, perdata) {
    let arrType = ['bank','currency', 'shipping']
    let editingData = this.state.editingData
    let objparams = {}
    objparams['type'] = '2'
    objparams['info'] = editingData
    
    for (let i in arrType) {
      let type = arrType[i]
      if (objparams['info'] && objparams['info'][type] && objparams['info'][type].constructor === Array && objparams['info'][type].length > 0) {
        objparams['info'][type].forEach((obj, index) => {
          if (obj && Object.keys(obj).length === 0) {
            delete objparams['info'][type][index]  
          }
        })
        objparams['info'][type] = objparams['info'][type].filter(Boolean)
      }
    }
    rxpost(config.api_config_setting, objparams, {
      '1': (json) => { this.fetchData(); alert('Save Success') }
    })
  }

  fetchData() {
    let editingData = this.state.editingData
    rxget(config.api_setting, {search_type: '2'}, {
      '1': (json) => { 
        let datasetting = json.data
        if (datasetting && datasetting[0] && datasetting[0].info) {
          let arrType = ['bank','currency','shipping']
          for (let i in arrType) {
            let type = arrType[i]
            if (datasetting[0].info[type]) { editingData[type] = datasetting[0].info[type]}
          }
          this.setState({ editingData: editingData })
        }
      }
    })
  }

  rxcurrencyConvert(x) {
    if (x) {
      return x.toString().replace(/,/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, ",");  
    } else {
      return x
    }
      
  }

  render() {
    return (
      <div className='admin-cardblock'>        
        <div className='admin-cardblock-head'>
          <div className='title'>Cài đặt</div>
          <div className='description'>Quản lý cài đặt</div>
          <div onClick={(e) => this.onClickSave()} className='btn-save'>Lưu thông tin</div>
        </div>
        <div className='row admin-cardblock-formbody'> 
          <div className='col-md-3 lk-box-admin-report'>
            <div>
              <div className='lk-head-report-text'><b>Thông tin thanh toán ngân hàng</b></div>
              <div className='lk-body-report-text'>Là những thông tin liên quan đến tên chủ tài khoản của shop, tên ngân hàng, số tài khoản ngân hàng, tên chi nhánh ngân hàng</div>
            
            </div>
          </div>
          <div className='col-md-9'>
            <div className='fullwidth-form-body'>
              <div className='row'>
                <div onClick={(e) => this.onClickAddBank('bank')} className='btn-clone'>Thêm thông tin ngân hàng</div>
              </div>
              <br></br>

              {(this.state.editingData.bank && this.state.editingData.bank.constructor === Array) && 
                <div>
                  {this.state.editingData.bank.map( (objbank, indexbank) => (
                    <div key={indexbank}>
                      <div className='row'>
                        <div className='col-md-4'>
                          <div className='lk-body-report-text'>Tên chủ tài khoản</div>
                          <input tabIndex='1' type='text' value={objbank.name || ''} onChange={(e) => this.onBlurDataArr(e, 'bank', indexbank, 'name')} className='fullwidth-input lk-input-text-code' />
                        </div>
                        <div className='col-md-4'>
                          <div className='lk-body-report-text'>Tên ngân hàng</div>
                          <input tabIndex='1' type='text' value={objbank.namebank || ''} onChange={(e) => this.onBlurDataArr(e, 'bank', indexbank, 'namebank')} className='fullwidth-input lk-input-text-code' />
                        </div>
                        <div className='col-md-3'>
                          <div className='lk-body-report-text'></div>
                          <div onClick={(e) => this.onClickDelBank('bank', indexbank)} className='btn-clone remove'>Xóa thông tin</div>
                        </div>
                      </div>
                      <div className='row'>
                        <div className='col-md-4'>
                          <div className='lk-body-report-text'>Số tài khoản ngân hàng</div>
                          <input tabIndex='1' type='text' value={objbank.numberbank || ''} onChange={(e) => this.onBlurDataArr(e, 'bank', indexbank, 'numberbank')} className='fullwidth-input lk-input-text-code' />
                        </div>
                        <div className='col-md-4'>
                          <div className='lk-body-report-text'>Tên chi nhánh ngân hàng</div>
                          <input tabIndex='1' type='text' value={objbank.bankbranch || ''} onChange={(e) => this.onBlurDataArr(e, 'bank', indexbank, 'bankbranch')} className='fullwidth-input lk-input-text-code' />
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              }
            </div>
          </div>
        </div>

        <div className='lk-line-box'></div>

        <div className='row admin-cardblock-formbody'> 
          <div className='col-md-3 lk-box-admin-report'>
            <div>
              <div className='lk-head-report-text'><b>Cài đặt tỉ giá ngoại tệ</b></div>
              <div className='lk-body-report-text'>
                Là cài đặt tỉ giá ngoại tệ để chuyển đổi mức giá sang các loại ngoại tệ khác nhau, VD: 1AUD = 16,500VND.<br/>
                Tỷ giá AUD được cài đặt mặc định.
              </div>
            
            </div>
          </div>
          <div className='col-md-9'>
            <div className='fullwidth-form-body'>
              <div className='row'>
                <div onClick={(e) => this.onClickAddBank('currency')} className='btn-clone'>Thêm loại ngoại tệ</div>
              </div>
              <br></br>

              {(this.state.editingData.currency && this.state.editingData.currency.constructor === Array) && 
                <div>
                  {this.state.editingData.currency.map((objcurrency, indexcurrency) => (
                    <div className='row' key={indexcurrency}>
                      <div className='col-md-3'>
                        <div className='lk-body-report-text'>Tên ngoại tệ viết tắt</div>
                        <input tabIndex='1' type='text' value={objcurrency.name || ''} onChange={(e) => this.onBlurDataArr(e, 'currency', indexcurrency, 'name')} className='fullwidth-input lk-input-text-code' />
                      </div>
                      <div className='col-md-3'>
                        <div className='lk-body-report-text'>Tỷ giá ngoại tệ</div>
                        <input tabIndex='1' type='text' value={this.rxcurrencyConvert(objcurrency.exchangerate) || ''} onChange={(e) => this.onBlurDataArr(e, 'currency', indexcurrency, 'exchangerate')} className='fullwidth-input lk-input-text-code' />
                      </div>
                      <div className='col-md-3'>
                        <div className='lk-body-report-text'>Ký hiệu tiền tệ</div>
                        <input tabIndex='1' type='text' value={objcurrency.symbol || ''} onChange={(e) => this.onBlurDataArr(e, 'currency', indexcurrency, 'symbol')} className='fullwidth-input lk-input-text-code' />
                      </div>
                      <div className='col-md-2'>
                        <div className='lk-body-report-text'></div>
                        <div onClick={(e) => this.onClickDelBank('currency', indexcurrency)} className='btn-clone remove'>Xóa thông tin</div>
                      </div>
                    </div>
                  ))}
                </div>
              }
            </div>
          </div>
        </div>

        <div className='lk-line-box'></div>

        <div className='row admin-cardblock-formbody'> 
          <div className='col-md-3 lk-box-admin-report'>
            <div>
              <div className='lk-head-report-text'><b>Cài đặt phí vận chuyển</b></div>
              <div className='lk-body-report-text'>Là cài đặt chi phí vận chuyển cho mỗi đơn hàng, input chi phí vận chuyển mặc định là giá AUD.</div>
            
            </div>
          </div>
          <div className='col-md-9'>
            <div className='fullwidth-form-body'>
              <div className='row'>
                <div onClick={(e) => this.onClickAddBank('shipping')} className='btn-clone'>Thêm phương thức vận chuyển</div>
              </div>
              <br></br>

              {(this.state.editingData.shipping && this.state.editingData.shipping.constructor === Array) && 
                <div>
                  {this.state.editingData.shipping.map( (objshipping, indexshipping) => (
                    <div className='row' key={indexshipping}>
                      <div className='col-md-4'>
                        <div className='lk-body-report-text'>Tên phương thức vận chuyển</div>
                        
                        <input tabIndex='1' type='text' value={objshipping.name || ''} onChange={(e) => this.onBlurDataArr(e, 'shipping', indexshipping, 'name')} className='fullwidth-input lk-input-text-code' />
                      </div>
                      {
                        this.state.editingData.shipping[indexshipping].config && this.state.editingData.shipping[indexshipping].config.constructor === Array &&
                        <div className='col-md-4'>
                        {
                          this.state.editingData.shipping[indexshipping].config.map((objtemp, indextemp) => (
                            <div key={indextemp} className='row'>
                            <div className='col-md-6'>
                              <div className='lk-body-report-text'>Khối lượng</div>
                              <input tabIndex='1' type='text' value={ this.rxcurrencyConvert(objtemp.weighttotal) || '' } onChange={(e) => this.onBlurDataArr(e, 'shipping', indexshipping, 'weighttotal', indextemp)} className='fullwidth-input lk-input-text-code' />
                            </div>
                            <div className='col-md-6'>
                              <div className='lk-body-report-text'>Chi phí</div>
                              <input tabIndex='1' type='text' value={ this.rxcurrencyConvert(objtemp.exchangerate) || ''} onChange={(e) => this.onBlurDataArr(e, 'shipping', indexshipping, 'exchangerate', indextemp)} className='fullwidth-input lk-input-text-code' />
                            </div>
                            </div>
                          ))
                        }
                        </div>
                      }
                      <div className='col-md-3'>
                        <div className='lk-body-report-text'></div>
                        <div onClick={(e) => this.onClickAddConfig('shipping', objshipping, 'config')} className='btn-clone'>Thêm khối lượng</div>
                        <div onClick={(e) => this.onClickDelBank('shipping', indexshipping)} className='btn-clone remove'>Xóa thông tin</div>
                      </div>
                    </div>
                  ))}
                </div>
              }
            </div>
          </div>
        </div>

        <div className='lk-line-box'></div>

      </div>
    )
  }
}

export default AdminConfig