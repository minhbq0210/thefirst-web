import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import { rxgetdate, rxcurrencyVnd, rxChangeAlias } from './../../classes/ulti'

polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config
// const numberWithCommas = (x) => {
//   return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","): '';
// }

class AdminCustomer extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [], 
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      editingData: {},
      orderinfoData: [],
      isEditCustomer: false,
    }
    this.state.dataLocal = global.getDataLocal()
    this.state.dataDistrict = []
  }

  componentDidMount() {
    this.fetchAlldata()
    this.timerDatafilter = null
  }

  fetchAlldata() {
    this.fetchData()
  }

  fetchData() {
    rxget(config.api_customer, this.state.paging, {
      '1': (json) => { this.setState({ data: json.data })}
    })
  }

  //////////////
  // CUSTOMER //
  //////////////
  onClickSort(e, stcol) {
    let paging = this.state.paging                  
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ paging: paging }, () => { this.fetchData() })    
  }

  onClickDataEdit(e, perdata, type) {
    let paramsorder = { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 }
    paramsorder['ids_order'] = perdata.ids_order
    if (perdata.ids_order !== undefined) {
      rxget(config.api_customer_allorder, paramsorder, {
        '1': (json) => { 
          this.setState({ orderinfoData: json.data })
        }
      })
    }

    if(!perdata._id) {
      this.setState({isEditCustomer: false})
    }

    if(type && type === 1) {
      perdata.street = perdata['address'][0].street ? perdata['address'][0].street : ''
      perdata.county = perdata['address'][0].county ? perdata['address'][0].county : ''
      perdata.city = perdata['address'][0].city ? perdata['address'][0].city : ''
      let dataDistricts = this.state.dataDistrict
      if(this.state.dataLocal && perdata.city) {
        let checkcity = this.state.dataLocal.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(perdata.city)) !== -1)
        if(checkcity && checkcity.length > 0 && checkcity.name !== perdata.city) {
          dataDistricts = checkcity[0].districts
          if(dataDistricts && perdata.county) {
            let checkcdistrict = Object.keys(dataDistricts).filter(key => rxChangeAlias(dataDistricts[key]).indexOf(rxChangeAlias(perdata.county)) !== -1)
            if(checkcdistrict) {
              perdata.county = dataDistricts[checkcdistrict]
            }
          }
          perdata.city = checkcity[0].name
        }
      }
      this.setState({ editingData: perdata, dataDistrict: dataDistricts, isEditCustomer: true })
    }

    let clone = JSON.parse(JSON.stringify(perdata))
    this.setState({ editingData: clone })
  }

  onClickDataDeleteNew(e, perdata) {    
    e.stopPropagation()
    rxget(config.api_customer_delete, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataRestore(e, perdata) {
    e.stopPropagation()
    rxget(config.api_customer_restore, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataUpdateSubmit(e) {
    // this.setState({ isEditCustomer: true })
    rxpost(config.api_customer_edit, this.state.editingData, {
      // rxpost(config.api_user_edit_customer, this.state.editingData, {
      '1': (json) => { 
        this.fetchData()
        alert('thành công')
        this.onClickDataEdit({}, {})
      }
    })
  }

  onClickDataCreateSubmit(e, perdata) {
    rxpost(config.api_customer, this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataTrash(e, isdeleted) {
    let paging = this.state.paging
    paging['search_is_deleted'] = isdeleted
    this.setState({ paging: paging }, () => {
      this.fetchData()
    }) 
  }

  onClickDataNew(e) {
    let timeStr = Date.now().toString()
    let clone = { name: 'Customer_'+ timeStr.substr(timeStr.length - 5), desc: '', created_at: 1, is_deleted: 0, is_active: 1, price: 100000, stock: 100, app: '5a50b4ebee345a312dfd1aec', appdist: '5a50b4ebee345a312dfd1aec,5a50adbaee345a312dfd1aea' }
    this.setState({ editingData: clone })
  }

  onBlurDatafilter(e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging
    paging['search_' + name] = e.target.value
    this.setState({ paging: paging })

    this.timerDatafilter = setTimeout((e, name) => {
      this.fetchData()
    }, WAIT_INTERVAL)    
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    if(name === 'street') {
      editingData['address'][0]['street'] = e.target.value
    } else {
      editingData[name] = e.target.value
    }
    this.setState({ editingData: editingData })    
  }

  handleChangeCity(e, name) {
    let editingData = this.state.editingData
    editingData['address'][0][name] = e.target.value
    let dataDistricts = this.state.dataDistrict
    if(name === 'city') {
      let checkcity = this.state.dataLocal.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(e.target.value)) !== -1)
      if(checkcity && checkcity.length > 0 && checkcity.name !== this.state.editingData['city']) {
        dataDistricts = checkcity[0].districts
        let key = Object.keys(dataDistricts)[0]
        editingData['address'][0]['county'] = dataDistricts[key]
      }
    }
    this.setState({editingData: editingData, dataDistrict: dataDistricts})
  }

  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  // Callback upload
  callbackUpload(e) {
    this.onBlurData({target: {value: e.images}}, 'img_landscape')
  }

  callbackUploadDetail(e) {    
    this.onBlurData({target: {value: e.images}}, 'img_detail')
  }  

  // Help show functions
  helpSortClass(stcol, extraclass) {
    extraclass = extraclass || []

    let result = ''    
    if (this.state.paging.st_col === stcol) {
      result = this.state.paging.st_type === 1 ? 'rx-sort-asc' : 'rx-sort-desc'
    }

    for(let i = 0; i < extraclass.length; i++) {      
      result += ' ' + extraclass[i]
    }

    return result 
  }

  onChangeAdress(e,key){
    let editingData = this.state.editingData 
    editingData.address[key] = e.target.value
    this.setState({editingData})
  }
  onClickEditCustomer(e){
    let isEditCustomer = this.state.isEditCustomer
    this.setState({isEditCustomer:!isEditCustomer})
  }

  render() {
    //total page
    let totalpage = 0
    if(this.state.data.length && this.state.data.length > 0){
      totalpage = Math.ceil(this.state.data.length / this.state.paging.pg_size)
    }

    // Data tables
    let data = this.state.data.map(perdata => (
      <tr key={perdata._id}>
        <td className='lk-action-edit' onClick={(e) => this.onClickDataEdit(e, perdata)}>{perdata.name}</td>
        <td>{perdata.phone}</td>
        {/*<td>{perdata.address[perdata.address.length - 1]}</td>*/}
        <td>{global.rxu.get(perdata, ['address',0,'street']) ? global.rxu.get(perdata, ['address',0,'street']) : global.rxu.get(perdata, ['address',1,'street'])}</td>
        <td><span className='lk-number-table'>{perdata.ids_order.length}</span></td>
        <td className='lk-action-edit'><Link to={{ pathname: '/admin/order/', id: perdata.ids_order[perdata.ids_order.length - 1] }}>{perdata.ids_order[perdata.ids_order.length - 1]}</Link></td> 
        <td>{rxgetdate(perdata.created_at)}</td>                   
        {/*<td><span className='table-total-money'>{rxcurrencyVnd(perdata.price)} </span></td>*/}
        <td>
          { this.state.paging.search_is_deleted !== 1 && <div>
            <span className='rx-action-edit' onClick={(e) => this.onClickDataEdit(e, perdata, 1)}>Edit</span>
            <span className='rx-action-delete' onClick={(e) => this.onClickDataDeleteNew(e, perdata)}>Xoá</span> 
          </div>}
          { this.state.paging.search_is_deleted === 1 && 
            <div> <span className='rx-action-restore' onClick={(e) => this.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}
        </td>
      </tr>
    ))

    // Data data order
    let dataorder = []
    if (this.state.orderinfoData && this.state.orderinfoData.length > 0) {
      dataorder = this.state.orderinfoData.map(perdata => (
        <tr key={perdata._id}>         
          <td className='lk-action-edit'><Link to={{ pathname: '/admin/order/', id: perdata._id }}>{perdata.order_code}</Link></td>
          <td>{rxgetdate(perdata.created_at)}</td>
          {(perdata.status_confirm !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>Chưa xác thực</span></td>}
          {(perdata.status_confirm === 1) && <td><span className='lk-status-box lk-status-confirm'>Đã xác thực</span></td>}   
          {(perdata.status_ship !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>Chưa giao hàng</span></td>}
          {(perdata.status_ship === 1) && <td><span className='lk-status-box lk-status-confirm'>Đã giao hàng</span></td>}        
          {(perdata.status_order_payment !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>Chờ xử lý</span></td>}
          {(perdata.status_order_payment === 1) && <td><span className='lk-status-box lk-status-confirm'>Đã thanh toán</span></td>}
          {(perdata.type_payment !== 2) && <td><span className='lk-status-box lk-status-confirm'>Có</span></td>}
          {(perdata.type_payment === 2) && <td><span className='lk-status-box lk-status-noconfirm'>Không</span></td>}      
          <td><span className='table-total-money'>{rxcurrencyVnd(perdata.price)} </span></td>
          <td>{perdata.type_channel_name}</td>
        </tr>
      ))
    }

    let dataaddress = []
    if (this.state.editingData.address) {
      dataaddress = this.state.editingData.address.map((perdata,key) => (
        // this.state.isEditCustomer ? 
        perdata.street && perdata.county && perdata.city &&
        <div className='orderblock-customer-detail-text' key={key}> {key+1} 
          <div><span>Địa chỉ: </span><span className='orderblock-customer-detail-address'>
            {global.rxu.get(perdata, ['street'], '')}, {global.rxu.get(perdata, ['county'], '')}, {global.rxu.get(perdata, ['city'], '')}
          </span></div>
          <hr/>
        </div>
      ))
    }

    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Khách hàng</div><div className='description'>Quản lý khách hàng</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            
            <div className='col-lg-12 col-sm-12'>
              <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurDatafilter(e, 'name')} /></span>
              <i className='icon-plus rx-recycle' onClick={(e) => this.onClickDataNew(e)}></i>
              { this.state.paging.search_is_deleted !== 1 && <i className='icon-trash rx-recycle' onClick={(e) => this.onClickDataTrash(e, 1)}></i> }              
              { this.state.paging.search_is_deleted === 1 && <i className='icon-list rx-recycle' onClick={(e) => this.onClickDataTrash(e, 0)}></i> }

              <div className='admin-table-pagination admin-pagin-right'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
                <div className='pagin-curr'>Total page: {totalpage}</div>
              </div>

              <div className='admin-table-wrap'><table className='admin-table-product preset'>
                <thead>
                  <tr>
                    <th className={this.helpSortClass('name', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'name')}>Tên khách hàng</th>
                    <th className={this.helpSortClass('phone', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'phone')}>Số điện thoại</th>
                    <th className={this.helpSortClass('street', ['rx-th-width-100'])}>Địa chỉ</th>
                    <th className={this.helpSortClass('amountorder', ['rx-th-width-60'])}>Tổng đơn hàng</th>
                    <th className={this.helpSortClass('lastorder', ['rx-th-width-100'])}>Đơn hàng gần nhất</th>   
                    <th className={this.helpSortClass('lastcreated_at', ['rx-th-width-220'])}>Thời gian đặt hàng gần nhất</th>               
                    {/*<th className={this.helpSortClass('price', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'price')}>Tổng tiền</th>*/}
                    <th>Thao tác</th>
                  </tr>
                </thead>
                <tbody>{data}</tbody>
              </table></div>
              <div className='admin-table-pagination'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
            </div>
          </div>
          <div className='clearfix-martop'></div>          
        </div>

        { this.state.editingData.created_at && <div className='admin-cardblock-form'>
            <div className='admin-cardblock-formname'>Thông tin khách hàng <b>{this.state.editingData.name}</b>
              <div className='admin-cardblock-formclose' >
                <a tabIndex='2' className='btn-cancel' onClick={(e) => this.onClickDataEdit(e, {})} onKeyPress={(e) => this.onClickDataEdit(e, {})}>Trở về</a>
                <a tabIndex='3' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>
              </div>
            </div>


            <div className='admin-cardblock-formbody'><div className='clearfix'>
            
            <div className='row'>
              <div className='col-sm col-md-8'>
                <div className='admin-orderblock-details-main'>
                  <div>
                    <div className='orderblock-customer-detail-title'>
                      <div className='orderblock-customer-detail-title-name'>
                        <lable className='orderblock-customer-lable-name'>Thông tin đơn hàng</lable>
                      </div>
                    </div>
                      
                  </div>
                  <div className='border-line-main'></div>
                  
                  <div className='admin-table-wrap'>
                  <table className='admin-table-product preset'>
                    <thead>
                      <tr>
                        <th>Mã</th>
                        <th>Ngày đặt</th>
                        <th>Xác thực</th>
                        <th>Giao hàng</th>
                        <th>Thanh toán</th>                  
                        <th>COD</th>
                        <th>Tổng tiền</th>
                        <th>Kênh</th>
                      </tr>
                    </thead>
                    <tbody>{dataorder}</tbody>
                  </table></div>
              
                </div>
              </div>
              <div className='col-sm col-md-4 admin-orderblock-customer-main'>
                <div className='orderblock-customer-detail'> 
                  <div className='orderblock-customer-detail-title'>
                    <div className='orderblock-customer-detail-title-name'>
                      <lable className='orderblock-customer-lable-name'>Thông tin cá nhân</lable>
                    </div>
                    <div className='orderblock-customer-detail-title-channel'>
                      <img className='lk-card-order-img lazyload' alt='ico_default' data-src={config.base_api + '/upload/image/ico_app_default.png'} />
                    </div>
                  </div>
                  <div className='orderblock-customer-detail-info'>
                    {this.state.isEditCustomer ? <div>
                      <div className='orderblock-customer-detail-text'>Khách hàng: 
                        <input tabIndex='1' value={global.rxu.get(this.state.editingData, ['name'], '')} onChange={(e) => this.onBlurData(e, 'name')}/>
                      </div>
                      <div className='orderblock-customer-detail-text'>SĐT: 
                      <input tabIndex='2' value={global.rxu.get(this.state.editingData, ['phone'], '')} onChange={(e) => this.onBlurData(e, 'phone')}/>
                      </div>
                    </div> : <div>
                      <div className='orderblock-customer-detail-name'>Khách hàng: {this.state.editingData.name} </div>
                      <div className='orderblock-customer-detail-name'>SĐT: {this.state.editingData.phone}</div>
                    </div>}
                  </div>
                </div>
                <div className='border-line-main'></div>

                <div className='orderblock-customer-address'>
                  <div className='orderblock-customer-detail-title'>
                    <div className='orderblock-customer-detail-title-name'>
                      <lable className='orderblock-customer-lable-name'>Địa chỉ giao hàng</lable>
                    </div>
                    {/*<div className='orderblock-customer-detail-title-channel' onClick={(e) => this.onClickEditCustomer(e)}>Edit</div>*/}
                  </div>
                  <div className='orderblock-customer-detail-info'>
                    {this.state.isEditCustomer ? <div>
                      <div className='orderblock-customer-detail-text'>Tỉnh/ Thành phố: 
                      <select className='fullwidth-select' value={this.state.editingData['address'][0].city} onChange={(e) => {this.handleChangeCity(e, 'city')}}>
                        {this.state.dataLocal && this.state.dataLocal.length > 0 && this.state.dataLocal.map((option, index) => (<option key={index} value={option.name}>{option.name}</option>))}
                      </select>
                      </div>
                      <div className='orderblock-customer-detail-text'>Quận/ Huyện: 
                      <select className='fullwidth-select' value={this.state.editingData['address'][0].county} onChange={(e) => {this.handleChangeCity(e, 'county')}}>
                        {this.state.dataDistrict && Object.keys(this.state.dataDistrict).length>0 && Object.keys(this.state.dataDistrict).map(key => (<option key={key} value={this.state.dataDistrict[key]}>{this.state.dataDistrict[key]}</option>))}
                      </select>
                      </div>
                      <div className='orderblock-customer-detail-text'>Địa chỉ: 
                      <input tabIndex='3' value={global.rxu.get(this.state.editingData.address[0], ['street'], '')} onChange={(e) => this.onBlurData(e, 'street')}/>
                      </div>
                    </div>  : dataaddress}
                  </div>
                </div>
                <div></div>
              </div>
            </div>  
            </div></div>
          </div> }

      </div>
    )
  }
}

export default AdminCustomer