import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import { rxgetdate } from './../../classes/ulti'

polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config

class AdminUser extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [],  
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10, search_admin: true },
      editingData: {},
      roleData:[],
      msg: ''
    }
    this.state.dataLocal = global.getDataLocal()
    this.state.editingData['city'] = global.rxu.get(this.state.editingData, ['address', 0, 'city'], 'Thành phố Hồ Chí Minh')
    this.state.editingData['county'] = global.rxu.get(this.state.editingData, ['address', 0, 'city'], 'Quận 1')
    this.state.dataDistrict = []
    if(this.state.dataLocal && this.state.editingData['city']) {
      let checkcity = this.state.dataLocal.find(obj => obj.name === this.state.editingData['city'] )
      if(checkcity) {
        let districts = []
        Object.keys(checkcity.districts).forEach(function(key) {
          districts.push(checkcity.districts[key])
        })
        this.state.dataDistrict = districts
      }
    }
  }

  componentDidMount() {
    this.fetchAlldata()
    this.timerDatafilter = null
  }

  fetchAlldata() {
    this.fetchData()
    this.fetchDataRole()
  }

  fetchData() {    
    rxget(config.api_user, this.state.paging, {
      '1': (json) => { 
        this.setState({ data: json.data }) 
      }
    })
  }

  fetchDataRole() {
    rxget(config.api_role_all, {}, {
      '1': (json) => { 
        this.setState({ roleData: json.data }) 
      }
    })
  }

  ////////////////
  // User ////////
  ////////////////
  onClickSort(e, stcol) {
    let paging = this.state.paging                  
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ paging: paging }, () => { this.fetchData() })    
  }

  onClickDataEdit(e, perdata) {
    perdata['city'] = global.rxu.get(perdata, ['address', 0, 'city'], 'Thành phố Hồ Chí Minh')
    perdata['county'] = global.rxu.get(perdata, ['address', 0, 'county'], 'Quận 1')
    perdata['street'] = global.rxu.get(perdata, ['address', 0, 'street'], '')
    let clone = JSON.parse(JSON.stringify(perdata))
    this.setState({ editingData: clone })
  }

  onClickUserNew(e) {    
    let clone = { fullname: '', email: '', phone: '', city: this.state.editingData['city'], county: this.state.editingData['county'], created_at: 1, is_deleted: 0, is_active: 1, username: '' }
    this.setState({ editingData: clone })
  }

  onClickDataDeleteNew(e, perdata) {    
    e.stopPropagation()
    rxget(config.api_user_delete, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataRestore(e, perdata) {
    e.stopPropagation()
    rxget(config.api_user_restore, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataUpdateSubmit(e, perdata) {
    let editingData = this.state.editingData
    if(editingData['street'] && editingData['county'] && editingData['city']) {
      if(editingData['address'].length === 0) {
        editingData['address'].push({street: editingData['street'], county: editingData['county'], city: editingData['city']})
      } else {
        editingData['address'] = [{street: editingData['street'], county: editingData['county'], city: editingData['city']}]
      }
      delete editingData['street']
      delete editingData['county']
      delete editingData['city']
    }
    rxpost(config.api_user_edit, editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataCreateSubmit(e, perdata) {
    let tempEditingData = this.state.editingData    
    if (!this.state.editingData.admin && typeof(this.state.editingData['admin']) === 'undefined') {
      tempEditingData.admin = 'true'
    }
    if (!this.state.editingData.roleid && typeof(this.state.editingData['roleid']) === 'undefined') {
      tempEditingData.roleid = this.state.roleData[0] ? this.state.roleData[0]['_id'] : null   
    }
    if(tempEditingData['street'] && tempEditingData['county'] && tempEditingData['city']) {
      if(tempEditingData['address'] && tempEditingData['address'].length === 0) {
        tempEditingData['address'].push({street: tempEditingData['street'], county: tempEditingData['county'], city: tempEditingData['city']})
      } else {
        tempEditingData['address'] = [{street: tempEditingData['street'], county: tempEditingData['county'], city: tempEditingData['city']}]
      }
      delete tempEditingData['street']
      delete tempEditingData['county']
      delete tempEditingData['city']
    }
    this.setState({editingData: tempEditingData}, () => {
      rxpost(config.api_user_admin, this.state.editingData, {
        '1': (json) => { 
          this.fetchData()
          this.onClickDataEdit({}, {})
          this.setState({ msg: '' })
        },
        '-2': (json) => {
          var strmsg = ''
          if (json.msg === 'Dupplicate data') {
            strmsg = 'Email đã tồn tại'
          } else if (json.msg === 'Email invalid format') {
            strmsg = 'Email không đúng định dạng'
          } else {
            strmsg = 'Các trường không được bỏ trống'
          }
          this.setState({ msg: strmsg })
        }
      })
    })    
  }

  onClickDataTrash(e, isdeleted) {
    let paging = this.state.paging
    paging['search_is_deleted'] = isdeleted
    this.setState({ paging: paging }, () => {
      this.fetchData()
    }) 
  }

  onClickDataNew(e) {
    let timeStr = Date.now().toString()
    let clone = { fullname: 'UserAdmin_'+ timeStr.substr(timeStr.length - 5), desc: '', created_at: 1, is_deleted: 0, is_active: 1}
    this.setState({ editingData: clone })
  }

  onBlurDatafilter(e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging
    paging['search_' + name] = e.target.value
    this.setState({ paging: paging })

    this.timerDatafilter = setTimeout((e, name) => {
      this.fetchData()
    }, WAIT_INTERVAL)    
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  handleChange(e, paging) {
    let params = this.state.editingData
    if (e.target.value === 0) {
      delete(params['roleid'])
    } else {
      params['roleid'] = e.target.value
      this.setState({editingData: params});
    }
  }

  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  helpSortClass(stcol, extraclass) {
    extraclass = extraclass || []

    let result = ''    
    if (this.state.paging.st_col === stcol) {
      result = this.state.paging.st_type === 1 ? 'rx-sort-asc' : 'rx-sort-desc'
    }
    for(let i = 0; i < extraclass.length; i++) {      
      result += ' ' + extraclass[i]
    }

    return result 
  }
  handleChangeCity(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    if(name === 'city') {
      let checkcity = this.state.dataLocal.find(obj => obj.name === e.target.value )
      if (checkcity) {
        let districts = []
        Object.keys(checkcity.districts).forEach(function(key) {
          districts.push(checkcity.districts[key])
        })
        this.setState({dataDistrict: districts})
      }
    }
    this.setState({editingData: editingData})
  }

  render() {
    // Data tables
    let data = this.state.data.map(perdata => (
      <tr key={perdata._id}>
        <td>{perdata.fullname}</td>
        <td>{perdata.username}</td>
        <td>{global.rxu.get(perdata,['address', 0, 'street'])}, {global.rxu.get(perdata,['address', 0, 'county'])}, {global.rxu.get(perdata,['address', 0, 'city'])}</td>
        <td>{perdata.email}</td>
        <td>{perdata.phone}</td>
        <td>{rxgetdate(perdata.created_at)}</td>                  
        <td>
          { this.state.paging.search_is_deleted !== 1 &&
            <div> {(perdata.is_active !== 0) && <span className='admin-table-on'>On</span>}{(perdata.is_active === 0) && <span className='admin-table-off'>Off</span>}
                  <span className='rx-action-edit' onClick={(e) => this.onClickDataEdit(e, perdata)}>Sửa</span>
                  <span className='rx-action-delete' onClick={(e) => this.onClickDataDeleteNew(e, perdata)}>Xoá</span> </div> }
          { this.state.paging.search_is_deleted === 1 && 
            <div> <span className='rx-action-restore' onClick={(e) => this.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}
        </td>
      </tr>
    ))

    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Người quản trị</div><div className='description'>Quản lý người quản trị</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            
            <div className='col-lg-12 col-sm-12'>
              <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurDatafilter(e, 'name')} /></span>
              <i className='icon-plus rx-recycle' onClick={(e) => this.onClickUserNew(e)}></i>
              { this.state.paging.search_is_deleted !== 1 && <i className='icon-trash rx-recycle' onClick={(e) => this.onClickDataTrash(e, 1)}></i> }              
              { this.state.paging.search_is_deleted === 1 && <i className='icon-list rx-recycle' onClick={(e) => this.onClickDataTrash(e, 0)}></i> }

              <div className='admin-table-pagination admin-pagin-right'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>

              <div className='admin-table-wrap'><table className='admin-table-product preset'>
                <thead>
                  <tr>
                    <th className={this.helpSortClass('fullname', ['rx-th-width-220'])} onClick={(e) => this.onClickSort(e, 'fullname')}>Tên người quản trị</th>
                    <th className={this.helpSortClass('username', ['rx-th-width-220'])} onClick={(e) => this.onClickSort(e, 'username')}>Username</th>
                    <th className={this.helpSortClass('address', ['rx-th-width-220'])} onClick={(e) => this.onClickSort(e, 'address')}>Địa chỉ liên hệ</th>
                    <th className={this.helpSortClass('email', ['rx-th-width-220'])} onClick={(e) => this.onClickSort(e, 'email')}>Địa chỉ email</th>
                    <th className={this.helpSortClass('phone', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'phone')}>Số điện thoại</th>
                    <th className={this.helpSortClass('created_at', ['rx-th-width-220'])} onClick={(e) => this.onClickSort(e, 'created_at')}>Ngày tạo</th>
                    <th>Thao tác</th>
                  </tr>
                </thead>
                <tbody>{data}</tbody>
              </table></div>
              <div className='admin-table-pagination'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
            </div>
          </div>
          <div className='clearfix-martop'></div>          
        </div>

        { this.state.editingData.created_at && <div className='admin-cardblock-form'>
            <div className='admin-cardblock-formname'>{(this.state.editingData.created_at !== 1) ? 'Sửa thông tin' : 'Tạo người quản trị'} <b>{(this.state.editingData.created_at !== 1) ? this.state.editingData.fullname : ''}</b><div className='admin-cardblock-formclose' onClick={(e) => this.onClickDataEdit(e, {})}>x</div></div>
            <div className='admin-cardblock-formbody'><div className='clearfix'>
              { this.state.msg && <div className='clearfix'><div className='lk-input-error' >{this.state.msg}</div></div> }
              <div className='rx-input-group-two'>
                <div><div className='fullwidth-label'>Tên người quản trị</div><input tabIndex='1' type='text' name='fullname' value={this.state.editingData.fullname || ''} onChange={(e) => this.onBlurData(e, 'fullname')} className='fullwidth-input' /></div>
                <div><div className='fullwidth-label'>Username</div><input tabIndex='2' type='text' name='username' value={this.state.editingData.username || ''} onChange={(e) => this.onBlurData(e, 'username')} className='fullwidth-input' /></div>
                <div><div className='fullwidth-label'>Mật khẩu</div><input tabIndex='3' type='password' name='password' value={this.state.editingData.password || ''} onChange={(e) => this.onBlurData(e, 'password')} className='fullwidth-input' disabled={(this.state.editingData.created_at !== 1)}/></div>
              </div>

              <div className='rx-input-group-two'>
                <div><div className='fullwidth-label'>Địa chỉ email</div><input tabIndex='1' type='text' name='email' value={this.state.editingData.email || ''} onChange={(e) => this.onBlurData(e, 'email')} className='fullwidth-input' /></div>
                <div>
                  <div className='fullwidth-label'>Tỉnh/ thành phố</div>
                  <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['city'], 'Thành phố Hồ Chí Minh')} onChange={(e) => {this.handleChangeCity(e, 'city')}}>
                    {this.state.dataLocal && this.state.dataLocal.length > 0 && this.state.dataLocal.map((option, index) => (<option key={index} value={option.name}>{option.name}</option>))}
                  </select>
                  {/*<input tabIndex='4' type='text' name='county' value={global.rxu.get(this.state.editingData,['address',0,'county']) ? global.rxu.get(this.state.editingData,['address',0,'county']) : global.rxu.get(this.state.editingData,['county'])}  onChange={(e) => this.onBlurData(e, 'county')} className='fullwidth-input' />*/}
                </div>
                <div>
                  <div className='fullwidth-label'>Quận/ Huyện</div>
                  <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['county'], 'Quận 1')} onChange={(e) => {this.handleChangeCity(e, 'county')}}>
                    {this.state.dataDistrict && this.state.dataDistrict.length>0 && this.state.dataDistrict.map((option, key) => (<option key={key} value={option}>{option}</option>))}
                  </select>
                  {/*<input tabIndex='5' type='text' name='city' value={global.rxu.get(this.state.editingData,['address',0,'city']) ? global.rxu.get(this.state.editingData,['address',0,'city']) : global.rxu.get(this.state.editingData,['city'])}  onChange={(e) => this.onBlurData(e, 'city')} className='fullwidth-input' />*/}
                </div>
                <div><div className='fullwidth-label'>Địa chỉ liên lạc</div><input tabIndex='3' type='text' name='address' value={global.rxu.get(this.state.editingData,['address',0,'street']) ? global.rxu.get(this.state.editingData,['address',0,'street']) : global.rxu.get(this.state.editingData,['street'])} onChange={(e) => this.onBlurData(e, 'street')} className='fullwidth-input' /></div>
              </div>

              <div className='rx-input-group-two'>
                <div><div className='fullwidth-label'>Số điện thoại</div><input tabIndex='1' type='text' name='phone' value={this.state.editingData.phone || ''} onChange={(e) => this.onBlurData(e, 'phone')} className='fullwidth-input' /></div>
                <div>
                  <div className='fullwidth-label'>Vai trò quản trị:</div>
                  <div className='lk-select-form' tabIndex='3'>
                    {this.state.roleData && <select className='select-search-category' defaultValue={this.state.editingData.roleid || ''} onChange={(e) => this.handleChange(e, this.state.paging)} >
                      {this.state.roleData.map((perdata,index) => <option key={index} value={perdata._id}>{perdata.name}</option>)}
                    </select>}
                  </div>
                </div>
              </div>

              <div className='admin-cardblock-btns'>
                <a tabIndex='3' className='btn-cancel' onClick={(e) => this.onClickDataEdit(e, {})} onKeyPress={(e) => this.onClickDataEdit(e, {})}>Trở về</a>
                {this.state.editingData.created_at !== 1 && <a tabIndex='3' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>}
                {this.state.editingData.created_at === 1 && <a tabIndex='4' className='btn-clone' onClick={(e) => this.onClickDataCreateSubmit(e)} onKeyPress={(e) => this.onClickDataCreateSubmit(e)}>Tạo mới</a>}
              </div>
            </div></div>
          </div> 
        }

      </div>
    )
  }
}

export default AdminUser