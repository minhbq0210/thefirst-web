import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import { rxgetdate } from './../../classes/ulti'

polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config
const numberWithCommas = (x) => {
  return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","): '';  
}

class AdminInventory extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [], 
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      editingData: {},
      valueData: {}
    }
  }

  componentDidMount() {
    this.fetchAlldata()
    this.timerDatafilter = null
  }

  fetchAlldata() {
    this.fetchData()
  }

  fetchData() {
    rxget(config.api_product_inventory, this.state.paging, {
      '1': (json) => { this.setState({ data: json.data }) }
    })
  }
 
  /////////////
  // ORDER //
  /////////////
  onClickSort(e, stcol) {
    let paging = this.state.paging                  
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ paging: paging }, () => { this.fetchData() })    
  }

  onClickDataUpdateSubmit(productid, perdataid, key) {
    let editingData = this.state.editingData
    let params = {_id: productid, key: key}
    if (editingData[perdataid] && editingData[perdataid]['value']) {
      params['amount'] = editingData[perdataid]['value']
      rxpost(config.api_product_inventory, params, {
        '1': (json) => { delete(editingData[perdataid]); delete(this.state.valueData[perdataid]); this.fetchData() }
      })
    }
  }

  onClickDataAdd(name, value, index) {
    let editingData = this.state.editingData
    if (!editingData[index]) {editingData[index] = {}}
    editingData[index][name] = (editingData[index][name]) ? !editingData[index][name] : value 
    let arrcheck = Object.keys(editingData[index])
    for (let key in arrcheck) {
      if (arrcheck[key].indexOf('check') !== -1 && arrcheck[key] !== name) { delete(editingData[index][arrcheck[key]]) }
    }  
    this.setState({ editingData: editingData }) 
  }

  onClickDataTrash(e, isdeleted) {
    let paging = this.state.paging
    paging['search_is_deleted'] = isdeleted
    this.setState({ paging: paging }, () => {
      this.fetchData()
    }) 
  }

  onBlurDatafilter(e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging
    paging['search_' + name] = e.target.value
    this.setState({ paging: paging })

    this.timerDatafilter = setTimeout((e, name) => {
      this.fetchData()
    }, WAIT_INTERVAL)    
  }

  onBlurDataKey(e, name, key) {
    let editingData = this.state.editingData
    let valueDatatmp = this.state.valueData
    if (!editingData[key]) {editingData[key] = {}}
    editingData[key][name] = e.target.value  
    valueDatatmp[key] = Number(editingData[key]['amount'])
    this.setState({ editingData: editingData, valueData: valueDatatmp })    
  }

  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  // Help show functions
  helpProductcat(cats) {   
    let result = ''
    if (cats) {
      for(let i = 0; i < cats.length; i++) { result += cats[i].name+ ', ' }
      return result
    }    
  }

  // Help show functions
  helpSortClass(stcol, extraclass) {
    extraclass = extraclass || []

    let result = ''    
    if (this.state.paging.st_col === stcol) {
      result = this.state.paging.st_type === 1 ? 'rx-sort-asc' : 'rx-sort-desc'
    }

    for(let i = 0; i < extraclass.length; i++) {      
      result += ' ' + extraclass[i]
    }

    return result 
  }

  helpShowOption(objOption) {
    let stroption = ''
    for (let key in objOption) {
      if (['key','data','amount'].indexOf(key) === -1) {
        if (objOption[key]['name'] && objOption[key]['value']) {
          stroption += objOption[key]['name'] +': '+ objOption[key]['value'] +' /'
        }
      } 
    }

    return stroption.slice(0, -1)
  }

  helpShowAmount(objOption, amountold, index) {
    let stramount = ''
    let editingData = this.state.editingData
    let numamount = (objOption && objOption.amount) ? Number(objOption.amount) : Number(amountold)
    let numsum = numamount
    stramount = numberWithCommas(numamount) + ''
    if (editingData[index] && editingData[index]['amount']) {
      let numset = Number(editingData[index]['amount'])
      if (editingData[index]['checkadd']) {
        numsum += numset
        stramount = numamount + ' -> '+ numsum
        editingData[index]['value'] = numsum
      } else if (editingData[index]['checkset']) {
        numsum = numset
        stramount = numamount + ' -> '+ numsum
        editingData[index]['value'] = numsum
      }
    }
    stramount = stramount.length>0 ? stramount : 0
    return stramount
  }

  render() {

    // Data tables
    let data = this.state.data.map((perdata, index) => (
      <tr key={index}>
        <td className='table-lowpadding'><img className='admin-table-product-img lazyload' alt='ico_default' data-src={config.base_api + '/upload/image/' + (perdata.img_landscape || 'ico_app_default.png')} /></td>
        <td>{perdata.name}<br></br><b>{this.helpShowOption(perdata.options)}</b></td>
        <td><span className='table-price'>{(perdata.options && perdata.options.data && perdata.options.data.price) ? numberWithCommas(perdata.options.data.price) : numberWithCommas(perdata.price)}</span></td>
        <td><span className='table-stock'>{this.helpShowAmount(perdata.options, perdata.stock, perdata._id + index)}</span></td>
        <td><small>{rxgetdate(perdata.created_at)}</small></td>  
        <td style={{width: '200px'}}>
          <div className='lk-block-table-function clearfix'>
            <div style={{fontSize: '12px'}} className={(this.state.editingData[perdata._id + index] && this.state.editingData[perdata._id + index]['checkadd']) ? 'lk-block-table-button-add lk-button-active' : 'lk-block-table-button-add'} onClick={(e) => this.onClickDataAdd('checkadd', true, perdata._id + index)}>Cộng thêm</div>
            <div style={{fontSize: '12px'}} className={(this.state.editingData[perdata._id + index] && this.state.editingData[perdata._id + index]['checkset']) ? 'lk-block-table-button-add lk-button-active' : 'lk-block-table-button-add'} onClick={(e) => this.onClickDataAdd('checkset', true, perdata._id + index)}>Thay đổi</div>
            <div style={{fontSize: '12px'}} className='lk-block-table-button-input'><input style={{fontSize: '12px', marginTop: '4px', height: '30px', paddingTop: '5px'}} type='number' value={this.state.valueData[perdata._id + index] || 0 } ref='resetNumber' className='fullwidth-input' onChange={(e) => this.onBlurDataKey(e, 'amount', perdata._id + index)}></input></div>
            <div style={{fontSize: '12px'}} className='lk-block-table-button-set' onClick={(e) => this.onClickDataUpdateSubmit(perdata._id, perdata._id + index, perdata.options.key)}>Lưu</div>
          </div>
        </td>
      </tr>))

    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Tồn kho</div><div className='description'>Quản lý tồn kho</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            
            <div className='col-lg-12 col-sm-12'>
              <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurDatafilter(e, 'name')} /></span>
              <div className='admin-table-pagination admin-pagin-right'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>

              <div className='admin-table-wrap'><table className='admin-table-product preset'>
                <thead><tr><th>Ảnh</th>
                  <th className={this.helpSortClass('name', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'name')} >Sản phẩm</th>
                  <th className={this.helpSortClass('price', ['rx-th-width-60 rx-th-alignright'])} onClick={(e) => this.onClickSort(e, 'price')}>Giá</th>
                  <th className={this.helpSortClass('stock', ['rx-th-width-60 rx-th-alignright'])} onClick={(e) => this.onClickSort(e, 'stock')}>Số lượng</th>
                  <th className={this.helpSortClass('created_at', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'created_at')}>Ngày</th>                  
                  <th style={{width: 100}}>Thao tác</th></tr></thead>
                <tbody>{data}</tbody>
              </table></div>
              <div className='admin-table-pagination'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
            </div>
          </div>
          <div className='clearfix-martop'></div>          
        </div>
      </div>
    )
  }
}

export default AdminInventory