import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { polyfill }  from 'es6-promise'
import { rxcurrencyVnd } from './../../classes/ulti'
import { rxget } from './../../classes/request'
import { withTranslation } from 'react-i18next'

const config = global.rxu.config

polyfill()

class AdminDashboard extends Component {
  constructor(props) {
    super(props)
    this.handleSelect = this.handleSelect.bind(this)
    this.mouseOverHandler = this.mouseOverHandler.bind(this)
    this.mouseOutHandler = this.mouseOutHandler.bind(this)
    this.mouseMoveHandler = this.mouseMoveHandler.bind(this)
    this.state = {   
      key: 1,
      rxsize:  this.props.rxsize  || 4,
      rxpagin: this.props.rxpagin || '',
      checktab: { today: true, yesterday: false, lastweek: false, thismonth: false, lastmonth: false },
      chart: { today: {},yesterday: {},lastweek: {}, thismonth: {}, lastmonth: {} },
      products: [],
      arrproducts: [],
      x: `${10}px`,
      y: `${10}px`,
      top: '',
      left: '',

      order: 0,
      order_wait: 0,
      order_ship: 0,
      order_payment: 0,

      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 }
    }
  }

  componentDidMount() {
    this._ismounted = true
    this.fetchData()
  }

  componentWillUnmount() {
    this._ismounted = false
  }

  fetchData() {
    rxget(config.api_dashboard, this.state.paging, {
      '1': (json) => { if (this._ismounted) { this.setState({chart: json.data.chart[0], products: json.data.products}, () => { this.initChar(json.data.chart[0]) }); this.countDiscountArr(this.state.chart.today) } }
    })
    rxget(config.api_order_all, this.state.paging, {
      '1': (json) => { if (this._ismounted) {
        let order_wait = 0
        let order_ship = 0
        let order_payment = 0
        for (let i = 0; i < json.data.length ; i++) {
          if (json.data[i].status_confirm === 0) {
            order_wait += 1
          } 
          if (json.data[i].status_ship === 0) {
            order_ship += 1
          }
          if (json.data[i].status_order_payment === 0) {
            order_payment += 1
          }
        }
        this.setState({ order: json.data.length, order_wait: order_wait, order_ship: order_ship, order_payment: order_payment }) 
      }}
    })
  }

  initChar (data) {
    let checktab = this.state.checktab
    if (checktab.today === true) {
      this.initCharBarDashboardChart(data.today)
    } else if (checktab.yesterday === true) {
      this.initCharBarDashboardChart(data.yesterday)
    } else if (checktab.lastweek === true) {
      this.initCharBarDashboardChart(data.lastweek)
    } else if (checktab.thismonth === true) {
      this.initCharBarDashboardChart(data.thismonth)
    } else if (checktab.lastmonth === true) {
      this.initCharBarDashboardChart(data.lastmonth)
    }
  }

  initCharBarDashboardChart (data) {
    let colorArray = ['rgba(56, 130, 244)', 'rgba(17, 196, 209)', 'rgba(248, 170, 243)', 'rgba(255, 159, 64)', 'rgba(242, 97, 118)']
    let labelstemp = ''
    let revstemp = []
    if (data && data.chart && data.chart.length > 0) {
      for (let i = 0; i < data.chart.length; i++) {
        if (data.chart[i] && typeof(data.chart[i].date) !== 'undefined' && typeof(data.chart[i].rev) !== 'undefined') {
          if (i === data.chart.length - 1){
            labelstemp += (this.convertDate(data.chart[i].date))
          } else {
            labelstemp += (this.convertDate(data.chart[i].date)) + ','
          }
          revstemp.push(Number(data.chart[i].rev))
        }
      }
    }
    let datachart = {}
    datachart['labels'] = labelstemp.split(',')
    datachart['revs'] = revstemp
    if (datachart && datachart.labels && datachart.revs) {
      if (typeof (this.CharBarDashBoard) !== 'undefined') { this.CharBarDashBoard.destroy() }
      let ctx = document.getElementById('Char_barDashBoard').getContext('2d')
      let configCharBarDashBoard = {
        type: 'bar',
        data: {
          labels: datachart.labels,
          datasets: [
            {
              label: this.props.t('Revenu'),
              backgroundColor: colorArray,
              data: datachart.revs
            }
          ]
        },
        options: {
          legend: { display: false },
          title: {
            display: false
          }
        }
      }
      this.CharBarDashBoard = new global.Chart(ctx, configCharBarDashBoard)
    }
  }

  renderDaterangeSelect () {
    return (
      <select className='chart__select' style={{ marginRight: '20px' }}>
        <option value='-1'> {this.props.t('All branches')}</option>
        <option value='0'> TP.HCM</option>
        <option value='1'> Hà Nội</option>
        <option value='2'> Đà Nẵng</option>
      </select>
    )
  }

  handleSelect(key) {
    this.setState({ key })
  }

  openCity(e, name) {
    let checktab = this.state.checktab
    checktab = {}
    checktab[name] = true
    this.setState({checktab: checktab}, () => { this.initChar(this.state.chart) })
  }

  convertDate(timestamp) {
    let u = new Date(timestamp * 1000)  
    let tempstr  =  ('0' + u.getUTCDate()).slice(-2) +'/'+ ('0' + (u.getUTCMonth() + 1)).slice(-2)
    return tempstr
  }

  mouseOverHandler(d, e) {
    this.setState({
      showToolTip: true,
      top: `${e.screenY + 150}px`,
      left: `${e.screenX - 250}px`,
      y: d.y,
      x: d.x});
  }

  mouseMoveHandler(d, e) {
    if (this.state.showToolTip) {
      this.setState({top: `${e.y + 150}px`, left: `${e.x - 250}px`});
    }
  }

  mouseOutHandler() {
    this.setState({showToolTip: false});
  }

  countDiscountArr(arr) {
    let arrresult = []
    if (arr && arr.arrproduct) {
      let arrproduct = arr.arrproduct
      let arrtmp = {}
      let arrsort = arrproduct.sort()
      let count = 1
      for (let i=0; i< arrsort.length; i++) {
        if (!arrtmp[arrsort[i]]) {arrtmp[arrsort[i]] = 1}
        if(arrsort[i] === arrsort[i+1]) 
        { 
          count++;
          arrtmp[arrsort[i]] = count
        }
        else {
          count = 1
        }
      }

      let keysSorted = Object.keys(arrtmp).sort(function(a,b){return arrtmp[b]-arrtmp[a]}).slice(0, 5)
      for (let i in keysSorted) {
        let obj = this.state.products.find(function (obj) { return obj._id === keysSorted[i]; });
        if (obj) {
          arrresult.push(obj)
        }
      }
    }
    this.setState({arrproducts: arrresult})
  }

  render() {
    const { t } = this.props;
    let tempSearchDetail = (<div></div>)
    let arrproducts = this.state.arrproducts
    if (arrproducts && arrproducts.constructor === Array) {
      tempSearchDetail = arrproducts.slice(0,5).map((perdata, key) => (      
        <div key={key} className='clearfix'>
          { perdata && 
          <div style={{position: 'relative'}}>
            <div className='cart-popup-product-detail clearfix'>
              <div className='clearfix' style={{borderBottom: 'solid 1px #eee', paddingBottom: '11px'}}>
                <Link to={`/product/${perdata.slug}`}>    
                  <img className='cart-popup-product-img-admin lazyload' alt='ico_default' data-src={config.base_api + '/upload/image/' + (perdata.img_landscape || 'ico_app_default.png')} />
                </Link>
                <div style={{float: 'right', width: '70%'}}>
                  <Link to={`/product/${perdata.slug}`}><div className='cart-popup-product-name-admin'>{perdata.name}</div></Link>
                  <div className='cart-popup-product-option-admin'>{t('Weight')}: {perdata.grams}</div>
                  <div className='cart-popup-product-option-admin'>{t('Price')}: {perdata.price}</div>
                </div>
              </div>                
            </div>            
          </div> }
        </div>
      )) 
    }

    let arrtab = [{id: 'today', value: t('Today')}, {id: 'yesterday', value: t('Yesterday')}, {id: 'lastweek', value: t('Lastweek')}, {id: 'thismonth', value: t('Thismonth')}, {id: 'lastmonth', value: t('Lastmonth')}]
    
    return (
      <div className='admin-cardblock'>        
        <div className='admin-cardblock-head'><div className='title'>{t('Dashboard')}</div><div className='description'>{t('Description')}</div></div>
        <div className='admin-cardblock-body'> 
          <div className='lk-blockchart-tab-head'>
            <div className='tabset'>
              <div className='tabset-head'> 
              { arrtab.map((perdata) => (
                <div key={perdata.id} style={{height:'70px', overflow: 'hidden'}} className={(this.state.checktab && this.state.checktab[perdata.id]) ? 'tablinks active' : 'tablinks'} onClick={(e) => {this.openCity(e, perdata.id); this.countDiscountArr(this.state.chart[perdata.id]) }}>
                  <div style={{height:'20px'}} className='clearfix'></div>
                  <div className='tab-panel-title'>{perdata.value}</div>
                  <div className='lk-dashboard-price'>{(this.state.chart[perdata.id] && this.state.chart[perdata.id]['rev']) ? rxcurrencyVnd(this.state.chart[perdata.id]['rev']) : 0} </div>
                  <div className='lk-dashboard-amount'>{(this.state.chart[perdata.id] && this.state.chart[perdata.id]['amount']) ? this.state.chart[perdata.id]['amount'] : 0} {t('Order')}</div>
                </div> ))
              }
              </div>
              <div className='tab-panels'>
                  <div className='clearfix'>
                    <div className='lk-dashboar-body-left'>
                      {/*<h4 className='chart__title'>Tổng doanh thu 
                        <select className='chart__select' style={{ marginRight: '20px' }}>
                          <option value='-1'> Tất cả chi nhánh</option>
                          <option value='0'> TP.HCM</option>
                          <option value='1'> Hà Nội</option>
                          <option value='2'> Đà Nẵng</option>
                        </select>
                      </h4>*/}
                      <div className='col-md-12'>
                        <div className='chart__item'>
                          <div className='chart__title'>{t('Total revenue')} {this.renderDaterangeSelect()} </div>
                          <div className='chart__content'>
                            <canvas id='Char_barDashBoard' width='550' height='450' />
                          </div>
                        </div>
                      </div>
                      {/*this.barChart(this.state.chart[perdata.id])*/}
                    </div>
                    <div className='lk-dashboar-body-right'>
                      <div className='col-md-12' >
                        <div className='chart__item' >
                          <div className='chart__title'>{t('Best - selling product')}</div>  
                          <div>
                            { arrproducts && arrproducts.length > 0 ?
                              <div>
                                <div>{tempSearchDetail}</div>
                              </div> : <div>
                                <div className='cart-popup-emptytext'>Không có sản phẩm bán ra trong phạm vi ngày này.</div>                            
                              </div>}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              
              </div>
              {this.state.showToolTip && 
                <div style={{top: this.state.top, left: this.state.left, position: 'absolute'}}>
                  <div className='lk-mouse-text'>
                    {rxcurrencyVnd(this.state.y)}
                  </div>
                </div>
              }
              <div className='row'>
                <div className='col-md-6'>
                  <div className='chart__item'>
                    <div className='chart__title'>
                      {t('Information of all orders')}                    
                      <select className='chart__select'>
                        <option value='-1'> {t('All branches')}</option>
                        <option value='0'> TP.HCM</option>
                        <option value='1'> Hà Nội</option>
                        <option value='2'> Đà Nẵng</option>
                      </select>
                    </div>
                    <div className='chart__content'>
                      <div className='row'>
                        <Link className='char__box' to='/admin/order'>
                          <div className='char__box__icon'><span className='char__box__rx__icon char__box__icon_bihuy'></span></div>
                          <p className='char__box__text'>{this.state.order} {t('All order')}</p>
                        </Link>
                        <Link className='char__box' to='/admin/order_wait'>
                          <div className='char__box__icon'>
                          <span className='char__box__rx__icon char__box__icon_choxacthuc'></span></div>
                          <p className='char__box__text'>{this.state.order_wait} {t('Order wait')}</p>
                        </Link>
                      </div>
                      <div className='row'>
                        <Link className='char__box' to='/admin/order_payment'>
                          <div className='char__box__icon'><span className='char__box__rx__icon char__box__icon_choxuly'></span></div>
                          <p className='char__box__text'>{this.state.order_payment} {t('Order payment')}</p>
                        </Link>
                        <Link className='char__box' to='/admin/order_ship'>
                          <div className='char__box__icon'><span className='char__box__rx__icon char__box__icon_danggiaohang'></span></div>
                          <p className='char__box__text'>{this.state.order_ship} {t('Order ship')}</p>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='col-md-6'>
                  <div className='chart__item'>
                    <div className='chart__title'>
                      {t('All warehouse information')}
                      <select className='chart__select'>
                        <option value='-1'> {t('All branches')}</option>
                        <option value='0'> TP.HCM</option>
                        <option value='1'> Hà Nội</option>
                        <option value='2'> Đà Nẵng</option>
                      </select>
                    </div>
                    <div className='chart__content'>
                      <div className='chart__content'>
                      <div className='row'>
                        <div className='char__box__ware'>
                        <div style={{color:'red'}} className='char__box__textware'>{t('Products below the norm')}<span style={{float:'right'}}>0</span></div>
                        <div className='char__box__textware'>{t('Inventory number')}<span style={{float:'right'}}>0</span></div>
                        <div className='char__box__textware'>{t('Inventory value')}<span style={{float:'right'}}>0</span></div>
                        </div>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation('translations')(AdminDashboard)