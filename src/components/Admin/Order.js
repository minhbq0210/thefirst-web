import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import { rxcurrencyVnd, rxgetdate, rxChangeAlias } from './../../classes/ulti'
import { withTranslation } from 'react-i18next'
import RxPrintOrder from './../Shares/RxPrintOrder'
import RxPrintLabel from './../Shares/RxPrintLabel'
import RxSelectTableProduct from './../Shares/RxSelectTableProduct'

polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config
const numberWithCommas = (x) => {
  return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","): '';
}

class AdminOrder extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [], 
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      editingData: {}, 
      totalpay:0,
      totalcards:0,
      discount:0,
      discount_bill: 0,
      discount_item: 0,
      display_print: false,
      display_print_label: false,
      editCustomer: false,
      logo: '',
      product: [],
      count: 0,
    }
    this.state.quantity = this.state.quantity || 0
    this.state.dataLocal = global.getDataLocal()
    this.state.dataDistrict = []
  }

  componentDidMount() {
    window.scrollTo(0, 0)
    this.fetchAlldata()
    this.timerDatafilter = null
  }

  fetchAlldata() {
    this.fetchData()
    this.fetchDataLogo()
    this.fetchDataProduct()
  }
  fetchDataLogo() {
    rxget(config.api_slide_logo, {}, { '1': (json) => { 
      if(json.data) { this.setState({logo: json.data}) }
    }})
  }
  fetchDataProduct() {
    rxget(config.api_product_getSale, {}, { '1': (json) => {
      if(json.data) { this.setState({product: json.data.product, count: json.data.totals})}
    }})
  }

  fetchData(strid) {
    let orderid = this.props.location.id
    let tempPaging = this.state.paging
    if (typeof(orderid) !== 'undefined') {
      strid = orderid
      tempPaging.searchid__id = orderid
    }      

    this.setState({paging: tempPaging},  () => {
      rxget(config.api_order, this.state.paging, {
        '1': (json) => { 
          this.setState({ 
            data: json.data.order,
            quantity: json.data.count 
          }) 
          if (strid) {
            let objorder = json.data.order.find(o => o._id === strid)
            this.onClickDataEdit({}, objorder)
          } 
        }
      })
    })
  }

  getDataProduct(perdata) {
    let paramsproduct = {'orderid': perdata._id}
    if (typeof(perdata._id) !== 'undefined') {
      rxget(config.api_order_optionproduct, paramsproduct, {
        '1': (json) => { 
          if (typeof(json.data) !== 'undefined' && json.data.length > 0) {
            var carttmp = perdata.detail.carts.map(x => Object.assign(x, json.data.find(y => y.product_id === x.data._id)))
            perdata.detail.carts = carttmp;
            this.setState({ editingData: perdata })
          }
        }
      })
    }
    return perdata
  }

  /////////////
  // ORDER //
  /////////////
  onClickSort(e, stcol) {
    let paging = this.state.paging                  
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ paging: paging }, () => { this.fetchData() })    
  }

  onClickDataEdit(e, perdataorder) {
    let perdata = this.getDataProduct(perdataorder)
    if(perdata.hasOwnProperty('detail') && typeof(perdata.detail) === 'string'){
      var arraycart = []      
      var objdetails = JSON.parse(perdata.detail)
      if (typeof(objdetails) === 'string') { objdetails = JSON.parse(objdetails)}
      
      var objhistory = (perdata.history_order) ? perdata.history_order : []
      for(var keycart in objdetails.carts){
        arraycart.push(objdetails.carts[keycart])
      }
      objdetails.carts = arraycart
      perdata.detail = objdetails
      perdata.history_order = objhistory
      perdata.street = perdata['address'][0].street ? perdata['address'][0].street : ''
      perdata.county = perdata['address'][0].county ? perdata['address'][0].county : ''
      perdata.city = perdata['address'][0].city ? perdata['address'][0].city : ''
      let dataDistricts = this.state.dataDistrict
      if(this.state.dataLocal && perdata.city) {
        let checkcity = this.state.dataLocal.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(perdata.city)) !== -1)
        if(checkcity && checkcity.length > 0 && checkcity.name !== perdata.city) {
          dataDistricts = checkcity[0].districts
          if(dataDistricts && perdata.county) {
            let checkcdistrict = Object.keys(dataDistricts).filter(key => rxChangeAlias(dataDistricts[key]).indexOf(rxChangeAlias(perdata.county)) !== -1)
            if(checkcdistrict) {
              perdata.county = dataDistricts[checkcdistrict]
            }
          }
          perdata.city = checkcity[0].name
        }
      }
      this.setState({ editingData: perdata, dataDistrict: dataDistricts })
    } else {
      let clone = JSON.parse(JSON.stringify(perdata))
      this.setState({ editingData: clone, editCustomer: false })
    }
  }

  onClickDataPrint(e, perdataorder, name) {
    if(name === 'invoice') { this.setState({ display_print: true, display_print_label: false }, (e) => window.print()) }
    if(name === 'label') { this.setState({ display_print_label: true, display_print: false }, (e) => window.print()) }
  }

  onClickDataDeleteNew(e, perdata) {    
    e.stopPropagation()
    let reason = prompt("Nhập lý do", "");
    if (reason) {
      perdata.reason_deleted = reason
      console.log(perdata.reason_deleted, 'reason')
      rxget(config.api_order_delete, perdata, {
        '1': (json) => { this.fetchData() }
      })
    }
    // let r = window.confirm('Bạn có chắc muốn xoá đơn hàng #'+perdata.order_code+'?')
    // if(r === true) {
    //   this.setState({checkPopUpReason: true})
    // }
    
  }

  onClickDataRestore(e, perdata) {
    e.stopPropagation()
    rxget(config.api_order_restore, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataUpdateSubmit(e, perdata) {
    let editingData = this.state.editingData
    if(typeof editingData.detail !== 'string') {editingData.detail = JSON.stringify(editingData.detail)}
    rxpost(config.api_order_edit, editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickOrderCheck(e,stredit) {
    // let tempEditingData = this.state.editingData
    // tempEditingData.checkship = false
    // this.setState({editingData: tempEditingData})

    // let editingData = {}
    let editingData = this.state.editingData
    editingData.checkship = false
    if (stredit === 'statusConfirm') {
      editingData['status_confirm'] = 1
      editingData.sentmail = true
    } else if (stredit === 'statusPayment' && this.state.editingData.status_confirm === 1) {
      editingData['status_order_payment'] = 1
    } else if (stredit === 'statusShip') {
      editingData['status_ship'] = 1
    } else if (stredit === 'statusRefund' && this.state.editingData.status_order_payment === 1) {
      editingData['status_order_payment'] = 2
      // editingData['amountrefund'] = numberWithCommas(this.helpCalMoneyRefund(this.state.editingData.detail, 50000))
      editingData['amountrefund'] = (this.helpCalMoneyRefund(this.state.editingData.detail, this.state.editingData.fee_ship) > this.state.editingData.totalpay) ? rxcurrencyVnd(this.state.editingData.totalpay) : rxcurrencyVnd(numberWithCommas(this.helpCalMoneyRefund(this.state.editingData.detail, this.state.editingData.fee_ship)))
      editingData['reasonrefund'] = this.state.editingData['reasonrefund'] || ''
    }
    
    editingData['_id'] = this.state.editingData._id
    editingData['updatestatus'] = stredit
    this.setState({editingData: editingData})
    if(typeof editingData.detail !== 'string') {editingData['detail'] = JSON.stringify(editingData['detail'])}
    // console.log(editingData)
    rxpost(config.api_order_edit, editingData, {
      '1': (json) => { 
        this.fetchData(json.data)
        this.onClickDataEdit({},this.state.editingData)
      }
    }) 
  }

  onClickOrderPopupRefund(e, perdata) {
    if (perdata.status_order_payment === 0){
      alert('Đơn hàng chưa được thanh toán!')
    } else {
      perdata['checkrefund'] = true
      this.onClickDataEdit(e, perdata)
    }
  }

  onClickOrderPopupRefundClose(e, perdata) {
    perdata['checkrefund'] = false
    this.onClickDataEdit(e, perdata) 
  }

  onClickOrderPopupShip(e, perdata) {
    if (perdata['checkship']) {
      perdata['checkship'] = !perdata['checkship']  
    } else {
      perdata['checkship'] = true
    }
    
    this.onClickDataEdit(e, perdata)  
  }

  onClickDataCreateSubmit(e, perdata) {
    rxpost(config.api_order, this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataTrash(e, isdeleted) {
    let paging = this.state.paging
    paging['search_is_deleted'] = isdeleted
    this.setState({ paging: paging }, () => {
      this.fetchData()
    }) 
  }

  onClickDataNew(e) {
    let timeStr = Date.now().toString()
    let clone = { name: 'Order_'+ timeStr.substr(timeStr.length - 5), desc: '', created_at: 1, is_deleted: 0, is_active: 1, price: 100000, stock: 100, app: '5a50b4ebee345a312dfd1aec', appdist: '5a50b4ebee345a312dfd1aec,5a50adbaee345a312dfd1aea' }
    this.setState({ editingData: clone })
  }

  onBlurDatafilter(e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging
    paging['search_' + name] = e.target.value
    this.setState({ paging: paging })

    this.timerDatafilter = setTimeout((e, name) => {
      this.fetchData()
    }, WAIT_INTERVAL)    
  }

  onBlurData(e, name, key = 'false') {
    let editingData = this.state.editingData
    if (key === 'false' ) {
      editingData[name] = e.target.value  
    } else {
      if (typeof(editingData[key]) === 'undefined') {
        editingData[key] = {}
      }
      editingData[key][name] = e.target.value  
    }
    this.setState({ editingData: editingData })    
  }

  onBlurDataCart(e, name, index) {
    if (this.state && this.state.editingData && this.state.editingData.detail && this.state.editingData.detail.carts) {
      let editingData = this.state.editingData
      let editingDataCart = editingData['detail']['carts']
      let amountrefurnd = 0
      let quantity = e.target.value
      if (!isNaN(e.target.value) && Number(quantity) > 0 && (Number(quantity) <= editingDataCart[index]['amount'])) {
        amountrefurnd = (Number(quantity) > editingDataCart[index]['amount']) ? editingDataCart[index]['amount'] : Number(quantity) < 0 ? 0 : Number(quantity)
      }
      editingDataCart[index]['amountrefurnd'] = amountrefurnd
      this.setState({ editingData: editingData })  
    }
  }

  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  // Callback upload
  callbackUpload(e) {
    this.onBlurData({target: {value: e.images}}, 'img_landscape')
  }

  callbackUploadDetail(e) {    
    this.onBlurData({target: {value: e.images}}, 'img_detail')
  }  

  // Help show functions
  helpSortClass(stcol, extraclass) {
    extraclass = extraclass || []

    let result = ''    
    if (this.state.paging.st_col === stcol) {
      result = this.state.paging.st_type === 1 ? 'rx-sort-asc' : 'rx-sort-desc'
    }

    for(let i = 0; i < extraclass.length; i++) {      
      result += ' ' + extraclass[i]
    }

    return result 
  }

  helpCalMoneyRefund(perdata, feeship) {
    let result = 0    
    if (perdata.carts) {
      for(let i = 0; i < perdata.carts.length; i++) {      
        let amountrefurnd = (perdata.carts[i]['amountrefurnd']) ? perdata.carts[i]['amountrefurnd'] : 0
        let price = perdata.carts[i].pricenew ? perdata.carts[i].pricenew : perdata.carts[i]['data']['price']
        result += price * amountrefurnd// * 16500
      }
      // result = (result !== 0 && feeship > 0) ? (result + feeship) : 0
    }
    return result 
  }

  onchangeDiscount(e, name) {
    let editingData = this.state.editingData
    let value = e.target.value
    value = value.toString().replace(/,/g,'')
    let discount =numberWithCommas(value)
    editingData['discount_bill'] = discount

    this.setState({editingData: editingData})
  }
  onClickApplyDiscount(e){
    let editingData = this.state.editingData
    let priceold = editingData['price']
    let pricenew = editingData['totalpay']
    let discount_bill =numberWithCommas(editingData['discount_bill'])
    if(discount_bill >= 0 && discount_bill <= 100){
      pricenew = priceold - (priceold*discount_bill) / 100
      editingData['totalpay'] = pricenew
      editingData['detail'] = JSON.stringify(editingData['detail'])
      rxpost(config.api_order_edit, this.state.editingData, {
        '1': (json) => { this.setState({editingData: editingData}) }
      })
      this.onClickDataEdit({},this.state.editingData)
    }
  }
  onchangeDiscountItem(e, id) {
    let value = e.target.value
    value = value.toString().replace(/,/g,'')
    let discount_item =numberWithCommas(value)
    let editingData = this.state.editingData
    let priceold = 0
    let price = 0
    for(let i in editingData['detail'].carts) {
      if(editingData['detail'].carts[i].id === id){
        if(discount_item >= 0 && discount_item <= editingData['detail'].carts[i].data.price){
          let carts = editingData['detail'].carts[i]
          priceold = carts.amount*carts.data.price
          carts.pricenew = priceold - discount_item
          carts.discount_item = value
          editingData['detail'].carts[i] = carts
        }
      }
      let priceItem = editingData['detail'].carts[i].data.price
      if(editingData['detail'].carts[i].pricenew >= 0){
        price += editingData['detail'].carts[i].pricenew
      }
      else{
        price += editingData['detail'].carts[i].amount*priceItem
      }
    }
    if(editingData.price_discount && editingData.price_discount > 0) {
      editingData['price'] = price - editingData.price_discount
    } else {
      editingData['price'] = price
    }
    this.setState({ editingData: editingData })
  }
  onClickCustomerEdit(e) {
    this.setState({ editCustomer: true })
  }
  onClickCustomerEditCancel(e) {
    this.setState({ editCustomer: false })
  }
  onClickCustomerUpdate(e) {
    let editingData = this.state.editingData
    if(editingData.street !== 'undefined' && editingData.county !== 'undefined' && editingData.city !== 'undefined') {
      if(editingData['address'] && editingData['address'].length>0){
        editingData['address'][0] = {street: editingData.street, county: editingData.county, city: editingData.city}
      }
    }
    editingData['detail'] = JSON.stringify(editingData['detail'])
    rxpost(config.api_order_edit, editingData, {
      '1': (json) => { 
        this.setState({ editingData: editingData, editCustomer: false})
        alert('Cập nhật thành công')
       }
    })
    this.onClickDataEdit({},this.state.editingData)
  }
  handleChangeCity(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    let dataDistricts = this.state.dataDistrict
    if(name === 'city') {
      let checkcity = this.state.dataLocal.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(e.target.value)) !== -1)
      if(checkcity && checkcity.length > 0 && checkcity.name !== this.state.editingData['city']) {
        dataDistricts = checkcity[0].districts
      }
    }
    this.setState({editingData: editingData, dataDistrict: dataDistricts})
  }
  onClickDelItem (e, perdata) {
    let editingData = this.state.editingData
    if(typeof editingData.detail === 'string') {editingData.detail = JSON.parse(editingData.detail)}
    let cartobj = editingData.detail.carts
    if(cartobj.length > 1 || this.state.editingData.status_confirm !== 1 ) {
      cartobj = cartobj.filter( el => el.id !== perdata.id )
      let arrcart = editingData.arrcart.filter(el => el !== perdata.id)
      editingData.detail.carts = cartobj
      editingData.arrcart = arrcart
      this.setState({ editingData: editingData }, (e) => this.helpCalMoneyOrder(cartobj))
    } else {
      alert('Không thể xoá sản phẩm, vui lòng kiểm tra lại')
    }
  }
  onBlurDataAddProduct(result, objcart, name) {
    let editingData = this.state.editingData
    if(typeof editingData.detail === 'string') {editingData.detail = JSON.parse(editingData.detail)}
    let cartobj = editingData.detail.carts
    let arrcart = editingData.arrcart
    if(result) {
      // cartobj = cartobj.filter( el => el.id !== objcart.id )
      let indexobj = cartobj.indexOf(objcart)
      let indexarr = arrcart.indexOf(objcart.id)
      // arrcart = editingData.arrcart.filter(el => el !== objcart.id)
      let product = {
        amount : 1,
        data : result,
        id : result._id,
        product_id : result._id,
        option : []
      }
      // cartobj.push(product)
      // arrcart.push(product.id)
      cartobj[indexobj] = product
      arrcart[indexarr] = product.id
      editingData.detail.carts = cartobj
      editingData.arrcart = arrcart
      this.setState({editingData: editingData}, (e) => this.helpCalMoneyOrder(cartobj))
    }
  }
  onBlurDataQuantity(e, objcart) {
    let editingData = this.state.editingData
    if(typeof editingData.detail === 'string') {editingData.detail = JSON.parse(editingData.detail)}
    let cartobj = editingData.detail.carts
    if(e.target.value > 0 && parseInt(e.target.value, 10)>0) {
      for(let i in cartobj) {
        if(cartobj[i].id === objcart.id){
          cartobj[i].amount = e.target.value
        }
      }
    }
    editingData.detail.carts = cartobj
    this.setState({editingData: editingData}, (e) => this.helpCalMoneyOrder(cartobj))
  }
  helpCalMoneyOrder(carts) {
    let editingData = this.state.editingData
    if(carts.length > 0){
      let price = 0
      for(let i = 0; i < carts.length; i++) {
        price += carts[i].pricenew ? Number(carts[i].pricenew) : (Number(carts[i].amount) * Number(carts[i].data.price))
      }
      editingData.price = price
      editingData.totalpay = price
    }
    this.setState({editingData: editingData})
  }

  render() {
    const { t } = this.props;
    //total page
    let totalpage = 0
    if(this.state.quantity && this.state.quantity > 0){
      totalpage = Math.ceil(this.state.quantity / this.state.paging.pg_size)
    }
    // this.state.discount_bill = this.state.editingData['discount_bill']
    // Data tables
    let data = this.state.data.map(perdata => (
      <tr key={global.rxu.get(perdata, ['_id'])}>
        <td className='lk-action-edit' onClick={(e) => this.onClickDataEdit(e, perdata)}>{global.rxu.get(perdata, ['order_code'])}</td>
        <td className='lk-table-date'>{rxgetdate(global.rxu.get(perdata, ['created_at']))}</td>
        <td className='lk-table-customer-name'>{global.rxu.get(perdata, ['name'])}</td>
        {(perdata.status_confirm !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>{t('noconfirm')}</span></td>}
        {(perdata.status_confirm === 1) && <td><span className='lk-status-box lk-status-confirm'>{t('Confirmed')}</span></td>}   
        {(perdata.status_ship !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>{t('noShip')}</span></td>}
        {(perdata.status_ship === 1) && <td><span className='lk-status-box lk-status-confirm'>{t('Shipped')}</span></td>}        
        {(perdata.status_order_payment === 0) && <td><span className='lk-status-box lk-status-noconfirm'>{t('waitpayment')}</span></td>}
        {(perdata.status_order_payment === 1) && <td><span className='lk-status-box lk-status-confirm'>{t('Paymented')}</span></td>}
        {(perdata.status_order_payment === 2) && <td><span className='lk-status-box lk-status-noconfirm'>{t('Refunded')}</span></td>}
        {(perdata.type_payment !== 2) && <td><span className='lk-status-box lk-status-confirm'>{t('Yes')}</span></td>}
        {(perdata.type_payment === 2) && <td><span className='lk-status-box lk-status-noconfirm'>{t('No')}</span></td>}      
        <td><span className='table-total-money'>{rxcurrencyVnd(global.rxu.get(perdata, ['price']))}</span></td>
        {this.state.paging.search_is_deleted === 1 ? <td>{global.rxu.get(perdata, ['reason_deleted'])}</td> : <td>{global.rxu.get(perdata, ['type_channel_name'])}</td>}
        <td>
          { this.state.paging.search_is_deleted !== 1 &&
            <div> {(perdata.is_active !== 0) && <span></span>/*<span className='admin-table-on'>On</span>*/}{(perdata.is_active === 0) && <span className='admin-table-off'>Off</span>}
                  {/*<span className='rx-action-edit' onClick={(e) => this.onClickDataPrint(e, perdata)}>Xem</span>*/}
                  <span className='rx-action-edit' onClick={(e) => this.onClickDataEdit(e, perdata)}>{t('Edit')}</span>
                  <span className='rx-action-delete' onClick={(e) => this.onClickDataDeleteNew(e, perdata)}>{t('Delete')}</span> </div> }
          { this.state.paging.search_is_deleted === 1 && 
            <div> <span className='rx-action-restore' onClick={(e) => this.onClickDataRestore(e, perdata)}>{t('Restore')}</span> </div>}
        </td>
      </tr>
    ))

    // Data info card in order
    let detailcards = []
    let detailcardsrefund = []
    let totalcards = 0
    if (this.state.editingData.detail && this.state.editingData.detail.carts) {
      detailcards = this.state.editingData.detail.carts.map((objcart, index) => (
        <div key={index} style={{marginTop:'20px'}} className='clearfix cart-page-product-row admin'>
          { (objcart && objcart.data) && 
          <div>
            <div className='lk-cart-order-row'>
              <img className='lk-card-order-img' alt='ico_default' src={config.base_api + '/upload/image/' + (global.rxu.get(objcart, ['data','img_landscape']) || 'ico_app_default.png')} />
              <div className='lk-card-order-lable'>
                <div className='lk-card-order-lable-name'>{global.rxu.get(objcart, ['data','name'])}</div>
                <div className='lk-card-order-lable-desc'>{t('Product ID')}: {global.rxu.get(objcart, ['data','code'])}</div>
              </div>
            </div>
            <div className='lk-cart-order-amount-price'>
              <div className='lk-cart-order-amount'>{numberWithCommas(global.rxu.get(objcart, ['data','price']))} x 
                {this.state.editingData.status_confirm !== 1 ? <input type='number' className='lk-card-order-quantity' value={global.rxu.get(objcart, ['amount'])} onChange={(e) => this.onBlurDataQuantity(e, objcart)}/>: <input type='number' className='lk-card-order-quantity' value={global.rxu.get(objcart, ['amount'])} disabled/>}
              </div>
              <div className='lk-cart-order-amount'>{numberWithCommas((objcart.amount*objcart.data.price).toFixed(2))} </div>
              <div className='lk-cart-order-amount'>
                {this.state.editingData.status_confirm !== 1 ? <input tabIndex='2' type='number' value={objcart.discount_item ? objcart.discount_item : 0 } onChange={(e) => this.onchangeDiscountItem(e, objcart.id)}/>:<input tabIndex='2' type='number' value={objcart.discount_item ? objcart.discount_item : 0 } disabled/>}
              </div>
              <div className='lk-cart-order-amount'>{objcart.pricenew >=0 ? objcart.pricenew.toFixed(2) : numberWithCommas((objcart.amount*objcart.data.price).toFixed(2))}</div>
            </div>
            {this.state.editingData.status_confirm !== 1 && <div className='lk-cart-out-of-stock'>
              <span className='lk-cart-remove' onClick={(e) => this.onClickDelItem(e, objcart)}>Delete</span>
              <div className='lk-cart-add-product'> 
                <RxSelectTableProduct options={this.state.product} results={objcart || {}} count={this.state.count} onChange={(results) => this.onBlurDataAddProduct(results, objcart, 'data')} />
              </div> 
            </div>}
            
            <span className='lk-cart-order-total'>{objcart.pricenew >=0 ? numberWithCommas((totalcards += objcart.pricenew).toFixed(2)) : numberWithCommas((totalcards += objcart.amount*objcart.data.price).toFixed(2))}</span>
            
          </div> }
        </div>
      ))

      detailcardsrefund = this.state.editingData.detail.carts.map((objcart, index) => (
        <tr key={index}>
          <td className='text-center width-300-px'><span>{global.rxu.get(objcart, ['data','name'])}</span></td>
          <td className='text-center'><span>{objcart.pricenew ? numberWithCommas(objcart.pricenew) : numberWithCommas(global.rxu.get(objcart, ['data','price']))} </span></td>
          <td className='text-center'>{global.rxu.get(objcart, ['amount'])}</td>
          <td className='text-center'>{objcart.amountrefurnd ? (Number(global.rxu.get(objcart, ['amountrefurnd'])) > global.rxu.get(objcart, ['amount'])) ? global.rxu.get(objcart, ['amount']) : global.rxu.get(objcart, ['amountrefurnd']) : 0}</td>
          <td className='text-center'><input tabIndex='2' type='number' name='amountrefurnd' value={objcart.amountrefurnd ? objcart.amountrefurnd : 0} onChange={(e) => this.onBlurDataCart(e, 'amountrefurnd', index)} className='fullwidth-input' /></td>
        </tr>
      ))
    }

    // Data info card in history
    let detailhistory = []
    if (this.state.editingData.history_order && this.state.editingData.history_order.constructor === Array ) {
      detailhistory = this.state.editingData.history_order.map((objorderhistory, indexhistory) => (
        <div key={indexhistory} className='orderblock-history-content'>
          <div className='orderblock-history-body'>Đơn hàng được đặt với hình thức {objorderhistory.text}</div>
          <div className='orderblock-history-time'>{rxgetdate(objorderhistory.time)}</div>
        </div>
      ))
    }

    let iconcheckbox = [
      <div className='bock-icon-check' key={0}>
        <svg height='1.5em' viewBox='0 0 24 24' className='bock-icon-check-svg'>
          <g className='lk-icon-check'>
              <path
                strokeDasharray={`72.7977 72.7977`}
                strokeDashoffset={`50.2409`}
                d='M20 6.7L9.3 17.3 4 12c0-4.4 3.6-8 8-8s8 3.6 8 8-3.6 8-8 8-8-3.6-8-8'
              />
          </g>
        </svg>
      </div> 
    ]
   
    return (
      <div>
        <div className='admin-cardblock'>
          <div className='admin-cardblock-head'><div className='title'>{t('All order')}</div><div className='description'>{t('Manage all orders')}</div></div>
          <div className='admin-cardblock-body'>
            <div className='row admin-cardblock-body-inner'>
              
              <div className='col-lg-12 col-sm-12'>
                <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder={t('Find')} onKeyUp={(e) => this.onBlurDatafilter(e, 'name')} /></span>
                { this.state.paging.search_is_deleted !== 1 && <i className='icon-trash rx-recycle' onClick={(e) => this.onClickDataTrash(e, 1)}></i> }              
                { this.state.paging.search_is_deleted === 1 && <span><i className='icon-list rx-recycle' onClick={(e) => this.onClickDataTrash(e, 0)}></i><span className='rx-total-deleted-order'>Total: {this.state.quantity}</span></span> }

                <div className='admin-table-pagination admin-pagin-right'>
                  {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                  <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                  {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
                  <div className='pagin-curr'>Total page: {totalpage}</div>
                </div>

                <div className='admin-table-wrap' style={{width: '100%'}}><table className='admin-table-product preset'>
                  <thead>
                    <tr>
                      <th className={this.helpSortClass('order_code', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'order_code')}>{t('Code')}</th>
                      <th className={this.helpSortClass('created_at', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'created_at')}>{t('Created')}</th>
                      <th className={this.helpSortClass('name', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'name')}>{t('customername')}</th>
                      <th className={this.helpSortClass('status_confirm', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'status_confirm')}>{t('Confirm')}</th>
                      <th className={this.helpSortClass('status_ship', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'status_ship')}>{t('Shipping')}</th>
                      <th className={this.helpSortClass('status_order_payment', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'status_order_payment')}>{t('Payment')}</th>                  
                      <th className={this.helpSortClass('type_payment', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'type_payment')}>COD</th>
                      <th className={this.helpSortClass('price', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'price')}>{t('Price')}</th>
                      {this.state.paging.search_is_deleted === 1 ? <th>{t('Reason for deletion')}</th> : <th>{t('Channel')}</th>}
                      <th style={{width: '100px'}}>{t('Action')}</th>
                    </tr>
                  </thead>
                  <tbody>{data}</tbody>
                </table></div>
                <div className='admin-table-pagination'>
                  {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                  <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                  {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
                </div>
              </div>
            </div>
            <div className='clearfix-martop'></div>          
          </div>

          { this.state.editingData.created_at && <div className='admin-cardblock-form'>
              <div className='admin-cardblock-formname'>{t('Edit order information')} <b>{this.state.editingData.order_code}</b>
                <div className='admin-cardblock-formbtns'>
                  <div className='admin-cardblock-btns'>
                    <a tabIndex='5' className='btn-cancel' onClick={(e) => this.onClickDataEdit(e, {})} onKeyPress={(e) => this.onClickDataEdit(e, {})}>{t('Back')}</a>
                    {this.state.editingData.created_at !== 1 && <a tabIndex='6' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>}
                    {this.state.editingData.created_at !== 1 && <a tabIndex='6' className='btn-edit' onClick={(e) => this.onClickDataPrint(e, this.state.editingData, 'invoice')} onKeyPress={(e) => this.onClickDataPrint(e, this.state.editingData, 'invoice')}>{t('Print the invoice')}</a>}
                    {this.state.editingData.created_at !== 1 && <a tabIndex='7' className='btn-edit' onClick={(e) => this.onClickDataPrint(e, this.state.editingData, 'label')} onKeyPress={(e) => this.onClickDataPrint(e, this.state.editingData, 'label')}>{t('print label')}</a>}
                  </div>
                </div>
              </div>
              <div className='admin-cardblock-formbody'><div className='clearfix'>
              
              <div className='row'>
                <div className='col-sm col-md-8'>
                  <div className='admin-orderblock-details-main'>
                    <div className='order-details-title'>
                      <div className='orderblock-customer-detail-title'>
                        <div className='orderblock-customer-detail-title-name'>
                          <span className='orderblock-customer-lable-name'>{t('Order details')}</span>
                        </div>
                        <div className='orderblock-customer-detail-title-channel'>{t('Channel')}: Web</div>
                      </div>
                      { (this.state.editingData.status_confirm * this.state.editingData.status_order_payment * this.state.editingData.status_ship ) === 1 && <div>{t('Status')}: {t('Accomplished')}</div> }              
                      { (this.state.editingData.status_confirm * this.state.editingData.status_order_payment * this.state.editingData.status_ship ) !== 1 && <div>{t('Status')}: {t('Unfinished')}</div> }     
                    </div>
                    <div className='border-line-main'></div>
                    <div className='order-details-header'>
                      <div className='after-discount'>Còn lại</div>
                      <div className='input-discount'>Giảm giá</div>
                      <div className='pre-discount'>Giá cũ</div>
                    </div>
                    <div className='order-details-content'>{detailcards}</div>
                    <div className='border-line-main'></div>
                    <div className='order-details-note'>
                      <div className='order-details-note-content'>
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('Subtotal')}:</div>
                          <div className='lk-quanty-order-right'>{rxcurrencyVnd(totalcards)} </div>
                        </div>
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('Discount on web')}:</div>
                          <div className='lk-quanty-order-right'>{rxcurrencyVnd(this.state.editingData.price_discount)}</div>
                        </div>
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('Shipping fee')}:</div>
                          <div className='lk-quanty-order-right'>{rxcurrencyVnd(this.state.editingData.fee_ship)}</div>
                        </div>
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('Total')}:</div>
                          <div className='lk-quanty-order-right'>{rxcurrencyVnd(this.state.editingData.price + this.state.editingData.fee_ship)}</div>
                        </div>
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('Discount for bill')}: </div>
                          <div className='lk-quanty-order-right'>
                            {this.state.editingData.status_confirm !== 1 && <span><input tabIndex='10' type='number' value={this.state.editingData['discount_bill'] ? this.state.editingData['discount_bill'] : 0} onChange={(e) => this.onchangeDiscount(e, 'discount_bill')} className='lk-quanty-order-right-input'/><span>%</span></span>}
                            {this.state.editingData.status_confirm === 1 && <span><input tabIndex='10' type='number' value={this.state.editingData['discount_bill'] ? this.state.editingData['discount_bill'] : 0} className='lk-quanty-order-right-input'disabled/><span>%</span></span>}
                          </div>
                        </div> 
                        <div className='lk-quanty-order-update-btn' onClick={(e) => this.onClickApplyDiscount(e)}>{t('Discount applies to both bill and item')}</div>
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('Amount payable')}:</div>
                          <div className='lk-quanty-order-right'>{ rxcurrencyVnd(Number(this.state.editingData.totalpay)+ Number(this.state.editingData.fee_ship))}</div>
                        </div> 
                        { this.state.editingData.status_order_payment === 2 &&
                          <div className='lk-quanty-order'>
                            <div className='lk-quanty-order-left'>{t('Amount refunded')} :</div>
                            <div className='lk-quanty-order-right'>{(this.helpCalMoneyRefund(this.state.editingData.detail, this.state.editingData.fee_ship) > this.state.editingData.totalpay) ? rxcurrencyVnd(this.state.editingData.totalpay) : rxcurrencyVnd(numberWithCommas(this.helpCalMoneyRefund(this.state.editingData.detail, this.state.editingData.fee_ship)))}</div>
                          </div> 
                        }           
                      </div>
                    </div>

                    <div className='border-line-main'></div>
                    <div className='order-details-function'>
                    
                      { this.state.editingData.status_confirm !== 1 && 
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('Order confirmation')}</div> 
                          <div className='lk-quanty-order-right'>
                            <div className='lk-order-button lk-button-order-right' onClick={(e) => this.onClickOrderCheck(e,'statusConfirm')} onKeyPress={(e) => this.onClickOrderCheck(e,'statusConfirm')}>{t('Confirm')}</div>
                          </div>
                        </div>
                      }  
                      { this.state.editingData.status_confirm === 1 && 
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('ORDER HAS BEEN VERIFIED')}</div>
                          {iconcheckbox}   
                        </div>
                      }  
                      <div className='border-line-main'></div>
                      { this.state.editingData.status_order_payment === 0 && 
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('PAYMENT WAIT FOR HANDLING')}</div>
                          <div className='lk-quanty-order-right'>
                            {this.state.editingData.status_confirm === 1 ?<div className='lk-order-button lk-button-order-left' onClick={(e) => this.onClickOrderCheck(e,'statusPayment')} onKeyPress={(e) => this.onClickOrderCheck(e,'statusPayment')}>{t('Payment confirmation')}</div>:
                            <div className='lk-order-button lk-button-order-left disable'>{t('Payment confirmation')}</div>}
                            <div className='lk-order-button lk-button-order-right lk-button-order-refund' onClick={(e) => this.onClickOrderPopupRefund(e, this.state.editingData)} onKeyPress={(e) => this.onClickOrderPopupRefund(e, this.state.editingData)}>{t('Refund')}</div>
                          </div>
                        </div>
                      }  
                      { this.state.editingData.status_order_payment === 1 && 
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('ORDER HAS BEEN PAYMENT')}</div>
                          <div className='lk-order-button lk-button-order-right lk-button-order-refund' onClick={(e) => this.onClickOrderPopupRefund(e, this.state.editingData)} onKeyPress={(e) => this.onClickOrderPopupRefund(e, this.state.editingData)}>{t('Refund')}</div>
                        </div>
                      } 
                      { this.state.editingData.status_order_payment === 2 && 
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('PAYMENT WAS REFUNDED')}</div>
                          {iconcheckbox}
                        </div>
                      } 
                      <div className='border-line-main'></div>
                      { this.state.editingData.status_ship !== 1 && 
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('DELIVERY')}</div>
                          {this.state.editingData.checkship !== true && <div className='lk-quanty-order-right'>{this.state.editingData.status_confirm === 1 ?<div className= 'lk-order-button lk-button-order-right' onClick={(e) => this.onClickOrderPopupShip(e, this.state.editingData)} onKeyPress={(e) => this.onClickOrderPopupShip(e,this.state.editingData)}>{t('DELIVERY')}</div>:<div className='lk-order-button lk-button-order-right disable' >{t('DELIVERY')}</div>}</div>}
                          {this.state.editingData.checkship === true && 
                            <div className='lk-quanty-order-right'>
                            <div className='lk-order-button lk-button-order-left' tabIndex='7' onClick={(e) => this.onClickOrderCheck(e, 'statusShip')} onKeyPress={(e) => this.onClickOrderCheck(e, 'statusShip')}>{t('Completed')}</div>
                            <div className='lk-order-button lk-button-order-right lk-button-order-refund' tabIndex='8' onClick={(e) => this.onClickOrderPopupShip(e,this.state.editingData)} onKeyPress={(e) => this.onClickOrderPopupShip(e,this.state.editingData)}>{t('Cancel')}</div>
                            </div>
                          } 
                        </div>
                      }  
                      {this.state.editingData.checkship === true && 
                            <div className='lk-blockorder-status-ship'>
                              <div>
                                <div className='orderblock-customer-detail-label'>{t('Address delivery')}:</div>
                                <div className='orderblock-customer-detail-content'>
                                  <div className='orderblock-customer-detail-label'>{t('Full name')}: {global.rxu.get(this.state.editingData, ['name'], '')} </div>
                                  <div className='orderblock-customer-detail-label'>{t('Phone number')}: {global.rxu.get(this.state.editingData, ['phone'], '')}</div>
                                  <div className='orderblock-customer-detail-label'>{t('Address')}: {global.rxu.get(this.state.editingData, ['address', 0, 'street'], '')}, {global.rxu.get(this.state.editingData, ['address', 0, 'county'], '')}, {global.rxu.get(this.state.editingData, ['address', 0, 'city'], '')}</div>
                                </div>
                              </div>

                              <div className='lk-box-option-ship'>
                                <div className='rx-input-group-two'>
                                  <div>
                                    <div className='orderblock-customer-detail-label'>{t('Get the goods from the warehouse')}:</div>
                                    <div className='lk-select-form' tabIndex='1'>
                                      <select onChange={(e) => this.onBlurData(e, 'pickuplocation', 'ship')}>
                                        <option value='Địa điểm mặc định'>Địa điểm mặc định</option>
                                        <option value='43/3A Lý Chính Thắng'>43/3A Lý Chính Thắng</option>
                                      </select>                            
                                    </div>
                                  </div>
                                  <div>
                                    <div className='orderblock-customer-detail-label'>{t('Weight total')} (kg):</div>
                                    <input tabIndex='2' type='text' name='totalweight' value={this.state.editingData.ship ? this.state.editingData.ship.totalweight : this.state.editingData.totalweight} onChange={(e) => this.onBlurData(e, 'totalweight', 'ship')} className='fullwidth-input' />
                                  </div>
                                </div>
                              </div>
                            
                              <div className='lk-box-option-ship'>
                                <div className='rx-input-group-two'>
                                  <div>
                                    <div className='orderblock-customer-detail-label'>{t('Delivery by')}:</div>
                                    <div className='lk-select-form' tabIndex='3'>
                                      <select onChange={(e) => this.onBlurData(e, 'transporters', 'ship')}>
                                        <option value='Viettle Post'>Viettle Post</option>
                                        <option value='Giao hàng nhanh'>Giao hàng nhanh</option>
                                      </select>                            
                                    </div>
                                  </div>
                                  <div>
                                    <div className='orderblock-customer-detail-label'>{t('Amount collected on behalf of')} (COD):</div>
                                    <input tabIndex='4' type='text' name='codamount' onChange={(e) => this.onBlurData(e, 'CODamount', 'ship')} className='fullwidth-input' />
                                  </div>
                                </div>
                              </div>

                              <div className='lk-box-option-ship'>
                                <div className='rx-input-group-two'>
                                  <div>
                                    <div className='orderblock-customer-detail-label'>{t('Note')}:</div>
                                    <input tabIndex='5' type='text' name='fieldnote' onChange={(e) => this.onBlurData(e, 'fieldnote', 'ship')} className='fullwidth-input' />
                                  </div>
                                  <div>
                                    <div className='orderblock-customer-detail-label'>{t('Waybill code')}:</div>
                                    <input tabIndex='6' type='text' name='trackingnumber' onChange={(e) => this.onBlurData(e, 'trackingnumber', 'ship')} className='fullwidth-input' />
                                  </div>
                                </div>
                              </div>
                            </div>
                          }
                      { this.state.editingData.status_ship === 1 && 
                        <div className='lk-quanty-order'>
                          <div className='lk-quanty-order-left'>{t('Delivered')}</div>
                          {iconcheckbox}     
                        </div>
                      }  
                    </div>
                    <div className='border-line-main'></div>
                    <div className='admin-orderblock-history-main'>
                      <div className='orderblock-history-title'>{t('History')}</div>
                      <div className='border-line-main'></div>
                      {detailhistory}
                    </div>
                  </div>
                </div>
                {this.state.editingData.checkrefund && 
                  <div className='lk-modal'>
                    <div className='lk-modal-content refund'>
                      <div className='lk-modal-header'>
                        <div className='lk-modal-title'>{t('Payment refunded')}</div>
                        <div className='lk-modal-close' onClick={(e) => this.onClickOrderPopupRefundClose(e, this.state.editingData)} onKeyPress={(e) => this.onClickOrderPopupRefundClose(e, this.state.editingData)}>x</div>
                      </div>

                      <div className='lk-modal-body'>
                        <div className='lk-order-refund'>
                          <div className='lk-table-order-refund table_scroll'>
                              <table>
                                  <thead>
                                      <tr>
                                        <th>{t('Product name')}</th>
                                        <th>{t('Price')}</th>
                                        <th>{t('Ordered')}</th>
                                        <th>{t('Quantity refunded')}</th>
                                        <th>{t('Quantity')}</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      {detailcardsrefund}
                                  </tbody>
                              </table>
                          </div>

                          <div className='lk-table-order-refund-totals'>
                            <div className='lk-refund-totals'>
                              <div className='lk-quanty-order'>
                                <div className='lk-quanty-order-left'>{t('Shipping fee')}:</div>
                                <div className='lk-quanty-order-right'>{rxcurrencyVnd(this.state.editingData.fee_ship)}</div>
                              </div>
                              <div className='lk-quanty-order'>
                                <div className='lk-quanty-order-left'>{t('The total amount is refundable')}:</div>
                                <div className='lk-quanty-order-right'>{(this.helpCalMoneyRefund(this.state.editingData.detail, this.state.editingData.fee_ship) > this.state.editingData.totalpay) ? rxcurrencyVnd(this.state.editingData.totalpay) : rxcurrencyVnd(numberWithCommas(this.helpCalMoneyRefund(this.state.editingData.detail, this.state.editingData.fee_ship)))}</div>
                              </div>
                            </div>
                          </div>

                          <div className='lk-reason-refund'>
                            <span className='lk-text-title-reason-refund'>{t('Reason for refund')}</span>
                            <div className='lk-content-reason-refund'>
                                <input tabIndex='3' type='text' name='reasonrefund' onChange={(e) => this.onBlurData(e, 'reasonrefund')} className='lk-text-reason-refund' />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className='lk-modal-footer clearfix'>
                        <div className='lk-pull-right'>
                          <div>
                            <span className='lk-button-cancel' onClick={(e) => this.onClickOrderPopupRefundClose(e, this.state.editingData)} onKeyPress={(e) => this.onClickOrderPopupRefundClose(e, this.state.editingData)}>{t('Cancel')}</span>
                            <span className='lk-button-refund' onClick={(e) => this.onClickOrderCheck(e, 'statusRefund')} onKeyPress={(e) => this.onClickOrderCheck(e, 'statusRefund')}>{t('Refund')} {(this.helpCalMoneyRefund(this.state.editingData.detail, this.state.editingData.fee_ship) > this.state.editingData.totalpay) ? rxcurrencyVnd(this.state.editingData.totalpay) : rxcurrencyVnd(numberWithCommas(this.helpCalMoneyRefund(this.state.editingData.detail, this.state.editingData.fee_ship)))}  </span>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>}
                <div className='col-sm col-md-4 admin-orderblock-customer-main'>
                  <div className='orderblock-customer-detail'> 
                    <div className='orderblock-customer-detail-title'>
                      <div className='orderblock-customer-detail-title-name'>
                        <span className='orderblock-customer-lable-name'>{t('Customer')}</span>
                      </div>
                      <div className='orderblock-customer-detail-title-channel'>
                        {this.state.editingData.status_confirm !== 1 ? this.state.editCustomer ? <div className='btn-cancel' onClick={(e) => this.onClickCustomerEditCancel(e)}>Cancel</div>
                          : <div className='btn-cancel' onClick={(e) => this.onClickCustomerEdit(e)}>Edit</div> : <div></div>}
                        {this.state.editCustomer && <div className='btn-edit' onClick={(e) => this.onClickCustomerUpdate(e)}>Update</div>}
                      </div>
                    </div>
                    <div className='orderblock-customer-detail-info'>
                      <div className='orderblock-customer-detail-name'>{this.state.editingData.name}</div>
                    </div>
                  </div>
                  <div className='border-line-main'></div>

                  {this.state.editingData.desc && <div><div className='orderblock-customer-address'>
                    <div className='orderblock-customer-detail-title'>
                      <div className='orderblock-customer-detail-title-name'>
                        <span className='orderblock-customer-lable-name'>{t('Note')}:</span>
                      </div>
                      <div className='orderblock-customer-detail-info'>
                        <span className='orderblock-customer-lable-name'>{global.rxu.get(this.state.editingData, ['desc'], '')}</span>
                      </div>
                    </div>
                  </div>
                  <div className='border-line-main'></div></div>}

                  <div className='orderblock-customer-address'>
                    <div className='orderblock-customer-detail-title'>
                      <div className='orderblock-customer-detail-title-name'>
                        <span className='orderblock-customer-lable-name'>{t('Address delivery')}</span>
                      </div>
                      {/*<div className='orderblock-customer-detail-title-channel'>Edit</div>*/}
                    </div>
                    <div className='orderblock-customer-detail-info'>
                      {this.state.editCustomer ? <div>
                        <div className='orderblock-customer-detail-text'>{t('Receiver')}: 
                          <input tabIndex='1' value={global.rxu.get(this.state.editingData, ['name'], '')} onChange={(e) => this.onBlurData(e, 'name')}/>
                        </div>
                        <div className='orderblock-customer-detail-text'>{t('Phone number')}: 
                        <input tabIndex='2' value={global.rxu.get(this.state.editingData, ['phone'], '')} onChange={(e) => this.onBlurData(e, 'phone')}/>
                        </div>
                        <div className='orderblock-customer-detail-text'>{t('Address')}: 
                        <input tabIndex='3' value={global.rxu.get(this.state.editingData, ['street'], '')} onChange={(e) => this.onBlurData(e, 'street')}/>
                        </div>
                        <div className='orderblock-customer-detail-text'>{t('District')}: 
                        <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['county'])} onChange={(e) => {this.handleChangeCity(e, 'county')}}>
                          {this.state.dataDistrict && Object.keys(this.state.dataDistrict).length>0 && Object.keys(this.state.dataDistrict).map(key => (<option key={key} value={this.state.dataDistrict[key]}>{this.state.dataDistrict[key]}</option>))}
                        </select>
                        </div>
                        <div className='orderblock-customer-detail-text'>{t('City')}: 
                        <select className='fullwidth-select' value={global.rxu.get(this.state.editingData, ['city'], '')} onChange={(e) => {this.handleChangeCity(e, 'city')}}>
                          {this.state.dataLocal && this.state.dataLocal.length > 0 && this.state.dataLocal.map((option, index) => (<option key={index} value={option.name}>{option.name}</option>))}
                        </select>
                        </div>
                      </div>:<div>
                        <div className='orderblock-customer-detail-text'>{t('Receiver')}: 
                          <span>{global.rxu.get(this.state.editingData, ['name'], '')}</span>
                        </div>
                        <div className='orderblock-customer-detail-text'>{t('Phone number')}: 
                          <span>{global.rxu.get(this.state.editingData, ['phone'], '')}</span>
                        </div>
                        <div className='orderblock-customer-detail-text'>{t('Address')}: 
                          <span>{global.rxu.get(this.state.editingData, ['street'], '')}, {global.rxu.get(this.state.editingData, ['county'], '')}, {global.rxu.get(this.state.editingData, ['city'], '')}</span>
                        </div>
                      </div>}
                    </div>
                  </div>
                  <div></div>
                </div>
              </div>  
              </div></div>
            </div> }
        </div>
        { this.state.editingData.created_at && this.state.display_print === true && <RxPrintOrder editingData={this.state.editingData} totalcards={totalcards} logo={this.state.logo}></RxPrintOrder>}
        { this.state.editingData.created_at && this.state.display_print_label === true && <RxPrintLabel editingData={this.state.editingData} totalcards={totalcards} logo={this.state.logo}></RxPrintLabel>}
      </div>
    )
  }
}

export default withTranslation('translations')(AdminOrder)