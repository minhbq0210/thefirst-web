import React, { Component } from 'react'
import { Switch } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { rxsetCookie, rxgetLocal, rxJson, rxget, rxsetLocal } from './../../classes/request'
import { withTranslation } from 'react-i18next'
import Parser from 'html-react-parser'
// import { rxsetCookie, rxsetLocal, rxgetLocal, rxJson } from './../../classes/request'

let adminCSS = '<style>.fb-customerchat, .fb_dialog { display: none !important }.rx-main-banner-inner, .lk-page-brand, .nav-wrap, .mobile-nav-block-lang { display: none; } .main-container { padding: 0px; margin: 0px; width: 100%; } .lk-page-custome-register.container { display: none; } .mainhead-container { display: none; } .mainfooter-container { display: none; } .rxmainnav-toggle { display: none !important; } .lk-box-find-category { display: none !important }  .container--shadow { display: none !important; }</style>'

const config = global.rxu.config

function checkpermission(strcheck) {
    let strper = rxgetLocal('arrper', '')
    let arrper = []
    if (strper && strper.length > 0 && strper.indexOf(',') !== -1) {
      strper = strper.replace(/"/g, '')
      arrper = strper.split(',')
      if (arrper[0].indexOf('[') !== -1) {
        arrper[0] = arrper[0].slice(1)
        let a = arrper.length - 1
        if (arrper[a].indexOf(']') !== -1) {
          let result = arrper[a].substring(0, arrper[a].length - 1)
          arrper[a] = result
        }
      }
    }
    if (arrper.length !== 0) {
      if (arrper.indexOf(strcheck) !== -1) {
        return true
      } else {
        return false
      }
    } else {
      return true
    }
}

class Admin_ extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userInf: {},
            roleobj: this.props.auth && this.props.auth.user && this.props.auth.user.roleobj ? this.props.auth.user.roleobj : [],
            usrrole: '',
            clickOrder: false,
            clickCate: false,
            clickProduct: false,
            clickPage: false,
            clickSetting: false,
            id_user: this.props.auth && this.props.auth.user && this.props.auth.user._id ? this.props.auth.user._id : ''
        }
        this.roles = {
          'super': ['supperadmin', 'admin'],
          'admin': ['supperadmin'],
          'dashboard': ['supperadmin', 'admin', 'sale', 'manager'],
          'sale':  ['supperadmin', 'admin','sale', 'manager'],
          'store': ['supperadmin', 'admin']
        }
        this.state.logo = ''
    }

    componentDidMount () {
        this.checkAuth()
        this.userInf()
        this.fetchDataLogo()
    }
    componentWillReceiveProps(nextProps) {
        // console.log(nextProps, 'nextProps')
        if(this.props.auth && this.props.auth.user && this.props.auth.user._id && this.props.auth.user._id !== this.state.id_user) {
            this.setState({roleobj: this.props.auth.user.roleobj})
        }
    }
    fetchDataLogo() {
        rxget(config.api_slide_logo, {}, { '1': (json) => { 
            if(json.data) { this.setState({logo: json.data}) }
        }})
    }

    checkAuth() {
        if (typeof(document) !== 'undefined') {
            let match = document.cookie.match(new RegExp('authorize=([^;]+)'));
            // console.log(match, 'manager')
            if (!match && this.props.history.location.pathname !== '/admin/login') {
                // console.log('-----------------')
                this.props.history.push('/admin/login')
            }
        }
    }

    userInf () {
        let usrInfo = this.state.usrInfo
        let strper = rxgetLocal('user', {})
        strper = rxJson(strper, { data: '' })
        strper ? usrInfo = strper : usrInfo = {}
        let usrrole = this.state.usrrole
        usrrole = global.rxu.get(usrInfo, ['roleobj', 0, 'name'], '')
        this.setState({ userInf: usrInfo, usrrole: usrrole })
      }

    onClickChange(name) {
        if (name === 'clickOrder') {
            this.setState({
                clickOrder: !this.state.clickOrder
            })
        } else if (name === 'clickCate') {
            this.setState({
                clickCate: !this.state.clickCate
            })
        } else if (name === 'clickProduct') {
            this.setState({
                clickProduct: !this.state.clickProduct
            })
        } else if (name === 'clickPage') {
            this.setState({
                clickPage: !this.state.clickPage
            })
        } else {
            this.setState({
                clickSetting: !this.state.clickSetting
            })
        }
    }

    getNavItemClassName(adminPathName) {
        const pathName = this.props.history.location.pathname.split('/')
        if (pathName.indexOf('admin') !== -1 && pathName.indexOf(adminPathName) !== -1) {
            return 'admin-mainnav-icon'
        } else {
            return ''
        }
    }

    getNavItemMainClassName(adminMainPathName) {
        const pathName = this.props.history.location.pathname.split('/')
        if (pathName.indexOf('admin') !== -1) {
            if (adminMainPathName === 'admin') {
                const pathNameStr = this.props.history.location.pathname
                if ( pathNameStr === '/admin') {
                    return 'admin-mainnav-active'
                }
            } else if (adminMainPathName === 'order') {
                if (pathName.indexOf('order') !== -1 || pathName.indexOf('order_wait') !== -1 || pathName.indexOf('order_ship') !== -1 || pathName.indexOf('order_payment') !== -1) {
                    return 'admin-mainnav-active'
                }
            } else if (adminMainPathName === 'category') {
                if (pathName.indexOf('category-product') !== -1 || pathName.indexOf('category-brand') !== -1) {
                    return 'admin-mainnav-active'
                }
            } else if (adminMainPathName === 'product') {
                if (pathName.indexOf('product') !== -1 || pathName.indexOf('inventory') !== -1) {
                    return 'admin-mainnav-active'
                }
            } else if (adminMainPathName === 'customer') {
                if (pathName.indexOf('customer') !== -1) {
                    return 'admin-mainnav-active'
                }
            } else if (adminMainPathName === 'discount') {
                if (pathName.indexOf('discount') !== -1) {
                    return 'admin-mainnav-active'
                }
            } else if (adminMainPathName === 'Page') {
                if (pathName.indexOf('content') !== -1 || pathName.indexOf('page') !== -1 || pathName.indexOf('settingfilter') !== -1 || pathName.indexOf('slide') !== -1) {
                    return 'admin-mainnav-active'
                }
            } else if (adminMainPathName === 'config') {
                if (pathName.indexOf('config') !== -1 || pathName.indexOf('question') !== -1 || pathName.indexOf('user') !== -1 || pathName.indexOf('user_role') !== -1 || pathName.indexOf('permission') !== -1) {
                    return 'admin-mainnav-active'
                }
            }
            // return 'admin-mainnav-active'
        } else {
            return ''
        }
    }
    changeLanguage(e, lng){
        if(e && e.target.value) {
            lng = e.target.value
        }
        this.props.i18n.changeLanguage(lng);
        window.location.reload(false)
    }

    logout () {
        rxsetCookie('authorize', '', 10)
        let newState = {}
        rxsetLocal('arrper', JSON.stringify(newState))
        this.props.history.push('/admin/login')
    }

    render() {
        const { t } = this.props
        const language =this.props.i18n.language
        // let usrrole = ''
        // try {
        //   usrrole = this.state.usrrole.toLowerCase()
        // } catch (e) {
        //   console.log(e)
        // }
        let url = this.state.logo['img_landscape'] ? (config.base_api + '/upload/image/' + this.state.logo['img_landscape']) : 'http://www.annawholesale.com.au/images/static/LOGO-ANNA-WHOLSALE.jpg'
        return (
            <div>
                {Parser(adminCSS)}
                <div className='admin-topnav'>
                    <Link to='/'><div className='mainhead-logo' style={{ backgroundImage: 'url(' + url +' )'}}></div></Link>
                    <div className="form-controlLanguage">
                        <div className={language === 'vi' ? "form-controlLanguage vn active" : "form-controlLanguage vn"} onClick={() => this.changeLanguage(null, 'vn')}></div>
                        <div className={language === 'en' ? "form-controlLanguage eng active" : "form-controlLanguage eng"} onClick={() => this.changeLanguage(null, 'en')}></div>
                    </div>
                </div>
                <div className='admin-mainnav'>
                    <div className='admin-mainnav-inner'>
                        {checkpermission('dashboard') && <div className='admin-mainnav-items'><div className={this.getNavItemMainClassName('admin')}><p className='admin-mainnav-title' style={{ cursor: 'pointer' }}><Link to='/admin'><i className='icon-speedometer nav-icon'></i>{t('Dashboard')}<i className='icon-speedometer-right icon-arrow-right'></i></Link></p></div></div>}
                        { checkpermission('order') &&
                            <div className='admin-mainnav-items'>
                                <div className={this.getNavItemMainClassName('order')}><p className='admin-mainnav-title' style={{ cursor: 'pointer' }} onClick={ () => { this.onClickChange('clickOrder') } }><i className='icon-docs nav-icon'></i>{t('Order')}<i className={ this.state.clickOrder ? 'icon-speedometer-right icon-arrow-up' : 'icon-speedometer-right icon-arrow-right'}></i></p></div>
                                { 
                                    this.state.clickOrder && <div>
                                        <i className={this.getNavItemClassName('order')}></i><Link className='admin-mainnav-name' to='/admin/order'>{t('All order')}</Link>
                                        <i className={this.getNavItemClassName('order_wait')}></i><Link className='admin-mainnav-name' to='/admin/order_wait'>{t('Order wait')}</Link>
                                        <i className={this.getNavItemClassName('order_ship')}></i><Link className='admin-mainnav-name' to='/admin/order_ship'>{t('Order ship')}</Link>
                                        <i className={this.getNavItemClassName('order_payment')}></i><Link className='admin-mainnav-name' to='/admin/order_payment'>{t('Order payment')}</Link>
                                    </div>
                                }
                            </div> 
                        }
                        { checkpermission('productcate') &&
                            <div className='admin-mainnav-items'>
                                <div className={this.getNavItemMainClassName('category')}><p className='admin-mainnav-title' style={{ cursor: 'pointer' }} onClick={ () => { this.onClickChange('clickCate') } }><i className='icon-wallet nav-icon'></i>{t('Category')}<i className={ this.state.clickCate ? 'icon-speedometer-right icon-arrow-up' : 'icon-speedometer-right icon-arrow-right'}></i></p></div>
                                {
                                    this.state.clickCate && <div>
                                        <i className={this.getNavItemClassName('category-product')}></i><Link className='admin-mainnav-name' to='/admin/category-product'>{t('Categories')} {t('favorite')}</Link>
                                        <i className={this.getNavItemClassName('category-brand')}></i><Link className='admin-mainnav-name' to='/admin/category-brand'>{t('Brand')} {t('favorite')}</Link>
                                        <i className={this.getNavItemClassName('best-collection')}></i><Link className='admin-mainnav-name' to='/admin/best-collection'>{t('Best collection')}</Link>
                                    </div>
                                }
                            </div>
                        }
                        {/*<div className='admin-mainnav-items'><Link to='/admin/order'><i className='icon-notebook nav-icon'></i>Chưa hoàn tất</Link></div>*/}
                        {/*<div className='admin-mainnav-items'>Vận chuyển</div>
                        <div className='admin-mainnav-name'><Link to='/admin/order'><i className='icon-paper-plane nav-icon'></i>Tổng quan</Link></div>
                        <div className='admin-mainnav-name'><Link to='/admin/order'><i className='icon-paper-plane nav-icon'></i>Vận chuyển</Link></div>
                        <div className='admin-mainnav-name'><Link to='/admin/order'><i className='icon-paper-plane nav-icon'></i>Thu hộ</Link></div>
                        <div className='admin-mainnav-name'><Link to='/admin/order'><i className='icon-paper-plane nav-icon'></i>Cấu hình</Link></div>*/}
                        { checkpermission('product') && this.state.roleobj && this.state.roleobj.findIndex(o => o === 'GETinventory') !== -1 && 
                            <div className='admin-mainnav-items'>
                                <div className={this.getNavItemMainClassName('product')}><p className='admin-mainnav-title' style={{ cursor: 'pointer' }} onClick={ () => { this.onClickChange('clickProduct') } }><i className='icon-tag nav-icon'></i>{t('Product')}<i className={ this.state.clickProduct ? 'icon-speedometer-right icon-arrow-up' : 'icon-speedometer-right icon-arrow-right'}></i></p></div>
                                {
                                    this.state.clickProduct && <div>
                                        <i className={this.getNavItemClassName('product')}></i><Link className='admin-mainnav-name' to='/admin/product'>{t('Product')}</Link>
                                        <i className={this.getNavItemClassName('inventory')}></i><Link className='admin-mainnav-name' to='/admin/inventory'>{t('Inventory')}</Link>
                                    </div>
                                }
                            </div>
                        }
                        {/*<div className='admin-mainnav-items'><Link to='/admin/product_attribute'><i className='icon-present nav-icon'></i>Nhóm sản phẩm</Link></div>
                        <div className='admin-mainnav-items'><Link to='/admin/product_option'><i className='icon-present nav-icon'></i>Tồn kho</Link></div>*/}
                        { checkpermission('customer') &&
                            <div className='admin-mainnav-items'>
                                <div className={this.getNavItemMainClassName('customer')}><p className='admin-mainnav-title' style={{ cursor: 'pointer' }}><i className='icon-people nav-icon'></i><Link to='/admin/customer'>{t('Customer')}<i className='icon-speedometer-right icon-arrow-right'></i></Link></p></div>
                            </div>
                        }
                        { checkpermission('discount') &&
                            <div className='admin-mainnav-items'>
                                <div className={this.getNavItemMainClassName('discount')}><p className='admin-mainnav-title' style={{ cursor: 'pointer' }}><i className='icon-present nav-icon'></i><Link to='/admin/discount'>{t('Discount')}<i className='icon-speedometer-right icon-arrow-right'></i></Link></p></div>
                            </div>
                        }
                        {/*<div className='admin-mainnav-items'>
                            <a className='admin-mainnav-title'>Báo cáo</a>
                            <div className='admin-mainnav-name'><Link to='/admin/report'><i className='icon-graph nav-icon'></i>Báo cáo</Link></div>
                        </div>*/}
                        { (checkpermission('content') || checkpermission('slide')) &&
                            <div className='admin-mainnav-items'>
                                <div className={this.getNavItemMainClassName('Page')}><p className='admin-mainnav-title' style={{ cursor: 'pointer' }} onClick={ () => { this.onClickChange('clickPage') } }><i className='icon-layers nav-icon'></i>{t('Website')}<i className={ this.state.clickPage ? 'icon-speedometer-right icon-arrow-up' : 'icon-speedometer-right icon-arrow-right'}></i></p></div>
                                {
                                    this.state.clickPage && 
                                    <div>
                                        {checkpermission('content') && <div><i className={this.getNavItemClassName('content')}></i><Link className='admin-mainnav-name' to='/admin/content'>{t('Content')}</Link></div> }
                                        {/* checkpermission('content') && <div><i className={this.getNavItemClassName('page')}></i><Link className='admin-mainnav-name' to='/admin/page'>{t('Page content')}</Link></div> }
                                        <div className='admin-mainnav-name'><Link to='/admin/navigation'>Điều hướng</Link></div>
                                        <div className='admin-mainnav-name'><Link to='/admin/navigation'>Giao diện</Link></div>
                                        <div><i className={this.getNavItemClassName('settingfilter')}></i><Link className='admin-mainnav-name' to='/admin/settingfilter'>{t('Setting filter')}</Link></div>*/}
                                        { checkpermission('slide') && <div><i className={this.getNavItemClassName('slide')}></i><Link className='admin-mainnav-name' to='/admin/slide'>Slide | video | banner</Link></div> }
                                        { checkpermission('advertisement') && <div><i className={this.getNavItemClassName('advertisement')}></i><Link className='admin-mainnav-name' to='/admin/advertisement'>Advertisement</Link></div> }
                                    </div>
                                }
                            </div>
                        }
                        { (checkpermission('config') || checkpermission('user') || checkpermission('role') || checkpermission('permission')) && 
                            <div className='admin-mainnav-items'>
                                <div className={this.getNavItemMainClassName('config')}><p className='admin-mainnav-title' style={{ cursor: 'pointer' }} onClick={ () => { this.onClickChange('clickSetting') } }><i className='icon-settings nav-icon'></i>{t('Setting')}<i className={ this.state.clickSetting ? 'icon-speedometer-right icon-arrow-up' : 'icon-speedometer-right icon-arrow-right'}></i></p></div>
                                {
                                    this.state.clickSetting && 
                                    <div>
                                        {checkpermission('config') && <div><i className={this.getNavItemClassName('config')}></i><Link className='admin-mainnav-name' to='/admin/config'>{t('Config')}</Link></div>}
                                        {/*<div><i className={this.getNavItemClassName('question')}></i><Link className='admin-mainnav-name' to='/admin/question'>{t('Question')}</Link></div>*/}
                                        {checkpermission('user') && <div><i className={this.getNavItemClassName('user')}></i><Link className='admin-mainnav-name' to='/admin/user'>{t('User')}</Link></div> }
                                        {checkpermission('role') && <div><i className={this.getNavItemClassName('user_role')}></i><Link className='admin-mainnav-name' to='/admin/user_role'>{t('Role')}</Link></div> }
                                        {checkpermission('permission') && <div><i className={this.getNavItemClassName('permission')}></i><Link className='admin-mainnav-name' to='/admin/permission'>{t('Permission')}</Link></div> }
                                    </div>
                                }
                            </div>
                        }
                        <div className='admin-mainnav-items admin-mainnav-title' style={{ cursor: 'pointer', padding: '.4rem 0.2rem', paddingLeft: '2.5rem' }} onClick={(e) => this.logout()}><i className='icon-power nav-icon'></i>{t('Logout')}</div>
                        <div className='admin-mainnav-end'></div>
                    </div>
                </div>

                <div className='admin-container'>
                    <div className='admin-container-inner'>
                        <Switch>{renderRoutes(this.props.route.routes)}</Switch>
                    </div>
                </div>        
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
  auth: state.auth
})

const mapDispatchToProps = { }

const Admin = connect(
  mapStateToProps,
  mapDispatchToProps
)(Admin_)

export default withTranslation('translations')(Admin)
