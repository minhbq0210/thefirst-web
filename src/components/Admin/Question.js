import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'

import CKEditor from 'react-ckeditor-component'

import { connect } from 'react-redux'
import { rxnavToggle, rxnavClose } from './../../redux'

polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config

class AdminQuestion_ extends Component {
  constructor(props) {
    super(props)
    this.onChangeContentCKE = this.onChangeContentCKE.bind(this)
    this.state = {
      data: [],
      dataMeta: [],
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10, search_type: 1 },
      editMeta: false,
      editingData: {metadata: {question: '', answer: ''}}
    }

    this.state.products = this.props.products || []
  }

  componentDidMount() {
    this.fetchAlldata()
    this.timerDatafilter = null
  }

  fetchAlldata() {
    this.fetchData()
  }

  fetchData() {
    rxget(config.api_setting, this.state.paging, {
      '1': (json) => { 
        if (json.data && json.data[0] && json.data[0].meta) {this.setState({ dataMeta: json.data[0].meta })}
      }
    })
  }

  // /////////////
  // // PRODUCT //
  // /////////////
  onClickDataEdit(e, perdata) {
    let clone = JSON.parse(JSON.stringify(perdata))
    if (clone && typeof(clone.id) !== 'undefined') {
      this.setState({ editingData: clone, editMeta: true})
    } else {
      this.setState({ editingData: {}, editMeta: false})
    }
  }

  onClickDataDelete(e, perdata) {
    e.stopPropagation()
    perdata['type'] = 1
    rxget(config.api_setting_delete, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataUpdateSubmit(e, perdata) {
    let objparams = {}
    objparams['type'] = '1'
    objparams['meta'] = this.state.editingData
    rxpost(config.api_setting_edit, objparams, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataCreateSubmit(e, perdata) {
    let objparams = {}
    let timeStr = Date.now().toString()
    objparams['meta'] = {id: timeStr.substr(timeStr.length - 12), metadata: this.state.editingData.metadata}
    objparams['type'] = '1'
    objparams['name'] = 'questionandanswer'
    objparams['desc'] = 'Frequently asked questions'
    rxpost(config.api_setting, objparams, {
      '1': (json) => { 
        this.fetchData() 
      }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataNew(e) {
    let clone = {metadata: { question: '', desc: '', answer: ''}}
    this.setState({ editingData: clone, editMeta: true })
  }

  onBlurDatafilter(e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging
    paging['search_' + name] = e.target.value
    this.setState({ paging: paging })

    this.timerDatafilter = setTimeout((e, name) => {
      this.fetchData()
    }, WAIT_INTERVAL)    
  }

  onChangeContentCKE(evt) {
    let editingData = this.state.editingData
    let newContent = evt.editor.getData()    
    editingData['metadata']['answer'] = newContent    
    this.setState({ editingData: editingData })
  } 

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData['metadata'][name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  render() {   
    let data = []
    if (this.state.dataMeta.constructor === Array) {
      data = this.state.dataMeta.map((perdata,index) => (
        <tr key={index}>
          <td>{perdata.metadata.question}</td>
          <td>{perdata.metadata.desc}</td>
          <td><div>
            <span className='rx-action-edit' onClick={(e) => this.onClickDataEdit(e, perdata)}>Sửa</span>
            <span className='rx-action-delete' onClick={(e) => this.onClickDataDelete(e, perdata)}>Xoá</span>
          </div></td>
        </tr>
      ))
    }

    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Hỏi đáp</div><div className='description'>Quản lý hỏi đáp trên trang</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            <div className='col-lg-12 col-sm-12'>
              <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurDatafilter(e, 'name')} /></span>
              <i className='icon-plus rx-recycle' onClick={(e) => this.onClickDataNew(e)}></i>
            
              <div className='admin-table-pagination admin-pagin-right'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>

              <div className='admin-table-wrap'><table className='admin-table-product preset'>
                <thead><tr>
                  <th className='rx-th-width-220'>Câu hỏi</th>                  
                  <th className='rx-th-width-220'>Mô tả</th>                  
                  <th className='rx-th-width-100'>Thao tác</th></tr></thead>
                <tbody>{data}</tbody>
              </table></div>
              <div className='admin-table-pagination'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
            </div>
          </div>
          <div className='clearfix-martop'></div>          
        </div>

        { this.state.editMeta && <div className='admin-cardblock-form'>
          <div className='admin-cardblock-formname'>Thông tin hỏi đáp<div className='admin-cardblock-formclose' onClick={(e) => this.onClickDataEdit(e, {})}>x</div></div>
          <div className='admin-cardblock-formbody'><div className='col-sm-8 clearfix'>
            <div className='fullwidth-label'>Tên nội dung</div><input tabIndex='1' type='text' defaultValue={this.state.editingData.metadata.question || ''} onChange={(e) => this.onBlurData(e, 'question')} className='fullwidth-input' />
            <div className='fullwidth-label'>Mô tả</div><input tabIndex='2' type='text' defaultValue={this.state.editingData.metadata.desc || ''} onChange={(e) => this.onBlurData(e, 'desc')} className='fullwidth-input' />
            
            <div className='fullwidth-label'>Nội dung</div>
            <CKEditor activeClass='p10' content={this.state.editingData.metadata.answer} events={{ 'change': this.onChangeContentCKE }} />
            
            <div className='admin-cardblock-btns'>
              <a tabIndex='10' className='btn-cancel' onClick={(e) => this.onClickDataEdit(e, {})} onKeyPress={(e) => this.onClickDataEdit(e, {})}>Trở về</a>
              {this.state.editingData.id && <a tabIndex='11' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>}
              {!this.state.editingData.id && <a tabIndex='12' className='btn-clone' onClick={(e) => this.onClickDataCreateSubmit(e)} onKeyPress={(e) => this.onClickDataCreateSubmit(e)}>Tạo mới</a>}
            </div>
          </div></div>
        </div> }          
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  rxnav: state.rxnav
})

const mapDispatchToProps = {  
  rxnavToggle,
  rxnavClose
}

const AdminQuestion = connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminQuestion_)

export default AdminQuestion