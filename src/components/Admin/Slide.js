import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'

import RxUpload from './../Shares/RxUpload'
import RxCrud from './../Shares/RxCrud'

polyfill()
const config = global.rxu.config
class AdminSlide extends Component {  

  constructor(props) {
    super(props)    
    this.state = {          
      flagUpdateCat: false, curCat: 0
    }

    this.onClickCatItem = this.onClickCatItem.bind(this)
    this.runUpdateCat = this.runUpdateCat.bind(this)
  }

  componentDidMount() {
  }
  
  onClickCatItem(e) {
    this.setState({flagUpdateCat: !this.state.flagUpdateCat, curCat: e._id})
  }

  runUpdateCat(e) {
    let paging = (this.state.curCat)? { searchid_appdist: this.state.curCat, st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 } : { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 }
    e.inthis.setState({paging: paging}, () => { e.inthis.fetchData() })    
  }
  
  // R E N D E R S
  renderHead(inthis) {
    return (<tr>
      <th>Ảnh</th>
      <th className={inthis.helpSortClass('name')} onClick={(e) => inthis.onClickSort(e, 'name')}>Tên</th>  
      <th className={inthis.helpSortClass('desc')}>Mô tả</th>                 
      <th className={inthis.helpSortClass('url')}>Đường dẫn Url</th> 
      <th className={inthis.helpSortClass('type')}>Phân loại</th>                 
      <th>Thao tác</th>
    </tr>)
  }

  renderBody(inthis) {
    return inthis.state.data.map(perdata => (<tr key={perdata._id}>
      <td className='table-lowpadding'><img className='admin-table-product-img lazyload' alt={perdata.name} data-src={config.base_api + '/upload/image/' + (perdata.img_landscape || 'ico_app_default.png')} /></td>
      <td>{perdata.name}</td> 
      <td>{perdata.desc}</td>        
      <td>{perdata.url}</td> 
      {perdata.type === '0' && <td>Banner</td>}
      {perdata.type === '1' && <td>Slider</td>}
      {perdata.type === '2' && <td>Brand</td>}
      {perdata.type === '3' && <td>SliderMobile</td>}
      {perdata.type === '4' && <td>Logo</td>}
      {perdata.type === '5' && <td>Banner Top</td>}
      {perdata.type === '6' && <td>Video</td>}
      <td>
        { inthis.state.paging.search_is_deleted !== 1 &&
          <div>{(perdata.is_active !== 0) && <span className='admin-table-on'>On</span>}{(perdata.is_active === 0) && <span className='admin-table-off'>Off</span>}                  
                <span className='rx-action-edit' onClick={(e) => inthis.onClickDataEdit(e, perdata)}>Sửa</span>
                <span className='rx-action-delete' onClick={(e) => inthis.onClickDataDelete(e, perdata)}>Xoá</span> </div> }
        { inthis.state.paging.search_is_deleted === 1 && 
          <div> <span className='rx-action-restore' onClick={(e) => inthis.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}
      </td>
    </tr>))
  }

  render() {        
    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Nội dung</div><div className='description'>Quản lý nội dung trên trang</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            <div className='col-lg-12 col-md-12 col-sm-12'>
              <RxCrud renderHead={this.renderHead} flagUpdate={this.state.flagUpdateCat} parentUpdate={this.runUpdateCat} renderBody={this.renderBody} api={'api_slide'} form={
              [{name: 'Hình ảnh', func: (inthis) => (<RxUpload callback={(e) => inthis.callbackUpload(e)} images={inthis.state.editingData.img_landscape} nameImage='Slider'/>)},
              {name: 'Tên', func: (inthis) => (<input tabIndex='1' type='text' value={inthis.state.editingData.name} onChange={(e) => inthis.onBlurData(e, 'name')} className='fullwidth-input' />)},
              {name: 'Mô tả', func: (inthis) => (<input tabIndex='1' type='text' value={inthis.state.editingData.desc} onChange={(e) => inthis.onBlurData(e, 'desc')} className='fullwidth-input' />)},
              {name: 'Phân loại', func: (inthis) => (<select className='selectbox-admin' defaultValue={inthis.state.editingData.type ? inthis.state.editingData.type : '0'} onChange={(e) => inthis.onBlurData(e, 'type')}>
                        <option value='0'>Banner</option>
                        <option value='1'>Slide</option>
                        {/*<option value='2'>Brand</option>*/}
                        <option value='3'>Slider Mobile</option>
                        {/*<option value='4'>Logo</option>*/}
                        {/*<option value='5'>Banner Top</option>*/}
                        <option value='6'>Video</option>
                      </select>)},
              {name: 'Url', func: (inthis) => (<input tabIndex='2' type='text' value={inthis.state.editingData.url} onChange={(e) => inthis.onBlurData(e, 'url')} className='fullwidth-input' />)},
              ]} />
            </div>                      
          </div>
          <div className='clearfix-martop'></div>          
        </div>    
      </div>
    )
  }
}

export default AdminSlide