import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import { rxgetdate } from './../../classes/ulti'

polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config

class AdminPermission extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [], cate: [], 
      curobj: {cate: 0}, 
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      pagingcat: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      editingData: {} 
    }
  }

  componentDidMount() {
    this.fetchAlldata()
    this.timerDatafilter = null
  }

  fetchAlldata() {
    this.fetchData()
  }

  fetchData() {    
    rxget(config.api_permission, this.state.paging, {
      '1': (json) => { 
        this.setState({ data: json.data }) 
      }
    })
  }

  ////////////////
  // Permission //
  ////////////////
  onClickSort(e, stcol) {
    let paging = this.state.paging                  
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ paging: paging }, () => { this.fetchData() })    
  }

  onClickDataEdit(e, perdata) {
    if(perdata.hasOwnProperty('detail') && typeof(perdata.detail) === "string"){
      var arraycart = []
      var objdetails = JSON.parse(perdata.detail)
      var objhistory = JSON.parse(perdata.permission_history)
      for(var keycart in objdetails.carts){
        arraycart.push(objdetails.carts[keycart])
      }
      objdetails.carts = arraycart
      perdata.detail = objdetails
      perdata.permission_history = objhistory
    }
    let clone = JSON.parse(JSON.stringify(perdata))
    this.setState({ editingData: clone })
  }

  onClickPermissionNew(e, perdata) {
    e.stopPropagation()
    rxget(config.api_permission_genpermission, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataDeleteNew(e, perdata) {    
    e.stopPropagation()
    rxget(config.api_permission_delete, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataRestore(e, perdata) {
    e.stopPropagation()
    rxget(config.api_permission_restore, perdata, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataUpdateSubmit(e, perdata) {
    rxpost(config.api_permission_edit, this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickPermissionCheck(e,stredit) {
    let editingData = {}
    if (stredit === 'statusConfirm') {
      editingData['status_confirm'] = 1
    } else if (stredit === 'statusPayment') {
      editingData['status_permission_payment'] = 1
    } else if (stredit === 'statusShip') {
      editingData['status_ship'] = 1
    }
    
    editingData['permission_history'] = JSON.stringify(this.state.editingData.permission_history)
    editingData['_id'] = this.state.editingData._id
    editingData['updatestatus'] = stredit

    rxpost(config.api_permission_edit, editingData, {
      '1': (json) => { 
        this.fetchData(json.data)
      }
    })   
  }

  onClickDataTrash(e, isdeleted) {
    let paging = this.state.paging
    paging['search_is_deleted'] = isdeleted
    this.setState({ paging: paging }, () => {
      this.fetchData()
    }) 
  }

  onClickDataNew(e) {
    let timeStr = Date.now().toString()
    let clone = { name: 'Permission_'+ timeStr.substr(timeStr.length - 5), desc: '', created_at: 1, is_deleted: 0, is_active: 1}
    this.setState({ editingData: clone })
  }

  onBlurDatafilter(e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging
    paging['search_' + name] = e.target.value
    this.setState({ paging: paging })

    this.timerDatafilter = setTimeout((e, name) => {
      this.fetchData()
    }, WAIT_INTERVAL)    
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  helpSortClass(stcol, extraclass) {
    extraclass = extraclass || []

    let result = ''    
    if (this.state.paging.st_col === stcol) {
      result = this.state.paging.st_type === 1 ? 'rx-sort-asc' : 'rx-sort-desc'
    }
    for(let i = 0; i < extraclass.length; i++) {      
      result += ' ' + extraclass[i]
    }

    return result 
  }

  render() {
    // Data tables
    let data = this.state.data.map(perdata => (
      <tr key={perdata._id}>
        <td>{perdata.name}</td>
        <td>{perdata.desc}</td>
        <td>{perdata.id}</td>
        <td>{perdata.controller}</td>
        <td>{perdata.action}</td>
        <td>{perdata.method}</td>
        <td>{rxgetdate(perdata.created_at)}</td>                  
        <td>
          { this.state.paging.search_is_deleted !== 1 &&
            <div> {(perdata.is_active !== 0) && <span className='admin-table-on'>On</span>}{(perdata.is_active === 0) && <span className='admin-table-off'>Off</span>}
                  <span className='rx-action-edit' onClick={(e) => this.onClickDataEdit(e, perdata)}>Sửa</span>
                  <span className='rx-action-delete' onClick={(e) => this.onClickDataDeleteNew(e, perdata)}>Xoá</span> </div> }
          { this.state.paging.search_is_deleted === 1 && 
            <div> <span className='rx-action-restore' onClick={(e) => this.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}
        </td>
      </tr>
    ))

    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Quyền hạn</div><div className='description'>Quản lý quyền</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>
            
            <div className='col-lg-12 col-sm-12'>
              <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurDatafilter(e, 'name')} /></span>
              <i className='icon-plus rx-recycle' onClick={(e) => this.onClickPermissionNew(e)}>GenPermission</i>
              { this.state.paging.search_is_deleted !== 1 && <i className='icon-trash rx-recycle' onClick={(e) => this.onClickDataTrash(e, 1)}></i> }              
              { this.state.paging.search_is_deleted === 1 && <i className='icon-list rx-recycle' onClick={(e) => this.onClickDataTrash(e, 0)}></i> }

              <div className='admin-table-pagination admin-pagin-right'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>

              <div className='admin-table-wrap'><table className='admin-table-product preset'>
                <thead>
                  <tr>
                    <th className={this.helpSortClass('name', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'name')}>Tên quyền</th>
                    <th className={this.helpSortClass('desc', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'desc')}>Mô tả</th>
                    <th className={this.helpSortClass('id', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'id')}>Mã</th>
                    <th className={this.helpSortClass('controller', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'controller')}>Lớp quyền</th>
                    <th className={this.helpSortClass('action', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'action')}>Hành động</th>
                    <th className={this.helpSortClass('method', ['rx-th-width-100'])} onClick={(e) => this.onClickSort(e, 'method')}>Phương thức</th>
                    <th className={this.helpSortClass('created_at', ['rx-th-width-220'])} onClick={(e) => this.onClickSort(e, 'created_at')}>Ngày tạo</th>                  
                    <th>Thao tác</th>
                  </tr>
                </thead>
                <tbody>{data}</tbody>
              </table></div>
              <div className='admin-table-pagination'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
            </div>
          </div>
          <div className='clearfix-martop'></div>          
        </div>

        { this.state.editingData.created_at && <div className='admin-cardblock-form'>
            <div className='admin-cardblock-formname'>Sửa quyền <b>{this.state.editingData.name}</b><div className='admin-cardblock-formclose' onClick={(e) => this.onClickDataEdit(e, {})}>x</div></div>
            <div className='admin-cardblock-formbody'><div className='col-sm-5 clearfix'>
              <div className='fullwidth-label'>Tên quyền</div><input tabIndex='1' type='text' name='name' value={this.state.editingData.name || ''} onChange={(e) => this.onBlurData(e, 'name')} className='fullwidth-input' />
              <div className='fullwidth-label'>Mô tả</div><input tabIndex='3' type='text' name='desc' value={this.state.editingData.desc || ''} onChange={(e) => this.onBlurData(e, 'desc')} className='fullwidth-input' />
              
              <div className='admin-cardblock-btns'>
                <a tabIndex='2' className='btn-cancel' onClick={(e) => this.onClickDataEdit(e, {})} onKeyPress={(e) => this.onClickDataEdit(e, {})}>Trở về</a>
                {this.state.editingData.created_at !== 1 && <a tabIndex='3' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>}
              </div>
            </div></div>
          </div> 
        }

      </div>
    )
  }
}

export default AdminPermission