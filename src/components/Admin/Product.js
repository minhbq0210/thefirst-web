import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import { rxgetdate } from './../../classes/ulti'
import CKEditor from 'react-ckeditor-component'
import RxExpandable from './../Shares/RxExpandable'
import RxSelectbox from './../Shares/RxSelectbox'
import RxBrandbox from './../Shares/RxBrandbox'
import RxToggle from './../Shares/RxToggle'
import RxUpload from './../Shares/RxUpload'
import { WithContext as ReactTags } from 'react-tag-input'


polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config
const numberWithCommas = (x) => {
  return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","): '';
}

class AdminProduct extends Component {
  constructor(props) {
    super(props)
    this.onChangeContentCKE = this.onChangeContentCKE.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleAddition = this.handleAddition.bind(this)
    this.handleDrag = this.handleDrag.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeOption = this.handleChangeOption.bind(this)


    this.state = { 
      data: [], cate: [], brand:[], tag: [], cateall: [], brandall:[], option: [], arrcate: [],
      allcate: {_id: 0, name: 'Tất cả'}, 
      curobj: {cate: 0}, 
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      pagingcat: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      pagingbrand: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      editingCate: {},
      editingBrand: {},
      editingData: {},
      arrEditingData: [],
      arrimg: {},
      a: {},
      tags: [],
      suggestions: [],
      is_variant: false,
      optionname: 'title',
      selectOption: [],
      optionvalue: [],
      addVariant: [],
      checkinvent: false,
      checkship: false,
      checkpromo: false,
      checkparent: false,
      percentDiscount: 0, 
      dataProduct: {},
    }
  }

  componentDidMount() {
    this.fetchAlldata()
    this.timerCatefilter = null
    this.timerDatafilter = null
  }

  fetchAlldata() {
    this.fetchAllCate()
    this.fetchAllBrand()
    this.fetchData()
    // this.fetchOption()
    this.setState({ allcate: {_id: 0, name: 'Tất cả'} })
  }
  fetchAllCate() {
    rxget(config.api_category_option,{ pg_size: 1000 }, {
      '1': (json) => { this.setState({ cateall: this.sortName(json.data.cate) }) }
    })
  }

  fetchAllBrand() {
    rxget(config.api_category_brand,{ pg_size: 1000 }, {
      '1': (json) => { this.setState({ brandall: this.sortName(json.data.brand) }) }
    })
  }

  fetchData() {
    rxget(config.api_product, this.state.paging, {
      '1': (json) => { this.setState({ data: json.data }) }
    })
  }

  handleDelete(i) {
    let tags = this.state.editingData.tags
    tags.splice(i, 1)
    this.setState({tags: tags})
  }

  handleAddition(tag) {
    let tags = this.state.editingData.tags
    tags.push({
        id: tags.length + 1 || 0,
        text: tag
    })
    this.setState({tags: tags})
  }

  handleDrag(tag, currPos, newPos) {
    let tags = this.state.editingData.tags

    // mutate array
    tags.splice(currPos, 1)
    tags.splice(newPos, 0, tag)

    // re-render
    this.setState({ tags: tags })
  }

  /////////////
  // PRODUCT //
  /////////////
  onClickSort(e, stcol) {
    let paging = this.state.paging                  
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ paging: paging }, () => { this.fetchData() })    
  }

  onClickData(e, perdata) {
  }
  onBlurDataValue(value, name) {
    let editingData = this.state.editingData
    editingData[name] = value 
    this.setState({editingData: editingData})
  }

  onClickDataEdit(e, perdata) {
    let clone = JSON.parse(JSON.stringify(perdata))
    this.setState({ editingData: clone })
  }

  onClickDataDeleteNew(e, perdata) {  
    let params = {_id: perdata._id}
    e.stopPropagation()
    rxget(config.api_product_delete, params, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataRestore(e, perdata) {
    let perparams = {_id: perdata._id}
    e.stopPropagation()
    rxget(config.api_product_restore, perparams, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataUpdateSubmit(e, perdata) {
    rxpost(config.api_product_edit, this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataCreateSubmit(e, perdata) {
    rxpost(config.api_product, this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataTrash(e, isdeleted) {
    let paging = this.state.paging
    paging['search_is_deleted'] = isdeleted
    this.setState({ paging: paging }, () => {
      this.fetchData()
    }) 
  }

  onClickDataNew(e) {
    let timeStr = Date.now().toString()
    // let tempOption = {'key': 0, 'amount': 0,'price': 0, 'data': []}
    let clone = { name: 'Product_'+ timeStr.substr(timeStr.length - 5), desc: '', created_at: 1, is_deleted: 0, is_active: 1,is_hot: 0,tags: [], price: 0,price_discount: 0, stock: 0, app: '', appdist: '', options: [], code: '', barcode: '', grams: 0, seotitle: '', seometa: '', slug: '', vendor: '', content: '', brandlist: '' }
    this.setState({ editingData: clone })
  }

  onBlurDatafilter(e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging
    paging['search_' + name] = e.target.value
    this.setState({ paging: paging })

    this.timerDatafilter = setTimeout((e, name) => {
      this.fetchData()
    }, WAIT_INTERVAL)    
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    let strvalue = e.target.value
    if (name.indexOf('price') !== -1) {
      editingData[name] = strvalue.toString().replace(/(,)/g,'')
    } else {
      editingData[name] = e.target.value
    }
    this.setState({ editingData: editingData })  
  }

  toggleOne(e, check) {
    if (check === 'checkinvent') {
      let checked = this.state.checkinvent
      checked = !checked
      this.setState({ checkinvent: checked })
    } else if (check === 'checkship') {
      let checked = this.state.checkship
      checked = !checked
      this.setState({ checkship: checked })
    } else {
      let checked = this.state.checkpromo
      checked = !checked
      this.setState({ checkpromo: checked })
    }
  }

  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onChangeContentCKE(evt) {
    let editingData = this.state.editingData
    let newContent = evt.editor.getData()  
    editingData.content = newContent
    this.setState({ editingData: editingData })
  } 

  // Callback upload
  callbackUpload(e) {
    this.onBlurData({target: {value: e.images}}, 'img_landscape')
  }

  // Callback upload
  callbackUploadDetail(e) {    
    this.onBlurData({target: {value: e.images}}, 'image_list')
  }  

  // Help show functions
  helpProductcat(cats) { 
    let result = ''
    if (cats) {
      for(let i = 0; i < cats.length; i++) { 
        if(cats[i].is_brand === 0){ result += cats[i].name+ ', ' }
      }
      return result
    }    
  }
  
  helpSortClass(stcol, extraclass) {
    extraclass = extraclass || []

    let result = ''    
    if (this.state.paging.st_col === stcol) {
      result = this.state.paging.st_type === 1 ? 'rx-sort-asc' : 'rx-sort-desc'
    }

    for(let i = 0; i < extraclass.length; i++) {      
      result += ' ' + extraclass[i]
    }

    return result 
  }

  onClickDataOption(e) {
    let editingData = this.state.editingData
    let options = []    
    if (editingData['options'] && editingData['options'].length !== 0) { 
      options['key'] = editingData['options'].length + 1
      editingData['options'].push(options)
    } else {
      editingData['options'] = []
      editingData['options'].push(options)  
    }
    this.setState({editingData : editingData})
  }

  onClickDataOptionDelete(e,key) {
    e.stopPropagation()
    let editingData = this.state.editingData
    editingData['options'].pop(editingData.options[key])
    this.setState({ editingData: editingData })
  }

  onBlurDataOption(e, name, key) {
    let editingData = this.state.editingData
    if (name === 'amount') { editingData['options'][key]['amount'] = e.target.value }
    else {
      editingData['options'][key][name]['value'] = e.target.value
      editingData['options'][key][name]['text'] = e.target.value
    }
    this.setState({ editingData: editingData }) 
  }

  addAttributes(e) {
    let editingData = this.state.editingData
    if (editingData.attributes && editingData.attributes.length !== 0 ){
      editingData.attributes.push({'name': '', 'value': ''})
    } else {
      editingData.attributes = []
      editingData.attributes.push({'name': '', 'value': ''})
    }
    this.setState({ editingData: editingData })
  }

  onBlurDataAttribute(e, name, key) {
    let editingData = this.state.editingData
    if (editingData.attributes && editingData.attributes.length !== 0) {
      editingData.attributes[key][name] = e.target.value
    }
    this.setState({ editingData: editingData })
  }

  deleteAttributes(e, val) {
    let editingData = this.state.editingData
    if (editingData.attributes && editingData.attributes.length !== 0 ){
      editingData.attributes.splice(editingData.attributes.indexOf(val), 1)
    } 
    this.setState({ editingData: editingData })
  }

  addNewOption(e) {
    let editingData = this.state.editingData
    let temarr = {}
    temarr['key'] = editingData.options.length
    temarr['amount'] = 0
    temarr['price'] = 0
    temarr['data'] = []
    temarr['option'] = {}
    temarr['option']['type'] = ''
    temarr['option']['value'] = ''

    editingData.options.push(temarr)
    this.setState({ editingData: editingData, is_variant: true}) 
    
  }
  addOptions(e, val) {
    let editingData = this.state.editingData
    let type = this.state.optionname
    let selectOption = this.state.selectOption
    let temarr = {'sku': '', 'price': '','amount' : 1}
    
    if (editingData.options && editingData.options.length !== 0 && val ){
      temarr[type] = {'type' : type,'name' : type,'value' : val.key,'text' : val.value }
      temarr['key'] = editingData.options.length
      temarr['type'] = type
      
      if (selectOption.indexOf(val) === -1) {
        selectOption.push(val)
        editingData.options.push(temarr)
      } else {
        selectOption.splice(selectOption.indexOf(val), 1)
        editingData.options.splice(editingData.options.indexOf(temarr), 1)
      }   
    } else {
      editingData.options = []
      temarr['key'] = 0
      editingData.options.push(temarr)
    }
    this.setState({ editingData: editingData, selectOption: selectOption })
  }

   onBlurValue(e, val, key) {
    let editingData = this.state.editingData
    if (editingData.options && editingData.options.length !== 0) {
      let option = e.target.value
      let temarrdelete = ['amount', 'price', 'key', 'data']
      Object.keys(editingData.options[key]).forEach(objKey => {
          if (!temarrdelete.includes(objKey)) {
            delete editingData.options[key][objKey]
          }
      })
      if (typeof(editingData.options[key][option]) === 'undefined' ) { 
        editingData.options[key][option] = {}
        editingData.options[key][option]['type'] = e.target.value 
      } else {
        editingData.options[key][option]['type'] = e.target.value 
      }
      editingData.options[key][val] = e.target.value
    }
    this.setState({ editingData: editingData })
  }

  onBlurOption(e, val, key) {
    let editingData = this.state.editingData

    if (editingData.options && editingData.options.length !== 0) {
      // let option = e.target.value
      let optionKeyd =  editingData.options[key]['optionKey']
      if (val === 'option[value]') {
         if (typeof(editingData.options[key][optionKeyd]) === 'undefined' ) { 
          editingData.options[key][optionKeyd] = {type: optionKeyd}
        } else {
          editingData.options[key][optionKeyd]['value'] = e.target.value
          editingData.options[key][optionKeyd]['text'] = e.target.value
          editingData.options[key][optionKeyd]['name'] = e.target.value
        }
      }  
      else {
        editingData.options[key][val] = e.target.value
      }
    }
    this.setState({ editingData: editingData })
  }

  includesOption(val) {
    let tempOptionarr = ['amount', 'data', 'key']
    Object.keys(val).forEach(objKey => {
      if (tempOptionarr.includes(objKey)) {
        delete val[objKey]
      }
      return (Object.values(val)[0]['text'])
    })
  }

  deleteOption(e, val) {
    let editingData = this.state.editingData
    if (editingData.options && editingData.options.length !== 0 ){
      editingData.options.splice(editingData.options.indexOf(val), 1)
    }
    this.setState({ editingData: editingData })
  }


  fetchOption() {
    // rxget(config.api_category_option_all, this.state.pagingcat, {
    //   '1': (json) => { if (json.data.length > 0){this.setState({option: json.data, optionvalue: json.data[0].value})} }
    // })
  }

  toggleVariant (e) {
    let is_variant = this.state.is_variant
    is_variant = !is_variant
    this.setState({is_variant: is_variant})
  }


  handleChange(event) {
    let tempPaging = this.state.paging
    tempPaging.type = event.target.value
    this.setState({paging: tempPaging}, () => {
      rxget(config.api_category_option,  this.state.paging, {
        '1': (json) => { this.setState({ optionvalue: json.data.options.value }) }
      })

      this.setState({ optionname: event.target.value })
    })    
  }

  handleChangeOption(e, val) {
    let editingData = this.state.editingData
    let option = {
        key: 0,
        amount: 0,
        type: this.state.optionname,
        value: {type: this.state.optionname, name: this.state.optionname, value: val.key, text: val.value},
        data: {}
      }
   
    let selectOption = this.state.selectOption
    if (selectOption.indexOf(val) === -1) {
      editingData['options'].push(option)
      selectOption.push(val)
    } else {
      selectOption.splice(selectOption.indexOf(val), 1)
      editingData['options'].splice(editingData['options'].indexOf(option), 1)
    }
    
    this.setState({ selectOption: selectOption, editingData: editingData })
  }

  handleDeleteOption(e, val) {
    e.stopPropagation()
    let selectOption = this.state.selectOption
    selectOption.splice(selectOption.indexOf(val), 1)
    this.setState({selectOption: selectOption})
  }

  addVariant(e, val) {
    console.log(val)
  }

  changePercentDiscount(e){
    let editingData = this.state.editingData
    let strvalue = e.target.value
    editingData['price'] = (editingData['price_discount'] - ((editingData['price_discount'] * strvalue) / 100)).toFixed(2)
    this.setState({ percentDiscount: strvalue, editingData: editingData })  
  }

  sortName(obj){
    var byName = obj.slice(0);
    byName.sort(function(a,b) {
        var x = a.name.toLowerCase();
        var y = b.name.toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
    });
    return byName;
  }

  readExcelFile(e) {
    const scope = this
    // let editingData = this.state.editingData
    let arrEditingData = this.state.arrEditingData
    let XLSX = require('xlsx')
    let file = e.target.files[0]
    let reader = new FileReader()
    // let arrimg = new Array()
    reader.readAsArrayBuffer(file);
    reader.onload = function() {
      let tmp = new Uint8Array(reader.result);
      let wb = XLSX.read(tmp,{type:'array'});
      let sheet_name_list = wb.SheetNames
      let data = XLSX.utils.sheet_to_json(wb.Sheets[sheet_name_list[0]], {raw: true})
      for(let i in data){
        let editingData = {}
        editingData['price'] = (data[i].price/16500).toFixed(2)
        editingData['price_old'] = (data[i].price_old/16500).toFixed(2)
        arrEditingData.push(editingData)
      }
      scope.setState({arrEditingData: arrEditingData})
    };
  }
  uploadImage(e) {
    const scope = this
    // let editingData = this.state.editingData
    let XLSX = require('xlsx')
    let file = e.target.files[0]
    let reader = new FileReader()
    let arrimg = []
    reader.readAsArrayBuffer(file);
    reader.onload = function() {
      let tmp = new Uint8Array(reader.result);
      let wb = XLSX.read(tmp,{type:'array'});
      let sheet_name_list = wb.SheetNames
      let data = XLSX.utils.sheet_to_json(wb.Sheets[sheet_name_list[0]], {raw: true})
      for(let i in data){
        arrimg.push(data[i].Image)
      }
      scope.setState({arrimg: arrimg})
    };
  }
  onClickProductListCreateSubmit(e){
    rxget(config.api_product_updateprice,{}, {
      '1': (json) => { this.fetchData(); console.log('Thành công') }
    })
  }
  onClickImageListCreateSubmit(e){
    rxpost(config.base_api + '/upload/down', this.state.arrimg, {
      '1': (json) => {  }
    })
  }
  getFileExtension(filename) {
    return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
  }

  render() {
    let suggestions = []
    let tags = this.state.editingData.tags || []
    let percentDiscount = (this.state.editingData.price_discount !== 0 && this.state.editingData.price !== 0 ) ? (this.state.editingData.price_discount - this.state.editingData.price)/this.state.editingData.price_discount*100 : 0
    percentDiscount = !isNaN(percentDiscount) || !isFinite(percentDiscount) ? percentDiscount : 0
    let image_list = (typeof(this.state.editingData.image_list) !== 'string')? this.state.editingData.image_list : this.state.editingData.image_list.split(',').filter(v => v !== '')
    // Data tables
    let data = this.state.data.map(perdata => (
      <tr key={perdata._id}>
        <td className='table-lowpadding'><img className='admin-table-product-img lazyload' alt={perdata.name} data-src={config.base_api + '/upload/image/' + (perdata.img_landscape || 'ico_app_default.png')} /></td>
        <td>{perdata.name}</td>
        <td><span className='table-price'>{numberWithCommas(perdata.price)}</span></td>
        <td><span className='table-stock'>{numberWithCommas(perdata.stock)}</span></td>
        <td><small>{rxgetdate(perdata.created_at)}</small></td>        
        <td><small>{this.helpProductcat(perdata.appdistobj)}</small></td>
        <td>{(perdata.is_active !== 0) && <span className='admin-table-on'>On</span>}{(perdata.is_active === 0) && <span className='admin-table-off'>Off</span>}</td>
        <td>{(perdata.is_hot !== 0) && <span className='admin-table-hot'><span className='icon-fire'></span>Hot</span>}</td>
        <td>
          { this.state.paging.search_is_deleted !== 1 &&
            <div> {/*(perdata.is_hot !== 0) && <span>Hot</span>*/}
                  {/*(perdata.is_active !== 0) && <span className='admin-table-on'>On</span>}{(perdata.is_active === 0) && <span className='admin-table-off'>Off</span>*/} 
                  <span className='rx-action-edit' onClick={(e) => this.onClickDataEdit(e, perdata)}>Sửa</span>
                  <span className='rx-action-delete' onClick={(e) => this.onClickDataDeleteNew(e, perdata)}>Xoá</span> </div> }
          { this.state.paging.search_is_deleted === 1 && 
            <div> <span className='rx-action-restore' onClick={(e) => this.onClickDataRestore(e, perdata)}>Khôi phục</span></div>}
        </td></tr>))
    
    return (
      <div className='admin-cardblock'>
        <div className='admin-cardblock-head'><div className='title'>Sản phẩm</div><div className='description'>Quản lý sản phẩm</div></div>
        <div className='admin-cardblock-body'>
          <div className='row admin-cardblock-body-inner'>

            <div className='col-lg-12 col-md-12 col-sm-12'>

              <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput product' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurDatafilter(e, 'name')} /></span>
              <i className='icon-plus rx-recycle' onClick={(e) => this.onClickDataNew(e)}></i>
              { this.state.paging.search_is_deleted !== 1 && <i className='icon-trash rx-recycle' onClick={(e) => this.onClickDataTrash(e, 1)}></i> }              
              { this.state.paging.search_is_deleted === 1 && <i className='icon-list rx-recycle' onClick={(e) => this.onClickDataTrash(e, 0)}></i> }

              <div className='admin-table-pagination admin-pagin-right'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
              
              <div className='admin-table-wrap'><table className='admin-table-product preset'>
                <thead><tr><th>Ảnh</th>
                  <th className={this.helpSortClass('name', ['rx-th-width-220'])} onClick={(e) => this.onClickSort(e, 'name')} >Sản phẩm</th>
                  <th className={this.helpSortClass('price', ['rx-th-width-60 rx-th-alignright'])} onClick={(e) => this.onClickSort(e, 'price')}>Giá</th>
                  <th className={this.helpSortClass('stock', ['rx-th-width-60 rx-th-alignright'])} onClick={(e) => this.onClickSort(e, 'stock')}>Số lượng</th>
                  <th className={this.helpSortClass('created_at', ['rx-th-width-60'])} onClick={(e) => this.onClickSort(e, 'created_at')}>Ngày</th>                  
                  <th className='rx-th-width-100' >Danh mục</th>
                  <th className='rx-th-width-100' onClick={(e) => this.onClickSort(e, 'is_active')}>Trạng thái</th>
                  <th className='rx-th-width-100' onClick={(e) => this.onClickSort(e, 'is_hot')}>Nổi bật</th>
                  <th style={{width: 100}}>Thao tác</th></tr></thead>
                <tbody>{data}</tbody>
              </table></div>
              <div className='admin-table-pagination'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
            </div>
          </div>
          <div className='clearfix-martop'></div>  
        { this.state.editingData.created_at && <div className='admin-cardblock-form'>
            <div className='admin-cardblock-formname'>Sửa sản phẩm <b>{this.state.editingData.name}</b>
              <div className='admin-cardblock-formbtns'>
                <div className='admin-cardblock-btns'>
                  <a tabIndex='5' className='btn-cancel' onClick={(e) => this.onClickDataEdit(e, {})} onKeyPress={(e) => this.onClickDataEdit(e, {})}>Trở về</a>
                  {this.state.editingData.created_at !== 1 && <a tabIndex='6' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>}
                  <a tabIndex='7' className='btn-clone' onClick={(e) => this.onClickDataCreateSubmit(e)} onKeyPress={(e) => this.onClickDataCreateSubmit(e)}>Tạo mới</a>
                </div>
              </div>
            </div>
            <div className='row admin-cardblock-formbody'>
              <div className='admin-card admin-cardblock-formbody-left col-lg-8 col-md-8 col-sm-12 clearfix'>
              <div className='admin-cardblock-body'>
                <div className='row fullwidth-form-body'>
                  <div className='fullwidth-label'>Tên sản phẩm (Viet ver) *</div><input tabIndex='8' type="text" value={this.state.editingData.name} onChange={(e) => this.onBlurData(e, 'name')} className='fullwidth-input' />
                  <div className='fullwidth-label'>Tên sản phẩm (Eng ver) *</div><input tabIndex='9' type="text" value={this.state.editingData.nameEng} onChange={(e) => this.onBlurData(e, 'nameEng')} className='fullwidth-input' />
                  <div className='fullwidth-label'>Nội dung</div>
                  <CKEditor activeClass='p10' content={this.state.editingData.content} events={{ 'change': this.onChangeContentCKE }} />
                </div>
                <div className='row fullwidth-form-body'>
                  <div className='col-md-3 col-sm-3 img-detail'>
                    <div className='fullwidth-label'>Hình ảnh *</div>
                    <RxUpload callback={(e) => this.callbackUpload(e)} images={this.state.editingData.img_landscape} />                
                  </div>
                  <div className='col-md-9 col-sm-9'>
                    <div className='fullwidth-label'>Hình chi tiết(* Hiển thị ở trang chi tiết sản phẩm)</div>
                    <RxUpload callback={(e) => this.callbackUploadDetail(e)} images={image_list} single='0' />
                  </div>
                </div>
                <div className='fullwidth-form-body row'>
                  <div className='form-body-heda-content row'>
                    <div className='col-md-12 heda'>Giá sản phẩm</div>
                    <div className='col-md-6'><div className='fullwidth-label'>Giá bán*</div>
                      <input tabIndex='1' type='text' value={numberWithCommas(this.state.editingData.price)} onChange={(e) => this.onBlurData(e, 'price')} className='fullwidth-input' />
                    </div>
                    <div className='col-md-6'><div className='fullwidth-label'>Giá so sánh (trước khi giảm)</div>
                      <input tabIndex='1' type='text' value={this.state.editingData.price_discount ? numberWithCommas(this.state.editingData.price_discount) : ''} onChange={(e) => this.onBlurData(e, 'price_discount')} className='fullwidth-input' />
                    </div>
                    <div className='col-md-6'><div className='fullwidth-label'>Tỷ lệ giảm giá (%)</div>
                      <input tabIndex='1' type='text' placeholder={this.state.editingData.price && this.state.editingData.price_discount ? percentDiscount.toFixed(2) : 0} onChange={(e) => this.changePercentDiscount(e)} className='fullwidth-input' />
                    </div>
                    <div className='col-md-6'><div className='fullwidth-label'>Giá nhập vào</div>
                      <input tabIndex='1' type='text' value={numberWithCommas(this.state.editingData.price_old)} onChange={(e) => this.onBlurData(e, 'price_old')} className='fullwidth-input' />
                    </div>
                  </div>
                  <div className='form-body-heda-content row'>
                    <div className='col-md-12 heda'>Tồn kho</div>
                    <div className='col-md-6'><div className='fullwidth-label'>Mã sản phẩm</div>
                      <input type='text' value={this.state.editingData.code} onChange={(e) => this.onBlurData(e, 'code')} className='fullwidth-input' />
                    </div>
                    <div className='col-md-6'><div className='fullwidth-label'>Chính sách tồn kho</div>
                      <select className='selectbox-admin' defaultValue={this.state.editingData.inventory_policy ? this.state.editingData.inventory_policy : 0} onChange={(e) => this.onBlurData(e, 'inventory_policy')}>
                        <option value='0'>Không quản lý tồn kho</option>
                        <option value='1'>Có quản lý tồn kho</option>
                      </select>
                      { this.state.editingData.inventory_policy === 1 ? <div>
                      <label className='lk-checbox-main'>Đồng ý cho khách hàng đặt hàng khi đã hết hàng
                        <input type='checkbox' defaultChecked={this.state.checkinvent} className='checkbox' onClick={(e) => this.toggleOne(e, 'checkinvent')} />
                        <span className='checkmark'></span>
                      </label></div> : <div></div> }
                    </div>
                    { this.state.editingData.inventory_policy === 1 ? <div className='col-md-6'>
                      <div className='fullwidth-label'>Số lượng</div>
                      <input tabIndex='1' type='text' value={this.state.editingData.stock} onChange={(e) => this.onBlurData(e, 'stock')} className='fullwidth-input' />
                    </div> : <div></div> } 
                  </div>
                  <div className='form-body-heda-content row'>
                    <div className='col-md-12 heda'>Khối lượng sản phẩm</div>
                    <div className='col-md-6'><div className='fullwidth-label'>(Gram)</div>
                      <input tabIndex='1' type='text' value={numberWithCommas(this.state.editingData.grams)} onChange={(e) => this.onBlurData(e, 'grams')} className='fullwidth-input' />
                    </div>
                  </div>
                </div>
                <div className='fullwidth-form'>
                  <div className='fullwidth-label'>Thay đổi SEO</div>
                  <div className='heda_hide'>Thiết lập các thẻ mô tả giúp khách hàng dễ dàng tìm thấy sản phẩm trên công cụ tìm kiếm như Google.</div>
                  <RxExpandable minHeight='120px'>
                    <div className='rxcontent-adminseo-wrap'>
                      <div className='fullwidth-label rxcontent-adminseo-demo'>
                        <div className='rxcontent-adminseo-seotitle'>{this.state.editingData.seotitle}</div>
                        <div className='rxcontent-adminseo-seoslug'>{this.state.editingData.slug}</div>
                        <div className='rxcontent-adminseo-seometa'>{this.state.editingData.seometa}</div>
                      </div>
                      <div className='fullwidth-label'>SEO title</div><input tabIndex='15' type='text' value={this.state.editingData.seotitle} onChange={(e) => this.onBlurData(e, 'seotitle')} className='fullwidth-input' />
                      <div className='fullwidth-label'>SEO slug</div><input tabIndex='16' type='text' value={this.state.editingData.slug} onChange={(e) => this.onBlurData(e, 'slug')} className='fullwidth-input' />
                      <div className='fullwidth-label'>SEO meta</div><input tabIndex='17' type='text' value={this.state.editingData.seometa} onChange={(e) => this.onBlurData(e, 'seometa')} className='fullwidth-input' />
                    </div>
                  </RxExpandable>
                </div>
                </div></div> 
              <div className='admin-card admin-cardblock-formbody-right col-lg-4 col-md-4 col-sm-12 clearfix'>
                <div className='admin-cardblock-body'>
                  <div className='fullwidth-form fullwidth-form-body'>
                    <div className='fullwidth-label-label col-md-6 col-sm-6 clearfix'>
                      <div className='fullwidth-label'>Nổi bật</div><RxToggle value={this.state.editingData.is_hot} onToggle={(newValue) => this.onBlurDataValue(newValue, 'is_hot')}></RxToggle>
                    </div>
                    <div className='fullwidth-label-label col-md-6 col-sm-6 clearfix'>
                      <div className='fullwidth-label'>Trạng thái</div><RxToggle value={this.state.editingData.is_active} onToggle={(newValue) => this.onBlurDataValue(newValue, 'is_active')}></RxToggle>
                    </div>
                  </div>
                  <div className='fullwidth-form-body'>
                    <div className='fullwidth-label'>Danh mục</div>
                    <RxSelectbox options={this.state.cateall} results={this.state.editingData.appdist || ''} onChange={(result) => {this.onBlurDataValue(result, 'appdist')}} />              
                  </div>
                  <div className='fullwidth-form-body'>
                    <div className='fullwidth-label'>Thương hiệu</div>
                    <RxBrandbox options={this.state.brandall} results={this.state.editingData.brandlist || ''} onChange={(result) => {this.onBlurDataValue(result, 'brandlist')}} />              
                  </div>
                  <div className='fullwidth-form-body'>
                    <div className='fullwidth-label'>SEO keywords</div>
                    <div className='rxselectbox-wrap'>
                      <div className='rxselectbox-result clearfix' tabIndex='22'>
                        <ReactTags tags={tags} suggestions={suggestions} handleDelete={this.handleDelete} handleAddition={this.handleAddition} autocomplete={1} minQueryLength={1} placeholder='Nhập từ khoá....'
                          classNames={{ tagInput: 'tagsinput', selected: '', tag: 'tags', remove: 'tagsremove', suggestions: 'tagsuggest', }} />
                      </div>
                    </div> 
                  </div>
                  <div className='fullwidth-form-body'>
                    <div className='fullwidth-label'>Nhà sản xuất</div>
                    <div className='rxselectbox-wrap'>
                      <div className='rxselectbox-result clearfix' tabIndex='22'>
                        <input tabIndex='1' type='text' value={this.state.editingData.vendor} onChange={(e) => this.onBlurData(e, 'vendor')} className='fullwidth-input' />
                      </div>
                    </div> 
                  </div>
                  <div className='fullwidth-form-body'>
                    <div className='fullwidth-label'>Khuyễn mãi</div>
                    <div className='rxselectbox-wrap'>
                      <label className='lk-checbox-main'>Không áp dụng khuyến mãi
                        <input type='checkbox' defaultChecked={this.state.checkpromo} className='checkbox' onClick={(e) => this.toggleOne(e, 'checkpromo')} />
                        <span className='checkmark'></span>
                      </label>
                    </div> 
                  </div>                  
                </div>
              </div>
              <div className='admin-cardblock-btns'>
                <a tabIndex='19' className='btn-cancel' onClick={(e) => this.onClickDataEdit(e, {})} onKeyPress={(e) => this.onClickDataEdit(e, {})}>Trở về</a>
                {this.state.editingData.created_at !== 1 && <a tabIndex='20' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>}
                <a tabIndex='21' className='btn-clone' onClick={(e) => this.onClickDataCreateSubmit(e)} onKeyPress={(e) => this.onClickDataCreateSubmit(e)}>Tạo mới</a>
              </div>
            </div>
          </div> }
        </div>
      </div>
    )
  }
}

export default AdminProduct