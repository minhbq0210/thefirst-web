import React, { Component } from 'react'
import { polyfill } from 'es6-promise'
import { rxpost, rxsetCookie, rxsetLocal } from './../../classes/request'
import { loginAction } from './../../redux'
import { connect } from 'react-redux'

polyfill()
const config = global.rxu.config

class AdminLogin_ extends Component {
  constructor(props) {
    super(props)
    this.state = global.ultiStaticContext(this.props.staticContext) || { user: '', pass: '', msg: '' }
    this.changeUserName = this.changeUserName.bind(this)
    this.changePass = this.changePass.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    // rxsetLocal('user', undefined)
    // rxsetLocal('arrper', undefined)
    // rxsetCookie('authorize', undefined)
  }

  changeUserName(event) {
    this.setState({username: event.target.value})
  }

  changePass(event) {
    this.setState({pass: event.target.value})
  }

  handleSubmit(event) {
    event.preventDefault()    
    rxpost(config.api_authorize, { username: this.state.username, password: this.state.pass }, { 
      '1': (json) => { 
        (json.data && json.data.user) ? rxsetCookie('authorize', json.data.user.authorization, 120) : rxsetCookie('authorize', json.data.authorization, 120)

        json.data && json.data.user ? this.props.loginAction(json.data.user, {data: json.data.user }) : this.props.loginAction(json.data.user, {data: json.data.user })
        rxsetLocal('arrper', JSON.stringify(json.data.arrper))
        // rxsetLocal('arrper', json.data.arrper )
        this.setState({ msg: '' })

        let arrper = json.data.arrper || []
        // console.log(arrper, 'lll')
        if(arrper && arrper.findIndex(o => o === 'dashboard') !== -1) {
          this.props.history.push('/admin')
        } else if(arrper && arrper.findIndex(o => o === 'order') !== -1) {
          this.props.history.push('/admin/order')
        } else if(arrper && arrper.findIndex(o => o === 'content') !== -1) {
          this.props.history.push('/admin/content')
        } else {
          this.props.history.push('/admin')
        }
      },
      '-2': (json) => {
        this.setState({ msg: 'Sai thông tin tài khoản !' }) }
      })
  }

  setCookie() {    
  }

  render() {
    return (
      <div className='admin-login-container'>
        <div className='admin-login-inner'>
          <div className='admin-login-head'>ĐĂNG NHẬP</div>
          <div className='admin-login-body'>
            <form onSubmit={this.handleSubmit}>
              <div>
                <div className='fullwidth-label'>Tài khoản</div><input type="text" autoComplete='username' onChange={this.changeUserName} className='fullwidth-input' />
              </div>
              <div>
                <div className='fullwidth-label'>Mật khẩu</div><input type="password" autoComplete='password' onChange={this.changePass} className='fullwidth-input' /> 
              </div>
              
              { this.state.msg && 
                <div className='admin-login-error' >{this.state.msg}</div>
              }
              <input type="submit" value="Đăng nhập" className='fullwidth-submit' />
            </form>
          </div>          
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({ })

const mapDispatchToProps = {  
  loginAction
}

const AdminLogin = connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminLogin_)

export default AdminLogin