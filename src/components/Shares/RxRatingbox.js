import React, { Component } from 'react'
import { polyfill } from 'es6-promise'
import { rxCountStart } from './../Helpers/RxCountStart.js'

polyfill() 
const config = global.rxu.config

class RxRatingbox extends Component {
  constructor(props, context) {
    super(props, context)
    
    let reviewsrating = {}
    if (typeof(this.props.reviewsrating.score) === 'undefined') {
      reviewsrating = {score: 5, content: '', title: 'Rất tốt'}
    } else {
      reviewsrating = this.props.reviewsrating
    }
    
    this.state = {
      reviewsrating: reviewsrating,    
      infodata: this.props.infodata,
      dataarrratings: [0,0,0,0,0],
      results: {},
      checkchangetitle: false
    }
  }

  onBlurData(e, name) {
    let editingData = (typeof(this.state.reviewsrating) !== 'undefined') ? this.state.reviewsrating : {}
    editingData[name] = e.target.value
    if (name === 'title') {this.setState({ checkchangetitle: true })}
    this.setState({ reviewsrating: editingData })    
  }

  onClickDataUpdateSubmit(e, perdata) {
    this.props.onChange(perdata)
    this.setState({ reviewsrating: {} })    
  }

  getArrayRating(index) {
    let arrRating = [0,0,0,0,0]
    for (var i=0; i < arrRating.length ; i++) {
      arrRating[i] = (i < index) ? 1 : 0
    }

    let reviewArr = ['Rất tốt', 'Không tốt', 'Bình thường', 'Tốt', 'Tương đối tốt', 'Rất tốt']
    if (this.state.checkchangetitle === false) {
      let tempReviewRating = this.state.reviewsrating
      tempReviewRating.title = (typeof reviewArr[index] !== 'undefined') ? reviewArr[index] : reviewArr[0]
    }
    
    return arrRating
  }

  onClickChooseRatings(e, index, arrlen) {
    let tempReviewRating = this.state.reviewsrating || {}
    tempReviewRating.score = index+1
    let arrtmp = this.getArrayRating(index)
    this.setState({ dataarrratings: arrtmp, reviewsrating: tempReviewRating })
  }
  
  render() {
    let dataarrratingstmp = []
    let arrayratings = [0,0,0,0,0]
    if (typeof(this.state.reviewsrating) !== 'undefined') {
      let score = this.state.reviewsrating.score
      arrayratings = this.getArrayRating(score)
    } 

    dataarrratingstmp = arrayratings.map((perdata,index) => (
      <div key={index}>
      {perdata === 0 && <div className='rx-star-off-big' onClick={(e) => this.onClickChooseRatings(e, index, this.state.dataarrratings.length)} onMouseOver={(e) => this.onClickChooseRatings(e, index, this.state.dataarrratings.length)}></div> }
      {perdata === 1 && <div className='rx-star-big' onClick={(e) => this.onClickChooseRatings(e, index, this.state.dataarrratings.length)} onMouseOver={(e) => this.onClickChooseRatings(e, index, this.state.dataarrratings.length)}></div>}
      </div>
    ))

    return (
      <div className='lk-box-product-reviews-main'>
        <div className='lk-box-product-reviews-main-rating'>
          <div className='lk-box-product-reviews-img'>
            <img className='lk-img-rating lazyload' data-src={config.base_api + '/upload/image/' + this.state.infodata.img_landscape} alt='' title={this.state.infodata.name} /> 
          </div>
          <div className='lk-box-product-reviews-main-inforating'>
            <div className='cart-page-customer-name'>{this.state.infodata.name}</div>
            <div>
              <div className='figure-body-rating-star'>
                  {rxCountStart(this.state.infodata.ratingsSumary)}
                  <div className='rx-star-text'>({(typeof(this.state.infodata.ratings) !== 'undefined') && this.state.infodata.ratings.length} {(typeof(this.state.infodata.ratings) === 'undefined') && 0}  nhận xét)</div>
                </div>
            </div>
          </div>
        </div>

        <div className='lk-box-product-reviews-main-formrating'>
          <div className='lk-box-product-reviews-title lk-margin-bottom'>Đánh giá của bạn</div>
          <div className='clearfix lk-margin-bottom'>
            <div className='figure-body-rating-star'>
              {dataarrratingstmp}
            </div>
          </div>
          <div className='lk-margin-bottom'>
            <div className='cart-page-customer-name'>Tiêu đề đánh giá</div>
            <input tabIndex='1' type='text' name='name' onChange={(e) => this.onBlurData(e, 'title')} className='fullwidth-input' value={this.state.reviewsrating.title || ''} />
          </div>
          <div className='lk-margin-bottom'>
            <div className='cart-page-customer-name'>Nhận xét sản phẩm</div>
            <textarea tabIndex='2' name='name' className='fullwidth-input' rows='5' placeholder='Nhận xét của bạn về sản phẩm này' onChange={(e) => this.onBlurData(e, 'content')} value={this.state.reviewsrating.content || ''}/>
          </div>
          <div className='lk-box-product-reviews-botton'>
            <div tabIndex='3' className='lk-reviews-product-submit' onClick={(e) => this.onClickDataUpdateSubmit(e, this.state.reviewsrating)}>Lưu thay đổi</div>
          </div>
        </div>
      </div>
    )    
  }
}

RxRatingbox.defaultProps = { onChange: () => {}, reviewsrating: {}, results: {} }
export default RxRatingbox