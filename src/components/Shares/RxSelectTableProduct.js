/*eslint-disable */

import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import { rxcurrencyVnd, rxgetdate, rxChangeAlias } from './../../classes/ulti'

const config = global.rxu.config

class RxSelectboxmulti extends Component {
  constructor (props, context) {
    super(props, context)

    this.state = {
      toggle: false,
      display: false,
      multiselected: [],
      onlyone: this.props.onlyone,
      options: this.props.options || [],
      results: this.props.results,
      stock: 0,
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      categories: [], cateobj: [],
      count: this.props.count,
      showAppdist: false,
      appdist_name: '',
    }
  }
  componentDidMount () {
    this.fetchAllData()
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    let results = this.state.results
    if (nextProps.results !== this.state.results) {
      if (typeof(nextProps.results) === 'string') { results = JSON.parse(nextProps.results)} else { results = nextProps.results }
      this.setState({results: results})
    }
    if (nextProps.count !== this.state.count) {
      this.setState({count: count})
    }
  }
  toggle () {
    this.setState({ toggle: !this.state.toggle })
  }
  toggle1 (e) {
    e.stopPropagation()
    this.setState({ display: !this.state.display })
  }
  onClickItem (e, item) {
    e.stopPropagation()
    let selectArr = this.state.multiselected
    let tempIndex = selectArr.indexOf(item._id)
    if (tempIndex === -1) {
      if (this.state.onlyone) { selectArr = []; selectArr.push(item._id) } else { selectArr.push(item._id) }
    } else {
      selectArr.splice(tempIndex, 1)
    }
    if (this.state.keys === 'keystring') {
      this.setState({ multiselected: selectArr }, () => {
        let tempResults = selectArr ? (selectArr.constructor === Array ? selectArr.join(',') : selectArr) : ''
        this.props.onChange(tempResults)
      })
    } else {
      this.setState({ multiselected: selectArr }, () => { this.props.onChange(item) })
    }
    // this.toggle1(e)
  }
  fetchallProduct() {
    if(this.props.type !== 'ads') {
      rxget(config.api_product_getSale, this.state.paging, {
        '1': (json) => { this.setState({ options: json.data.product }) }
      })
    } else {
      rxget(config.api_category_option_all, this.state.paging, {
        '1': (json) => { this.setState({ options: json.data.cates }) }
      })
    }
  }
  fetchAllData () {

  }
  onChangeFilter (e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging // { ...this.state.paging }
    let selectArr = this.state.multiselected
    let appdist_name = this.state.appdist_name
    let showAppdist = this.state.showAppdist

    if (name === 'appdist') {
      showAppdist = false
      if (e === -1) { delete paging['searchid_appdist']; appdist_name = 'Tất cả danh mục' }
      else {
        paging['searchid_appdist'] = e._id
        appdist_name = e.name
        if (e._id === -1) { delete paging['searchid_appdist'] }
      }
    } else {
      if (e.target.value === '') {
        delete paging['search_' + name]
      } else {
        paging['search_' + name] = e.target.value
      }
    }

    paging.pg_page = 1
    this.setState({ paging: paging, selectArr: selectArr, appdist_name: appdist_name, showAppdist: showAppdist })
    this.timerDatafilter = setTimeout((e) => {
      this.fetchallProduct()
    })
  }
  onClickAll (e) {
    if (this.state.keys === 'keystring') {
      let checkall = this.state.checkall
      let multiselected = this.state.multiselected
      if (checkall) {
        this.state.options && this.state.options.map((item, e) => {
          multiselected.push(item._id)
          return multiselected
        })
      } else {
        multiselected = []
      }
      this.setState({ checkall: !checkall, multiselected: multiselected }, (e) => {
        let tempResults = multiselected ? (multiselected.constructor === Array ? multiselected.join(',') : multiselected) : ''
        this.props.onChange(tempResults)
      })
    }
  }
  onClickResult (e, item) {
    e.stopPropagation()
    let tempIndex = this.state.results.indexOf(item._id)
    if (tempIndex !== -1) {
      let tempResults = this.state.results
      tempResults.splice(tempIndex, 1)
      this.setState({ results: tempResults }, () => {
        let tempResults = this.state.results ? (this.state.results.constructor === Array ? this.state.results.join(',') : this.state.results) : ''
        this.props.onChange(tempResults)
      })
    }
  }
  onRemoveSelectedItem (e, id) {
    // e.stopPropagation();
    let results = this.state.results
    if (typeof (results) === 'object') { results = Object.keys(results) }
    results = results.constructor !== Array ? results.split(',').filter(result => result !== id) : results.filter(result => result !== id)
    this.setState({ results: results.join(',') })
  }
  // UNSAFE_componentWillReceiveProps(nextProps) {
  //   if (nextProps.api !== this.state.api) {
  //     this.setState({api: nextProps.api})
  //   }
  // }
  // Help show functions
  helpProductcat(cats) {
    let result = ''
    if (cats) {
      for(let i = 0; i < cats.length; i++) {
        if(cats[i].is_brand === 0){ result += cats[i].name+ ', ' }
      }
      return result
    }
  }
  // P A G I N
  onclickFirst(e) {
    let paging = this.state.paging
    paging.pg_page = 1
    this.setState({ paging: paging }, () => { this.fetchallProduct() })
  }
  onclickLast(e) {
    let paging = this.state.paging
    let countAll = this.state.count / this.state.paging.pg_size
    if (countAll > parseInt(countAll,10)) {
      paging.pg_page = parseInt(countAll,10) + 1
    } else {
      paging.pg_page = countAll
    }
    this.setState({ paging: paging }, () => { this.fetchallProduct() })
  }
  onClickPaginBack (e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) : paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchallProduct() })
  }
  onClickPaginNext (e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchallProduct() })
  }
  onClickPaginCur(e) {
    let paging = this.state.paging
    if (this.state.options.length >= this.state.paging.pg_size) {
      paging.pg_page = e
    }
    else {
      paging.pg_page = e
    }
    this.setState({ paging: paging }, () => { this.fetchallProduct() })
  }
  showListAppdist() {
    this.setState({showAppdist: true})
  }
  onBlurDatafilterAppdist(e, name) {
    let appdist_name = this.state.appdist_name
    let cateobj = this.state.cateobj
    let paging = this.state.paging
    let catetmp =this.state.categories.filter(function(obj) { return obj.name.toLowerCase().indexOf(e.target.value.toLowerCase()) > -1 })
    appdist_name = e.target.value
    if(!appdist_name) {delete paging['searchid_appdist']}
    this.setState({appdist_name: appdist_name, cateobj: catetmp, paging: paging}, () => {
      if(!appdist_name) {this.fetchallProduct(); this.state.showAppdist = false}
    })
  }
  renderPaging() {
    let result = []
    let listData = this.state.count
    let total = 0
    if ((listData / this.state.paging.pg_size) > parseInt(listData / this.state.paging.pg_size,10)) {
      total = parseInt(listData / this.state.paging.pg_size,10) + 1
    } else {
      total = parseInt(listData / this.state.paging.pg_size,10)
    }
    result.push(<div key={'first'} className='adtable__paginback adtable__paginfirst' onClick={(e) => this.onclickFirst(e)}><i /></div>)
    if (this.state.paging.pg_page === 1) {
      result.push(<div key={'non-back'} className='adtable__paginback icon-arrow-left' />)
    } else {
      result.push(<div key={'back'} className='adtable__paginback adtable__paginnext__active' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left' /></div>)
    }
    if (total > 5) {
      if (this.state.paging.pg_page === 1) {
        for (let i = 1; i <= (total); i++) {
          if (i < 4) {
            result.push(<div key={i} className={this.state.paging.pg_page === i ? 'adtable__pagincurr adtable__pagincurr--active' : 'adtable__pagincurr'} onClick={(e) => this.onClickPaginCur(i)} >{i}</div>)
          } else if (i === 4) {
            result.push(<div key={i} className='adtable__pagingMore' >...</div>)
          } else if (i === total) {
            result.push(<div key={i} className={this.state.paging.pg_page === i ? 'adtable__pagincurr adtable__pagincurr--active' : 'adtable__pagincurr'} >{i}</div>)
          }
        }
      } else {
        for (let i = 1; i <= (total); i++) {
          if (i === this.state.paging.pg_page - 1) {
            result.push(<div key={i} className='adtable__pagingMore' >...</div>)
          } else if ((i === this.state.paging.pg_page) || (i === this.state.paging.pg_page + 1)) {
            result.push(<div key={i} className={this.state.paging.pg_page === i ? 'adtable__pagincurr adtable__pagincurr--active' : 'adtable__pagincurr'} onClick={(e) => this.onClickPaginCur(i)} >{i}</div>)
          } else if (i === total) {
            result.push(<div key={i} className={this.state.paging.pg_page === i ? 'adtable__pagincurr adtable__pagincurr--active' : 'adtable__pagincurr'} onClick={(e) => this.onClickPaginCur(i)} >{i}</div>)
          } else if (i === this.state.paging.pg_page + 2) {
            result.push(<div key={i} className='adtable__pagingMore' >...</div>)
          }
        }
      }
    } else if (total === 0) {
      result.push(<div key={'none'} className='adtable__pagincurr adtable__pagincurr--active'>{this.state.paging.pg_page}</div>)
    } else {
      for (let i = 1; i <= (total); i++) {
        result.push(<div key={i} className={this.state.paging.pg_page === i ? 'adtable__pagincurr adtable__pagincurr--active' : 'adtable__pagincurr'} onClick={(e) => this.onClickPaginCur(i)}>{i}</div>)
      }
    }
    if (this.state.paging.pg_page < total) {
      result.push(<div key={'next'} className='adtable__paginnext adtable__paginnext__active' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right' /></div>)
    } else {
      result.push(<div key={'non-next'} className='adtable__paginnext icon-arrow-right ' />)
    }
    result.push(<div key={'last'} className='adtable__paginnext adtable__paginlast ' onClick={(e) => this.onclickLast(e)}><i /></div>)
    return result
  }
  renderForm () {
    let result = this.state.results
    let type = this.props.type || ''
    let sellableItem = Object.values(result)
    let sellable = []
    let classHl = ''
    return (
      <div className='adtable__innerscrollwrap'>
        <table className='adtable__inner'>
          <thead><tr>
            <th>Hình ảnh</th>
            {type !== 'ads' && <th>Mã HH</th>}
            <th>Tên HH</th>
            {type !== 'ads' && <th>Đơn giá</th>}
            {type !== 'ads' && <th>số lượng</th>}
            {type !== 'ads' && <th>Danh mục</th>}
            <th>Ngày tạo</th>
          </tr></thead>
          <tbody>
            {this.state.options && this.state.options.map((item, index) => {
              /*if(this.state.results.length > 0) {
                classHl = this.state.results.map(function(e) { return e.id; }).indexOf(item._id) !== -1 ? 'selecttable__highlight' : ''
              } else {
                classHl = this.state.results.indexOf(item['_id']) !== -1 ? 'selecttable__highlight' : ''
              }*/

              return (
                <tr onClick={(e) => this.onClickItem(e, item)} key={index} className={classHl}>
                  <td><img className='rxtable-img-product' src={config.base_api + '/upload/image/' + (global.rxu.get(item, ['img_landscape']) || 'ico_app_default.png')}/></td>
                  {type !== 'ads' && <td className='rx_bold'>{item.code}</td>}
                  <td>{item.name}</td>
                  {type !== 'ads' && <td className='rxcenter'>{global.rxu.strToMoney(item.price)}</td>}
                  {type !== 'ads' && <td className='rxcenter'>{global.rxu.strToMoney(item.stock)}</td>}
                  {type !== 'ads' && <td><small>{this.helpProductcat(item.appdistobj)}</small></td>}
                  <td><small>{rxgetdate(item.created_at)}</small></td>
                </tr>
              )
            })
            }
          </tbody>
        </table>
        <div className='adform__btns slc_order'>
          <a tabIndex='24' className='adform__btncancel tooltip' onClick={(e) => this.toggle1(e)} >Quay lại</a>
          <div className='adtable__paginProduct adtable__pagin--botProduct slc_order'>
            {this.renderPaging()}
          </div>
        </div>
      </div>
    )
  }
  render () {
    return (
      <div className={this.state.display ? 'rxselectbox-wrap' : 'rxselectbox-wrap'}>
        <div className='textbox w100 fullwidth-input clearfix' onClick={(e) => this.toggle1(e)}>
          {!this.state.results.length && this.props.type !== 'ads' ? <span className='rxselectbox-resultempty' /> : <span className='rxselectbox-resultempty'>Nhấp vào để chọn</span>}
          {this.props.type !== 'ads' && <span>{'Change'}</span> }
        </div>
        {!this.state.toggle && this.state.display && <div className='modal-backdrop_table' onClick={(e) => this.toggle1(e)} />}

        {!this.state.toggle && <div className={this.state.display ? 'rxselectbox-toggle-product active _cusSup' : 'rxselectbox-toggle-product '}>
          <div className='rxcloseform'><i className='icon-close nav-icon _icon_close' onClick={(e) => this.toggle1(e)} /></div>

          <div className='rxselectbox-filter'>
            {/*<span className='inputfilter adtable__dropdown_appdist'>
              <span onClick={(e) => this.showListAppdist()}><input type='text' className="inputfilter fullwidth-input" onChange={(e) => this.onBlurDatafilterAppdist(e, 'appdist')} placeholder='--Chọn danh mục--' value={this.state.appdist_name || ''}/></span>
              <div className={this.state.showAppdist ? 'dropdown-appdist active' : 'dropdown-appdist'}>
                <div className='appdist__item' onClick={(e) => this.onChangeFilter(-1, 'appdist')}>Tất cả danh mục</div>
                {this.state.cateobj.map(pol => (<div className='appdist__item' onClick={(e) => this.onChangeFilter(pol, 'appdist')} key={pol._id}>{pol.name}</div>))}
              </div>
            </span>*/}
            <input className='inputfilter fullwidth-input' type='text' placeholder='Nhập tên sản phẩm' onKeyUp={(e) => this.onChangeFilter(e, 'name')} />
            {/* !this.state.onlyone && <div className='chooseall' onClick={(e) => this.onClick(e)}>Tạo mới</div> */}
          </div>
          {this.renderForm()}
        </div> }
      </div>
    )
  }
}
RxSelectboxmulti.defaultProps = { onChange: () => {}, options: [], results: [] }
// Options : {key: , text: }
export default RxSelectboxmulti
