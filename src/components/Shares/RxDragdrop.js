import React, { Component } from 'react'
import { polyfill } from 'es6-promise'
polyfill()

class RxDragdrop extends Component {
  constructor(props, context) {
    super(props, context)    
    this.state = {}   
  }

  preventDefault(event) {
    event.preventDefault()
  }

  dragStart(e) {
    this.dragged = e.currentTarget
    e.dataTransfer.effectAllowed = 'move'
    e.dataTransfer.setData('text/html', e.currentTarget)
  }

  dragEnd(e) {
    this.dragged.style.display = 'block'
    // this.dragged.parentNode.removeChild(placeholder)

    // Update state
    let data = this.state.data
    let from = Number(this.dragged.dataset.id)
    let to = Number(this.over.dataset.id)
    if(from < to) to--
    data.splice(to, 0, data.splice(from, 1)[0])
    this.setState({data: data})
  }

  drop(event) {
    event.preventDefault()
    var data = ''
    try {
      data = JSON.parse(event.dataTransfer.getData('text'))
    } catch (e) {
      return data
    }
  }  

  render() {    
    return (
      <div draggable='true' className='rxdragdrop-container' onDragStart={() => this.dragStart} onDragOver={() => this.preventDefault} onDrop={() => this.drop}>Drag & Drop   {this.props.children}  </div>
    )
  }
}

export default RxDragdrop
