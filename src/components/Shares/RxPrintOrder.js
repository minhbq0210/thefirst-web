import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxcurrencyVnd, rxgetdatetimeprint } from './../../classes/ulti'
import { withTranslation } from 'react-i18next'
const { rxgetLocal } = global.rootRequire('classes/request')

polyfill()

const config = global.rxu.config
const numberWithCommas = (x) => {
  return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","): '';
}

class RxPrintOrder extends Component {

  constructor(props) {
    super(props)
    this.state = { editingData: this.props.editingData || {}, totalcards: this.props.totalcards, logo: this.props.logo }
    this.state.currency = 0
    let strcatecurrency = rxgetLocal('currency')
    if (strcatecurrency) {
      let objcurrency = {name: '', exchangerate:''}
      try {objcurrency = JSON.parse(strcatecurrency)} catch(e) {}
      if (objcurrency && objcurrency.name) {
        this.state.currency = objcurrency
      }
    }
  }

  componentDidMount() {
  }


  render() {
    const {t} = this.props
    let detailcardsprint = []
    let detail = this.state.editingData.detail
    if(typeof detail === 'string') {detail = JSON.parse(detail)}
    detailcardsprint = detail && detail.carts && detail.carts.map((objcart, index) => (
    (objcart && objcart.data) && 
      <tr key={index}>
        <td className='lk-print-lable-number'><img className='lazyload' data-src={config.base_api + '/upload/image/' + (objcart.data.img_landscape  || 'ico_app_default.png')} alt={global.rxu.get(objcart, ['data','name'], '')}/></td>
        <td className='lk-print-lable-name print_order'>{global.rxu.get(objcart, ['data','name'], '')}</td>
        <td className='lk-print-lable-number'>{global.rxu.get(objcart, ['amount'])}</td>
        <td className='lk-print-lable-number'>{rxcurrencyVnd(global.rxu.get(objcart, ['discount_item'], 0))}</td>
        <td className='lk-print-lable-number'>{objcart.pricenew >=0 ? rxcurrencyVnd(objcart.pricenew) : rxcurrencyVnd(numberWithCommas(objcart.amount*objcart.data.price))}</td>
      </tr>
    ))
    let url = this.state.logo['img_landscape'] ? (config.base_api + '/upload/image/' + this.state.logo['img_landscape']) : 'http://www.annawholesale.com.au/images/static/LOGO-ANNA-WHOLSALE.jpg'
    return (
      <div className='rxpopup-addnew-cus__ print'>
        <div className='adblock'>
          <div className='adblock__body'>
            <div className='adblock__inner'>
              <div className='adform_header print_order row'>
                <div className='adform_header_left'>
                  <div className='adform_header-info'>
                     <div className='_print_form_title'>Anna Wholesale</div>
                     <div className='_print_form_add'>Hàng Úc Xách Tay</div>
                     <div className='_print_form_phone'>Address: shop 3/12-26 Little Regent St Chippendale, Sydney 2008</div>
                     <div className='_print_form_phone'>Phone: +61450928599 - Kevin</div>
                   </div>
                </div>
                <div className='adform_header_right'>
                  <img className='adform_header_logo lazyload' data-src={url} alt='AustraliaChemistWarehouse'/>
                </div>
                <div className='adform_header_title_invoice'>
                  <div className='adform_header_title'>{t('TAX INVOICE')}</div>
                  <div className='adform_header_time'>{t('Date')}: {rxgetdatetimeprint(Date.now()/1000)}</div>
                </div>
              </div>
              <div className='adform_header_info print_order row'>
                <div className='receiver_address '>
                  <div className='adform_ct'><div>{t('customername')}: </div><b>{global.rxu.get(this.state.editingData, ['name'])}</b></div>
                  <div className='adform_ct'><div>{t('Address delivery')}: </div><b>{global.rxu.get(this.state.editingData, ['address',0,'street'])}, {global.rxu.get(this.state.editingData, ['address',0,'county'])}, {global.rxu.get(this.state.editingData, ['address',0,'city'])} </b></div>
                  <div className='adform_ct phone'><div>{t('Phone number')}: </div><b>{global.rxu.get(this.state.editingData, ['phone'])} </b></div>
                </div>
                <div className='adform_header_info_note'>
                  <div className='adform_header_code'>{t('Invoice code')}: {global.rxu.get(this.state.editingData, ['order_code'], '')}</div>
                  <div className='adform_header_info_time'>{t('Order date')}: {rxgetdatetimeprint(global.rxu.get(this.state.editingData, ['created_at'],Date.now()/1000))}</div>
                  <div className='receiver_address_note'>
                    <span>{t('Note')}: </span>
                    <b className='adform_ct note_text'>{global.rxu.get(this.state.editingData, ['desc'], '')}</b>
                  </div>
                </div>
              </div>
              <div className='adform__body _cus'>
                <div className='adtable__main'>
                  <table className='adtable__inner print_order'>
                    <thead>
                      <tr>
                        <th className='lk-print-lable-number'>{t('Image')}</th>
                        <th className='lk-print-lable-number'>{t('Product name')}</th>
                        <th className='lk-print-lable-number'>{t('Quantity')}</th>
                        <th className='lk-print-lable-number'>{t('Discount')}</th>
                        <th className='lk-print-lable-number'>{t('Amount')}</th>
                      </tr>
                    </thead>
                    <tbody>
                      {detailcardsprint}
                    </tbody>
                  </table>
                </div>
                <div className='adform__body_total'>
                  <div className='adform_ct'><b>{t('Subtotal')}:</b> <span>{rxcurrencyVnd(this.state.totalcards)}</span></div>
                  <div className='adform_ct'><b>{t('Discount on web')}:</b> <span>{rxcurrencyVnd(this.state.editingData.price_discount)}</span></div>
                  <div className='adform_ct'><b>{t('Shipping fee')}:</b> <span>{rxcurrencyVnd(this.state.editingData.fee_ship)}</span></div>
                  <div className='adform_ct'><b>{t('Total amount')}:</b> <span>{ rxcurrencyVnd(Number(this.state.editingData.totalpay)+ Number(this.state.editingData.fee_ship))}</span></div>
                </div>
              </div>
              <div className='adform__footer'>
                <div className='adfrom_footer_content'>
                  <div className='adform_footer_transform'>
                    <div className='adform_ct row'><span>{t('Form of payment')}:</span><span>{this.state.editingData.type_payment === 1 ? 'Thanh toán khi nhận hàng' : 'Chuyển khoản'}</span></div>
                    <div className='adform_ct row'><span>{t('Form of transportation')}:</span><span>{global.rxu.get(this.state.editingData, ['history_order', 0, 'text'], '')}</span></div>
                  </div>
                  <div>Thanks for your visiting our Store at Anna Wholesale</div>
                  <div>Please Note: we do not accept returns on any items due to changing minds, only exchange on certain items</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation('translations')(RxPrintOrder)