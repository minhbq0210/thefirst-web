import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
// import ReactCrop from 'react-image-crop'
// import 'react-image-crop/dist/ReactCrop.css'

polyfill()
const config = global.rxu.config

class RxUpload extends Component {
  constructor(props) {
    super(props)
    this.state = {
      images: props.images || [],
      callback: props.callback,
      single: props.single || 1,
      image_review: null,
      crop: this.props.scale === 'img_profile' ? { aspect: 3/3, width: 50, } : { unit: "%", width: 50, height: 70, x: 5, y: 5},
      resize_image: false,
      multi_file: false
    }    
  }

  componentDidMount() {    
  }

  onClickDataUpload(e) {
    this.refs.data_image.click()
  }

  onChangeDataImageFile(e) {
    e.stopPropagation()
    e.preventDefault()

    if (typeof(e.target.files[0]) !== 'undefined') {
      let file = e.target.files[0]
      let data = new FormData()
      data.append('uploadFile', file, file.name)
      fetch(config.base_api + '/upload', { method: 'POST', body: data }).then(res => res.json()).then((json) => {
        if (json.success === 1) {
          this.onBlurData(json)
        }
      })
    }
  }  

  onBlurData(json) {
    this.setState({images: json.data}, () => {
      if (typeof this.state.callback === 'function') {
        this.state.callback(this.state)
      }      
    })    
  }

  // Array
  onClickDataUploadArr(e, index) {
    try {
      this.refs['data_image_' + index].click()  
    } catch(e) {
      console.log(e)
    }
  }

  onChangeDataImageFileArr(e, index) {
    e.stopPropagation()
    e.preventDefault()

    if (typeof(e.target.files[0]) !== 'undefined') {
      let file = e.target.files[0]
      let data = new FormData()
      data.append('uploadFile', file, file.name)
      fetch(config.base_api + '/upload', { method: 'POST', body: data }).then(res => res.json()).then((json) => {
        if (json.success === 1) {
          this.onBlurDataArr(json, index)
        }
      })
    }
  }

  onBlurDataArr(json, index) {
    let tempImages = this.state.images
    tempImages[index] = json.data
    this.setState({images: tempImages}, () => {
      if (typeof this.state.callback === 'function') {
        this.state.callback(this.state)
      }  
    })
  }  

  onClickImgRemove(e, index) {
    let tempImages = this.state.images
    tempImages.splice(index, 1)
    this.setState({images: tempImages}, () => {
      if (typeof this.state.callback === 'function') {
        this.state.callback(this.state)
      }      
    })
  }

  onSelectFile(e, name, index) {
    if (e.target.files && e.target.files.length > 0) {
      // if(this.props.nameImage === 'Slider') {
        this.onChangeDataImageFile(e.target.files[0])
      // }
      // const reader = new FileReader();
      // reader.addEventListener("load", () =>
      //   this.setState({ image_review: reader.result, resize_image: true })
      // );
      // reader.readAsDataURL(e.target.files[0]);
    }
    if ( name && name === 'multi_file') {
      this.setState ({ multi_file: true, index: index})
    } else {
      this.setState ({ multi_file: false })
    }
  }

  onImageLoaded = image =>{
    this.imageRef = image;
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        "newFile.jpeg"
      );
      this.setState({ croppedImageUrl });
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage( image, crop.x * scaleX, crop.y * scaleY, crop.width * scaleX, crop.height * scaleY, 0, 0, crop.width, crop.height );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          console.error("Canvas is empty");
          return;
        }
        blob.name = fileName
        window.URL.revokeObjectURL(this.fileUrl)
        this.fileUrl = window.URL.createObjectURL(blob)
        resolve(this.fileUrl)
        this.setState({fileNew: blob})
      }, "image/jpeg");
    });
    
  }
  onClickResizePopupClose(){
    this.setState({ resize_image: false, croppedImageUrl: null, image_review: null })
    this.onImageLoaded(null)
  }
  onClickResizeCheck(e, name) {
    e.stopPropagation()
    e.preventDefault()
    let fileurl = this.state.croppedImageUrl
    let fileNew = this.state.fileNew
    fileNew['name'] = fileurl.split("/").pop() + '.jpg'
    let data = new FormData()
    data.append('uploadFile', fileNew, fileNew.name)
    fetch(config.base_api + '/upload', { method: 'POST', body: data }).then(res => res.json()).then((json) => {
      if (json.success === 1) {
        if(!this.state.multi_file) {
          this.onBlurData(json)
        }
        else {
          this.onBlurDataArr(json, this.state.index)
        }
        this.setState({ resize_image: false })
      }
    })
  }

  render() {
    let tmpImgStr
    let tmpImgStrAdd
    if (this.state.single === 1) {
      let tempImages = (typeof(this.state.images) === 'string') ? this.state.images : 'ico_app_default.png'
      tmpImgStr = 
        <div>
          <input type="file" id="file" ref="data_image" style={{display: "none"}} onChange={(e) => this.onChangeDataImageFile(e)}/>
          <img className='admin-product-img-upload' alt='ico_default' src={config.base_api + '/upload/image/' + tempImages} onClick={(e) => this.onClickDataUpload(e) }/>
        </div>
    } else {
      let tempLastIndex = 0
      tmpImgStr = this.state.images.map((perdata, index) => {
        let tempRef = 'data_image_' + index
        tempLastIndex = index
        return (
          <div key={index} className='admin-product-img-multi'>
            <div className='admin-product-img-remove' onClick={(e) => this.onClickImgRemove(e, index)}>x</div>
            <input type="file" id="file" ref={tempRef} style={{display: "none"}} onChange={(e) => this.onChangeDataImageFileArr(e, index)}/>
            <img className='admin-product-img-upload' alt='ico_default' src={config.base_api + '/upload/image/' + (perdata  || 'ico_app_default.png')} onClick={(e) => this.onClickDataUploadArr(e, index) }/>
          </div>
        )
      })

      tempLastIndex += 1; let tempLastRef = 'data_image_' + tempLastIndex
      tmpImgStrAdd = 
        <div key={tempLastIndex} className='admin-product-img-multi'>
          <input type="file" id="file" ref={tempLastRef} style={{display: "none"}} onChange={(e) => this.onChangeDataImageFileArr(e, tempLastIndex)}/>
          <img className='admin-product-img-upload' alt='ico_default' src={config.base_api + '/upload/image/' + (this.state.images[tempLastIndex]  || 'ico_app_default.png')} onClick={(e) => this.onClickDataUploadArr(e, tempLastIndex) }/>
        </div>        
    }
    

    return (
      <div className='clearfix'>{tmpImgStr}{tmpImgStrAdd}</div>
    )
  }
}

export default RxUpload