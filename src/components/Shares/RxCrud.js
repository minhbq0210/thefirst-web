import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { rxget, rxpost } from './../../classes/request'
import { rxsget } from './../../classes/ulti'

polyfill()

const WAIT_INTERVAL = 500
const config = global.rxu.config

class RxCrud extends Component {
  
  constructor(props, context) {    
    super(props, context)
    this.onChangeContentCKE = this.onChangeContentCKE.bind(this)
    this.onChangeContentCKE1 = this.onChangeContentCKE1.bind(this)

    this.state = {
      flagUpdate: false,
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      editingData: {},
      valuetext: '',
      data: []
    }
  }

  componentDidMount() {
    this.fetchAlldata()
  }

  componentWillReceiveProps(nextProps) {
    if ([false, true].indexOf(nextProps.flagUpdate) !== -1 && nextProps.flagUpdate !== this.state.flagUpdate) {
      this.setState({ flagUpdate: nextProps.flagUpdate })
      this.run('parentUpdate', {})
    }
  }

  fetchAlldata() {
    this.fetchData() 
  }

  fetchData() { 
    let paging = this.state.paging
    if(this.props.api === 'api_category_collection' || this.props.api === 'api_category_collectioncate') {
      rxget(rxsget(config, this.props.api), paging, {
        '1': (json) => { this.setState({ data: json.data.cate }, () => this.props.setData(json.data.cate)) }
      })
    } else {
      rxget(rxsget(config, this.props.api), paging, {
        '1': (json) => { this.setState({ data: json.data }, () => this.props.setData(json.data)) }
      })
    }
  }

  run(name, params) {
    if (params) { params.inthis = this } else { params = this }
    if (typeof this.props[name] !== 'undefined' && typeof this.props[name] === 'function') {
      return this.props[name](params)
    } else if (typeof this[name] !== 'undefined' && typeof this[name] === 'function') {
      return this[name]()
    }
  }

  // C L I C K   E V E N T 
  onClickData(e, perdata) {}
  onClickSort(e, stcol) {
    let paging = this.state.paging                  
    paging.st_type = (paging.st_col !== stcol)? -1 : (-1 * (paging.st_type))
    paging.st_col = stcol

    this.setState({ paging: paging }, () => { this.fetchData() })    
  }  

  onClickDataNew(e) {
    let timeStr = Date.now().toString()
    let clone = { name: 'Content_'+ timeStr.substr(timeStr.length - 5), desc: '', created_at: 1, is_deleted: 0, is_active: 1, is_hot: 0, price: 100000, app: '', appdist: '' }
    if(this.props.api === 'api_category_collection') { clone.type = 1; clone.child = 1 }
    if(this.props.api === 'api_category_collectioncate') { clone.type = 1; clone.child = 0 }
    this.setState({ editingData: clone })    
  }

  onClickDataEdit(e, perdata) {
    if (typeof perdata['inthis'] !== 'undefined') { delete perdata['inthis'] }
    let clone = JSON.parse(JSON.stringify(perdata))
    this.setState({ editingData: clone })
  }

  onClickDataDelete(e, perdata) {
    e.stopPropagation()
    rxget(rxsget(config, this.props.api + '_delete'), {_id: perdata._id}, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataRestore(e, perdata) {
    e.stopPropagation()
    rxget(rxsget(config, this.props.api + '_restore'), {_id: perdata._id}, {
      '1': (json) => { this.fetchData() }
    })
  }

  onClickDataUpdateSubmit(e, perdata) {
    rxpost(rxsget(config, this.props.api + '_edit'), this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataCreateSubmit(e, perdata) {
    rxpost(rxsget(config, this.props.api), this.state.editingData, {
      '1': (json) => { this.fetchData() }
    })
    this.onClickDataEdit({}, {})
  }

  onClickDataTrash(e, isdeleted) {
    let paging = this.state.paging
    paging['search_is_deleted'] = isdeleted
    this.setState({ paging: paging }, () => {
      this.fetchData()
    })
  }

  callbackUpload(e) {
    this.onBlurData({target: {value: e.images}}, 'img_landscape')
  }

  // B L U R   E V E N T 
  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })
  }

  onBlurBrandValue(value, name) {
    let editingData = this.state.editingData
    editingData[name] = value 
    this.setState({editingData: editingData})
  }

  onBlurDataValue(value, name) {    
    let editingData = this.state.editingData
    editingData[name] = value 
    this.setState({editingData: editingData})
  }

  onBlurDatafilter(e, name) {
    clearTimeout(this.timerDatafilter)
    let paging = this.state.paging
    paging['search_' + name] = e.target.value
    this.setState({ paging: paging })

    this.timerDatafilter = setTimeout((e, name) => {
      this.fetchData()
    }, WAIT_INTERVAL)    
  }

  // P A G I N
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onChangeContentCKE(evt) {
    let editingData = this.state.editingData
    let newContent = evt.editor.getData()     
    editingData.content = newContent
    this.setState({ editingData: editingData })
  }

  onChangeContentCKE1(evt) {
    let editingData = this.state.editingData
    let newContent = evt.editor.getData()     
    editingData.desc = newContent
    this.setState({ editingData: editingData })
  }  

  onClickDelProperty(e, indextext) {
    let editingData = this.state.editingData
    delete editingData['value'][indextext]
    editingData['value'] = editingData['value'].filter(Boolean)
    this.setState({editingData: editingData})  
  }

  handleKeyPress(e, type) {
    let valuetext = this.state.valuetext
    if ((e.which === 13 || e.keyCode === 13) && type === 'addproperty' && valuetext.length > 0) {
      let editingData = this.state.editingData

      if (editingData && editingData['value'] && editingData['value'].constructor === Array && editingData['value'].length > 0) {
        editingData['value'].push({key: valuetext, value: valuetext})
      } else {
        editingData['value'] = [{key: valuetext, value: valuetext}]
      }
      this.setState({editingData: editingData, valuetext: ''})
    }
  }

  updateInputValue(e, type) {
    let valuetext = this.state.valuetext
    valuetext = e.target.value
    this.setState({valuetext: valuetext})
  }


  // H E L P E R S 
  helpProductcat(cats) {    
    let result = ''
    if (cats) {
      for(let i = 0; i < cats.length; i++) { result += cats[i].name+ ', ' }
      return result
    }    
  }

  helpSortClass(stcol, extraclass) {
    extraclass = extraclass || []

    let result = ''
    if (this.state.paging.st_col === stcol) {
      result = this.state.paging.st_type === 1 ? 'rx-sort-asc' : 'rx-sort-desc'
    }

    for(let i = 0; i < extraclass.length; i++) {      
      result += ' ' + extraclass[i]
    }

    return result 
  }

  onBlurDataProducts (e, name) {
    let editingData = this.state.editingData
    let product_id = editingData.product_id || []
    let product_obj = editingData.product_obj || []
    let findIndex = editingData.product_id ? editingData.product_id.findIndex(o => o === e._id) : -1
    if(findIndex === -1) {
      if (name === 'product') {
        if (e && e._id) {
          // let value = e
          product_id.push(e._id)
          let product = {
            _id: e._id,
            name: e.name,
            img_landscape: e.img_landscape,
            price: e.price
          }
          product_obj.push(product)
        }
      }
      editingData.product_id = product_id
      editingData.product_obj = product_obj
      this.setState({ editingData: editingData})
    } else {
      alert('Trung Du Lieu')
    }
  }

  onBlurDataCates (e, name) {
    let editingData = this.state.editingData
    let appdist = editingData.appdist || []
    let appdistobj = editingData.appdistobj || []
    let findIndex = editingData.appdist ? editingData.appdist.findIndex(o => o === e._id) : -1
    if(findIndex === -1) {
      if (name === 'cates') {
        if (e && e._id) {
          // let value = e
          appdist.push(e._id)
          let product = {
            _id: e._id,
            name: e.name,
            img_landscape: e.img_landscape
          }
          appdistobj.push(product)
        }
      }
      editingData.appdist = appdist
      editingData.appdistobj = appdistobj
      this.setState({ editingData: editingData})
    } else {
      alert('Trung Du Lieu')
    }
  }

  onClickDataDeleteItem (e, item, name) {
    let editingData = this.state.editingData
    let r = window.confirm('Deleted?')
    if(name === 'product' && r === true) {
      editingData['product_id'] = editingData['product_id'].filter(o => o !== item._id)
      editingData['product_obj'] = editingData['product_obj'].filter(o => o._id !== item._id)
      this.setState({ editingData: editingData })
    }
    if(name === 'cates' && r === true) {
      editingData['appdist'] = editingData['appdist'].filter(o => o !== item._id)
      editingData['appdistobj'] = editingData['appdistobj'].filter(o => o._id !== item._id)
      this.setState({ editingData: editingData })
    }
  }

  renderForm() {    
    if (this.state.editingData.created_at && this.props.form) {
      let form = this.props.form.map((perdata, index) => (<div key={index}><div className='fullwidth-label'>{perdata.name}</div>{perdata.func(this)}</div>))      
      return form
    }
  }  

  render() {    
    return (
      <div>
        <span className='admin-cardblock-filterwrap'><input className='admin-cardblock-filterinput' type='text' placeholder='Tìm' onKeyUp={(e) => this.onBlurDatafilter(e, 'name')} /></span>
        <i className='icon-plus rx-recycle' onClick={(e) => this.onClickDataNew(e)}></i>
        { this.state.paging.search_is_deleted !== 1 && <i className='icon-trash rx-recycle' onClick={(e) => this.onClickDataTrash(e, 1)}></i> }              
        { this.state.paging.search_is_deleted === 1 && <i className='icon-list rx-recycle' onClick={(e) => this.onClickDataTrash(e, 0)}></i> }

        <div className='admin-table-pagination admin-pagin-right rx-no-after'>
          {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
          <div className='pagin-curr'>{this.state.paging.pg_page}</div>
          {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
        </div>

        <div><div className='admin-table-wrap'>
        <table className='admin-table-product preset'>
          <thead>{this.run('renderHead')}</thead>
          <tbody>{this.run('renderBody')}</tbody>
        </table></div>
          <div className='admin-table-pagination admin-pagin-right'>
            {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
            <div className='pagin-curr'>{this.state.paging.pg_page}</div>
            {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
          </div>
        </div>

        { this.state.editingData.created_at && <div className='admin-cardblock-form'>
            <div className='admin-cardblock-formname'>Sửa sản phẩm <b>{this.state.editingData.name}</b><div className='admin-cardblock-formclose' onClick={(e) => this.onClickDataEdit(e, {})}>x</div></div>
            <div className='admin-cardblock-formbody'><div className='clearfix'>
              {this.run('renderForm')}
              
              <div className='admin-cardblock-btns'>
                <a tabIndex='10' className='btn-cancel' onClick={(e) => this.onClickDataEdit(e, {})} onKeyPress={(e) => this.onClickDataEdit(e, {})}>Trở về</a>
                {this.state.editingData.created_at !== 1 && <a tabIndex='11' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>}
                <a tabIndex='12' className='btn-clone' onClick={(e) => this.onClickDataCreateSubmit(e)} onKeyPress={(e) => this.onClickDataCreateSubmit(e)}>Tạo mới</a>
              </div>
            </div></div>
          </div> }
      </div>
    )
  }
}

RxCrud.defaultProps = {
  api: '', // api name
  renderHead: () => {}, 
  renderBody: () => {}, 
  renderPage: () => {},  
  data: {},
  setData: () => {}
}

export default RxCrud
