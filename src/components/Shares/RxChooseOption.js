import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
// import { rxconfig, rxcurrencyVnd } from './../../classes/ulti'
import { rxget } from './../../classes/request'

const config = global.rxu.config

polyfill()
// const numberWithCommas = (x) => {
//   return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// }
class RxChooseOption extends Component {  
  constructor(props, context) {
    super(props, context)    
    this.state = { 
      options: this.props.options || [],
      key: 1,   
      rxsize:  this.props.rxsize  || 4,
      rxpagin: this.props.rxpagin || '',
      checktab: {today: true,yesterday: false,lastweek: false, thismonth: false, lastmonth: false},
      products: [],
      arrproducts: [],
      x: `${10}px`,
      y: `${10}px`,
      top: '',
      left: '',
      showBoxOption: this.props.showBoxOption || false
    }

    this.state.arroption = [
      {name: 'Tiêu đề', datatype: 'title', type: 'tieude',checked: false, property: [], arrchoosed: []},
      // {name: 'Kích thước', datatype: 'size', type: 'kichthuoc', checked: false, property: ['XS', 'S', 'M', 'L', 'XL', 'XXL'], arrchoosed: []},
      // {name: 'Màu sắc', datatype: 'color', type: 'mausac', checked: false, property: ['Bạc', 'Hồng', 'Tím', 'Trắng', 'Vàng', 'Xám', 'Xanh', 'Đen', 'Đỏ'], arrchoosed: []},
      // {name: 'Vật liệu', datatype: 'physical', type: 'vatlieu', checked: false, property: [], arrchoosed: []},
      // {name: 'Kiểu dáng', datatype: 'design', type: 'kieudang', checked: false, property: [], arrchoosed: []},
      {name: 'Khối lượng', datatype: 'mass', type: 'khoiluong', checked: false, property: ['10g', '30g','45g','20g', '50g', '100g', '500g'], arrchoosed: []},
      // {name: 'Dung tích', datatype: 'size', type: 'dungtich', checked: false, property: ['10ml', '20ml', '30ml', '40ml', '50ml', '100ml'], arrchoosed: []},
      {name: 'Tạo tùy chọn mới', datatype: 'newoption', type: 'tuychon', checked: false, property: [], arrchoosed: []},
    ]
    // this.state.arroption = [
      // {name: 'Khối lượng', datatype: 'mass', type: 'khoiluong', checked: false, property: ['10g', '30g','45g','20g', '50g', '100g', '500g'], arrchoosed: []},
      // {name: 'Dung tích', datatype: 'size', type: 'dungtich', checked: false, property: ['10ml', '20ml', '30ml', '40ml', '50ml', '100ml'], arrchoosed: []},
    // ]

    this.state.arroptiontmp = [{name: '', value: '', type: 'tieude', arrchoosed: []}]
    this.state.valuetext = {}
    this.state.arroptionchoose = []
    this.state.arroptionchooseedit = []
    this.state.arrtype = []

    if (this.state.options) {
      this.state.options.forEach(obj => {
        let strtmp = ''
        let jsontmp = {}
        let arrproperty = []
        if (obj.sort) {
          arrproperty = obj.sort.split('/')
        } else {
          Object.keys(obj).forEach(property => {
            if (!['amount', 'price', 'data', 'key', 'sort'].includes(property)) {
              arrproperty.push(property)
            } 
          })
        }
        
        arrproperty.forEach(keyproperty => { 
          jsontmp[keyproperty] = obj[keyproperty]['text']
        })
        jsontmp['name'] = strtmp.slice(0, -3)
        jsontmp['amount'] = obj.amount || ''
        jsontmp['price'] = obj.price || ''
        jsontmp['code'] = (obj.data && obj.data['code']) ? obj.data['code'] : obj['code'] || ''
        jsontmp['barcode'] = (obj.data && obj.data['barcode']) ? obj.data['barcode'] : obj['barcode'] || ''
        this.state.arroptionchooseedit.push(jsontmp)
      })
    }
  }

  componentDidMount() {
    this._ismounted = true
    this.fetchData()
  }

  componentWillUnmount() {
    this._ismounted = false
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.options) {
      let arroptionchooseedit = []
      nextProps.options.forEach(obj => {
        let strtmp = ''
        let jsontmp = {}
        let arrproperty = []
        if (obj.sort) {
          arrproperty = obj.sort.split('/')
        } else {
          Object.keys(obj).forEach(property => {
            if (!['amount', 'price', 'data', 'key', 'sort'].includes(property)) {
              arrproperty.push(property)
            } 
          })
        }
        
        arrproperty.forEach(keyproperty => { 
          jsontmp[keyproperty] = obj[keyproperty]['text']
        })
        jsontmp['name'] = strtmp.slice(0, -3)
        jsontmp['amount'] = obj.amount || ''
        jsontmp['price'] = obj.price || ''
        jsontmp['code'] = (obj.data && obj.data['code']) ? obj.data['code'] : obj['code'] || ''
        jsontmp['barcode'] =(obj.data && obj.data['barcode']) ? obj.data['barcode'] : obj['barcode'] || ''
        arroptionchooseedit.push(jsontmp)
      })
      this.setState({arroptionchooseedit: arroptionchooseedit})
    }
  }

  fetchData() {
    rxget(config.api_dashboard, this.state.paging, {
      '1': (json) => { if (this._ismounted) { this.setState({chart: json.data.chart[0], products: json.data.products})} }
    })
  }

  onClickShowOption(e) {
    let showBoxOption = this.state.showBoxOption || false
    this.setState({showBoxOption: !showBoxOption})
  }

  changeSelect(e, index) {
    let arroption = this.state.arroption
    let arroptiontmp = this.state.arroptiontmp
    let objoption = arroption.find((option) => option.type === e.target.value)
    if (objoption) {
      objoption.checked = true
      arroptiontmp[index].property = objoption.property
      arroptiontmp[index].datatype = objoption.datatype
    }
    arroptiontmp[index].type = e.target.value
    this.setState({arroption: arroption, arroptiontmp: arroptiontmp})
  }

  onClickAddOption(e) {
    let arroption = this.state.arroption
    let arroptiontmp = this.state.arroptiontmp
    if (arroptiontmp.length <= arroption.length) {
      arroptiontmp.push({name: '', value: '', type: 'tieude', arrchoosed: []})
      this.setState({arroptiontmp: arroptiontmp})
    }
  }

  handleKeyPress(e, type) {
    if (e.which === 13 || e.keyCode === 13) {
      this.calArrChoose(type, e.target.value)
    }
  }

  onClickAddProperty(e, type, value) {
    this.calArrChoose(type, value)
  }

  onClickAddEditOption(e) {
    let arroptionchooseedit = this.state.arroptionchooseedit
    let jsonfirst = {}
    if (arroptionchooseedit.constructor === Array && arroptionchooseedit[0]){jsonfirst = arroptionchooseedit[0]} 
    jsonfirst = JSON.parse(JSON.stringify(jsonfirst))
    Object.keys(jsonfirst).forEach(key => { jsonfirst[key] = ''})
    arroptionchooseedit.push(jsonfirst)
    this.setState({arroptionchooseedit: arroptionchooseedit})
  }

  onClickDelProperty(e, indexoptiontmp, indextext) {
    let arroptiontmp = this.state.arroptiontmp
    delete arroptiontmp[indexoptiontmp]['arrchoosed'][indextext]
    this.setState({arroptiontmp: arroptiontmp})
  }

  onClickDelOption(e, indexoptiontmp) {
    let arroptiontmp = this.state.arroptiontmp
    delete arroptiontmp[indexoptiontmp]
    this.setState({arroptiontmp: arroptiontmp})
  }

  onClickDelOptionChoose(e, indexchoose) {
    let arroptionchoose = this.state.arroptionchoose
    delete arroptionchoose[indexchoose]
    arroptionchoose = arroptionchoose.filter(Boolean)
    this.setState({arroptionchoose: arroptionchoose}, () => {
      let options = this.updatePropsOption()
      this.props.onChange(options)
    })
  }

  onClickDelEditOptionChoose(e, indexchoose) {
    let arroptionchooseedit = this.state.arroptionchooseedit
    delete arroptionchooseedit[indexchoose]
    arroptionchooseedit = arroptionchooseedit.filter(Boolean)
    this.setState({arroptionchooseedit: arroptionchooseedit})
  }

  updateInputValue(e, type) {
    let valuetext = this.state.valuetext
    valuetext[type] = e.target.value
    this.setState({valuetext: valuetext})
  }

  calArr(arr1, arr2) {
    let arrtmp = []
    for(let i in arr1) {
      for (let j in arr2) {
        let text = arr1[i] + ' / ' + arr2[j]
        arrtmp.push(text)
      }
    }
    return arrtmp
  }

  calArrChoose(type, value) {
    if (type && value) {
      let arroptiontmp = this.state.arroptiontmp.filter(Boolean)
      let arroptionchoose = this.state.arroptionchoose.filter(Boolean)

      let objoption = arroptiontmp.find((option) => option.type === type)
      if (objoption) {objoption.arrchoosed.push(value)}

      let arrtmp = arroptiontmp[0]['arrchoosed']
      for (let index in arroptiontmp) {
        if (typeof(arroptiontmp[Number(index)+1]) !== 'undefined') {
          arrtmp = this.calArr(arrtmp, arroptiontmp[Number(index)+1]['arrchoosed'])
        } 
      }
      arroptionchoose = []
      arrtmp.forEach((obj, index) => {
        let objopt = {name: obj, amount: '', price: '', code: '', barcode: ''}
        arroptionchoose.push(objopt)
      })

      this.setState({arroptiontmp: arroptiontmp, arroptionchoose: arroptionchoose, valuetext: {}}, () => {
        let options = this.updatePropsOption()
        this.props.onChange(options)
      })
    }
  }

  onBlurDataOption(e, name, index) {
    let arroptionchoose = this.state.arroptionchoose
    let strvalue = e.target.value
    if (name.indexOf('price') !== -1) {
      arroptionchoose[index][name] = strvalue.toString().replace(/(,)/g,'')
    } else {
      arroptionchoose[index][name] = e.target.value
    }
    // arroptionchoose[index][name] = e.target.value 
    this.setState({arroptionchoose: arroptionchoose}, () => {
      let options = this.updatePropsOption()
      this.props.onChange(options)
    })
  }

  onBlurDataEditOption(e, name, index) {
    let arroptionchooseedit = this.state.arroptionchooseedit
    let strvalue = e.target.value
    if (strvalue === '$ Na') {
      strvalue = 0
    }
    if (name.indexOf('price') !== -1) {
      arroptionchooseedit[index][name] = strvalue.toString().replace(/(,)/g,'')
    } else {
      arroptionchooseedit[index][name] = e.target.value
    }
    // arroptionchooseedit[index][name] = e.target.value
    this.setState({arroptionchooseedit: arroptionchooseedit}, () => {
      let options = []
      arroptionchooseedit.forEach((obj, index) => {
        Object.keys(obj).forEach(key => {
          if (!['code','barcode','amount','price','name'].includes(key) && obj[key].constructor === String) {
            obj[key] = {text: obj[key], value: obj[key], name: key, type: key}
          }
        })
        options.push(obj)
      })
      this.props.onChange(options)
    })
  }

  updatePropsOption() {
    let arroptionchoose = this.state.arroptionchoose
    let options = []
    arroptionchoose.forEach((obj, index) => {
      let arrname = obj.name.split(' / ')
      let jsontmp = {}
      // let strsort = ''
      for (let keyproperty in arrname) {
        let datatype = this.state.arroptiontmp[Number(keyproperty)]['datatype']
        jsontmp[datatype] = {text: arrname[keyproperty], value: arrname[keyproperty], name: datatype, type: datatype}
      }
      jsontmp['amount'] = obj.amount || ''
      jsontmp['price'] = obj.price || ''
      jsontmp['key'] = index
      jsontmp['data'] = {code: obj.code || '', barcode: obj.barcode || ''}
      options.push(jsontmp)
    })
    return options
  }

  changeName(strname) {
    let name = '' 
    if (strname === 'mass') {name = 'Khối lượng'}
    else if (strname === 'size') {name = 'Dung tích'}
    // else if (strname === 'physical') {name = 'Vật liệu'}
    else {name = strname || ''}
    return name
  }

  render() {
    let arroptionchooseedit = this.state.arroptionchooseedit
    let arrkeyheader = []
    if (arroptionchooseedit && arroptionchooseedit.constructor === Array && arroptionchooseedit[0]) {
      Object.keys(this.state.arroptionchooseedit[0]).forEach(key => {
        if (!['code','barcode','amount','price','name'].includes(key)) {
          arrkeyheader.push(key)
        }
      })
    }

    return (
      <div className='form-body-heda-content row'>
        <div className='col-md-12 heda'>Thuộc tính sản phẩm
          <div className='note_m'>Thiết lập thuộc tính khối lượng cho sản phẩm</div>
          {/*<div className='note_m'>Sản phẩm có nhiều phiên bản</div>
          <div className='note_m'>Sản phẩm có các phiên bản dựa theo thuộc tính như khối lượng hay thể tích ?</div>*/}
          <div className='admin-cardblock-btn-add-new'>
            <a tabIndex='21' onClick={(e) => this.onClickShowOption(e)} className='btn-clone'>Thêm phiên bản</a></div>
        </div>
        <div className='col-md-12 lk-box-admin-report'>
          <div className=''>
            <div className='lk-line-box'></div>
            {this.state.showBoxOption &&<div>
              <div className='row'>
                <div className='col-md-2'>Tên thuộc tính</div>
                <div className='col-md-8'>Giá trị thuộc tính </div>
                <div className='col-md-2'></div>
              </div>

              {this.state.arroptiontmp.map((option, indexoptiontmp) => (
                <div className='row' key={indexoptiontmp}>
                  <div className='col-md-2'>
                  <select onChange={e => this.changeSelect(e, indexoptiontmp)} value={this.state.value}>
                    {this.state.arroption.map((per, indexoption) => (
                      <option key={indexoption} value={per.type} className={(per.checked) ? '' : 'text-dark'}>{per.name}</option>
                    ))}
                  </select>
                  </div>
                  <div className='col-md-8'>
                    <div className='bootstrap-tagsinput'> 
                      {option.arrchoosed.map((opttext, indextext) => (<span className='tag label label-tags' key={indextext}>{opttext}<span className='remove-property' onClick={e => this.onClickDelProperty(e, indexoptiontmp, indextext)} ></span></span> ))}
                      <input type='' placeholder='' value={this.state.valuetext[option.type] || ''} onKeyPress={e => this.handleKeyPress(e, option.type)} onChange={e => this.updateInputValue(e, option.type)}/>
                    </div>
                    {(option.property && option.property.constructor === Array) && <div className="clearfix mt10">
                       <div>
                          <div className='input-group'>
                            {option.property.map(((objproperty, indexproperty) => (<div className='item-property' key={indexproperty}><label onClick={e => this.onClickAddProperty(e, option.type, objproperty)}>{objproperty}</label></div>)))}
                          </div>
                       </div>
                    </div>}
                    
                  </div>
                  <div className='col-md-2 lk-del-option'><div className='btn-del-option' onClick={(e) => this.onClickDelOption(e, indexoptiontmp)}>Xóa</div></div>
                </div>
              ))}
              
              <div className='row'>
                <div className='col-md-2'><div className='btn-add-property' onClick={(e) => this.onClickAddOption(e)}>Thêm thuộc tính</div></div>
                <div className='col-md-8'></div>
                <div className='col-md-2'></div>
              </div>
              <div className='lk-line-box'></div>
              
              <div className='table-wrap'>
                <table className='table-admin'>
                  <thead> 
                    <tr> 
                      <th>Mẫu sản phẩm</th> 
                      {/*<th>Số lượng</th>
                      <th>Giá cả</th>
                      <th>Mã sản phẩm</th>
                      <th>Barcode</th>*/}
                      <th></th>
                    </tr> 
                  </thead>
                  <tbody>
                    {this.state.arroptionchoose.map((objchoose, indexchoose) => (
                      <tr key={indexchoose}>
                        <td>{objchoose.name}</td>
                        {/*<td><input className='fullwidth-input' type='text' value={objchoose.amount || ''} onChange={e => this.onBlurDataOption(e, 'amount',indexchoose)}/></td>
                        <td><input className='fullwidth-input' type='text' value={objchoose.price || ''} onChange={e => this.onBlurDataOption(e, 'price',indexchoose)}/></td>
                        <td><input className='fullwidth-input' type='text' value={objchoose.code || ''} onChange={e => this.onBlurDataOption(e, 'code', indexchoose)}/></td>
                        <td><input className='fullwidth-input' type='text' value={objchoose.barcode || ''} onChange={e => this.onBlurDataOption(e, 'barcode', indexchoose)}/></td>*/}
                        <td className='lk-del-option'><div className='btn-del-option' onClick={(e) => this.onClickDelOptionChoose(e, indexchoose)}>Xóa</div></td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div> 
            </div>}

            {(!this.state.showBoxOption && this.state.options && this.state.options.constructor === Array && this.state.options.length > 0) &&<div>
              <div className='table-wrap'>
                <table className='table-admin'>
                  <thead> 
                    <tr> 
                      {arrkeyheader.map(perchoose => (
                        <th key={perchoose}>{this.changeName(perchoose)}</th>
                      ))}
                      {/*<th>Số lượng</th> 
                      <th>Giá cả</th> 
                      <th>Mã sản phẩm</th> 
                      <th>Barcode</th> */}
                      <th></th> 
                    </tr> 
                  </thead>
                  <tbody>
                    {this.state.arroptionchooseedit.map((objchoose, indexchoose) => (
                      <tr key={indexchoose}>
                        {arrkeyheader.map(perchoose => (
                          <td key={perchoose+''+indexchoose}><input className='fullwidth-input' type='text' value={objchoose[perchoose] && !objchoose[perchoose]['optionKey'] ? objchoose[perchoose]['text'] || objchoose[perchoose] : ''} onChange={e => this.onBlurDataEditOption(e, perchoose, indexchoose)}/></td>
                        ))}
                        {/*<td><input className='fullwidth-input' type='text' value={objchoose.amount || ''} onChange={e => this.onBlurDataEditOption(e, 'amount',indexchoose)}/></td>
                        <td><input className='fullwidth-input' type='text' value={objchoose.price || ''} onChange={e => this.onBlurDataEditOption(e, 'price',indexchoose)}/></td>
                        <td><input className='fullwidth-input' type='text' value={objchoose.code || ''} onChange={e => this.onBlurDataEditOption(e, 'code', indexchoose)}/></td>
                        <td><input className='fullwidth-input' type='text' value={objchoose.barcode || ''} onChange={e => this.onBlurDataEditOption(e, 'barcode', indexchoose)}/></td>*/}
                        <td className='lk-del-option'><div className='btn-del-option' onClick={(e) => this.onClickDelEditOptionChoose(e, indexchoose)}>Xóa</div></td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div> 
              <div className='btn-add-property' onClick={(e) => this.onClickAddEditOption(e)}>Thêm thuộc tính</div>
            </div>}

          </div>
        </div>
      </div>
    )
  }
}

RxChooseOption.defaultProps = { onChange: () => {}, options: [], results: [] }

export default RxChooseOption