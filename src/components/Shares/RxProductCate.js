import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { polyfill }  from 'es6-promise'
import { rxcurrencyVnd, rxChangeAlias } from './../../classes/ulti'

import { connect } from 'react-redux'
import { cartClear, cartAdd } from './../../redux'
import { rxCountStart } from './../Helpers/RxCountStart.js'
import { rxget } from './../../classes/request'

polyfill()
const config = global.rxu.config

class ProductCate_ extends Component {
  constructor(props) {
    super(props)
    this.state = {  
      products: [], 
      categories: [],   
      checktab: {},
      rxsize:  this.props.rxsize  || 4,
      rxpagin: this.props.rxpagin || '',
      paging: { st_full: 'created_at:desc', st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 12, price: {min: 100, max: 100000}},
    }
    this.state = this.props.staticContext || { products: [], paging: { st_full: 'created_at:desc', st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 12, price: {min: 100, max: 100000}}}
    this.state.categories = this.props.categories || []

    if (this.state.categories && this.state.categories.constructor === Array && this.state.categories[0] && this.state.categories[0]['_id']) {
      this.state.checktab = {}
      this.state.checktab[this.state.categories[0]['_id']] = true
    }
  }

  onClickHeader(e, newtitle) {
    this.props.callback(newtitle)
  }

  chooseCate(e, id, name) {
    let paging = this.state.paging
    let checktab = {}
    checktab[id] = true
    paging['searchid_appdist'] = id
    paging['searchcat_name'] = name 
    this.setState({checktab: checktab, paging: paging}, () => {this.fetchData()})
  }

  componentDidMount() {
    this.fetchData()
  }

  fetchData() {
    let paging = this.state.paging
    if (this.state.paging && !this.state.paging.searchid_appdist && this.state.categories && this.state.categories.constructor === Array && this.state.categories[0] && this.state.categories[0]['_id']) {
      paging['searchid_appdist'] = this.state.categories[0]['_id']
      paging['searchcat_name'] = this.state.categories[0]['name'] 
    }
    this.setState({paging: paging}, () => {
      rxget(config.api_home_product, paging, {
        '1': (json) => {
          this.setState({ products: json.data.products }) }
      })
    })
  }

  render() {
  
    let arrCategories = []
    let widthtags = 100
    if (this.props.category && this.props.category.categories && this.props.category.categories.constructor === Array) {
      arrCategories = this.props.category.categories
      if (arrCategories.length > 0 ) {
        widthtags = 100 / arrCategories.length
      }
    }
    
    let detailtabheader = arrCategories.map((category) => (
      <div key={category._id} className={(this.state.checktab && this.state.checktab[category._id]) ? 'tabproductmain active' : 'tabproductmain'} style={{width: widthtags+"%"}} onClick={(e) => {this.chooseCate(e, category._id, category.name) }}>
        <div className='tabproduct-box'>
          <div className='tabproduct-text'>
            <span>{category.name}</span>
          </div>
        </div>
      </div>
    ))
    
    let products = this.state.products.map(product => (
      <div className='rx-product-item col-sm-lg col-sm-2 show' key={product._id}>
        <div className='rx-product-figure'>
          <div className='rx-product-figure-header'>
            <Link to={`/product/${product.slug}`}>
              <img className='lazyload' data-src={config.base_api + '/upload/image/' + product.img_landscape} alt='' title='' />
            </Link>
          </div>
          <div className='rx-product-figure-body'>
          <div className='figure-body-rating-star'>
              {rxCountStart(product.ratingsSumary)}
              <div className='rx-star-text'>({(typeof(product.ratings) !== 'undefined') && product.ratings.length} {(typeof(product.ratings) === 'undefined') && 0}  nhận xét)</div>
            </div>
            <div className='clearfix'></div>
            <div className='figure-body-title'>{product.name}</div>
            <div className='rx-relative'><div className='figure-body-price'>{rxcurrencyVnd(product.price)} <i className='rxprice-i'></i></div><div className='figure-body-freeship'></div></div>
          </div>
          <div className='rx-product-action'>
            <div className='rx-action-like'></div>
            <div className='rx-action-card'></div>
            <div className='rx-action-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(product._id, { amount: 1, data: product }) }}><span className='rx-icon rx-icon-cart'></span>Thêm vào giỏ hàng</div>
          </div>
        </div>
      </div>
    ))
    let search_cate = (this.state.paging.searchcat_name) ? this.state.paging.searchcat_name : 'all'
    return (
      <div>
        <div className=''>
          <div className='tabproduct'>
            <div> 
              {detailtabheader} 
            </div>
            <div className='tabproduct-panels active'> 
              <div className='row row-nopad-col rx-cat-product'>{products}</div> 
            </div>
            <div className='lk-show-all-box'> 
              <Link to={`/search/${rxChangeAlias(search_cate)}`}><div className='lk-button-show-all'>Xem tất cả</div></Link>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  cart: state.cart,
  category: state.category
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd
}

const ProductCate = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductCate_)

export default ProductCate