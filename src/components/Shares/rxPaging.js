/*eslint-disable */
import React, { Component } from 'react'

class RxPaging extends Component {
  constructor (props, context) {
    super(props, context)
    this.state = {
      paging: this.props.paging,
      count: this.props.count,
      length: this.props.length,
    }
  }
  UNSAFE_componentWillReceiveProps(nextProps, prevProps) {
    if (nextProps.paging !== this.state.paging) {
      this.setState({ paging: nextProps.paging })
    }
    if (nextProps.count !== this.state.count) {
      this.setState({ count: nextProps.count })
    }
    if (nextProps.length !== this.state.length) {
      this.setState({ length: nextProps.length })
    }
  }

  // P A G I N
  onclickFirst(e) {
    let paging = this.state.paging
    paging.pg_page = 1
    this.setState({ paging: paging }, () => { this.props.onChange(paging) })
  }
  onclickLast(e) {
    let paging = this.state.paging
    let countAll = this.state.count / this.state.paging.pg_size
    if (countAll > parseInt(countAll,10)) {

      paging.pg_page = parseInt(countAll,10) + 1 
    } else {
      paging.pg_page = countAll
    }
    this.setState({ paging: paging }, () => { this.props.onChange(paging) })
  }
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.props.onChange(paging) })
  }
  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.props.onChange(paging) })
  }
  onClickPaginCur(e) {
    let paging = this.state.paging
    if (this.state.length >= this.state.paging.pg_size) {
      paging.pg_page = e
    }
    else {
      paging.pg_page = e
    }
    this.setState({ paging: paging }, () => { this.props.onChange(paging) })
  }
  render() {
    let result = []
    let listData = this.state.count
    let total = 0
    if ((listData / this.state.paging.pg_size) > parseInt(listData / this.state.paging.pg_size,10)) {
      total = parseInt(listData / this.state.paging.pg_size,10) + 1
    } else {
      total = parseInt(listData / this.state.paging.pg_size,10)
    }
    result.push(<div key={'first'} className='adtable__paginback adtable__paginfirst' onClick={(e) => this.onclickFirst(e)}><i /></div>)
    if (this.state.paging.pg_page === 1) {
      result.push(<div key={'non-back'} className='adtable__paginback icon-arrow-left' />)
    } else {
      result.push(<div key={'back'} className='adtable__paginback adtable__paginnext__active' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left' /></div>)
    }
    if (total > 5) {
      if (this.state.paging.pg_page === 1) {
        for (let i = 1; i <= (total); i++) {
          if (i < 4) {
            result.push(<div key={i} className={this.state.paging.pg_page === i ? 'adtable__pagincurr adtable__pagincurr--active' : 'adtable__pagincurr'} onClick={(e) => this.onClickPaginCur(i)} >{i}</div>)
          } else if (i === 4) {
            result.push(<div key={i} className='adtable__pagingMore' >...</div>)
          } else if (i === total) {
            result.push(<div key={i} className={this.state.paging.pg_page === i ? 'adtable__pagincurr adtable__pagincurr--active' : 'adtable__pagincurr'} >{i}</div>)
          }
        }
      } else {
        for (let i = 1; i <= (total); i++) {
          if (i === this.state.paging.pg_page - 1) {
            result.push(<div key={i} className='adtable__pagingMore' >...</div>)
          } else if ((i === this.state.paging.pg_page) || (i === this.state.paging.pg_page + 1)) {
            result.push(<div key={i} className={this.state.paging.pg_page === i ? 'adtable__pagincurr adtable__pagincurr--active' : 'adtable__pagincurr'} onClick={(e) => this.onClickPaginCur(i)} >{i}</div>)

          } else if (i === total) {
            result.push(<div key={i} className={this.state.paging.pg_page === i ? 'adtable__pagincurr adtable__pagincurr--active' : 'adtable__pagincurr'} onClick={(e) => this.onClickPaginCur(i)} >{i}</div>)
          
          } else if (i === this.state.paging.pg_page + 2) {
            result.push(<div key={i} className='adtable__pagingMore' >...</div>)

          }
        }
      }
    } else if (total === 0) {
      result.push(<div key={'none'} className='adtable__pagincurr adtable__pagincurr--active'>{this.state.paging.pg_page}</div>)
    } else {
      for (let i = 1; i <= (total); i++) {
        result.push(<div key={i} className={this.state.paging.pg_page === i ? 'adtable__pagincurr adtable__pagincurr--active' : 'adtable__pagincurr'} onClick={(e) => this.onClickPaginCur(i)}>{i}</div>)
      }
    }

    if (this.state.paging.pg_page < total) {
      result.push(<div key={'next'} className='adtable__paginnext adtable__paginnext__active' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right' /></div>)
    } else {
      result.push(<div key={'non-next'} className='adtable__paginnext icon-arrow-right ' />)
    }
    result.push(<div key={'last'} className='adtable__paginnext adtable__paginlast ' onClick={(e) => this.onclickLast(e)}><i /></div>)

    return result
  }
}

RxPaging.defaultProps = {  }

export default RxPaging
