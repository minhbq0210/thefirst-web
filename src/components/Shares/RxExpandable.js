import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
polyfill()

class RxExpandable extends Component {  
  constructor(props, context) {
    super(props, context)    
    this.state = { 
      toggle: this.props.toggle,
      minHeight: this.props.minHeight,
      maxHeight: this.props.maxHeight
    }
  }

  toggle() {
    this.setState({toggle: !this.state.toggle})
  }

  render() {
    return (
      <div className='rxexpand-wrap' style={{height: this.state.toggle? this.state.maxHeight: this.state.minHeight}}>
        {this.props.children}
        <div className={'rxexpand-toggle ' + (this.state.toggle === true? 'rx-active': '')} onClick={() => this.toggle()}></div>
      </div>
    )
  }
}

RxExpandable.defaultProps = { onToggle: () => {}, toggle: false, minHeight: '100px', maxHeight: 'auto' }

export default RxExpandable