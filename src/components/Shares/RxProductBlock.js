import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { polyfill }  from 'es6-promise'
import { connect } from 'react-redux'

import { rxcurrencyVnd } from './../../classes/ulti'
import { cartClear, cartAdd } from './../../redux'
import { rxCountStart } from './../Helpers/RxCountStart.js'

polyfill()
const config = global.rxu.config

class ProductBlock_ extends Component {
  constructor(props) {
    super(props)
    this.state = {      
      rxsize:  this.props.rxsize  || 4,
      rxpagin: this.props.rxpagin || ''
    }

    this.state = this.props.staticContext || { products: [] }
    this.state.products = this.state.products || []
  }

  onClickHeader(e, newtitle) {
    this.props.callback(newtitle)
  }

  render() {
    let products = this.props.products.map((product, index) => {
      if (index > (this.state.rxsize - 1)) {
        return (<span key={index}></span>)
      } else {
        return (        
          <div className='rx-product-item col-sm-lg col-sm-5c show' key={product._id}>
            <div className='rx-product-figure'>
              { product.price_discount ? <div className='rx-product-sale-of'><p><span>-{Math.floor((product.price_discount - product.price)/product.price_discount*100)}</span>%</p></div> : <div></div>}
              <div className='rx-product-figure-header'>
                <Link to={`/product/${product.slug}`}>
                  <img className='lazyload' data-src={config.base_api + '/upload/image/' + product.img_landscape} alt='' title='' />
                </Link>
              </div>
              <div className='rx-product-figure-body'>
                <Link to={`/product/${product.slug}`}>
                  <div className='figure-body-rating-star'>
                    {rxCountStart(product.ratingsSumary)}
                    <div className='rx-star-text'>({(typeof(product.ratings) !== 'undefined') && product.ratings.length} {(typeof(product.ratings) === 'undefined') && 0}  nhận xét)</div>
                  </div>
                  <div className='clearfix'></div>
                  <div className='figure-body-title'>{product.name}</div>
                  <div className='rx-relative'><div className='figure-body-price'>{rxcurrencyVnd(product.price)} <i className='rxprice-i'></i> { product.price_discount ?<span className='price-discount'>{rxcurrencyVnd(product.price_discount)}<i className='rxprice-i'></i></span>:<span></span>}</div><div className='figure-body-freeship'></div></div>
                </Link>
              </div>
              <div className='rx-product-action'>
                <div className='rx-action-like'></div>
                <div className='rx-action-card'></div>
                <div className='rx-action-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(product._id, { amount: 1, data: product }) }}><span className='rx-icon rx-icon-cart'></span>Thêm vào giỏ hàng {}</div>
              </div>
            </div>
          </div>)
      }      
    })
    return (
      <div>
        <div>
          <div>{ this.props.rxtitle && <h2>{this.props.rxtitle}</h2> }</div>
          <div className='row row-nopad-col'>{products}</div>
          <div className='clearfix'></div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  cart: state.cart
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd
}

const ProductBlock = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductBlock_)

export default ProductBlock