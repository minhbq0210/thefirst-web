import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { withTranslation } from 'react-i18next'

polyfill()

const config = global.rxu.config

class RxPrintLabel extends Component {

  constructor(props) {
    super(props)
    this.state = { editingData: this.props.editingData || {}, totalcards: this.props.totalcards, logo: this.props.logo }
  }

  componentDidMount() {
  }


  render() {
    let address = this.state.editingData.address ? this.state.editingData.address[0] : {street: '', county: '', city: ''}
    let url = this.state.logo['img_landscape'] ? (config.base_api + '/upload/image/' + this.state.logo['img_landscape']) : 'http://www.annawholesale.com.au/images/static/LOGO-ANNA-WHOLSALE.jpg'
    return (
      <div className='rxpopup-addnew-cus__ print' style={{minHeight: 'unset'}}>
        <div className='adblock'>
          <div className='adblock__body'>
            <div className='adblock__inner'>
              <div className='adblock__label'>
                <div className='adblock__label-logo'>
                  <img data-src={url} alt='' className='lazyload'/>
                </div>
                <div className='adblock__label-sale'>
                  <div>ANNASTORE WHOLESALE - HANG UC XACH TAY</div>
                  <div>29 STREET 51, WARD BINH THUAN, DISTRICT 7, HO CHI MINH</div>
                </div>
                <div className='adblock__label-cusinfo'>TO
                  <div>{global.rxu.get(this.state.editingData, ['name'])}</div>
                  <div>{global.rxu.get(address, ['street'], '')}, {global.rxu.get(address, ['county'], '')}, {global.rxu.get(address, ['city'], '')}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation('translations')(RxPrintLabel)