import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'

polyfill()

class CarouselLeftArrow extends Component {
  render() {
    return (
      <div className='jcarousel-control-prev carousel__arrow' onClick={this.props.onClick}>›</div>
    );
  }
}

export default CarouselLeftArrow