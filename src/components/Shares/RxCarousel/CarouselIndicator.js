import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'

polyfill()

class CarouselIndicator extends Component {
  render() {
    return ( <li><a className={ this.props.index === this.props.activeIndex ? "carousel__indicator carousel__indicator--active" : "carousel__indicator" } onClick={this.props.onClick}>_</a></li> )
  }
}

export default CarouselIndicator