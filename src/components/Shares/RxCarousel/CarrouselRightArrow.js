import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'

polyfill()

class CarouselRightArrow extends Component {
  render() {
    return (
      <div className='jcarousel-control-next carousel__arrow' onClick={this.props.onClick}>‹</div>
    );
  }
}

export default CarouselRightArrow