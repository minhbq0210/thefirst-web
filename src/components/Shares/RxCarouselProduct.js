import React, { Component } from 'react'
import ReactDOM from 'react-dom'
// import { Link } from 'react-router-dom'

class RxCarouselProduct extends Component {  

  constructor(props) {
    super(props)
    this.state = { dragStart: 0, dragStartTime: new Date(), index: 0, lastIndexs: 0, transition: false }
    this.state.children = this.props.children || []
  }

  componentWillMount() {
    const { selected } = this.props;

    this.setState({
      index: selected,
      lastIndexs: selected,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.children) {
      this.setState({ children: nextProps.children })
    }
    const { selected } = this.props;
    if (selected !== nextProps.selected) {
      this.goToSlide(nextProps.selected);
    }
    this.setState({children: nextProps.children})
  }

  getDragX(event, isTouch) {
    return isTouch ?
      event.touches[0].pageX :
      event.pageX;
  }

  handleDragStart(event, isTouch) {
    const x = this.getDragX(event, isTouch);

    this.setState({
      dragStart: x,
      dragStartTime: new Date(),
      transition: false,
      slideWidth: ReactDOM.findDOMNode(this.refs.slider).offsetWidth,
    });
  }

  handleDragMove(event, isTouch) {
    const {
      dragStart,
      lastIndexs,
      slideWidth,
    } = this.state;

    const x = this.getDragX(event, isTouch);
    const offset = dragStart - x;
    const percentageOffset = offset / slideWidth;
    const newIndex = lastIndexs + percentageOffset;
    const SCROLL_OFFSET_TO_STOP_SCROLL = 30;

    // Stop scrolling if you slide more than 30 pixels
    if (Math.abs(offset) > SCROLL_OFFSET_TO_STOP_SCROLL) {
      event.stopPropagation();
      // event.preventDefault();
    }

    this.setState({
      index: newIndex,
    });
  }

  handleDragEnd() {
    const {
      children,
    } = this.props;
    const {
      dragStartTime,
      index,
      lastIndexs,
    } = this.state;

    const timeElapsed = new Date().getTime() - dragStartTime.getTime();
    const offset = lastIndexs - index;
    const velocity = Math.round(offset / timeElapsed * 10000);

    let newIndex = Math.round(index);

    if (Math.abs(velocity) > 5) {
      newIndex = velocity < 0 ? lastIndexs + 1 : lastIndexs - 1;
    }

    if (newIndex < 0) {
      newIndex = 0;
    } else if (newIndex >= children.length) {
      newIndex = children.length - 1;
    }

    this.setState({
      dragStart: 0,
      index: newIndex,
      lastIndexs: newIndex,
      transition: true,
    });
  }

  goToSlide(index, event) {
    const {
      children,
      loop,
    } = this.props;

    if (event) {
      // event.preventDefault();
      event.stopPropagation();
    }

    if (index < 0) {
      index = loop ? children.length - 1 : 0;
    } else if (index >= children.length) {
      index = loop ? 0 : children.length - 1;
    }

    this.setState({
      index: index,
      lastIndexs: index,
      transition: true,
    })
  }

  renderNav() {
    const { lastIndexs } = this.state
    const nav = this.state.children.map((slide, i) => {
    const buttonClasses = i === lastIndexs ? 'Slider-navButton Slider-navButton--active' : 'Slider-navButton';
    return (
      <div
        className={ buttonClasses }
        key={ i }
        onClick={ (event) => this.goToSlide(i, event) } >
        <div className='lk-img-product-block'>
          <img data-src={(slide && slide.props && slide.props.children && slide.props.children.props && slide.props.children.props.src) ? slide.props.children.props.src : ''} alt=''  className='lk-img-product lazyload'/>
        </div>
      </div>
    );
    })

    return (
      <div className={this.state.children.length > 4 ? 'Slider-nav block' : 'Slider-nav'}>{ nav }</div>
    );
  }

  renderArrows() {
    const {
      // children,
      // loop,
      showNav,
    } = this.props;
    const { lastIndexs } = this.state;
    const arrowsClasses = showNav ? 'Slider-arrows-product' : 'Slider-arrows-product Slider-arrows--noNav';
    
    return (
      <div className={ arrowsClasses }>
          <div className='Slider-arrow-product Slider-arrow-product--left' onClick={ (event) => this.goToSlide(lastIndexs - 1, event) }><span className='iconanna-arrow-back mlkicon-Arrow'></span></div>   
          <div className='Slider-arrow-product Slider-arrow-product--right' onClick={ (event) => this.goToSlide(lastIndexs + 1, event) }><span className='iconanna-arrow-next mlkicon-Arrow'></span></div>
      </div>
    );
  }

  render() {
    const {
      children,
      showArrows,
      showNav,
    } = this.props;
    const {
      index,
      transition,
    } = this.state;


    const slidesStyles = {
      width: `${ 100 * Object.keys(children).length }%`,
      transform: `withTranslationX(${ -1 * index * (100 / Object.keys(children).length) }%)`,
    };
    const slidesClasses = transition ? 'Slider-single rx-slider-custom-single Slider-slides--transition-single' : 'Slider-single rx-slider-custom-single';

    return (
      <div className='Slider rx-slider-custom' ref='slider'>
        {children.length>0 &&(showArrows ? this.renderArrows() : null) }
        { children.length>0 &&(showNav ? this.renderNav() : null) }

        <div
          className='Slider-inner'
          onTouchStart={ (event) => this.handleDragStart(event, true) }
          onTouchMove={ (event) => this.handleDragMove(event, true) }
          onTouchEnd={ () => this.handleDragEnd(true) }>
          <div
            className={ slidesClasses }
            style={ slidesStyles }>
            { children }
          </div>
        </div>
      </div>
    );
  }
}

RxCarouselProduct.defaultProps = {
  loop: false,
  selected: 0,
  showArrows: true,
  showNav: true,
}

export default RxCarouselProduct