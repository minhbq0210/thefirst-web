import React, { Component } from 'react'
import ReactDOM from 'react-dom'
// import { Link } from 'react-router-dom'

class RxCarousel extends Component {  

  constructor(props) {
    super(props)
    this.state = { dragStart: 0, dragStartTime: new Date(), index: 0, lastIndexs: 0, transition: false }
  }

  componentWillMount() {
    const { selected } = this.props;

    this.setState({
      index: selected,
      lastIndexs: selected,
    });
  }

  componentWillReceiveProps(nextProps) {
    const { selected } = this.props;

    if (selected !== nextProps.selected) {
      this.goToSlide(nextProps.selected);
    }
  }

  getDragX(event, isTouch) {
    return isTouch ?
      event.touches[0].pageX :
      event.pageX;
  }

  handleDragStart(event, isTouch) {
    const x = this.getDragX(event, isTouch);

    this.setState({
      dragStart: x,
      dragStartTime: new Date(),
      transition: false,
      slideWidth: ReactDOM.findDOMNode(this.refs.slider).offsetWidth,
    });
  }

  handleDragMove(event, isTouch) {
    const {
      dragStart,
      lastIndexs,
      slideWidth,
    } = this.state;

    const x = this.getDragX(event, isTouch);
    const offset = dragStart - x;
    const percentageOffset = offset / slideWidth;
    const newIndex = lastIndexs + percentageOffset;
    const SCROLL_OFFSET_TO_STOP_SCROLL = 30;

    // Stop scrolling if you slide more than 30 pixels
    if (Math.abs(offset) > SCROLL_OFFSET_TO_STOP_SCROLL) {
      event.stopPropagation();
      // event.preventDefault();
    }

    this.setState({
      index: newIndex,
    });
  }

  handleDragEnd() {
    const {
      children,
    } = this.props;
    const {
      dragStartTime,
      index,
      lastIndexs,
    } = this.state;

    const timeElapsed = new Date().getTime() - dragStartTime.getTime();
    const offset = lastIndexs - index;
    const velocity = Math.round(offset / timeElapsed * 10000);

    let newIndex = Math.round(index);

    if (Math.abs(velocity) > 5) {
      newIndex = velocity < 0 ? lastIndexs + 1 : lastIndexs - 1;
    }

    if (newIndex < 0) {
      newIndex = 0;
    } else if (newIndex >= children.length) {
      newIndex = children.length - 1;
    }

    this.setState({
      dragStart: 0,
      index: newIndex,
      lastIndexs: newIndex,
      transition: true,
    });
  }

  goToSlide(index, event) {
    const {
      children,
      loop,
    } = this.props;

    if (event) {
      // event.preventDefault();
      event.stopPropagation();
    }

    if (index < 0) {
      index = loop ? children.length - 1 : 0;
    } else if (index >= children.length) {
      index = loop ? 0 : children.length - 1;
    }

    this.setState({
      index: index,
      lastIndexs: index,
      transition: true,
    })
  }

  renderNav() {
    const { children } = this.props;
    const { lastIndexs } = this.state;

    const nav = children.map((slide, i) => {
      const buttonClasses = i === lastIndexs ? 'Slider-navButton Slider-navButton--active' : 'Slider-navButton';
      return (
        <button
          className={ buttonClasses }
          key={ i }
          onClick={ (event) => this.goToSlide(i, event) } />
      );
    })

    return (
      <div className='Slider-nav'>{ nav }</div>
    );
  }

  renderArrows() {
    const {
      // children,
      // loop,
      showNav,
    } = this.props;
    const { lastIndexs } = this.state;
    const arrowsClasses = showNav ? 'Slider-arrows' : 'Slider-arrows Slider-arrows--noNav';
    
    return (
      <div className={ arrowsClasses }>
          <div className='Slider-arrow Slider-arrow--left' onClick={ (event) => this.goToSlide(lastIndexs - 1, event) }><span className='mlkicon-ArrowBack mlkicon-Arrow'></span></div>   
          <div className='Slider-arrow Slider-arrow--right' onClick={ (event) => this.goToSlide(lastIndexs + 1, event) }><span className='mlkicon-ArrowNext mlkicon-Arrow'></span></div>
      </div>
    );
  }

  render() {
    const {
      children,
      showArrows,
      showNav,
    } = this.props;

    const {
      index,
      transition,
    } = this.state;


    const slidesStyles = {
      width: `${ 100 * Object.keys(children).length }%`,
      transform: `withTranslationX(${ -1 * index * (100 / Object.keys(children).length) }%)`,
    };
    const slidesClasses = transition ? 'Slider-single rx-slider-custom-single Slider-slides--transition-single' : 'Slider-single rx-slider-custom-single';

    return (
      <div className='Slider rx-slider-custom' ref='slider'>
        { showArrows ? this.renderArrows() : null }
        { showNav ? this.renderNav() : null }

        <div
          className='Slider-inner'
          onTouchStart={ (event) => this.handleDragStart(event, true) }
          onTouchMove={ (event) => this.handleDragMove(event, true) }
          onTouchEnd={ () => this.handleDragEnd(true) }>
          <div
            className={ slidesClasses }
            style={ slidesStyles }>
            { children }
          </div>
        </div>
      </div>
    );
  }
}

RxCarousel.defaultProps = {
  loop: false,
  selected: 0,
  showArrows: true,
  showNav: true,
}

export default RxCarousel