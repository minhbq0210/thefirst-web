import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose } from './../../redux'

import { rxget } from './../../classes/request'
import { rxgetdate, rxcurrencyVnd } from './../../classes/ulti'

const config = global.rxu.config
// const numberWithCommas = (x) => {
//   return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// }

class CustomerOrder_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [],
      editingData: {},
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  componentDidMount() {
    this.fetchAlldata()
  }

  fetchAlldata() {
    this.fetchData()
  }

  fetchData() {
    let tempPaging = this.state.paging
    if (this.props.auth && this.props.auth.user && this.props.auth.user.customerid) {
      tempPaging.customerid = this.props.auth.user.customerid
    }
    this.setState({ paging: tempPaging }, () => {
      rxget(config.api_customer_allorder, this.state.paging, {
        '1': (json) => { 
          this.setState({ data: json.data })
        }
      })
    })
  }

  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  render() {

    let dataorder = []
    if (this.state.data && this.state.data.length > 0) {
      dataorder = this.state.data.map(perdata => (
        <tr key={perdata._id}>         
          <td className='lk-action-edit'><Link to={{ pathname: '/customer/tracking/', id: perdata._id }}>{perdata._id}</Link></td>
          <td>{rxgetdate(perdata.created_at)}</td>
          {(perdata.status_confirm !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>Chưa xác thực</span></td>}
          {(perdata.status_confirm === 1) && <td><span className='lk-status-box lk-status-confirm'>Đã xác thực</span></td>}   
          {(perdata.status_ship !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>Chưa giao hàng</span></td>}
          {(perdata.status_ship === 1) && <td><span className='lk-status-box lk-status-confirm'>Đã giao hàng</span></td>}        
          {(perdata.status_order_payment !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>Chờ xử lý</span></td>}
          {(perdata.status_order_payment === 1) && <td><span className='lk-status-box lk-status-confirm'>Đã thanh toán</span></td>}
          {(perdata.type_payment !== 2) && <td><span className='lk-status-box lk-status-confirm'>Có</span></td>}
          {(perdata.type_payment === 2) && <td><span className='lk-status-box lk-status-noconfirm'>Không</span></td>}      
          <td><span className='table-total-money'>{rxcurrencyVnd(perdata.price)} </span></td>
          <td>{perdata.type_channel_name}</td>
        </tr>
      ))
    }

    return (
      <div className='lk-customer-page'>
        <div className='row rx-row-nomar'>
          <div className='col-sm col-md-12'>
            <div className='rxcustomer-title'>Quản lý đơn hàng</div>
              
            <div className='admin-table-wrap'>
              <table className='admin-table-product preset'>
                <thead>
                  <tr>
                    <th>Mã</th>
                    <th>Ngày đặt</th>
                    <th>Xác thực</th>
                    <th>Giao hàng</th>
                    <th>Thanh toán</th>                  
                    <th>COD</th>
                    <th>Tổng tiền</th>
                    <th>Kênh</th>
                  </tr>
                </thead>
                <tbody>{dataorder}</tbody>
              </table></div>
              <div className='admin-table-pagination'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>

          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose
}

const CustomerOrder = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerOrder_)

export default CustomerOrder