import React, { Component } from 'react'
import { Switch } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import { Link } from 'react-router-dom'

import { connect } from 'react-redux'
import { logoutAction } from './../../redux'

// let adminCSS = '<style>.main-container {padding: 0px; margin: 0px; width: 100%;} .mainhead-container {display: none;} .mainfooter-container {display: none;}</style>'

class Customer_ extends Component {
  
  constructor (props) {
    super(props)
    this.state = {
      chooseSelect: {pg_page: 1, pg_sum: 1}
    }
  }  

  onClickLogout(e) {
    this.props.history.push('/')
    this.props.logoutAction()
  }

  render() {
    return (
      <div className='lk-customer-main'>
        <div className='lk-customer-topnav'></div>
        <div className='lk-customer-mainnav'>
          <div className='lk-customer-mainnav-inner'>        
            { this.props.auth ? <div className='lk-customer-mainnav-name'>Chào {(this.props.auth.user.firstname) ? this.props.auth.user.firstname : 'Bạn'}</div> : <div></div>}
            <div className='lk-customer-mainnav-item'><Link to='/customer/tracking'><i className='icon-notebook nav-icon'></i>Tra cứu đơn hàng</Link></div>
            {(this.props.auth && this.props.auth.user && Object.keys(this.props.auth.user).length !== 0) && <div className='lk-customer-mainnav-item'><Link to='/customer/order'><i className='icon-notebook nav-icon'></i>Quản lý đơn hàng</Link></div>}
            <div className='lk-customer-mainnav-item'><Link to='/customer/favorite'><i className='icon-notebook nav-icon'></i>Sản phẩm yêu thích</Link></div>
            {(this.props.auth && this.props.auth.user && Object.keys(this.props.auth.user).length !== 0) && <div className='lk-customer-mainnav-item'><Link to='/customer/purchased'><i className='icon-notebook nav-icon'></i>Sản phẩm đã mua</Link></div>}
            {(this.props.auth && this.props.auth.user && Object.keys(this.props.auth.user).length !== 0) && <div className='lk-customer-mainnav-item'><Link to='/customer/account'><i className='icon-notebook nav-icon'></i>Thông tin tài khoản</Link></div>}
            {(this.props.auth && this.props.auth.user && Object.keys(this.props.auth.user).length !== 0) && <div className='lk-customer-mainnav-item'><a onClick={(e) => this.onClickLogout()}><i className='icon-notebook nav-icon'></i>Đăng xuất</a></div>}
          </div>
        </div>

        <div className='lk-customer-container'>
          <div className='admin-container-inner'>
            <Switch>{renderRoutes(this.props.route.routes)}</Switch>
          </div>
        </div>        
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  auth: state.auth
})

const mapDispatchToProps = {  
  logoutAction
}

const Customer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Customer_)

export default Customer