import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose } from './../../redux'

import { rxget } from './../../classes/request'
import { rxgetdate, rxcurrencyVnd } from './../../classes/ulti'

const config = global.rxu.config
// const numberWithCommas = (x) => {
//   return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// }

class CustomerOrderTracking_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [],
      editingData: {},
      paging: { st_col: 'created_at', st_type: -1},
      msg: '',
      cardData: [],
      checktracking: false
    }
  }

  onBlurData(e, name) {
    let params = this.state.paging
    params[name] = e.target.value
    this.setState({ paging: params })    
  }

  componentDidMount() {
    let tempPaging = this.state.paging
    if (this.props.location.id) {
      tempPaging.orderid = this.props.location.id
      this.setState({ paging: tempPaging }, () => {
        this.fetchData()
      })
    }
  }

  fetchData() {
    rxget(config.api_home_order, this.state.paging, {
      '1': (json) => { 
        if (json.data.order) {
          var cardDetail = {}
          if (json.data && json.data.order && json.data.order.detail) {
            cardDetail = JSON.parse(json.data.order.detail)['carts']
          }
          this.setState({ data: json.data.order, msg: '', checktracking: true, cardData: cardDetail })  
        } else {
          this.setState({ msg: 'Mã đơn hàng không tồn tại' })
        }
      },
      '-2': (json) => {
        this.setState({ msg: 'Thông tin không chính xác !' })
      }
    })
  }

  onClickTrackingOrder(e) {
    if (typeof(this.state.paging['orderid'] !== 'undefined' )) {
      this.fetchData()  
    } else {
      this.setState({ msg: 'Vui lòng điền mã đơn hàng' })
    }
  }

  getDate(timestamp) {
    let dayarr = ['Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy']
    timestamp = timestamp * 1000
    let u = new Date(timestamp)  
    let tempstr  = ('0' + u.getUTCDate()).slice(-2) + '-' + ('0' + (u.getUTCMonth() + 1)).slice(-2) + '-' + u.getUTCFullYear()
    let tempstrdate = dayarr[u.getDay()] + ', ' + tempstr
    return tempstrdate
  }

  findTime(index, arrdata) {
    if (arrdata.length > 0) {
      var objdata = arrdata.filter(x => x.status === index)[0]
      var timeupdate = (objdata && objdata.time) ? rxgetdate(objdata.time) : ''
      return timeupdate
    } else {
      return ''
    }
  }

  render() {
    // Data info card in order
    let detailcards = []
    if (this.state.cardData) {
      detailcards = Object.keys(this.state.cardData).map((key) => (
        <tr key={key}>
          <td>
            <Link to={`/product/${this.state.cardData[key]['data']['slug']}`}>{this.state.cardData[key]['data']['name']}</Link>
          </td>
          <td>
            <div className='lk-text-center'>{this.state.cardData[key]['amount']}</div>
          </td>
        </tr>
      ))   
    }

    let nameProcess = [<span key={0}>ĐẶT HÀNG THÀNH CÔNG</span>]
    if (this.state.data && this.state.data.status_order) {
      switch(this.state.data.status_order) {
        case 1: nameProcess = [<span key={0}>ĐẶT HÀNG THÀNH CÔNG</span>]; break;
        case 2: nameProcess = [<span key={0}>ĐANG ĐÓNG GÓI</span>]; break;
        case 3: nameProcess = [<span key={0}>ĐANG ĐÓNG GÓI</span>]; break;
        case 4: nameProcess = [<span key={0}>ĐANG GIAO HÀNG</span>]; break;
        case 5: nameProcess = [<span key={0}>ĐANG GIAO HÀNG</span>]; break;
        case 6: nameProcess = [<span key={0}>GAO HÀNG THÀNH CÔNG</span>]; break;
        default: nameProcess = [<span key={0}>ĐẶT HÀNG THÀNH CÔNG</span>]; break;   
      }
    }

    return (
      <div className='lk-customer-page'>
        <div className='row rx-row-nomar'>
          <div className='col-sm col-md-12'>
            <div className='rxcustomer-title'>Tra cứu tình trạng đơn hàng</div>
            
            {this.state.checktracking && <div className='admin-table-wrap'>
              <div className='lk-order-tracking-main-block'>
              <div className='lk-order-tracking-main-title'>Mã đơn hàng: {this.state.data['_id']}</div>
              <div className='lk-order-tracking-main-title'>Tình trạng đơn hàng: <span>{nameProcess}</span></div>
              <div className='lk-header-progress-container'>
                <ol className='lk-header-progress-list'>
                  <li className='lk-header-progress-item done'><div className='lk-process-item-block'><b>Đặt hàng thành công</b><div className='lk-process-item-time'><span>{this.findTime(1, this.state.data['history_order'])}</span></div></div></li>
                  <li className={(typeof(this.state.data) !== 'undefined' && this.state.data['status_order'] < 2) ? 'lk-header-progress-item todo' : 'lk-header-progress-item done' }><div className='lk-process-item-block'><b>Người bán đã tiếp nhận đơn hàng</b><div className='lk-process-item-time'><span>{this.findTime(2, this.state.data['history_order'])}</span></div></div></li>
                  <li className={(typeof(this.state.data) !== 'undefined' && this.state.data['status_order'] < 2) ? 'lk-header-progress-item todo' : 'lk-header-progress-item done' }><div className='lk-process-item-block'><b>Đang đóng gói</b><div className='lk-process-item-time'><span>{this.findTime(2, this.state.data['history_order'])}</span></div></div></li>
                  <li className={(typeof(this.state.data) !== 'undefined' && this.state.data['status_order'] < 4) ? 'lk-header-progress-item todo' : 'lk-header-progress-item done' }><div className='lk-process-item-block'><b>Đang vận chuyển</b><div className='lk-process-item-time'><span>{this.findTime(4, this.state.data['history_order'])}</span></div></div></li>
                  <li className={(typeof(this.state.data) !== 'undefined' && this.state.data['status_order'] < 6) ? 'lk-header-progress-item todo' : 'lk-header-progress-item done' }><div className='lk-process-item-block'><b>Giao hàng thành công</b><div className='lk-process-item-time'><span>{this.findTime(6, this.state.data['history_order'])}</span></div></div></li>
                </ol>
              </div>
              </div>

              <div className='lk-order-tracking-main-block'>
                <div className='row top'>
                  <div className='col-lg-3 col-md-3 item'>
                    <div className='lk-item-padding-box'>
                      <div className='lk-itemtitle-padding-box'>Địa chỉ người nhận</div>
                      <p>{this.state.data['name']}</p>
                      <p>{this.state.data['phone']}</p>
                      <p>{this.state.data['address']}</p>
                    </div>
                  </div>

                  <div className='col-lg-3 col-md-3 item'>
                    <div className='lk-item-padding-box'>
                      <div className='lk-itemtitle-padding-box'>Thời gian giao hàng &amp; Vận chuyển</div>
                      <p>Vận chuyển có phí (dự kiến giao hàng vào {this.getDate((Number(this.state.data['created_at']) || 0) + 3*3600*24)} )</p>
                      <p>Phí vận chuyển: 50.000 </p>
                    </div>
                  </div>

                  <div className='col-lg-3 col-md-3 item'>
                    <div className='lk-item-padding-box'>
                      <div className='lk-itemtitle-padding-box'>Địa chỉ người nhận</div>
                      <p>{this.state.data['name']}</p>
                      <p>{this.state.data['phone']}</p>
                      <p>{this.state.data['address']}</p>
                    </div>
                  </div>

                  <div className='col-lg-3 col-md-3 item'>
                    <div className='lk-item-padding-box'>
                      <div className='lk-itemtitle-padding-box'>Phương thức thanh toán</div>
                      <p>Thanh toán tiền mặt khi nhận hàng</p>
                    </div>
                  </div>
                </div>
              </div>

              <div className='lk-order-tracking-main-block'>
                <div className='admin-table-wrap'>
                  <div className='lk-order-tracking-main-title'>Đơn hàng bao gồm</div>
                  <table className='admin-table-product preset'>
                    <thead>
                      <tr>
                        <th>Tên sản phẩm</th>
                        <th>Số lượng</th>
                      </tr>
                    </thead>
                    <tbody>
                      {detailcards}
                    </tbody>
                    <tfoot>
                      <tr>
                        <td><strong>Tổng</strong></td>
                        <td><div className='lk-text-center'><strong>{rxcurrencyVnd(this.state.data.price)} ₫</strong></div></td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>}
            
            <div>
              <div className='lk-order-tracking-main-title'>Tra cứu theo mã đơn hàng</div>
              <div className='lk-order-tracking-main-desc'>Điền vào các thông tin bên dưới để xem tình trạng vắn tắt của Đơn hàng</div>
              <div className='lk-order-tracking-main-form clearfix'>
                <div className='lk-order-tracking-text'><b>Mã đơn hàng</b></div>
                <div className='clearfix'>
                  <div className='lk-order-tracking-input-form'>
                    <input tabIndex='1' type='text' name='orderid' onChange={(e) => this.onBlurData(e, 'orderid')} className='lk-order-tracking-input fullwidth-input' />
                    <div className='lk-order-tracking-text cart-page-error'>{this.state.msg}</div>
                  </div>
                  <div className='lk-order-tracking-main-button' onClick={(e) => this.onClickTrackingOrder(e)}>Tra cứu</div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose
}

const CustomerOrderTracking = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerOrderTracking_)

export default CustomerOrderTracking