import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { favoriteDelete, rxnavToggle, rxnavClose } from './../../redux'

import { rxcurrencyVnd } from './../../classes/ulti'

const config = global.rxu.config

class CustomerFavorite_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      editingData: {},
      chooseSelect: {pg_page: 1, pg_sum: 1},
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onClickPaginBack(e) {
    let paging = this.state.chooseSelect
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ chooseSelect: paging }, () => { this.parseArrayPagin(Object.keys(this.props.favorite.favorites)) })
  }

  onClickPaginNext(e) {
    let paging = this.state.chooseSelect
    paging.pg_page += 1
    this.setState({ chooseSelect: paging }, () => { this.parseArrayPagin(Object.keys(this.props.favorite.favorites)) })
  }

  parseArrayPagin(array) {
    let arraypagin = []
    if (array) {
      let tempchooseSelect = this.state.chooseSelect
      tempchooseSelect.pg_sum = Math.ceil(array.length / 10)
      arraypagin = array.slice((this.state.chooseSelect.pg_page-1)*10, ((this.state.chooseSelect.pg_page-1)*10)+10)  
    }
    return arraypagin
  }

  render() {
    let tempCartDetail = []
    if (this.props.favorite) {
      tempCartDetail = this.parseArrayPagin(Object.keys(this.props.favorite.favorites)).map(key => (
        <div key={key} className='clearfix cart-page-product-row'>
          { (this.props.favorite.favorites[key] && this.props.favorite.favorites[key].data) && 
          <div>
            <div className='cart-page-product-name'>
              <div className='cart-page-product-rm' onClick={() => { this.props.favoriteDelete(this.props.favorite.favorites[key].id) }}>x</div>
              <Link to={`/product/${this.props.favorite.favorites[key].data.slug}`}>
                <img className='cart-page-product-img lazyload' alt='ico_default' data-src={config.base_api + '/upload/image/' + (this.props.favorite.favorites[key].data.img_landscape || 'ico_app_default.png')} />
              </Link>
              {this.props.favorite.favorites[key].data.name}<br/>

              <div className='cart-page-product-size'>Kích thước: XL</div>
              <div className='cart-page-product-color'>Màu sắc: XL</div>
              <div className='cart-page-product-price'>{rxcurrencyVnd(this.props.favorite.favorites[key].data.price)}<span className='cart-page-product-priceold'>{rxcurrencyVnd(this.props.favorite.favorites[key].data.price * 1.2)}</span></div>
            </div>
          </div> }
        </div>      
      ))
    }
    
    return (
      <div className='lk-customer-page'>
        <div className='row rx-row-nomar'>
          <div className='col-sm col-md-12'>
            <div className='rxcustomer-title'>Sản phẩm yêu thích</div>
            <div className='lk-cart-page-body'>{tempCartDetail}</div>
            <div className='admin-table-pagination admin-pagin-right'>
              {(this.state.chooseSelect.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
              <div className='pagin-curr'>{this.state.chooseSelect.pg_page}</div>
              {(this.state.chooseSelect.pg_sum >= this.state.chooseSelect.pg_page) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
            </div>
          </div>
        </div>  
      </div> 
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  favorite: state.favorite
})

const mapDispatchToProps = {  
  favoriteDelete,
  rxnavToggle,
  rxnavClose
}

const CustomerFavorite = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerFavorite_)

export default CustomerFavorite