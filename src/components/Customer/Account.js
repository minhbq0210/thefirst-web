import React, { Component } from 'react'

import { connect } from 'react-redux'
import { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose, loginAction } from './../../redux'

import { rxpost } from './../../classes/request'
// import { rxconfig } from './../../classes/ulti'

const config = global.rxu.config

class CustomerOrder_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      editingData: {},
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onClickDataUpdateSubmit(e) {
    let editingData = this.state.editingData
    if (this.props.auth && this.props.auth.user && this.props.auth.user._id) {
      editingData['_id'] = this.props.auth.user._id
      rxpost(config.api_user_edit_customer, editingData, {
        '1': (json) => {
          this.setState({ msg: '' })
          this.props.loginAction(json.data, {data: json.data })
          alert('Cập nhật thành công')
        }
      })
    } else {
      alert('Không thể cập nhật ')
    }
  }

  render() {
    return (
      <div className='lk-customer-page'>
        <div className='row rx-row-nomar'>
          <div className='col-sm col-md-12'>
            <div className='rxcustomer-title'>Thông tin khách hàng</div>
              {this.props.auth && 
                <div className='lk-customer-page-info-body col-sm-12 col-md-5 clearfix'>
                  <div>
                    <div className='cart-page-customer-name'>Họ tên</div>
                    <input tabIndex='1' type='text' name='fullname' defaultValue={this.props.auth.user.fullname || ''} onChange={(e) => this.onBlurData(e, 'fullname')} className='fullwidth-input' />
                  </div>
                  <div>
                    <div className='cart-page-customer-phone'>Địa chỉ email</div>
                    <input tabIndex='1' type='text' name='email' defaultValue={this.props.auth.user.email || ''} onChange={(e) => this.onBlurData(e, 'email')} className='fullwidth-input' disabled/>
                  </div>
                  <div>
                    <div className='cart-page-customer-name'>Số điện thoại</div>
                    <input tabIndex='1' type='text' name='phone' defaultValue={this.props.auth.user.phone || ''} onChange={(e) => this.onBlurData(e, 'phone')} className='fullwidth-input' />
                  </div>
                  <div>
                    <div className='cart-page-customer-name'>Địa chỉ giao hàng</div>
                    <input tabIndex='1' type='text' name='phone' defaultValue={this.props.auth.user.address || ''} onChange={(e) => this.onBlurData(e, 'address')} className='fullwidth-input' />
                  </div>

                  <div className='admin-cardblock-btns'>
                    <a tabIndex='1' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>Cập nhật</a>
                  </div>
                </div>

              }
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose,
  loginAction
}

const CustomerOrder = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerOrder_)

export default CustomerOrder