import React, { Component } from 'react'

import { connect } from 'react-redux'
import { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose } from './../../redux'

import { rxpost, rxgetLocal } from './../../classes/request'
// import { rxconfig } from './../../classes/ulti'

const config = global.rxu.config

class CustomerOrder_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      editingData: {},
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onClickOrderSubmit(e) {
    let editingData = this.state.editingData
    editingData['detail'] = rxgetLocal('rxcart')
    editingData['price'] = this.helpCalCart()

    this.setState({ editingData: editingData }, () => {
      rxpost(config.api_order, this.state.editingData, {
        '1': (json) => {
          this.props.cartClear()
          this.setState({ msg: '' })
          this.props.history.push("/")
        },
        '-2': (json) => {
          this.setState({ msg: 'Thiếu thông tin đặt hàng !' })
        }
      })
    })
  }

  helpCalCart() {
    let total = 0
    for (let key in this.props.cart.carts) {      
      if (this.props.cart.carts.hasOwnProperty(key)) {
        if (this.props.cart.carts[key] && this.props.cart.carts[key].data) {
          total += this.props.cart.carts[key].data.price * this.props.cart.carts[key].amount
        }
      }
    }

    return total
  }

  render() {
    return (
      <div className='rx-payment-page'>
        <div className='row rx-row-nomar'>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose
}

const CustomerOrder = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerOrder_)

export default CustomerOrder