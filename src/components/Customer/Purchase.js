import React, { Component } from 'react'
import { connect } from 'react-redux'
import { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose } from './../../redux'

import { rxget, rxpost } from './../../classes/request'
import { rxcurrencyVnd } from './../../classes/ulti'
import { rxCountStart } from './../Helpers/RxCountStart.js'
import RxRatingbox from './../Shares/RxRatingbox'

const config = global.rxu.config
// const numberWithCommas = (x) => {
//   return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// }

class CustomerPurchase_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [],
      editingData: {},
      iseditting: false,
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      msg: '',
      dataarrratings: [0,0,0,0,0],
      reviewsrating: {}
    }
  }

  onBlurData(e, name) {
    let editingData = (typeof(this.state.reviewsrating) !== 'undefined') ? this.state.reviewsrating : {}
    editingData[name] = e.target.value
    this.setState({ reviewsrating: editingData })    
  }

  componentDidMount() {
    this.fetchAlldata()
  }

  fetchAlldata() {
    this.fetchData()
  }

  fetchData() {
    let tempPaging = this.state.paging    
    if (this.props.auth && this.props.auth.user && this.props.auth.user.customerid) {
      tempPaging.customerid = this.props.auth.user.customerid      
    }
    this.setState({paging: tempPaging}, () => {
      rxget(config.api_customer_allproduct, this.state.paging, {
        '1': (json) => { 
          this.setState({ data: json.data })
        }
      })
    })  
  }

  onClickDataUpdateSubmit(objreviewsrating) {
    let paramsrating = {}
    let reviewsrating = objreviewsrating
    if (reviewsrating && reviewsrating !== 'null' && reviewsrating !== 'undefined') {
      paramsrating['ratings'] = {}
      paramsrating['ratings'] = reviewsrating
      paramsrating['ratings']['customerid'] = this.props.auth.user.customerid
      paramsrating['ratings']['name'] = this.props.auth.user.fullname
      paramsrating['ratings']['isCustomer'] = 1
    }
    paramsrating['_id'] = this.state.editingData._id
    rxpost(config.api_product_rating, paramsrating, {
      '1': (json) => { this.fetchData()}
    })
    this.onClickDataEdit({}, {})
    this.setState({ reviewsrating: {}, dataarrratings: [0,0,0,0,0], iseditting: false}) 
  }

  onClickDataUpdateCancel(e, perdata) {
    this.onClickDataEdit({}, {})
    this.setState({ reviewsrating: {}, dataarrratings: [0,0,0,0,0], iseditting: false}) 
  }

  onClickDataEdit(e, perdata) {
    let clone = JSON.parse(JSON.stringify(perdata))
    let objrating = {}
    if (typeof(perdata._id) !== 'undefined' && typeof(perdata.ratings) !== 'undefined' && perdata.ratings.length > 0) {
      let customerid = this.props.auth.user.customerid
      objrating = perdata.ratings.find(o => o.customerid === customerid )
      this.setState({reviewsrating: objrating})
    }
    this.setState({ editingData: clone, iseditting: true})
  }

  render() {
    let dataproduct = []
    if (this.state.data && this.state.data.length > 0) {
      dataproduct = this.state.data.map(perdata => (
        <div className='clearfix lk-customer-product-row'  key={perdata._id}>
          <div className='lk-box-product-img'>
            <img className='lazyload' data-src={config.base_api + '/upload/image/' + perdata.img_landscape} alt='' title='{perdata.name}' /> 
          </div>
          <div className='lk-box-product-info'>
            <div className='lk-card-order-lable-name'>{perdata.name} </div>
            <div className='lk-card-order-lable-desc rxprice'>{rxcurrencyVnd(perdata.price)} ₫</div>
          </div>
          <div className='lk-box-product-ratings'>
            
            <div className='clearfix lk-product-positon-start' onClick={(e) => this.onClickDataEdit(e, perdata)}>
              <div className='figure-body-rating-star'>
                {rxCountStart(perdata.ratingsSumary)}
              </div>
            </div>
            <div className='lk-product-command-text'>({perdata.ratings.length} nhận xét)</div>
            <div className='clearfix row lk-product-box-addcart '>
              <div className='rx-heart'></div>
              <div className='rx-action-addcart rx-action-addcart-lg lk-product-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(perdata._id, { amount: 1, data: perdata }) }}>
                <span className="rx-icon rx-icon-cart">
                </span>Thêm vào giỏ hàng
              </div>
            </div>
          </div>
        </div> 
      ))
    }

    return (
      <div className='lk-customer-page'>
        <div className='row rx-row-nomar'>
          {!this.state.iseditting &&
          <div className='col-sm col-md-12'>
            <div className='rxcustomer-title'>Sản phẩm đã mua</div>
            <div>{dataproduct}</div>
            <div className='admin-table-pagination'>
              {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
              <div className='pagin-curr'>{this.state.paging.pg_page}</div>
              {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
            </div>
          </div> }

          {this.state.iseditting &&
          <div className='col-sm col-md-12'>
            <div className='lk-main-rating-title'>
              <div className='rxcustomer-title'>Đánh giá sản phẩm {this.state.editingData.name}</div>
              <div className='lk-reviews-rating-close'><div className='admin-cardblock-formclose'onClick={(e) => this.onClickDataUpdateCancel(e, {})}>x</div></div>
            </div>
            <RxRatingbox reviewsrating={this.state.reviewsrating} infodata={this.state.editingData} onChange={(result) => {this.onClickDataUpdateSubmit(result)}} /> 
          </div> }
        </div>  
      </div> 
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose
}

const CustomerPurchase = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerPurchase_)

export default CustomerPurchase