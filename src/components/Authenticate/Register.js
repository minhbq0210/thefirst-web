import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import { rxpost } from './../../classes/request'
// import { rxconfig } from './../../classes/ulti'

const config = global.rxu.config
 
class AuthRegister extends Component {
	
	constructor(props) {
    super(props)
    this.state = { 
      editingData: {},
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onClickRegisterSubmit(e) {
    let editingData = this.state.editingData

    this.setState({ editingData: editingData }, () => {
      rxpost(config.api_user, this.state.editingData, {
        '1': (json) => {
          this.setState({ msg: '' })
          this.props.history.push("/login")
        },
        '-2': (json) => {
          var strmsg = ''
          if (json.msg === 'Dupplicate data') {
            strmsg = 'Email đã tồn tại'
          } else if (json.msg === 'Email invalid format') {
            strmsg = 'Email không đúng định dạng'
          } else {
            strmsg = 'Các trường không được bỏ trống'
          }
          this.setState({ msg: strmsg })
        }
      })
    })
  }

  render() {
    return (
      <div className='lk-login-customer-page'>
        <div className='row lk-row-nomar'>
          <div className='rxauthen-left-container lk-contact'></div>
          <div className='lk-page-customer-info lk-page-info-register'>
            <div className='lk-login-section-title-wrap clearfix'>
              <Link className='lk-login-section-title' to='login'>Đăng nhập</Link>
              <div className='lk-login-section-title rx-active'>Đăng ký</div>
            </div>

            <div className='lk-register-customer-inner'>
						  <div className='clearfix'>
                { this.state.msg && <div className='clearfix'><div className='lk-input-error' >{this.state.msg}</div></div> }
						    <div className='lk-input-group-row'>
                  <div>Email *</div>
                  <input tabIndex='1' type='text' name='email' onChange={(e) => this.onBlurData(e, 'email')} className='fullwidth-input' />
                </div>
                <div className='lk-input-group-row'>
                  <div>Mật khẩu *</div>
                  <input tabIndex='2' type='password' name='password' onChange={(e) => this.onBlurData(e, 'password')} className='fullwidth-input' />
                </div>
                <div className='lk-input-group-row'>
						      <div>Họ *</div>
						      <input tabIndex='3' type='text' name='lastname' onChange={(e) => this.onBlurData(e, 'lastname')} className='fullwidth-input' />
						    </div>
                <div className='lk-input-group-row'>
                  <div>Tên *</div>
                  <input tabIndex='4' type='text' name='firstname' onChange={(e) => this.onBlurData(e, 'firstname')} className='fullwidth-input' />
                </div>
						    <div className='lk-input-group-row'>
						      <div>Số điện thoại *</div>
						      <input tabIndex='5' type='text' name='phone' onChange={(e) => this.onBlurData(e, 'phone')} className='fullwidth-input' />
						    </div>
                <div className='lk-input-group-row'>
                  <div>Địa chỉ *</div>
                  <input tabIndex='6' type='text' name='address' onChange={(e) => this.onBlurData(e, 'address')} className='fullwidth-input' />
                </div>
                <div className='lk-input-group-row'>
                  <div>Quận/ Huyện *</div>
                  <input tabIndex='7' type='text' name='county' onChange={(e) => this.onBlurData(e, 'county')} className='fullwidth-input' />
                </div>
                <div className='lk-input-group-row'>
                  <div>Tỉnh/ Thành phố *</div>
                  <input tabIndex='8' type='text' name='city' onChange={(e) => this.onBlurData(e, 'city')} className='fullwidth-input' />
                </div>
						  </div><br/>
						  
						  <div className='lk-login-submit-section'>
						    <div tabIndex='10' className='lk-page-login-customer-submit' onClick={(e) => this.onClickRegisterSubmit(e)} onKeyPress={(e) => this.onClickRegisterSubmit(e)}>Đăng ký</div> 
						  </div>
						  <div className='lk-login-submit-section'></div>
						</div> 
          </div>
        </div>
      </div>
    )
  }
}

export default AuthRegister