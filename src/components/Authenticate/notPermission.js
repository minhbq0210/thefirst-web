/*eslint-disable */
import React, { Component } from 'react'

class notAuthenComponent extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  // R E N D E R S
  render () {
    return (
      <div className='adblock'>
        <div className='adblock__head'>
          <div className='adblock__title'>Không có quyền truy cập</div>
          <div className='adblock__desc'>Bạn không có quyền truy cập nội dung này !</div>
        </div>
        <div className='adblock__body'>
          <div className='adblock__inner'>
            
          </div>
        </div>
      </div>
    )
  }
}

export default notAuthenComponent
