import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import { connect } from 'react-redux'
import { loginAction, logoutAction } from './../../redux'
import { rxpost } from './../../classes/request'
// import { rxconfig } from './../../classes/ulti'

const config = global.rxu.config
 
class AuthLogin_ extends Component {
	
	constructor(props) {
    super(props)
    this.state = { 
      editingData: {},
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onClickLoginSubmit(e) {
    let editingData = this.state.editingData
    
    this.setState({ editingData: editingData }, () => {
      rxpost(config.api_authcutomer, this.state.editingData, {
        '1': (json) => {
          this.setState({ msg: '' })
          this.props.loginAction(json.data, {data: json.data })
          this.props.history.push("/")
        },
        '-2': (json) => {
          var strmsg = ''
          if (json.msg === 'Wrong input' || json.msg === 'Cant authorize!') {
            strmsg = 'Thông tin đăng nhập không chính xác'
          }
          this.setState({ msg: strmsg })
        }
      })
    })
  }

  render() {
    return (
      <div className='lk-login-customer-page'>
        <div className='row lk-row-nomar'>
          <div className='rxauthen-left-container lk-contact'></div>
          <div className='lk-page-customer-info lk-page-info-login'>
            <div className='lk-login-section-title-wrap clearfix'>
              <div className='lk-login-section-title rx-active'>Đăng nhập</div>              
              <Link className='lk-login-section-title' to='register'>Đăng ký</Link>
            </div>

            <div className='lk-login-customer-inner'>
						  <div>
                { this.state.msg && <div className='clearfix'><div className='lk-input-error' >{this.state.msg}</div></div> }
                <div className='lk-input-group-row'>
						      <div className='lk-cart-page-customer-name'>Email</div>
						      <input tabIndex='1' type='text' name='email' onChange={(e) => this.onBlurData(e, 'email')}  className='fullwidth-input' />
						    </div>

						    <div className='lk-input-group-row'>
						      <div className='lk-cart-page-customer-phone'>Mật khẩu</div>
						      <input tabIndex='2' type='password' name='phone' onChange={(e) => this.onBlurData(e, 'password')} className='fullwidth-input' />
						    </div>
						  </div><br/>
						  
              <div className='lk-login-submit-section'>
                <div className='lk-link-forgetpass'><a>Quên mật khẩu?</a></div>                   
              </div>

						  <div className='lk-login-submit-section'>
						    <div tabIndex='3' className='lk-page-login-customer-submit' onClick={(e) => { this.onClickLoginSubmit(e)}} onKeyPress={(e) => this.onClickLoginSubmit(e)}>Đăng nhập</div>                        
						  </div>						  
						</div> 

          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user
})

const mapDispatchToProps = {  
  logoutAction,
  loginAction
}

const AuthLogin = connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthLogin_)

export default AuthLogin