import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose } from './../../redux'

import { rxget, rxpost, rxgetLocal } from './../../classes/request'
import { rxcurrencyVnd } from './../../classes/ulti'

const config = global.rxu.config

class Payment_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      editingData: {},
      msg: '',
      msgerror: '',
      orderData: {},
      success: false,
      chkbox: false,
      total: 0
    }    
  }

  componentDidMount() {
    // If login
    setTimeout(() => { 
      if (this.props.auth && this.props.auth.user && this.props.auth.user._id) {
        let tempUser = this.props.auth.user
        let editingData = this.state.editingData   
        editingData['name'] = tempUser.fullname 
        editingData['phone'] = tempUser.phone      
        editingData['address'] = tempUser.address
        this.setState({ editingData: editingData })
      }
    }, 20)
  }
  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  parseCarts() {
    let editingData = this.state.editingData
    let arrcart = []
    if (this.props.cart && this.props.cart.carts) {
      for(let key in this.props.cart.carts) { arrcart.push(key)}
    }
    editingData['arrcart'] = arrcart
    this.setState({ editingData: editingData })  
  }

  postOrder() {
    this.parseCarts()
    rxpost(config.api_order, this.state.editingData, {
      '1': (json) => {
        this.props.cartClear()
        this.setState({ msg: '' })
        this.setState({ success: true})
        this.setState({ orderData: json.data})
      },
      '-2': (json) => {
        this.setState({ msg: 'Thiếu thông tin đặt hàng !' })
      }
    }) 
  }

  onClickApplyDiscount() {
    let params = {'search_code': this.state.editingData.discountcode}
    rxget(config.api_discount, params, {
      '1': (json) => {
        if (json.data.length > 0) {
          this.setState({ msg: '' })
          this.helpCalDiscount(json.data, this.helpCalCart()) 
        } else {
          this.setState({ msg: 'Mã giảm giá không chính xác' })
        }
      }
    })
  }

  onClickOrderSubmit(e) {
    console.log(1111111111111111111)
    let editingData = this.state.editingData
    editingData['detail'] = rxgetLocal('rxcart')
    editingData['price'] = this.helpCalCart()

    this.setState({ editingData: editingData }, () => {
      if (!this.state.chkbox) {
        this.postOrder()
      } else {
        let customerinfo = {}
        let arrname = (this.state.editingData['name'].length > 0) ? this.state.editingData['name'].split(" ") : []
        if (arrname.length > 1) {
          customerinfo['firstname'] = arrname[arrname.length - 1]
          customerinfo['lastname'] = arrname.slice(0,-1).join(" ")
        } else if (arrname.length === 1) {
          customerinfo['firstname'] = customerinfo['lastname'] = arrname[arrname.length - 1]
        } else {
          customerinfo['firstname'] = customerinfo['lastname'] = ''
        }
        customerinfo['email'] = this.state.editingData['email']
        customerinfo['password'] = this.state.editingData['password']
        customerinfo['phone'] = this.state.editingData['phone']
        customerinfo['address'] = this.state.editingData['address']
        rxpost(config.api_user, customerinfo, {
          '1': (json) => {
            this.setState({ msgerror: '' }, () => {this.postOrder()})
          },
          '-2': (json) => {
            if (json.msg === 'Dupplicate data') {
              this.setState({ msgerror: 'Email đã tồn tại' })
            } else if (json.msg === 'Email invalid format') {
              this.setState({ msgerror: 'Email không đúng định dạng' })
            } else {
              this.setState({ msgerror: 'Các trường không được bỏ trống' })
            }
          }
        })
      }
    })
  }

  helpCalCart() {
    let total = 0
    if (this.props.cart && this.props.cart.carts) {
      for (let key in this.props.cart.carts) {      
        if (this.props.cart.carts.hasOwnProperty(key)) {
          if (this.props.cart.carts[key] && this.props.cart.carts[key].data) {
            total += this.props.cart.carts[key].data.price * this.props.cart.carts[key].amount
          }
        }
      }
    }

    return total
  }

  helpCalDiscount(arrdiscount, priceold) {
    let objmeta = {}
    let pricenew = 0
    if (arrdiscount.length > 0 ){
      objmeta = arrdiscount[0]['metadiscount']
      if (objmeta['typepromotion'] === 1 ) {
        if (arrdiscount[0]['uselimit'] === false) {
          if ((arrdiscount[0]['amountdiscount'] - arrdiscount[0]['amountdiscountused']) > 0 ) {
            pricenew = priceold - objmeta['discountvalue']
          } else {
            this.setState({ msg: 'Đã hết số lượt giảm giá' })
            pricenew = priceold
          }
        } else {
          pricenew = priceold - objmeta['discountvalue']
        }
      } else if (objmeta['typepromotion'] === 2) {
        pricenew = priceold - (priceold * objmeta['discountvalue'] / 100) 
      } else if (objmeta['typepromotion'] ===  3) {
        pricenew = priceold - objmeta['discountvalue']
      }
    }
    this.setState({total: pricenew})
  }


  handleChangeChk(e, checkbox) {
    this.setState({chkbox: !checkbox})
  }

  getDate(timestamp) {
    let dayarr = ['Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy']
    timestamp = timestamp * 1000
    let u = new Date(timestamp)  
    let tempstr  = ('0' + u.getUTCDate()).slice(-2) + '-' + ('0' + (u.getUTCMonth() + 1)).slice(-2) + '-' + u.getUTCFullYear()
    let tempstrdate = dayarr[u.getDay()] + ', ' + tempstr
    return tempstrdate
  }
  getKey(key) {
    if (key !== 'undefined') {
      let tempkey = key.split('|')
      return tempkey[1]
    }
  }

  render() {
    let tempCartDetail = []
    if (this.props.cart && this.props.cart.carts) {
      if (Object.keys(this.props.cart.carts).length !== 0) {
        tempCartDetail = Object.keys(this.props.cart.carts).map(key => (
          <div key={key} className='clearfix cart-page-product-row'>
            { (this.props.cart.carts[key] && this.props.cart.carts[key].id) && 
            <div>
              <div className='cart-page-product-name'>
                <div className='cart-page-product-rm' onClick={() => { this.props.cartDelete(this.props.cart.carts[key].id) }}>x</div>
                <img className='cart-page-product-img' alt='ico_default' data-src={config.base_api + '/upload/image/' + (this.props.cart.carts[key].data.img_landscape || 'ico_app_default.png')} />
                {this.props.cart.carts[key].data.name}<br/>
                
                {this.props.cart.carts[key].data.options[0].name ? <div className='cart-page-product-size'>{this.props.cart.carts[key].data.options[0].name.name} : {this.props.cart.carts[key].data.options[this.getKey(key)].name.text}</div>
                 : <div></div> }
                <div className='cart-page-product-price'>{rxcurrencyVnd(this.props.cart.carts[key].data.price)}<span className='cart-page-product-priceold'>{this.props.cart.carts[key].data.price_discount ? rxcurrencyVnd(this.props.cart.carts[key].data.price_discount) : ''}</span></div>
              </div>
              <div className='cart-page-product-sub' onClick={(e) => { this.props.cartAdd(this.props.cart.carts[key].id, { amount: 1, data: {} }) }}>+</div>
              <div className='cart-page-product-amount'>{this.props.cart.carts[key].amount}</div>
              <div className='cart-page-product-add' onClick={(e) => { this.props.cartSub(this.props.cart.carts[key].id, { amount: 1, data: {} }) }}>-</div>
            </div> }
          </div>      
        ))
      } else { tempCartDetail =  <div className='cart-null'> Giỏ hàng của bạn đang trống!</div> }
    }
    
    let total = (this.state.total === 0) ? this.helpCalCart() : this.state.total
    return (
      <div className='rx-payment-page'>
        {/*<div className='rx-cat-title'>Trang đặt hàng</div>*/}
        {this.state.success === false && <div className='row rx-row-nomar'>
          <div className='col-sm col-md-6 cart-page-left'>
            <div className='cart-page-section-title'>Đơn hàng</div>
            <div className='cart-page-body'>{tempCartDetail}</div>

            {/*<div className='cart-page-section-title'>Vận chuyển, thanh toán</div>*/}            
            <div className='cart-page-shipmoney'>Tạm tính <i>(Đã bao gồm VAT)</i><b>{rxcurrencyVnd(total)}</b></div> 
          </div>
          <div className='col-sm col-md-6 cart-page-customer-info'>
            <div className='cart-page-section-title'>Thông tin khách hàng</div>
            <div className='clearfix cart-page-customer-inner'>
              
              { !(this.props.auth && this.props.auth.user && this.props.auth.user._id) ?
                <div>
                  <div className='rx-input-group-two'>
                    <div>
                      <div className='cart-page-customer-name'>Họ và tên</div>
                      <input tabIndex='1' type='text' name='name' onChange={(e) => this.onBlurData(e, 'name')} className='fullwidth-input' placeholder='Nhập họ và tên'/>
                    </div>
                    <div>
                      <div className='cart-page-customer-phone'>Số điện thoại</div>
                      <input tabIndex='2' type='text' name='phone' onChange={(e) => this.onBlurData(e, 'phone')} className='fullwidth-input' placeholder='Nhập số điện thoại liên hệ'/>
                    </div>
                    <div className='cart-page-customer-email'>Email</div>
                  <input tabIndex='3' type='email' name='email' onChange={(e) => this.onBlurData(e, 'email')} className='fullwidth-input' placeholder='Nhập email của bạn'/>
                  </div>
                  <div className='cart-page-customer-address'>Địa chỉ</div>
                  <input tabIndex='4' type='text' name='address' onChange={(e) => this.onBlurData(e, 'address')} className='fullwidth-input' placeholder='Nhập địa chỉ'/>
                </div> : <div>
                  <div className='rx-input-group-two'>
                    <div>
                      <div className='cart-page-customer-name'>Họ tên</div>
                      <div className='cart-page-customer-detail'>{this.state.editingData.name}</div> 
                    </div>
                    <div>
                      <div className='cart-page-customer-phone'>Số điện thoại</div>
                      <div className='cart-page-customer-detail'>{this.state.editingData.phone}</div>
                    </div>
                  </div>
                  <div className='cart-page-customer-address'>Địa chỉ</div>
                  <div className='cart-page-customer-detail'>{this.state.editingData.address}</div>
                </div> }

              <div className='cart-page-customer-address'>Ghi chú</div>
              <textarea className='fullwidth-input' rows='5' placeholder='Ghi chú về đơn hàng'/>

              { !(this.props.auth && this.props.auth.user && this.props.auth.user._id) ? <div className='input-group'>
                <input type='checkbox' id='check1' tabIndex='0' defaultChecked={this.state.chkbox}  onChange={(e) => {this.handleChangeChk(e, this.state.chkbox)}}/>
                <label htmlFor='check1'>Tạo tài khoản mới (giúp việc đặt hàng ở những lần sau trở nên nhanh hơn)</label>                
              </div> : <div></div> }

              { !(this.props.auth && this.props.auth.user && this.props.auth.user._id) && this.state.chkbox && 
                <div className='rx-input-group-two'>
                  <div>
                    <div className='cart-page-customer-name'>Email { this.state.msgerror && <div className='cart-page-error' >{this.state.msgerror}</div> }</div>
                    <input tabIndex='3' type='text' name='email' onChange={(e) => this.onBlurData(e, 'email')} className='fullwidth-input' />
                  </div>
                  <div>
                    <div className='cart-page-customer-phone'>Mật khẩu</div>
                    <input tabIndex='3' type='password' name='password' onChange={(e) => this.onBlurData(e, 'password')} className='fullwidth-input' />
                  </div>
                </div>}

              <div className='cart-page-shipping-way'><b className='cart-page-shipping-way-title'>Phương thức vận chuyển</b> Tiết kiệm : <b>50.000</b> - Nội thành</div>
              <div className='cart-page-payment-way'><b className='cart-page-payment-way-title'>Phương thức thanh toán</b> COD</div>
              { this.state.msg && <div className='cart-page-error' >{this.state.msg}</div> }
              <div className='cart-page-payment-submit-section'>
                <div className='row rx-row-nomar'>
                  <div className='col-sm col-md-6'>
                    <div className='row rx-row-nomar' style={{paddingRight: '10px'}}>
                      <input tabIndex='2' type='text' name='discountcode' placeholder='Mã giảm giá' onChange={(e) => this.onBlurData(e, 'discountcode')} className='col-sm col-md-8' style={{margin: '0px'}} />                    
                      <div className='col-sm col-md-4 cart-page-payment-voucher' onClick={(e) => this.onClickApplyDiscount(e)}>Áp dụng</div>
                    </div>
                  </div>
                  <div className='col-sm-12 col-md-6' style={{paddingTop: '5px', textAlign: 'center', fontSize: '10px'}}>
                    <i style={{fontSize: '10px'}}>(Vui lòng kiểm tra lại đơn hàng trước khi Đặt Mua)</i>
                  </div>
                </div>
                <div className='row rx-row-nomar cart-page-customer-submit-fixbottom'>
                  <div className='col-sm-12 col-md-6'>
                    <div className='cart-page-payment-subtotal-label'>Tổng cộng <br/><i>({Object.keys(this.props.cart.carts).length} sản phẩm)</i></div> <div className='cart-page-payment-subtotal'>{rxcurrencyVnd(total + 50000)} ₫</div>
                  </div> 
                  <div className='col-sm-12 col-md-6'>
                    <div tabIndex='4' className='cart-page-customer-submit' onClick={(e) => this.onClickOrderSubmit(e)} onKeyPress={(e) => this.onClickOrderSubmit(e)}>Hoàn tất</div>                    
                  </div>                                  
                </div>                
                                
              </div>
            </div>            
          </div>
        </div> }
        {this.state.success === true && <div className='lk-login-customer-page'>
            <div className='row lk-row-nomar'>
              <div className='rxauthen-left-container lk-contact'>
              </div>
              <div className='lk-page-customer-info lk-page-info-login'>
                <div className='lk-page-line-mainbox clearfix'>
                  <div className='lk-block-cicle-icon-check'>
                    <svg height='1.5em' viewBox='0 0 35 35' className='bock-icon-check-svg'>
                      <g className='lk-icon-check'><path strokeDasharray={`72.7977 72.7977`} strokeDashoffset={`50.2409`} d='M20 6.7L9.3 17.3 4 12c0-4.4 3.6-8 8-8s8 3.6 8 8-3.6 8-8 8-8-3.6-8-8'/></g>
                    </svg>
                  </div> 
                  <span>Cám ơn bạn đã mua hàng tại Annahouse</span>
                </div>
                <div className='lk-payment-complete-text'>Mã số đơn hàng của bạn:</div>
                <div className='lk-box-orderid'><span>{this.state.orderData._id}</span></div>
                <div>
                  {this.props.auth && <div className='lk-payment-complete-text'>Bạn có thể xem lại đơn hàng của bạn <Link to='/customer/order'>tại đây</Link></div>}
                  <div className='lk-payment-complete-text'>Thời gian dự kiến giao hàng vào {this.getDate(this.state.orderData.created_at + 259200)} đến {this.getDate(this.state.orderData.created_at + 432000)}</div>
                  {this.props.auth && <div className='lk-payment-complete-text'>Thông tin chi tiết đơn hàng đã được gửi đến địa chỉ email: <b>{this.props.auth.user.email}</b> . Nếu không tìm thấy vui lòng kiểm tra trong hộp thư spam </div>}
                </div>
                
                <hr></hr>
                <div className='lk-payment-complete-text'><b>Câu hỏi thường gặp</b></div>
                <ul>
                  <li><div className='lk-payment-complete-text'>Xác nhận đơn như thế nào?</div></li>
                  <li><div className='lk-payment-complete-text'>Thời gian giao hàng</div></li>
                  <li><div className='lk-payment-complete-text'>Chính sách đổi trả</div></li>
                </ul>
              </div>
            </div>
          </div>}
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose
}

const Payment = connect(
  mapStateToProps,
  mapDispatchToProps
)(Payment_)

export default Payment