let crypto = require('crypto')
let validate = require('./rxValidate.js')
let rxu = {}

rxu.PRICE_RATIO = 20/100
rxu.MINIMUM_STOCK = 10
rxu.MAXIMUM_STOCK = 10000
// U L T I L I T Y //
rxu.json = function (str) {
  let result = {}
  try {
    result = JSON.parse(str)
  } catch (e) {}

  return result
}

rxu.filter = function (params, filter) {
  for (let index in params) {
    if (filter.indexOf(index) !== -1) {
      params.set(index, undefined, { strict: false })
    }
  }
  return params
}

rxu.validate = function (params, rules) {
  let returnSum = true
  let returnData = {}

  try {
    for (let index in rules) {
      let tempResult = validate.value(params[index], rules[index])
      if (!tempResult['approved']) {
        returnData[index] = []
        tempResult.each(function (err) { returnData[index].push(err) })
        returnSum = false
      }
    }

    returnData['rxresult'] = returnSum

  // Case: error from validate lib
  } catch (e) { console.log(e) }

  return returnData
}

rxu.get = function (variable, keys, defaultVal) {
  defaultVal = (defaultVal || defaultVal === 0) ? defaultVal : ''
  let resultVal = defaultVal

  try {
    // Keys is array of index
    if (Array.isArray(keys)) {
      let tempResult = variable
      for (let i in keys) {
        tempResult = tempResult[keys[i]]
      }
      resultVal = tempResult

    // Keys is a string
    } else { resultVal = variable[keys] }

  // Case exception, access undefined variable
  } catch (e) { resultVal = defaultVal }

  // Case undefined after all
  if (resultVal === undefined) { resultVal = defaultVal }

  // HAPPYENDING
  return resultVal
}

rxu.isactiveNav = function (route, checkname, classname) {
  let result = route.localeCompare(checkname)
  if (route.indexOf(checkname) === 0 && result === 0) {
    return classname
  } else {
    return ''
  }
}

rxu.scrollTop = function() {
  window.scrollTo(0, 0);
}

// S T R I N G //
rxu.replaceAt = function(str, index, rep) {
  return str.substring(0, index) + rep + str.substring(index + 1);
}

rxu.replaceFrom = function(str, search, replace, from) {
  let temp = str;
  if (temp.length > from) {
    return temp.slice(0, from) + str.slice(from).replace(search, replace);
  }
  return temp;
}

rxu.removeTrailingZeros = function(str) {
  let temp = str;

  while(temp[temp.length - 1] === '0') {
    temp = rxu.replaceAt(temp, temp.length - 1, '');
  }

  if (temp[temp.length - 1] === ',' || temp[temp.length - 1] === '.') {
    temp = rxu.replaceAt(temp, temp.length - 1, '');
  }

  return temp;
}

rxu.numberToFix = function (x) {
  x = parseInt(x, 10) === x ? x : Number(x).toFixed(5)
  x = x.toString().replace('.',',')
  x = x.replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  x = rxu.replaceFrom(x, '.', '', x.indexOf(','))
  x = rxu.removeTrailingZeros(x)
  return x

  // // x = Number(x).toFixed(5)
  // return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : ''
}

rxu.isFloat = function(n) {
  return n === +n && n !== (n|0);
}

rxu.numberToFixInput = function(num) {
  // var num_parts = num.toString().split(".");
  // var num_parts = rxu.isFloat(num) ? Number(num).toFixed(5).split(".") : num.toString().split(".");
  // num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");

  // if (Number(num_parts[1]) === 0) {
  //   num_parts.pop();
  // }

  // return num_parts.join(",");

  var num_parts = num.toString().split(".");
  num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  return num_parts.join(",");


}

rxu.strToMoney = function (x) {
  x = Number(x).toFixed(0)
  return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') : ''
}

rxu.strTotalMoney = function (x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}

rxu.strToMoneyOr = function (x, key) {
  if (x) {
    x = Number(x).toFixed(0)
    return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') : ''
  }
  else {
    return x;
  }
}

rxu.strGen = function (length, source) {
  length = length || 8
  source = source || '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

  let result = ''
  for (let i = length; i > 0; --i) result += source[Math.floor(Math.random() * source.length)]

  return result
}
rxu.gettodate = function () {
  let today = new Date()
  let dd = today.getDate()
  let mm = today.getMonth() + 1 // January is 0!
  let yyyy = today.getFullYear()
  if (dd < 10) { dd = '0' + dd }
  if (mm < 10) { mm = '0' + mm }
  today = yyyy + mm + dd
  return today
}
rxu.gencode = function (pre, length, source) {
  let today = rxu.gettodate()
  length = length || 4
  source = source || '0123456789'
  pre = pre || 'BL'
  let result = pre + '.' + today + '.'
  for (let i = length; i > 0; --i) result += source[Math.floor(Math.random() * source.length)]

  return result
}
rxu.genNewcode = function (pre, length, source) {
  let today = new Date()
  let mm = today.getMonth() + 1 // January is 0!
  let yyyy = today.getFullYear()
  length = length || 'TP'
  if (mm < 10) { mm = '0' + mm }
  if (pre < 10) { pre = '00' + pre }
  if (pre >= 10 && pre <= 100) { pre = '0'+ pre}
  let result = length + '_' + pre + '/' + mm + '/' + yyyy
  return result
}
rxu.strToSlug = function (str) {
  str = str.replace(/^\s+|\s+$/g, '')
  str = str.toLowerCase()

  let from = 'âấầậẫẩăằắặẵẳàáäâẩạãêềếệễèéëêẽểẻẹìíïîỉịòóöôỏọơờớợỡởôồốộỗổùúüûủụưừứựữửñçđ·/_,:;'
  let to = 'aaaaaaaaaaaaaaaaaaaeeeeeeeeeeeeeiiiiiioooooooooooooooooouuuuuuuuuuuuncd------'
  for (let i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-') // collapse dashes

  return str
}

// D A T E T I M E //
rxu.now = function (unit) {
  const hrTime = process.hrtime()
  switch (unit) {
    case 'milli': return hrTime[0] * 1000 + hrTime[1] / 1000000
    case 'micro': return hrTime[0] * 1000000 + hrTime[1] / 1000
    case 'nano': return hrTime[0] * 1000000000 + hrTime[1]
    default: return hrTime[0] * 1000 + hrTime[1] / 1000000
  }
}

rxu.date = function (format) {
  format = format || 'yyyy/mm/dd'

  let dateObj = new Date()
  let shortDate = ''
  let year; let month; let date; let hour; let minute = ''

  switch (format) {
    case 'yyyy/mm/dd':
      month = ('0' + (dateObj.getMonth() + 1)).slice(-2)
      date = ('0' + dateObj.getDate()).slice(-2)
      year = dateObj.getFullYear()
      shortDate = year + '/' + month + '/' + date
      break

    case 'yyyy/mm/dd/hh/mm':
      year = dateObj.getFullYear()
      month = ('0' + (dateObj.getMonth() + 1)).slice(-2)
      date = ('0' + dateObj.getDate()).slice(-2)
      hour = ('0' + dateObj.getHours()).slice(-2)
      minute = ('0' + dateObj.getMinutes()).slice(-2)
      shortDate = year + '/' + month + '/' + date + '/' + hour + '/' + minute
      break

    default:
      shortDate = dateObj.getFullYear() + '/' + dateObj.getMonth() + '/' + dateObj.getDate()
      break
  }

  return shortDate
}
rxu.toDate = function (value) {
  var dateObj = new Date(value * 1000)
  var year = dateObj.getFullYear()
  var month = ('0' + (dateObj.getMonth() + 1)).slice(-2)
  var date = ('0' + dateObj.getDate()).slice(-2)
  var hour = ('0' + dateObj.getHours()).slice(-2)
  var minute = ('0' + dateObj.getMinutes()).slice(-2)
  var shortDate = hour + ':' + minute + ', ' + date + '/' + month + '/' + year
  return shortDate
}
rxu.toDateNotMin = function (value) {
  var dateObj = new Date(value * 1000)
  var year = dateObj.getFullYear()
  var month = ('0' + (dateObj.getMonth() + 1)).slice(-2)
  var date = ('0' + dateObj.getDate()).slice(-2)
  // var hour = ('0' + dateObj.getHours()).slice(-2)
  // var minute = ('0' + dateObj.getMinutes()).slice(-2)
  var shortDate = date + '/' + month + '/' + year
  return shortDate
}
rxu.inDays = function (date) {
  const NOW = new Date()
  const times = [["giây", 1], ["phút", 60], ["giờ", 3600], ["ngày", 86400], ["tuần", 604800], ["tháng", 2592000], ["năm", 31536000]]

  date = new Date(date * 1000) || new Date()
  var diff = Math.round((NOW - date) / 1000)
  for (var t = 0; t < times.length; t++) {
      if (diff < times[t][1]) {
          if (t === 0) {
              return "Cách đây vài giây"
          } else {
              diff = Math.round(diff / times[t - 1][1])
              return  diff + " " + times[t - 1][0] + ' trước'
          }
      }
    }
}
rxu.inWeeks = function (d1, d2) {
  var t2 = d2.getTime()
  var t1 = d1.getTime()
  return parseInt((t2 - t1) / (24 * 3600 * 1000 * 7), 10)
}
rxu.inMonths = function (d1, d2) {
  var d1Y = d1.getFullYear()
  var d2Y = d2.getFullYear()
  var d1M = d1.getMonth()
  var d2M = d2.getMonth()
  return (d2M + 12 * d2Y) - (d1M + 12 * d1Y)
}
rxu.inYears = function (d1, d2) {
  return d2.getFullYear() - d1.getFullYear()
}
rxu.numberToName = function(m) {
  m = Number(m).toFixed(0)
    let string = {
      0: '', //donvi
      1: '', //chuc
      2: '', //tram
      3: '', //nghin
      4: '', // chuc nghin
      5: '', // tram nghin
      6: '', // trieu
      7: '', // chuc trieu
      8: '', // tram trieu
      9: '', // ty
      10: '', // chuc ty
      11: '', // tram ty
      12: '', // nghin ty
      13: '', //chuc nghin ty
      14: '', // tram nghin ty
      15: '', // trieu ty
      16: '', // chuc trieu ty
      17: '', // tram trieu ty
      18: '' // ty ty
    }
    if(m > 0) {
      // m = 1000001
      let num = m
      let count = 0
      while(num > 0) {
        num = Math.floor(num/10)
        if(num > 0) {
          count += 1
        }
      }
      let zeroBehind = 0
      num = m
      for(let i = 0; i <= count; i++) {
        let head = Math.floor(Math.floor(num / 10) % 10)
        let tail = Math.floor(num % 10)
        num /= 10
        string[i] = rxu.spellNum(m, tail, head, zeroBehind)
        zeroBehind += 1
      }
      num = m
      for(let i = 3; i <= count; i += 3) {
        let zero = 1
        for(let z = i-3; z < i; z++) {
          zero *= 10
        }
        if(Math.floor(num % zero) === 0) {
          string[i-1] = ''
          string[i-2] = ''
          string[i-3] = ''
        }
        num = Math.floor(num/zero)
      }
      let money = ''
      for(let i = count; i >=0; i--) {
        if(string[i] !== '') {
          money += string[i] + ' '
        }
      }
      money += 'đồng'
      return money[0].toUpperCase() + money.substring(1)
    }
  }

rxu.spellNum = function(raw, num, numbefor, zeroBehind) {
    let spell = ''
    let tens = 'mươi', hundreds = 'trăm', thousands = 'nghìn', millions = 'triệu', billions = 'tỷ'
    switch(zeroBehind) {
      case 0:
        switch (num) {
          case 1:
            if(numbefor !== 1 && numbefor !== 0) {
              spell = 'mốt'
            }
            else {
              spell = 'một'
            }
            break
          case 2:
            spell = 'hai'
            break
          case 3:
            spell = 'ba'
            break
          case 4:
            spell = 'bốn'
            break
          case 5:
            if(numbefor !== 1) {
              spell = 'năm'
            }
            else {
              spell = 'lăm'
            }
            break
          case 6:
            spell = 'sáu'
            break
          case 7:
            spell = 'bảy'
            break
          case 8:
            spell = 'tám'
            break
          case 9:
            spell = 'chín'
            break
          default:
            spell = tens
        }
        break
      case 1:
      case 4:
      case 7:
        switch (num) {
          case 1:
            spell = 'mười'
            break
          case 2:
            spell = 'hai '+tens
            break
          case 3:
            spell = 'ba '+tens
            break
          case 4:
            spell = 'bốn '+tens
            break
          case 5:
            spell = 'năm '+tens
            break
          case 6:
            spell = 'sáu '+tens
            break
          case 7:
            spell = 'bảy '+tens
            break
          case 8:
            spell = 'tám '+tens
            break
          case 9:
            spell = 'chín '+tens
            break
          default:
            spell = 'lẻ'
        }
        break
      case 2:
      case 5:
      case 8:
        switch (num) {
          case 0:
            spell = 'không '+hundreds
            break
          case 1:
            spell = 'một '+hundreds
            break
          case 2:
            spell = 'hai '+hundreds
            break
          case 3:
            spell = 'ba '+hundreds
            break
          case 4:
            spell = 'bốn '+hundreds
            break
          case 5:
            spell = 'năm '+hundreds
            break
          case 6:
            spell = 'sáu '+hundreds
            break
          case 7:
            spell = 'bảy '+hundreds
            break
          case 8:
            spell = 'tám '+hundreds
            break
          case 9:
            spell = 'chín '+hundreds
            break
          default:
            spell = thousands
        }
        break
      case 3:
        switch (num) {
          case 1:
            if(numbefor !== 1 && numbefor !== 0) {
              spell = 'mốt '+thousands
            }
            else {
              spell = 'một '+thousands
            }
            break
          case 2:
            spell = 'hai '+thousands
            break
          case 3:
            spell = 'ba '+thousands
            break
          case 4:
            spell = 'bốn '+thousands
            break
          case 5:
            if(numbefor !== 3) {
              spell = 'năm '+thousands
            }
            else {
              spell = 'lăm '+thousands
            }
            break
          case 6:
            spell = 'sáu '+thousands
            break
          case 7:
            spell = 'bảy '+thousands
            break
          case 8:
            spell = 'tám '+thousands
            break
          case 9:
            spell = 'chín '+thousands
            break
          default:
            spell = thousands
        }
        break
      case 6:
        switch (num) {
          case 1:
            if(numbefor !== 1 && numbefor !== 0) {
              spell = 'mốt '+millions
            }
            else {
              spell = 'một '+millions
            }
            break
          case 2:
            spell = 'hai '+millions
            break
          case 3:
            spell = 'ba '+millions
            break
          case 4:
            spell = 'bốn '+millions
            break
          case 5:
            if(numbefor !== 3) {
              spell = 'năm '+millions
            }
            else {
              spell = 'lăm '+millions
            }
            break
          case 6:
            spell = 'sáu '+millions
            break
          case 7:
            spell = 'bảy '+millions
            break
          case 8:
            spell = 'tám '+millions
            break
          case 9:
            spell = 'chín '+millions
            break
          default:
            spell = millions
        }
        break
      case 9:
        switch (num) {
          case 1:
            if(numbefor !== 1 && numbefor !== 0) {
              spell = 'mốt '+billions
            }
            else {
              spell = 'một '+billions
            }
            break
          case 2:
            spell = 'hai '+billions
            break
          case 3:
            spell = 'ba '+billions
            break
          case 4:
            spell = 'bốn '+billions
            break
          case 5:
            if(numbefor !== 3) {
              spell = 'năm '+billions
            }
            else {
              spell = 'lăm '+billions
            }
            break
          case 6:
            spell = 'sáu '+billions
            break
          case 7:
            spell = 'bảy '+billions
            break
          case 8:
            spell = 'tám '+billions
            break
          case 9:
            spell = 'chín '+billions
            break
          default:
            spell = billions
        }
        break
      default:
    }
    return spell
  }
// A S Y N C //
rxu.to = function (promise) {
  return promise.then(data => {
    return [null, data]
  }).catch(err => [err])
}

rxu.toex = function (obj, func, ...params) {
  return rxu.to(new Promise((resolve, reject) => {
    obj[func](...params, (result) => {
      resolve(result)
    })
  }))
}

rxu.runAsync = function (orm, func, ...params) {
  return new Promise((resolve, reject) => {
    orm[func](...params, (err, result) => {
      if (err) { reject() } else { resolve(result) }
    })
  })
}

rxu.runAsyncTo = function (orm, func, ...params) {
  return rxu.to(rxu.runAsync(orm, func, ...params))
}

// H A S H //
rxu.md5 = function (strsource) {
  let passHash = crypto.createHash('md5').update(strsource).digest('hex')
  return passHash
}

rxu.genhex = function () {
  let newToken = crypto.randomBytes(64).toString('hex')
  return newToken
}

// E N C R Y P T  -  D E C R Y P T //
// S Y S T E M //
rxu.log = function (msg) {
  console.log(msg)
}

rxu.emit = function (reqObj, e) {
  e = e || {}
  setImmediate(() => { reqObj.res.inthis.emit('error', reqObj.req, reqObj.res, {}, e) })
}

rxu.parseRawValue = value => {
  value = value || 0
  let temp = value.toString();

  if (temp.split(',').length > 2) {
  	return '';
  }
  else {
  	temp = temp.replace(/\./g, '')
    temp = temp.replace(/(,)/g, '.');
  }
  
  return temp;
}

rxu.genRandomId = () => {
    var S4 = function() {
      return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

// B U I L D I N //
module.exports = rxu
