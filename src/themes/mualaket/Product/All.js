import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { polyfill }  from 'es6-promise'
import { connect } from 'react-redux'
import $ from 'jquery'
import { withTranslation } from 'react-i18next'
import ReactGA from 'react-ga'
import InputRange from 'react-input-range'
import {Helmet} from "react-helmet";
const { rxget, rxgetLocal } = global.rootRequire('classes/request')
const { rxChangeAlias, rxcurrencyVnd, helpKeywords } = global.rootRequire('classes/ulti')
const { cartClear, cartAdd, rxsearchClear } = global.rootRequire('redux')
const RxProductWatched = global.rootRequiretheme('components/Shares/RxProductWatched').default
const Loader = global.rootRequire("components/Shares/loader").default
polyfill()
const config = global.rxu.config
class ProductAll_ extends Component {
  constructor(props) {    
    super(props)
    this.maxPrice = 500
    this.minPrice = 0
    let strcat = this.props.match.params.cat
    let strname = this.props.match.params.search
    this.timeout = null
    this.state = global.ultiStaticContext(this.props.staticContext) || {cat: strcat, search: strname}
    this.state.categories = this.state.categories || []
    this.state.arrcategories = []
    this.state.products = this.state.products || []
    this.state.cates   = this.state.cates || []
    this.state.products_one = this.state.products_one || []
    this.state.quantity = this.state.quantity || 0   
    this.state.paging = this.state.paging || {}
    this.state.option_all = this.state.option_all || []
    this.state.imgbanners = this.state.imgbanners || []
    this.state.showFilter = false
    this.state.showSorter = false
    this.state.toggleFilCat = false
    this.state.toggleFilBrand = false
    this.state.toggleFilSize = false
    this.state.toggleFilColor = false
    this.state.toggleFilPrice = false
    this.state.paging.st_full = this.state.paging.st_full || 'created_at:desc'
    this.state.paging.st_col  = this.state.paging.st_col  || 'created_at'
    this.state.paging.st_type = this.state.paging.st_type || -1
    this.state.paging.pg_page = this.state.paging.st_page || 1
    this.state.paging.pg_size = this.state.paging.st_size || 16
    this.state.paging.price = this.state.paging.price || { min: this.minPrice, max: this.maxPrice }
  
    this.state.cat = strcat
    this.state.search = strname
    this.state.searchtext = {}
    this.state.trademark = ''
    this.state.products_viewed = []
    this.state.categoryonly = []
    this.state.brandall = []
    this.state.brandonly = []
    this.state.arrcateobj = []
    this.state.arrbrandobj = []
    this.state.checkscroll = { number: 1, type: 'next' }
    this.state.loading = false
    let currencyname = 'AUD'
    try {
      currencyname = JSON.parse(rxgetLocal('currency'))['name']
    } catch(e) {}
    this.state.currencyname = currencyname || 'AUD'
    if (this.state.currencyname === 'VND') {
      this.minPrice = 0
      this.maxPrice = 500 * 16500
      this.state.paging.price = { min: 0 , max: 500 * 16500 }
    }
    
  }
  componentDidMount() {
    let strcat = this.props.match.params.cat ? this.props.match.params.cat : ''
    let strname = this.props.match.params.search ? this.props.match.params.search : ''
    ReactGA.initialize('UA-157753374-1')
    ReactGA.pageview(`/search/${strcat}/${strname}`)

    this.initData()
    // this.checkCateBrandobj(this.state.cat)
    // this.fetchData() 
    this.fetchOption()   
    try {$(window).scrollTop(0)} catch(err) {console.log(err)}
    if (this.props && this.props.viewed && this.props.viewed.products && this.props.viewed.products.constructor === Object) {
      Object.keys(this.props.viewed.products).forEach(key => {
        if (this.props.viewed.products && this.props.viewed.products[key] && this.props.viewed.products[key]['data']) {
          this.state.products_viewed.push(this.props.viewed.products[key]['data'])  
        }
      })
    }
    if (this.props.location && this.props.location.search && this.props.location.search.indexOf('trademark') !== -1) {
      let strsearch = this.props.location.search.replace(/\?/g,'')
      let arrsearch = strsearch.split('&')
      let trademarkindex = arrsearch.filter(s => s.includes('trademark'))
      if (trademarkindex && trademarkindex.length > 0) {
        let arrtrademarkstr = trademarkindex[0].split('=')
        let trademarkstr = arrtrademarkstr[1] || ''
        if (trademarkstr) {
          trademarkstr = rxChangeAlias(trademarkstr.replace(/ /g,'').replace(/-/g,''))
          this.setState({trademark: trademarkstr})
        }
      }
    }
  }
  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.match.params.cat && this.state.cat !== nextProps.match.params.cat) {
      this.setState({ cat: nextProps.match.params.cat })
    }
    if (nextProps.rxsearch || typeof(nextProps.location.categoryid) !== 'undefined') {
      let paging = this.state.paging
      let arrcateobj = this.state.arrcateobj
      let arrbrandobj = this.state.arrbrandobj
      if (typeof(nextProps.location.categoryid) !== 'undefined') {
        if (nextProps.location.categoryid === '0') {
          delete paging['searchid_appdist']
          delete paging['searcharr__id']
          delete paging['search_name']
        } else {
          paging['searchid_appdist'] = nextProps.location.categoryid
        }
        if (this.state.categories && this.state.categories.length > 0) {
          let objcate = this.state.categories.find(function (objct) { return objct._id === nextProps.location.categoryid })
          if (objcate) { 
            paging.searchname_appdist = objcate.name
            arrcateobj = []
            if (arrcateobj.length > 0) {
              let checkcateobj = arrcateobj.findIndex(obj => obj._id === paging.searchid_appdist) 
              if (checkcateobj === -1) {
                arrcateobj.push({_id: paging.searchid_appdist, name: paging.searchname_appdist, desc: objcate.desc, tags: objcate.tags })
              }
            } else {
              arrcateobj.push({_id: paging.searchid_appdist, name: paging.searchname_appdist, desc: objcate.desc, tags: objcate.tags })
              // arrcateobj = []
              // this.props.history.push('/search/all')
            }
          }
        }
      } else if (typeof(nextProps.location.brandid) !== 'undefined') {
        if (nextProps.location.brandid === '') {
          delete paging['searchid_brandlist']
          delete paging['searchbrand__id']
        } else {
          paging['searchid_brandlist'] = nextProps.location.brandid
        }
        if (this.state.brandall && this.state.brandall.length > 0) {
          let objcate = this.state.brandall.find(function (objct) { return objct._id === nextProps.location.brandid })
          if (objcate) {
            paging.searchname_brandlist = objcate.name
            arrbrandobj = []
            if (arrbrandobj.length > 0) {
              let checkcateobj = arrbrandobj.findIndex(obj => obj._id === paging.searchid_brandlist) 
              if (checkcateobj === -1) {
                arrbrandobj.push({_id: paging.searchid_brandlist, name: paging.searchname_brandlist })
              }
            } else {
              arrbrandobj.push({_id: paging.searchid_brandlist, name: paging.searchname_brandlist })
              // arrbrandobj = []
              // this.props.history.push('/search/all')
            }
          }
        }
      } else if (nextProps.rxsearch && nextProps.rxsearch.params && nextProps.rxsearch.params.name && nextProps.rxsearch.params.name === 'Header') {
        if (typeof(nextProps.rxsearch.params['search_name']) !== 'undefined') {
          paging['search_name'] = nextProps.rxsearch.params['search_name']
        } else {
          delete paging['search_name']
        }
        if (typeof(nextProps.rxsearch.params['searchid_appdist']) !== 'undefined') {
          paging['searchid_appdist'] = nextProps.rxsearch.params['searchid_appdist']
          // arrcateobj = [{_id: paging.searchid_appdist, name: paging.searchname_appdist }]
        } else {
          delete paging['searchid_appdist']
          delete paging['searchname_appdist']
          delete paging['searcharr__id']
          arrcateobj = []
        }
      } else if (nextProps.rxsearch && nextProps.rxsearch.params && (nextProps.rxsearch.params !== {} || nextProps.rxsearch.params === {}) && typeof(nextProps.rxsearch.params.name) === 'undefined') {
        if (typeof(nextProps.rxsearch.params['search_name']) !== 'undefined') {
          paging['search_name'] = nextProps.rxsearch.params['search_name']
        } else {
          delete paging['search_name']
        }
        if (typeof(nextProps.rxsearch.params['searchid_appdist']) !== 'undefined') {
          paging['searchid_appdist'] = nextProps.rxsearch.params['searchid_appdist']
          arrcateobj = [{_id: paging.searchid_appdist, name: paging.searchname_appdist }]
        } else {
          delete paging['searchid_appdist']
          delete paging['searchname_appdist']
          delete paging['searcharr__id']
          arrcateobj = []
        }
      }
      this.setState({ paging: paging, arrcateobj: arrcateobj, arrbrandobj: arrbrandobj, loading: true }, () => {this.fetchData()}) 
    }
  }
  initData() {
    rxget(config.api_category_cate_brand, {}, { '1': (json) => { 
      let categories = this.parsecate(json.data.childArrCate)
      let arrcategories = this.parsearrcate(json.data.childArrCate)
      let paging = this.state.paging
      let arrbrandobj = this.state.arrbrandobj
      let arrcateobj = this.state.arrcateobj
      if (this.state.cat && this.state.cat !== 'all' && categories.constructor === Array) {
        let objcate = categories.find((obj) => { return obj.slug === this.state.cat })
        if (objcate && objcate._id) {
          if (this.props && this.props.location && this.props.location.brandid) {
            paging.searchid_brandlist = objcate._id
            paging.searchname_brandlist = objcate.name
            arrbrandobj = [{_id: paging.searchid_brandlist, name: paging.searchname_brandlist, desc: objcate.desc, tags: objcate.tags }]
          }
          if (this.props && this.props.location && this.props.location.categoryid) {
            paging.searchid_appdist = objcate._id
            paging.searchname_appdist = objcate.name
            arrcateobj = [{_id: paging.searchid_appdist, name: paging.searchname_appdist, desc: objcate.desc, tags: objcate.tags }]
          }
        }
      } 
      if(this.props && this.props.location && this.props.location.categoryarr) {
        let categoryarr = this.props.location.categoryarr
        let arrcate = categories.filter(o => categoryarr.indexOf(o._id) !== -1)
        arrcateobj = arrcate
      }
      this.setState({ categories: categories, arrcategories: arrcategories, paging: paging, arrcateobj: arrcateobj, arrbrandobj: arrbrandobj, loading: true }, () => {this.fetchData()}) 
      let categoryonly = json.data.dbarrCateOnly
      let brandall = this.parsebrand(json.data.childArrBrand)
      let brandonly = json.data.dbarrBrandOnly //this.parsebrand(json.data.dbarrBrandOnly)
      if(categoryonly && brandonly) {
        this.checkCateBrandobj(this.state.cat, categoryonly, brandonly)
      }
      this.setState({ categoryonly: categoryonly, brandall: brandall, brandonly: brandonly })
    }})
  }
  parsecate(arrcate) {
    let arrcatetmp = []
    let arrcatemain = []
    Object.keys(arrcate).map(catekey => {
      arrcatetmp = arrcate[catekey]
      arrcatemain = arrcatemain.concat(arrcatetmp)
      return arrcatemain
    })
    return arrcatemain
  }
  parsearrcate(arrcate) {
    let arrcatetmp = []
    Object.keys(arrcate).map(catekey => {
      if (catekey === '0') {
        arrcatetmp = arrcate[catekey]
      }
      return arrcatetmp
    })
    arrcatetmp.forEach(objcate => {
      objcate.childArr = arrcate[objcate.id] || []
    })
    return arrcatetmp
  }
  parsebrand(arrbrand) {
    let arrbrandtmp = []
    Object.keys(arrbrand).map(brandkey => {
      let cate = this.state.categories.find(function (objct) { return objct._id === brandkey })
      if(cate) {
        cate['brand'] = arrbrand[brandkey]
        arrbrandtmp.push(cate)
      }
      return arrbrandtmp
    })
    return arrbrandtmp
  }
  checkCateBrandobj(cat, cate, brand) {
    let paging = JSON.parse(JSON.stringify(this.state.paging))
    let categories = this.state.categories
    let categoryonly = cate //this.state.categoryonly
    let arrcateobj = this.state.arrcateobj || []
    let brandall = brand //this.state.brandall
    let arrbrandobj = []
    if (this.props.rxsearch && this.props.rxsearch.params && this.props.rxsearch.params['search_name'] ){
      paging['search_name'] = this.props.rxsearch.params['search_name'] 
    } else {
      delete paging['search_name']
    }
    if (this.state.search) {
      paging['search_name'] = this.state.search
    } else if (paging['search_name']) { delete paging['search_name'] }
    
    if (cat && cat === 'all') {
      delete paging['searchid_appdist']
      delete paging['searchname_appdist']
      delete paging['searcharr__id']
      delete paging['searchbrand__id']
      delete paging['searchid_brandlist']
      delete paging['searchname_brandlist']
      this.setState({ arrcateobj: [], arrbrandobj: [], paging: paging })
    }
    if (cat && cat !== 'all' && categories && categories.constructor === Array && categoryonly && categoryonly.constructor === Array && brandall && brandall.constructor === Array) {
      let objcate = categoryonly.find((obj) => { return obj.slug === cat })
      if (objcate && objcate._id) {
        paging['searchid_appdist'] = objcate._id
        paging['searchname_appdist'] = objcate.name
        arrcateobj = [{_id: objcate._id, name: objcate.name, desc: objcate.desc, tags: objcate.tags}]
      }
      let objbrand = brandall.find((obj) => { return obj.slug === cat })
      if (objbrand && objbrand._id) {
        paging['searchid_brandlist'] = objbrand._id
        paging['searchname_brandlist'] = objbrand.name
        arrbrandobj = [{_id: objbrand._id, name: objbrand.name, desc: objbrand.desc, tags: objbrand.tags}]
      }
    }

    if (paging['searchid_appdist'] && paging['searchid_appdist'].length > 0) {
      let findid = paging['searchid_appdist']
      let objcate = categoryonly.find(function (objcts) { return objcts._id === findid })
      if (objcate) { paging['searchname_appdist'] = objcate.name }
    }
    if (paging['searchid_brandlist'] && paging['searchid_brandlist'].length > 0) {
      let findid_brand = paging['searchid_brandlist']
      let objbrand = categories.find(function (objcts) { return objcts._id === findid_brand })
      if (objbrand) { paging['searchname_brandlist'] = objbrand.name }
    }
    this.setState({ paging: paging, arrcateobj: arrcateobj, arrbrandobj: arrbrandobj, loading: true }, () => {this.fetchData()} )
  }
  fetchData(pagingtmp) {
    let paging = JSON.parse(JSON.stringify(this.state.paging))
    let categoryid = this.props.location.categoryid
    let brandid = this.props.location.brandid
    let categoryarr = this.props.location.categoryarr
    if (typeof(categoryid) !== 'undefined' && typeof(paging.searchid_appdist) === 'undefined') {
      paging.searchid_appdist = categoryid
    }
    if (typeof(brandid) !== 'undefined' && typeof(paging.searchid_brandlist) === 'undefined') {
      paging.searchid_brandlist = brandid
    }
    if (typeof(categoryarr) !== 'undefined' && typeof(paging.searcharr__id) === 'undefined') {
      paging.searcharr__id = categoryarr.join(',')
    }
    if (pagingtmp && pagingtmp.st_full) {
      paging = pagingtmp
    }
    
    let arrcatestr = this.state.arrcateobj.map(x => x._id)
    let arrbrandstr = this.state.arrbrandobj.map(x => x._id)
    if(arrcatestr && arrcatestr.length > 0) {
      delete paging.searchid_appdist
      delete paging.searchname_appdist
      paging.searcharr__id = arrcatestr.join(',')
    }
    if(!arrcatestr || arrcatestr.length === 0){
      delete paging.searcharr__id
      delete paging.searchid_appdist
      delete paging.searchname_appdist
    }
    if(arrbrandstr && arrbrandstr.length > 0) {
      delete paging.searchid_brandlist
      delete paging.searchname_brandlist
      paging.searchbrand__id = arrbrandstr.join(',')
    }
    if(!arrbrandstr || arrbrandstr.length === 0) {
      delete paging.searchbrand__id
      delete paging.searchid_brandlist
      delete paging.searchname_brandlist
      // this.props.history.push('/search/all')
    }
    if (this.props.rxsearch && this.props.rxsearch.params && this.props.rxsearch.params['search_name'] ){
      paging['search_name'] = this.props.rxsearch.params['search_name'] 
    } else {
      delete paging['search_name']
    }
    if (this.state.search) {
      paging['search_name'] = this.state.search
    } else if (paging['search_name']) { delete paging['search_name'] }
    if (categoryid === '0') {
      delete paging.searchid_appdist
      delete paging.searcharr__id
      delete paging.searchid_brandlist
      delete paging.searchbrand__id
      this.setState({ arrcateobj: [], arrbrandobj: [] })
    }

    if (typeof(paging.price.minVND) !== 'undefined' && typeof(paging.price.maxVND) !== 'undefined') {
      paging.searchprice_min = paging.price.minVND
      paging.searchprice_max = paging.price.maxVND
    }
    rxget(config.api_home_product, paging, {
      // '1': (json) => { this.setState({ products: json.data.products, quantity: json.data.count, paging: paging }); $('html, body').animate({ scrollTop: 0 }, 200, () => {}) }        
      '1': (json) => { this.setState({ products: json.data.products, quantity: json.data.count, paging: paging, loading: false })}        
    })
  }
  onClickSorting(e, item) {    
    let paging = this.state.paging
    let pagingArr = item.i.split(':')
    paging['st_full'] = item.i
    paging['st_col']  = pagingArr[0]
    paging['st_type'] = (pagingArr[1] === 'desc') ? -1 : 1
    this.setState({ paging: paging, loading: true }, () => { 
      this.fetchData()
    }) 
  }
  handleChange(value) {
    let i = value
    let paging = this.state.paging
    let pagingArr = i.split(':')
    paging['st_full'] = i
    paging['st_col']  = pagingArr[0]
    paging['st_type'] = (pagingArr[1] === 'desc') ? -1 : 1
    this.setState({ paging: paging, loading: true }, () => {   
      this.fetchData()
    }) 
  }
  onClickChangepage(e, type, number) {
    let paging = this.state.paging
    // Prevent 0
    type = (paging.pg_page <= 1 && type === 'back') ? 'no' : type
    type = (this.state.products.length === 0 && type === 'next') ? 'no' : type
    switch (type) {      
      case 'back':
        paging.pg_page = paging.pg_page - 1
        break;
      case 'next':
        paging.pg_page = paging.pg_page + 1
        break;
      case 'backfirst':
        paging.pg_page = 1
        break;
      case 'nextlast':
        paging.pg_page = Math.ceil(this.state.quantity / paging.pg_size)
        break;
      default:
        break;
    }
    this.setState({ paging: paging, loading: true }, () => {
      this.backtotop()
      this.fetchData()
    })
  }
  onClickChangeCat(e, item) {
    let arrcateobj = this.state.arrcateobj
    let paging = JSON.parse(JSON.stringify(this.state.paging)) 
    if (item) {
      paging.searchid_appdist = item._id
      paging.searchname_appdist = item.name
      let checkitem = arrcateobj.findIndex(obj => obj._id === item._id)
      if (checkitem !== -1) {
        arrcateobj.splice(checkitem, 1);
      } else {
        arrcateobj.push({_id: item._id, name: item.name, desc: item.desc, tags: item.tags})  
      }
      
    } else {
      delete(this.props.location.categoryid)
      delete(paging.searchid_appdist)
      delete(paging.searchname_appdist)
      arrcateobj = []
      for (let j in this.state.categories) {
        let cate = this.state.categories[j] 
        arrcateobj.push({_id: cate._id, name: cate.name, desc: cate.desc, tags: cate.tags})
      }
    }
    paging.pg_page = 1
    this.setState({ paging: paging, arrcateobj: arrcateobj, loading: true }, () => {
      this.backtotop()
      this.fetchData()
    }) 
  }
  onClickChangeBrand(e, item) {
    let arrbrandobj = this.state.arrbrandobj
    let paging = JSON.parse(JSON.stringify(this.state.paging)) 
    if (item) {
      paging.searchid_brandlist = item._id
      paging.searchname_brandlist = item.name
      let checkitem = arrbrandobj.findIndex(obj => obj._id === item._id)
      if (checkitem !== -1) {
        arrbrandobj.splice(checkitem, 1);
      } else {
        arrbrandobj.push({_id: item._id, name: item.name, desc: item.desc, tags: item.tags})  
      }
      
    } else {
      delete(this.props.location.categoryid)
      delete(paging.searchid_brandlist)
      delete(paging.searchname_brandlist)
      arrbrandobj = []
      for (let j in this.state.categories) {
        let cate = this.state.categories[j] 
        arrbrandobj.push({_id: cate._id, name: cate.name, desc: cate.desc, tags: cate.tags})
      }
    }
    paging.pg_page = 1
    this.setState({ paging: paging, arrbrandobj: arrbrandobj, loading: true }, () => {
      this.backtotop()
      this.fetchData()
    }) 
  }
  onClickchangeFilter(e, name, item) {
    let paging = this.state.paging
    if (!item || (typeof(paging[name]) !== 'undefined' && item === paging[name])) {
      delete(paging[name])
    } if (!item || (typeof(paging[name]) !== 'undefined')) {
      this.filterOption(paging[name], item)
      if (paging[name].length === 0) {delete(paging[name])}
    } else {
      paging[name] = [item] 
    }
    if (name === 'searchname_appdist') {
      this.props.history.push('/search/all') 
      delete(paging.searchid_appdist)
      delete(paging.searchname_appdist)
    }
    if (name === 'searchname_brandlist') {
      this.props.history.push('/search/all') 
      delete(paging.searchid_brandlist)
      delete(paging.searchname_brandlist)
    }
    
    this.setState({ paging: paging, loading: true }, () => {
      this.backtotop()
      this.fetchData()
    })
  }
  onClickchangeFilterPaging(e, name, index) {
    let paging = this.state.paging
    let arrcateobj = this.state.arrcateobj
    let arrbrandobj = this.state.arrbrandobj
    if (name === 'searcharr__id') {
      arrcateobj.splice(index, 1)
      paging.searcharr__id = arrcateobj.join(',')
    }
    if (name === 'searchbrand__id') {
      arrbrandobj.splice(index, 1)
      paging.searchbrand__id = arrbrandobj.join(',')
    }
    if (arrcateobj.length === 0 && arrbrandobj.length === 0) {
      if(paging.search_name && paging.search_name.length > 0) {
        this.props.history.push('/search/all/'+paging.search_name)
      } else {
        this.props.history.push('/search/all')
      }
    }
    this.setState({ paging: paging, arrcateobj: arrcateobj, arrbrandobj:arrbrandobj, loading: true }, () => {this.fetchData()})
  } 
  filterOption(paging, item) {
    let pagingtmp = []
    if (paging.constructor !== Array) {
      pagingtmp.push(paging)
      paging = pagingtmp
    }
    if (paging.constructor === Array) {
      if (!item || (typeof(paging) !== 'undefined' && paging.indexOf(item) === -1 )) {
        paging.push(item)
      } else {
        paging.splice(paging.indexOf(item), 1)
      }  
    }
    return paging
  }
  onClickchangePriceFilter(e, name, value) {
    let paging = this.state.paging
    if (!paging.price) paging.price = {min: 0, max: 500}
    paging.price[name] = value
    paging['searchprice_' + name] = value
    this.setState({ paging: paging, loading: true }, () => {
      clearTimeout(this.timeout)
      this.timeout = setTimeout(() => {
        this.fetchData()
      }, 800)
    })
  }
  onChangeFilterPrice(value) {
    let paging = this.state.paging
    paging.price = value
    paging.price.min = value.min < this.minPrice ? this.minPrice : value.min
    paging.price.max = value.max > this.maxPrice ? this.maxPrice : value.max
    // Set up search param for price
    paging.price.min === this.minPrice ? delete(paging.searchprice_min) : paging.searchprice_min = paging.price.min
    paging.price.max === this.maxPrice ? delete(paging.searchprice_max) : paging.searchprice_max = paging.price.max
    if (typeof(paging.searchprice_min) === 'undefined') {
      paging.searchprice_min = this.minPrice
    }
    if (typeof(paging.searchprice_max) === 'undefined') {
      paging.searchprice_max = this.maxPrice
    }
    if (this.state.currencyname === 'VND') {
      paging.searchprice_min = value.min / 16500
      paging.searchprice_max = value.max / 16500
    }
    this.setState({ paging: paging, loading: true }, () => {
      clearTimeout(this.timeout)      
      this.timeout = setTimeout(() => {
        this.fetchData()
      }, 800)
    })
  }
  onClickClearsearch() {
    this.props.rxsearchClear()
    let paging = this.state.paging
    delete paging.search_name
    this.setState({ paging: paging, loading: true }, () => {
      if (this.state.cat) {
        this.props.history.push('/search/'+this.state.cat) 
      } else {
        this.fetchData()  
      }
    })
  }
  
  toggleFiltMobile() {
    this.setState({ showFilter: !this.state.showFilter })
  }
  toggleFiltDest(key) {
    let k = this.state[key]
    this.setState({ [key]: !k })
  }
  toggleSortMobile() {
    this.setState({ showSorter: !this.state.showSorter })
  }
  fetchOption() {
    let paging = this.state.paging
    rxget(config.api_home_option, {}, {
      '1': (json) => { 
        let trademark = this.state.trademark
        if (trademark) {
          let trademarkarr = json.data.find(objtrade => objtrade.type === 'trademark')
          if (trademarkarr && trademarkarr.value && trademarkarr.value.constructor === Array) {
            try {
              let trademarkArr = trademarkarr.value.find(trade => rxChangeAlias(trade.value.toLowerCase().replace(/ /g,'').replace(/-/g,'')) === rxChangeAlias(trademark.toLowerCase().replace(/ /g,'').replace(/-/g,'')) )
              paging['searchoption_trademark'] = [trademarkArr.value]
              trademark = trademarkArr.value
            } catch(e) { console.log(e) } 
          }  
        }
        this.setState({ option_all: json.data, trademark: trademark , loading: true}) //, () =>  {console.log(14);this.fetchData()})
      }        
    })
  }
  backtotop() {
    let intervalId = setInterval(this.scrollStep.bind(this), 16.66);
    this.setState({ intervalId: intervalId });
  }
  scrollStep() {
    if (window.pageYOffset === 0) {
        clearInterval(this.state.intervalId);
    }
    window.scroll(0, window.pageYOffset - 60);
  }
  // B L U R   E V E N T 
  onBlurDataSeach(e, name) {
    let searchtext = this.state.searchtext
    searchtext[name] = e.target.value
    this.setState({ searchtext: searchtext })
  }
  onFilterDataSeach(arr, text) {
    if (text && text.length > 0) {
      return arr.filter((item, index) => rxChangeAlias(item).indexOf(rxChangeAlias(text)) !== -1)
    } else {
      return arr
    }
  }
  onClickDelAllFilter() {
    let paging = this.state.paging
    let arrcateobj = this.state.arrcateobj
    arrcateobj = []
    let arrbrandobj = this.state.arrbrandobj
    arrbrandobj = []
    Object.keys(paging).forEach(key => {
      if (key.indexOf('searchname_appdist') !== -1 || key.indexOf('searchname_brandlist') !== -1 || key.indexOf('searchprice_') !== -1 || key.indexOf('searchoption_') !== -1 || key.indexOf('searchid_appdist') !== -1 || key.indexOf('searchid_brandlist') !== -1 || key.indexOf('search_name') !== -1) {
        delete(paging[key])
        if (key === 'search_name') {
          this.onClickClearsearch()
        }
      }
    })
    this.setState({arrcateobj: arrcateobj, arrbrandobj: arrbrandobj}, () => {
      this.props.history.push('/search/all') 
      this.fetchData({ pg_page: 1, pg_size: 12, st_col: "created_at", st_full: "created_at:desc", st_type: -1, price: {min: 0, max: 500} })
    })
  }
  animateProduct(key) {
    let checkscroll = this.state.checkscroll
    // let eleDom = document.getElementsByClassName('blockproduct__colwrap')[0]
    // let z = eleDom.scrollWidth - eleDom.clientWidth
    
    if (checkscroll['type'] !== key) {
      checkscroll['number'] = 1
      checkscroll['type'] = key
    }
    if (key === 'next') {
      $('.blockproduct__colwrap').animate({scrollLeft: checkscroll['number']*300}, 500);
    } else {
      $('.blockproduct__colwrap').animate({scrollLeft: -checkscroll['number']*300}, 500);
    }
    if (checkscroll['type'] === key) {
      checkscroll['number'] += 1  
    }
    this.setState({checkscroll: checkscroll})
  }
  render() {
    const { t } = this.props
    let language = this.props.i18n.language
    let totalpage = 0
    if(this.state.quantity && this.state.quantity > 0){
      totalpage = Math.ceil(this.state.quantity / this.state.paging.pg_size)
    }
    
    // let tempOptionSize = [], tempOptionColor = [], tempOptionName = []
    let tempOption = {}
    this.state.option_all.map(option => {
      if (tempOption && !tempOption[option.type]) {
        tempOption[option.type] = []
        if (option && option.value) {
          option.value.map((temp)=> {
            if (temp.key) { 
              return tempOption[option.type].push(temp.value)
            } else {
              return tempOption
            }
          })
        }
        return tempOption
      } else {
        return tempOption
      }
    })
    let categoriestmp = (this.state.searchtext['cat'] && this.state.searchtext['cat'].length > 0) ? this.state.categoryonly.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(this.state.searchtext['cat'])) !== -1) : this.state.categoryonly 

    // let brandtmp = (this.state.searchtext['brand'] && this.state.searchtext['brand'].length > 0) ? this.state.arrcategories.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(this.state.searchtext['brand'])) !== -1) : this.state.arrcategories 
    let categories = categoriestmp.map(category => ( category.is_brand === 0 &&

      <div className={'search-categories-item ' + ((this.state.paging && this.state.arrcateobj.findIndex(cateobj => cateobj._id === category._id ) !==  -1 ) ? 'rx-active': '')} key={category._id}>
        <div className='rx-filter-option-choose' onClick={(e) => this.onClickChangeCat(e, category)}>
          <span className={((this.state.paging && this.state.arrcateobj.findIndex(cateobj => cateobj._id === category._id ) !==  -1) ? 'filter-input iconanna-check': 'filter-input')}></span>
          <span>{language === 'vi' ? category.name : (category.nameEng ? category.nameEng : category.name)}</span>
        </div>
      </div>
    ))
    let brandtmp = []
    if (this.state.arrcateobj && this.state.arrcateobj.length > 0) {
      for (let key in this.state.arrcateobj) {
        if (this.state.brandall && this.state.brandall.length > 0) {
          let obj = this.state.brandall.find((obj) => { return obj._id === this.state.arrcateobj[key]._id })
          if (obj) {
            brandtmp = brandtmp.concat(obj.brand)
          }
          brandtmp = (this.state.searchtext['brand'] && this.state.searchtext['brand'].length > 0) ? brandtmp.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(this.state.searchtext['brand'])) !== -1) : brandtmp
        }
      }
    } else {
      brandtmp = (this.state.searchtext['brand'] && this.state.searchtext['brand'].length > 0) ? this.state.arrcategories.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(this.state.searchtext['brand'])) !== -1) : this.state.arrcategories 
    }
    let brand = []
    if (brandtmp && brandtmp.length > 0) {
      brand = brandtmp.map(category => ( category.is_brand === 1 &&
        <div className={'search-categories-item ' + ((this.state.paging && this.state.arrbrandobj.findIndex(brandobj => brandobj._id === category._id ) !==  -1 ) ? 'rx-active': '')} key={category._id}>
          <div className='rx-filter-option-choose' onClick={(e) => this.onClickChangeBrand(e, category)}>
            <span className={((this.state.paging && this.state.arrbrandobj.findIndex(brandobj => brandobj._id === category._id ) !==  -1) ? 'filter-input iconanna-check': 'filter-input')}></span>
            <span>{ category.name }</span>
          </div>
        </div>
      ))
    }
    let arrcatename = this.state.arrcateobj.map(obj => obj.name)
    if (this.state.products)  {}
    let products = this.state.products.map(product => (
      <div className='rx-product-item col-sm-lg col-sm-3 show' key={product._id}>
        <div className='rx-product-figure'>
          <Link to={`/product/${product.slug}`}>
          <div className='rx-product-figure-header'>
            <img className='rx-product-figure-header-banner lazyload' alt='' data-src={product.is_hot === 0 ? '/images/static/new@3x.jpg': '/images/static/hot@3x.jpg'}/>
            <span to={`/product/${product.slug}`}>
              <img className='rx-product-figure-header-img lazyload' data-src={config.base_api + '/upload/image/' + product.img_landscape} alt='' title='' />
            </span>
          </div>
          <div className='rx-product-figure-body'>
            {/*<div className='figure-body-brand-inlist'>{product.vendor || 'adidas'}</div>*/}
            <div className='figure-body-title'>{product.name}</div>
            <div className='rx-relative'>
              <div className='figure-body-price'>{rxcurrencyVnd(product.price)} </div>
              <div className='figure-body-freeship'></div>
              {product.price_discount ? <div className='price-discount'>{rxcurrencyVnd(product.price_discount)}</div>:<div className='price-discount'></div>}
            </div>
            <div className='clearfix'></div>            
          </div>
          </Link>
          <div className='rx-product-action'>
            <div className='rx-action-card'></div>
            <div className='rx-action-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(product._id, { amount: 1, data: product }) }}><span className='rx-cart mlkicon-Cart'></span></div>
          </div>
        </div>
      </div>
    ))
    let SEOtitle = ((this.state.arrcateobj && this.state.arrcateobj.length > 0) ? this.state.arrcateobj[0].name : 'Tìm kiếm') + ((this.state.paging && this.state.paging.search_name) ? (': ' + this.state.paging.search_name) : '')
    let SEOkeywords = (global.rxu.get(this.state, 'arrcateobj', []).length > 0 && global.rxu.get(this.state.arrcateobj[0], 'tags', []).length > 0) ? helpKeywords(this.state.arrcateobj[0].tags) : 'Anna Wholesale'
    let SEOdescription = (this.state.arrcateobj && this.state.arrcateobj[0] && this.state.arrcateobj[0].desc.length > 0) ? this.state.arrcateobj[0].desc : global.SEOdesc
    // console.log(this.props, '000')
    return (
      <div className='container--grey'><div className='container'><div className='rx-main-page-search'>
        <Helmet>
          <title>{ `${ SEOtitle }` }</title>
          <link rel="canonical" href={`${this.props.location.pathname}`} />
          <meta name="description" content={SEOdescription} />
          <meta name="keywords" content={SEOkeywords} />
        </Helmet>
        <div className='rx-category rx-category-inlist '>
          <div className={'rx-categories rx-categories-inlist rx-categories-filtered '}>
            <div className='rx-categories-header-inlist' onClick={(e) => { this.toggleFiltMobile() }}>{t('PRODUCT FILTERS')}<span className='rx-mobile-show icon-pencil icons'></span></div>            
            <div className='rx-categories-filtered-content'>
              <div className='rx-search-filtered-number'>{t('Have')+' '}<b>{this.state.quantity}</b> {t('Products found')}</div>
              <div className='rx-search-name-detail'>{ (this.state.paging && this.state.paging.search_name) ? <span onClick={() => { this.onClickClearsearch() }}>{t('Start with')} : {this.state.paging.search_name}... <span className='rx-filtered-label-delete iconanna-delete'></span></span> : <span></span>}</div>
              <div className='rx-filtered-group clearfix'>
                { this.props.location.categoryid === '0' ?
                  <span></span> : <div>
                    {(this.state.arrcateobj && this.state.arrcateobj.constructor === Array) && this.state.arrcateobj.map((cateobj, index) => (
                      <div key={cateobj._id}>
                        <span onClick={(e) => this.onClickchangeFilterPaging(e, 'searcharr__id', index)} className='rx-active rx-filtered-size rx-filtered-label'>{language === 'vi' ? cateobj.name : (cateobj.nameEng ? cateobj.nameEng : cateobj.name)} <span className='rx-filtered-label-delete iconanna-delete'></span></span>
                      </div>
                    ))} 
                                  
                    {(this.state.option_all && this.state.option_all.constructor === Array) && this.state.option_all.map(option => (
                      <div key={option._id}>
                        {this.state.paging['searchoption_'+option.type] && this.state.paging['searchoption_'+option.type].constructor === Array && this.state.paging['searchoption_'+option.type].map((index,key) => (<span key={key} onClick={(e) => this.onClickchangeFilter(e, 'searchoption_'+option.type, index)} className='rx-active rx-filtered-size rx-filtered-label'>{index} <span className='rx-filtered-label-delete iconanna-delete'></span></span>)) }
                      </div>  
                    ))}
                  </div>
                }
                {(this.state.arrbrandobj && this.state.arrbrandobj.constructor === Array) && this.state.arrbrandobj.map((cateobj, index) => (
                      <div key={cateobj._id}>
                        <span onClick={(e) => this.onClickchangeFilterPaging(e, 'searchbrand__id', index)} className='rx-active rx-filtered-size rx-filtered-label'>{language === 'vi' ? cateobj.name : (cateobj.nameEng ? cateobj.nameEng : cateobj.name)} <span className='rx-filtered-label-delete iconanna-delete'></span></span>
                      </div>  
                    ))}  
              </div>
              { (this.state.currencyname === 'AUD' && this.state.paging.price && (this.state.paging.price.min || this.state.paging.price.max))? <div className='rx-filtered-group clearfix'>
                {(this.state.paging.price.min && this.state.paging.price.min !== 0 ) ? <span onClick={(e) => this.onClickchangePriceFilter(e, 'min', 0)} className='rx-active rx-filtered-color rx-filtered-label'>&gt; {this.state.paging.price.min} AU<span className='rx-filtered-label-delete iconanna-delete'></span></span>: <div></div>}
                {(this.state.paging.price.max && this.state.paging.price.max !== 500) ? <span onClick={(e) => this.onClickchangePriceFilter(e, 'max', 500)} className='rx-active rx-filtered-size rx-filtered-label'>&lt; {this.state.paging.price.max} AU<span className='rx-filtered-label-delete iconanna-delete'></span></span>: <div></div>}                
              </div>: <div></div> }
              { (this.state.currencyname === 'VND' && this.state.paging.price && (this.state.paging.price.min || this.state.paging.price.max))? <div className='rx-filtered-group clearfix'>
                {(this.state.paging.price.min && this.state.paging.price.min !== 0 ) ? <span onClick={(e) => this.onClickchangePriceFilter(e, 'min', 0)} className='rx-active rx-filtered-color rx-filtered-label'>&gt; {this.state.paging.price.min} đ<span className='rx-filtered-label-delete iconanna-delete'></span></span>: <div></div>}
                {(this.state.paging.price.max && this.state.paging.price.max !== 500*16500) ? <span onClick={(e) => this.onClickchangePriceFilter(e, 'max', 500*16500)} className='rx-active rx-filtered-size rx-filtered-label'>&lt; {this.state.paging.price.max} đ<span className='rx-filtered-label-delete iconanna-delete'></span></span>: <div></div>}                
              </div>: <div></div> }
              <div className='rx-categories-filtered-delete' onClick={(e) => this.onClickDelAllFilter()}>{t('Clear filter')}</div>
            </div>
          </div>
          <div className={'rx-categories rx-categories-inlist rx-categories-filtered' + (this.state.showFilter ? 'rx-show-filter' : '')}>
            <div className='rx-categories-header-inlist rx-filter-title' onClick={(e) => { this.toggleFiltDest('toggleFilCat') }}>{t('Category product')} <span className={!this.state.toggleFilCat ? 'mlkicon-Plus rxmlkiconPlus iconanna-plus' : 'mlkicon-Minus rxmlkiconPlus iconanna-minus'} onClick={(e) => { this.toggleFiltDest('toggleFilCat') }}></span> {this.state.toggleFilCat ? <div className='rx-linebottom'></div> : <div></div>}</div>
            {this.state.toggleFilCat ? <div className='rx-cat-table'> 
              <div className='rx-filter-search'>
                <input type='text' className='boxsearch-inputsearch' placeholder={t('Search categories')} onChange={(e) => this.onBlurDataSeach(e, 'cat')}/>
                <span className='rx-filter-iconsearch iconanna-search'></span>
              </div>
              {/*<div className={'search-categories-item ' + ((!this.state.paging || !this.state.paging.searchid_appdist) ? 'rx-active': '')}>
                <div className='rx-filter-option-choose'>
                  <span className={((!this.state.paging || !this.state.paging.searchid_appdist) ? 'filter-input iconanna-check': 'filter-input')}></span>
                  <Link to={`/search/all`} onClick={(e) => this.onClickChangeCat(e)}>Tất cả danh mục</Link>
                </div>
              </div>*/}
              <div className='rx-cat-bg'>
              {categories}
            </div></div> : <div></div> }
          </div>
          <div className={'rx-categories rx-categories-inlist rx-categories-filtered' + (this.state.showFilter ? 'rx-show-filter' : '')}>
            <div className='rx-filter-container'>
              <div className='rx-filter-title' onClick={(e) => { this.toggleFiltDest('toggleFilPrice') }}>{t('Price FILTERS')} <i>({ this.state.currencyname === 'VND' ? 'VND': this.state.currencyname })</i> <span className={ !this.state.toggleFilPrice ? 'mlkicon-Plus rxmlkiconPlus iconanna-plus' : 'mlkicon-Minus rxmlkiconPlus iconanna-minus' } onClick={(e) => { this.toggleFiltDest('toggleFilPrice') }}></span> {this.state.toggleFilPrice ? <div className='rx-linebottom'></div> : <div></div>}</div>              
              { this.state.currencyname === 'AUD' && this.state.toggleFilPrice ? <div className='rx-filter-body clearfix rx-filter-price'>
                <InputRange step={50} maxValue={this.maxPrice} minValue={this.minPrice} value={this.state.paging.price} onChange={value => this.onChangeFilterPrice(value)} />
              </div> : <div></div> }
              { this.state.currencyname === 'VND' && this.state.toggleFilPrice ? <div className='rx-filter-body clearfix rx-filter-price'>
                <InputRange step={50*16500} maxValue={this.maxPrice} minValue={this.minPrice} value={this.state.paging.price} onChange={value => this.onChangeFilterPrice(value)} />
              </div> : <div></div> }   
            </div>
          </div> 
          <div className={'rx-categories rx-categories-inlist rx-categories-filtered' + (this.state.showFilter ? 'rx-show-filter' : '')}>
            <div className='rx-categories-header-inlist rx-filter-title' onClick={(e) => { this.toggleFiltDest('toggleFilBrand') }}>{t('Brand')} <span className={!this.state.toggleFilBrand ? 'mlkicon-Plus rxmlkiconPlus iconanna-plus' : 'mlkicon-Minus rxmlkiconPlus iconanna-minus'} onClick={(e) => { this.toggleFiltDest('toggleFilBrand') }}></span> {this.state.toggleFilBrand ? <div className='rx-linebottom'></div> : <div></div>}</div>
            {this.state.toggleFilBrand ? <div className='rx-cat-table'> 
              <div className='rx-filter-search'>
                <input type='text' className='boxsearch-inputsearch' placeholder={t('Search brands')} onChange={(e) => this.onBlurDataSeach(e, 'brand')}/>
                <span className='rx-filter-iconsearch iconanna-search'></span>
              </div>
              <div className='rx-cat-bg'>
              {brand}
            </div></div> : <div></div> }
          </div>
          {/*(this.state.option_all && this.state.option_all.constructor === Array) && this.state.option_all.map(option => (
            <div className='rx-categories rx-categories-inlist rx-categories-filtered rx-show-filter' key={option._id} >
              <div className='rx-filter-title' onClick={(e) => { this.toggleFiltDest('toggleFil'+option.type) }}>{option.name} <span className={!this.state['toggleFil'+option.type] ? 'mlkicon-Plus rxmlkiconPlus iconanna-plus' : 'mlkicon-Minus rxmlkiconPlus iconanna-minus'} onClick={(e) => { this.toggleFiltDest('toggleFil'+option.type) }}></span> {this.state['toggleFil'+option.type] ? <div className='rx-linebottom'></div> : <div></div>}</div>
              {this.state['toggleFil'+option.type] ? <div className='rx-filter-search'>
                <input type='text' className='boxsearch-inputsearch' placeholder={'Tìm kiếm ' + option.name.toLowerCase()} onChange={(e) => this.onBlurDataSeach(e, option.type)}/>
                <span className='rx-filter-iconsearch iconanna-search'></span>
              </div> : <div></div>}
              {this.state['toggleFil'+option.type] ? <div className='rx-filter-body clearfix rx-filter-color'>
              { (option.type && tempOption[option.type]) && this.onFilterDataSeach(tempOption[option.type], this.state.searchtext[option.type]).map((value, index) => (
                  <div key={index} onClick={(e) => this.onClickchangeFilter(e, 'searchoption_'+option.type, value)} className={(this.state.paging && this.state.paging['searchoption_'+option.type] && this.state.paging['searchoption_'+option.type].indexOf(value) !== -1 ? 'rx-filter-option rx-hightligh': 'rx-filter-option rx-active')}><div className='rx-filter-option-choose'><span className='filter-input iconanna-check'></span>{value}</div></div>
                ))                                
              }</div> : <div></div>}
            </div>
          ))*/}          
        </div>
        <div className='rx-product-list-container'>
          <div className='rx-product-list-inner'>
            <div className="rx-navbar">
              <div className='list-nav'>{t('Home')}</div>
              <div className='list-nav active'>{(arrcatename.length === 1) ? arrcatename[0] : (arrcatename.length > 1) ? 'Sản phẩm chọn lọc' : t('All product')}</div>
            </div>
            <div className='rx-cat-title'>{t('Product')} <b>{(arrcatename.length === 1) ? arrcatename[0] : (arrcatename.length > 1) ? 'chọn lọc' : t('All product')}</b></div>   
            <div className='rx-cat-banner'>
              <img className='lazyload' alt='' data-src='/images/static/ArrowBackNext/banner@3x.png'/>
            </div>         
            <div className='rx-cat-sorting clearfix'>
              <div className='rx-cat-sorting-name'>
                <i>{t('Sorted by')}:</i>
                <div className='rx-cat-boxsorting'>
                  <div className={(typeof(this.state.paging) !== 'undefined' && this.state.paging.st_full === 'sold:desc') ? 'rx-cat-sorting-category rx-sort-active' : 'rx-cat-sorting-category'}><div onClick={(e) => this.handleChange('sold:desc')} className='rx-cat-sorting-hot rx-text-sorting'><span className='iconanna-star'></span>{t('The hottest')}</div></div>
                  <div className={(typeof(this.state.paging) !== 'undefined' && this.state.paging.st_full === 'created_at:desc') ? 'rx-cat-sorting-category rx-sort-active' : 'rx-cat-sorting-category'}><div onClick={(e) => this.handleChange('created_at:desc')} className='rx-cat-sorting-new rx-text-sorting'><span className='iconanna-arrival'></span>{t('The new')}</div></div>
                  <div className={(typeof(this.state.paging) !== 'undefined' && this.state.paging.st_full === 'price:asc') ? 'rx-cat-sorting-category rx-sort-active' : 'rx-cat-sorting-category'}><div onClick={(e) => this.handleChange('price:asc')} className='rx-cat-sorting-price-increase rx-text-sorting'><span className='iconanna-sortup'></span>{t('Increases')}</div></div>
                  <div className={(typeof(this.state.paging) !== 'undefined' && this.state.paging.st_full === 'price:desc') ? 'rx-cat-sorting-category rx-sort-active' : 'rx-cat-sorting-category'}><div onClick={(e) => this.handleChange('price:desc')} className='rx-cat-sorting-price-decrease rx-text-sorting'><span className='iconanna-sortdown'></span>{t('Decreases')}</div></div>
                </div>  
              </div>
              { (typeof(this.state.paging) !== 'undefined' && this.state.quantity > 0) &&
                <div className='rx-cat-pagin rxhidden'>    
                  <span className='btn-backs iconanna-back' onClick={(e) => this.onClickChangepage(e, 'backfirst')}></span>            
                  { this.state.paging.pg_page > 1 && <span onClick={(e) => this.onClickChangepage(e, 'back')} className='mlkicon-Left btn-backs'></span> }
                  { this.state.paging.pg_page > 1 && this.state.paging.pg_page === totalpage && <span className='rx-number' onClick={(e) => this.onClickChangepage(e, 'backfirst')}>1</span>}
                  { this.state.paging.pg_page > 1 && this.state.paging.pg_page === totalpage && totalpage > 3 && <span className='rx-number'>...</span>} 
                  { this.state.paging.pg_page > 1 && 
                  <span onClick={(e) => this.onClickChangepage(e, 'back')}>{ this.state.paging.pg_page - 1 }</span> }
                  <span className='rx-number rx-active'>{this.state.paging.pg_page}</span>
                  {(this.state.quantity && this.state.quantity > 0 && (this.state.quantity / 16) > this.state.paging.pg_page) && <span className='rx-number' onClick={(e) => this.onClickChangepage(e, 'next')}>{this.state.paging.pg_page + 1}</span>} 
                  {totalpage > (this.state.paging.pg_page + 1) && totalpage > 3 && <span className='rx-number'>...</span>}
                  {totalpage > (this.state.paging.pg_page + 1) && <span className='rx-number total' onClick={(e) => this.onClickChangepage(e, 'nextlast')}>{totalpage}</span>}
                  {(this.state.quantity && this.state.quantity > 0 && (this.state.quantity / 16) > this.state.paging.pg_page) && <span onClick={(e) => this.onClickChangepage(e, 'next')} className='mlkicon-Right btn-nexts rx-back-next-active' ></span>}
                  <span className='btn-nexts iconanna-next' onClick={(e) => this.onClickChangepage(e, 'nextlast')}></span> 
                </div> }              
            </div>
            <Loader isLoading={this.state.loading}>
              <div className='row rx-cat-product'>{products}</div>
            </Loader>
            <div className='clearfix'></div>
            <div className='rx-cat-sorting clearfix martop20'>  
              <span className='rx-sorting-up' onClick={() => this.backtotop()}><span className='rx-cat-back-to-top'><span className='mlkicon-Up icon-Up'></span></span><span className='rx-sorting-up-text'>Back to Top</span></span>      
              { (typeof(this.state.paging) !== 'undefined' && this.state.quantity > 0) &&
              <div className='rx-cat-pagin'>    
                <span className='btn-backs iconanna-back' onClick={(e) => this.onClickChangepage(e, 'backfirst')}></span>            
                { this.state.paging.pg_page > 1 && <span onClick={(e) => this.onClickChangepage(e, 'back')} className='mlkicon-Left btn-backs'></span> }
                { this.state.paging.pg_page > 1 && this.state.paging.pg_page === totalpage && <span className='rx-number' onClick={(e) => this.onClickChangepage(e, 'backfirst')}>1</span>}
                { this.state.paging.pg_page > 1 && this.state.paging.pg_page === totalpage && totalpage > 3 && <span className='rx-number'>...</span>} 
                { this.state.paging.pg_page > 1 && 
                <span onClick={(e) => this.onClickChangepage(e, 'back')}>{ this.state.paging.pg_page - 1 }</span> }
                <span className='rx-number rx-active'>{this.state.paging.pg_page}</span>
                {(this.state.quantity && this.state.quantity > 0 && (this.state.quantity / 16) > this.state.paging.pg_page) && <span className='rx-number' onClick={(e) => this.onClickChangepage(e, 'next')}>{this.state.paging.pg_page + 1}</span>} 
                {(totalpage > (this.state.paging.pg_page + 1) && totalpage > 3) && <span className='rx-number'>...</span>}
                {totalpage > (this.state.paging.pg_page + 1) && <span className='rx-number' onClick={(e) => this.onClickChangepage(e, 'nextlast')}>{totalpage}</span>}
                {(this.state.quantity && this.state.quantity > 0 && (this.state.quantity / 16) > this.state.paging.pg_page) && <span onClick={(e) => this.onClickChangepage(e, 'next')} className='mlkicon-Right btn-nexts rx-back-next-active' ></span>}
                <span className='btn-nexts iconanna-next' onClick={(e) => this.onClickChangepage(e, 'nextlast')}></span> 
              </div> } 
            </div>
          </div>
        </div>
      </div>
      </div>
      {this.state.products_viewed.length > 0 &&
      <div className='blockfull--product-new'>
        <div className='container'>
          <div className='tabs'>
            <div className='tabs-title'>
              <div className='tabs__title'><div className='tabs_title_head'>{t('Product')} <b>{t('watched')}</b></div></div>              
            </div>
            <div className='rx-main-product'>
              <RxProductWatched className='blockproduct' products={this.state.products_viewed} staticContext={this.props.staticContext}/>
              <div onClick={() => { this.animateProduct('next')}} className='blockproduct-arr jcarousel-control-prev carousel__arrow'><span className='mlkicon-ArrowNext mlkicon-Arrow'></span></div>
              <div onClick={() => { this.animateProduct('pre')}} className='blockproduct-arr jcarousel-control-next carousel__arrow'><span className='mlkicon-ArrowBack mlkicon-Arrow'></span></div>
              <Link to={`/search/all`} onClick={(e) => {try {$(window).scrollTop(0)} catch(err) {console.log(err)}}}><div className='blockproduct__more'>{t('SEE MORE')}</div></Link>
            </div>
          </div>
        </div>
      </div>}
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => ({
  cart: state.cart,
  viewed: state.viewed,
  rxsearch: state.rxSearch
})
const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  rxsearchClear
}
const ProductAll = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductAll_)
export default withTranslation('translations')(ProductAll)