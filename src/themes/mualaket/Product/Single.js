import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { polyfill }  from 'es6-promise'
import { connect } from 'react-redux'
import $ from 'jquery'
import { withTranslation } from 'react-i18next'
import Magnifier from "react-magnifier"
import ReactGA from 'react-ga'
import {Helmet} from "react-helmet";

const { rxget, rxpost } = global.rootRequire('classes/request')
const { rxgetdate, rxcurrencyVnd, helpKeywords } = global.rootRequire('classes/ulti')
const { cartClear, cartAdd, cartDelete, favoriteAdd, favoriteDelete, likeAdd, likeDelete, rxnavToggle, rxnavClose, viewedAdd } = global.rootRequire('redux')
const { rxCountStart } = global.rootRequire('components/Helpers/RxCountStart')
const RxProductWatched = global.rootRequiretheme('components/Shares/RxProductWatched').default
const RxRatingbox = global.rootRequiretheme('components/Shares/RxRatingbox').default
const RxCarouselProduct = global.rootRequire('components/Shares/RxCarouselProduct').default

polyfill()
const config = global.rxu.config

class ProductSingle_ extends Component {
  constructor (props) {
    super(props)
    let product = {}
    let productRelated = {}
    let slug = this.props.match.params.slug
    this.ignoreOptions = ['amount', 'key', 'data', 'price', 'name']
    product = []
    productRelated =  []
    
    this.maxPrice = 10000
    this.minPrice = 100

    let abc = global.ultiStaticContext(this.props.staticContext) || []
    
    this.state = {
      checklike:false,
      arrlike: [],
      thumbZoomin: false,
      slideIndex: 1,
      imgdefault: false,
      amount: 1,    
      slideCount: 1,
      product: global.rxu.get(abc, 'product', product), 
      slug: slug,
      productRelated: productRelated,
      choosenOptions: {},
      choosenOptionsAvalable: {},
      choosenOptionsKey: 0,
      choosenOptionsError: '', choosenOptionsSuccess: '',
      ratings: false,
      checkorder: false,
      hover: false,
      changeImgsource: false,
      reviewsrating: {},
      chooseSelect: {chooseCustomer: 0, chooseStart: 0, pg_page: 1, pg_sum: 1},
      arrratings: [],
      paging: { st_full: 'created_at:desc', st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10, price: {min: this.minPrice, max: this.maxPrice} },
      question: {},
      products_viewed: [],
      showAnwser: false,
    }
    this.state.imgbanners = this.state.imgbanners || []
    this.state.cates   = this.state.cates || []
    this.state.products_one = this.state.products_one || []
    this.nextSlide = this.nextSlide.bind(this)
    this.previousSlide = this.previousSlide.bind(this)
    this.updateHoverState = this.updateHoverState.bind(this)
    this.changeImgsource = this.changeImgsource.bind(this)

    this.handleMouseEnter = this.handleMouseEnter.bind(this)
    this.handleMouseLeave = this.handleMouseLeave.bind(this)
    this.state.slideIndex = this.state.slideIndex || 1
      
    this.state.paging = this.state.paging || {}
    this.state.showzoom = false
    this.state.toggleFilAnw = false
    this.state.paging.price = this.state.paging.price || {min: this.minPrice, max: this.maxPrice}
    this.state.checkscroll = {number: 1, type: 'next'}
  }

  componentDidMount () {
    let slug = this.state.slug

    ReactGA.initialize('UA-157753374-1')
    ReactGA.pageview(`/product/${slug}`)

    this.fetchData(slug)
    this.fetchDataRelated()
    this.fetchDataQuestion()
    this.fetchDataBanner()
    this.showSlides(1)
    if (this.props && this.props.viewed && this.props.viewed.products && this.props.viewed.products.constructor === Object) {
      Object.keys(this.props.viewed.products).forEach(key => {
        if (this.props.viewed.products && this.props.viewed.products[key] && this.props.viewed.products[key]['data']) {
          this.state.products_viewed.push(this.props.viewed.products[key]['data'])  
        }
      })
    }

  }

  componentWillReceiveProps (nextProps, nextContext) {
    // if this is change slug 
    if (nextProps.match.params.slug !== this.state.slug) {
      let slug = nextProps.match.params.slug
      this.setState({
        amount: 1,
        slug: slug,
        choosenOptions: {},
        choosenOptionsAvalable: {},
        choosenOptionsKey: 0,
        choosenOptionsError: '', choosenOptionsSuccess: ''
      }, () => { this.fetchData(slug) })
    }    
  }

  fetchData(slug) {
    rxget(config.api_home_productdetail, {slug: escape(slug)}, {
      '1': (json) => {
        if(json.data.product !== null) {
          this.setState({ product: json.data.product }); $('html, body').animate({ scrollTop: 0 }, 200, () => {})
          if (typeof(json.data.product.ratings) !== 'undefined') {
            let arrlen = json.data.product.ratings.length
            let tempChooseSelect = this.state.chooseSelect
            tempChooseSelect.pg_sum = Math.ceil(arrlen / 10)
            if (json.data.product) {
              this.props.viewedAdd(json.data.product._id, { data: json.data.product })  
            } 
            this.setState({chooseSelect: tempChooseSelect}, () => {
              this.parseRatings(this.state.chooseSelect)
            })                  
          }
        }
      }
    })
  }
  
  fetchDataRelated() {
    rxget(config.api_home_product, this.state.paging, {
      '1': (json) => { this.setState({ productRelated: json.data.products }) }
    })
  }

  fetchDataQuestion() {
    rxget(config.api_setting, this.state.paging, {
      '1': (json) => { this.setState({ question: json.data }); }
    })
  }

  onClickOption(e, item, disabled) {    
    
    this.setState({choosenOptionsError: ''})
    let tempChoosenOption = disabled? {} : this.state.choosenOptions
    
    if (typeof(tempChoosenOption[item.type]) !== 'undefined' && tempChoosenOption[item.type].value === item.value) {
      delete(tempChoosenOption[item.type])
    } else {
      tempChoosenOption[item.type] = item
    }

    this.setState({choosenOptions: tempChoosenOption}, () => {      

      // Check if match options set
      let tempOption = this.state.product.options      
      let tempOptionKey = 0
      for (var index in tempOption) {
        let tempCheck = true
        for (var index2 in tempOption[index]) {          
          if (this.ignoreOptions.indexOf(index2) === -1) {
            if (typeof(this.state.choosenOptions[index2]) !== 'undefined' && this.state.choosenOptions[index2]['value'] === tempOption[index][index2]['value']) {
            } else { tempCheck = false }
          }
        }
        if (tempCheck) { tempOptionKey = tempOption[index]['key'] }
      }
      this.setState({choosenOptionsKey: tempOptionKey}, () => {
        
        // Make prepare for available      
        let tempAvailable = {}
        tempAvailable[item.type] = tempAvailable[item.type] || []
        tempAvailable[item.type].push(item.value)

        for (var index in tempOption) {
          let tempCheck = true
          for (var index2 in this.state.choosenOptions) {
           if (tempOption[index][index2] && tempOption[index][index2]['value'] &&  this.state.choosenOptions[index2]['value'] === tempOption[index][index2]['value']) {              
            } else { tempCheck = false }
          }
            
          if (tempCheck) {            
            for (var index3 in tempOption[index]) {              
              if (this.ignoreOptions.indexOf(index3) === -1) {
                tempAvailable[index3] = tempAvailable[index3] || []
                tempAvailable[index3].push(tempOption[index][index3]['value'])                
              }
            }
          }
        }
  
        this.setState({choosenOptionsAvalable: tempAvailable})     
      })
    })    
  }

  nextSlide(e) {
    this.setState({ slideCount: this.state.slideCount + 1 })
  }

  previousSlide(e) {
    this.setState({ slideCount: this.state.slideCount - 1 })
  }

  updateHoverState(hover) {
    this.setState({ hover: hover })
  }

  changeImgsource(imgsource) {
    this.setState({ changeImgsource: imgsource })
  }

  onClickChangeAmount(amount) {
    amount = amount || 1
    let tempAmount = (this.state.amount + amount) >= 1 ? (this.state.amount + amount) : this.state.amount
    this.setState({amount: tempAmount})
  }

  onClickAddCart(e, product, key) {
    e.stopPropagation()    
    if (this.state.choosenOptionsKey || !this.state.product.options || !this.state.product.options.length || this.state.product.options.data !== 'undefined') {
      this.setState({choosenOptionsError: '', choosenOptionsSuccess: 'Mua hàng thành công, đã thêm vào giỏ hàng'})
      this.props.cartAdd(product._id + '|' + this.state.choosenOptionsKey, { amount: this.state.amount, data: product, key: this.state.choosenOptionsKey })
      if (!key) {this.props.rxnavClose(); window.location = '/payment'} else { console.log(2)}
    } else {
      this.setState({choosenOptionsError: 'Tuỳ chọn mua sản phẩm chưa hợp lệ', choosenOptionsSuccess: ''})
    }
    
  }

  onClickAddRemoveFavorite(e, product, checkclick) {
    e.stopPropagation()   
    if (checkclick === false) {
      this.props.favoriteAdd(product._id, { amount: this.state.amount, data: product })  
    } else {
      this.props.favoriteDelete(product._id) 
    }
  }
  onClickAddRemoveLike(e, perdata, checkclick, index) {
    let arrayratings = this.state.arrratings
    e.stopPropagation() 
    if (this.props.auth.user.customerid) {
      if (checkclick === false) {
      if (perdata.arrlikes.constructor === String) {
        perdata.arrlikes = perdata.arrlikes.split(',').filter(Boolean)
      }
      perdata.arrlikes.push(this.props.auth.user.customerid)
      arrayratings[index] = perdata
      let paramsrating = {}
      let reviewsrating = perdata
      paramsrating['ratings'] = {}
      paramsrating['ratings'] = reviewsrating
      paramsrating['ratings']['arrlikes'] = perdata.arrlikes
      paramsrating['ratings']['customerid'] =  perdata.customerid 
      paramsrating['ratings']['name'] = perdata.name
      paramsrating['ratings']['isCustomer'] = perdata.isCustomer
      paramsrating['_id'] = this.state.product._id
      rxpost(config.api_product_rating, paramsrating, {
        '1': (json) => {this.fetchData(this.state.slug); this.setState({ratings: false, reviewsrating: {}, arrratings: arrayratings})}
      })
      } else {
        if (perdata.arrlikes.constructor === String) {
          perdata.arrlikes = perdata.arrlikes.split(',').filter(Boolean)
        }
        let indexuser = perdata.arrlikes.indexOf(this.props.auth.user.customerid)
        if (indexuser > -1) {
          perdata.arrlikes.splice(indexuser, 1)
        }
        arrayratings[index] = perdata
        let paramsrating = {}
        let reviewsrating = perdata
        paramsrating['ratings'] = {}
        paramsrating['ratings'] = reviewsrating
        paramsrating['ratings']['arrlikes'] = perdata.arrlikes
        paramsrating['ratings']['customerid'] =  perdata.customerid 
        paramsrating['ratings']['name'] = perdata.name
        paramsrating['ratings']['isCustomer'] = perdata.isCustomer
        paramsrating['_id'] = this.state.product._id
        rxpost(config.api_product_rating, paramsrating, {
          '1': (json) => {this.fetchData(this.state.slug); this.setState({ratings: false, reviewsrating: {}, arrratings: arrayratings})}
        })
      }
    } else {
      alert('Vui lòng đăng nhập để like')
    }
  }

  onClickCheckRating(e, product) {
    let objrating = {}
    if (typeof(product._id) !== 'undefined' && typeof(product.ratings) !== 'undefined' && product.ratings.length > 0) {
      let customerid = (this.props.auth && this.props.auth.user && this.props.auth.user.customerid) ? this.props.auth.user.customerid : ''
      objrating = product.ratings.find(o => o.customerid === customerid )
      this.setState({reviewsrating: objrating})
    }
    this.setState({ratings: !this.state.ratings})
  }

  onClickDataUpdateSubmit(objreviewsrating) {
    let paramsrating = {}
    let reviewsrating = objreviewsrating
    let randomid = Math.random().toString(36).substr(2) + Math.floor((Math.random() * 90000) + 10000);
    if (reviewsrating && reviewsrating !== 'null' && reviewsrating !== 'undefined') {
      paramsrating['ratings'] = {}
      paramsrating['ratings']['arrlikes'] = []
      paramsrating['ratings'] = reviewsrating
      paramsrating['ratings']['customerid'] = (this.props.auth && this.props.auth.user && this.props.auth.user.customerid) ? this.props.auth.user.customerid : randomid
      paramsrating['ratings']['name'] = (this.props.auth && this.props.auth.user && this.props.auth.user.fullname) ? this.props.auth.user.fullname : 'Anonymous'
      paramsrating['ratings']['isCustomer'] = (this.state.reviewsrating && this.state.reviewsrating.isCustomer === 1) ? 1 : 2
    }
    paramsrating['_id'] = this.state.product._id
    rxpost(config.api_product_rating, paramsrating, {
      '1': (json) => {this.fetchData(this.state.slug); this.setState({ratings: false, reviewsrating: {}})}
    })
  }

  onBlurDataSelect(e, name) {
    let tempChooseSelect = this.state.chooseSelect    
    if (name === 'chooseCustomer') {
      tempChooseSelect.chooseCustomer = Number(e.target.value)
    }
    if (name === 'chooseStart') {
      tempChooseSelect.chooseStart = Number(e.target.value)
    }

    this.setState({chooseSelect: tempChooseSelect}, () => {
      this.parseRatings(this.state.chooseSelect)
    })    
  }

  onClickPaginBack(e) {
    let paging = this.state.chooseSelect
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ chooseSelect: paging }, () => { this.parseRatings(paging) })
  }

  onClickPaginNext(e) {
    let paging = this.state.chooseSelect
    paging.pg_page += 1
    this.setState({ chooseSelect: paging }, () => { this.parseRatings(paging) })
  }

  onClickScrolltoId(id) {
    if (typeof $('#' + id).offset !== 'undefined') {
      $('html, body').animate({ scrollTop: 0 }, 300, () => {})
    }
  }

  parseRatings(chooseSelect) {
    let arrayratings = this.state.product.ratings
    let arraytmp = []
    if (typeof(this.state.product.ratings) !== 'undefined' && this.state.product.ratings.length > 0) {
      for(var i = 0; i < arrayratings.length; i++)
      {       
        if (chooseSelect.chooseStart !== 0 && chooseSelect.chooseCustomer !== 0) {
          if (arrayratings[i].score === chooseSelect.chooseStart && arrayratings[i].isCustomer === chooseSelect.chooseCustomer) arraytmp.push(arrayratings[i])
        } else if (chooseSelect.chooseStart === 0 && chooseSelect.chooseCustomer !== 0) {
          if (arrayratings[i].isCustomer === chooseSelect.chooseCustomer) arraytmp.push(arrayratings[i])
        } else if (chooseSelect.chooseStart !== 0 && chooseSelect.chooseCustomer === 0) {
          if (arrayratings[i].score === chooseSelect.chooseStart) arraytmp.push(arrayratings[i])
        } else {
          arraytmp.push(arrayratings[i])
        }
      }
    }

    let tempChooseSelect = this.state.chooseSelect
    tempChooseSelect.pg_sum = Math.ceil(arraytmp.length/10)
    this.setState({chooseSelect: tempChooseSelect})

    let arraypagin = arraytmp.slice((chooseSelect.pg_page-1)*10, ((chooseSelect.pg_page-1)*10)+10)
    this.setState({ arrratings: arraypagin.reverse()})
  }


  helpOptionAvailable(item) {    
  }

  helpShowOptions(choosenOption) {
    choosenOption = choosenOption || {}  
    let result = []    
    let options = this.state.product.options || []
    
    for (let i = 0; i<= options.length - 1; i++) {      
      for(let index in options[i]) {
        if (this.ignoreOptions.indexOf(index) === -1) {          
          result[index] = result[index] || {}
          result[index][options[i][index]['value']] = options[i][index]
        }
      }
    }
    return result
  }

  plusSlides(e) {
    let slideIndex = this.state.slideIndex
    slideIndex += e
    this.setState({slideIndex: slideIndex}, ()=> {this.showSlides(this.state.slideIndex)})
  }

  currentSlide(e) {
    let slideIndex = this.state.slideIndex
    slideIndex = e
    this.setState({slideIndex: slideIndex}, ()=> {this.showSlides(this.state.slideIndex)})
  }

  showSlides(e) {
    let i
    let slideIndex = this.state.slideIndex
    let imgdefault = this.state.imgdefault
    let slides = $('.mySlides')
    let dots = $('.dot')
    if (e > slides.length) { slideIndex = 1 }    
    if (e < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = 'none'  
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(' active', '')
    }
    if (slides && slides[0] !== undefined && dots && dots[0] !== undefined) { 
      this.setState({ slideIndex: slideIndex }, ()=> {
        slides[this.state.slideIndex-1].style.display = 'block'  
        dots[this.state.slideIndex-1].className += ' active'
      })
    } else {
      imgdefault = true
      this.setState({imgdefault: imgdefault})
    }     
  }

  thumbZoomin(e) {
    e.stopPropagation()
    $('html, body').animate({ scrollTop: 0 }, 100, () => {})
    if(this.state.thumbZoomin) { $('body').removeClass('ff-scroll-disabled-sg')} else { $('body').addClass('ff-scroll-disabled-sg')}
    let thumbZoomin = this.state.thumbZoomin
    this.setState({thumbZoomin: !thumbZoomin})
  }

  handleMouseEnter(e) {
    this.setState({showzoom: true})
  } 

  handleMouseLeave(e) {
    this.setState({showzoom: false})
  }

  animateProduct(key) {
    let checkscroll = this.state.checkscroll
    // let eleDom = document.getElementsByClassName('blockproduct__colwrap')[0]
    // let z = eleDom.scrollWidth - eleDom.clientWidth
    
    if (checkscroll['type'] !== key) {
      checkscroll['number'] = 1
      checkscroll['type'] = key
    }

    if (key === 'next') {
      $('.blockproduct__colwrap').animate({scrollLeft: checkscroll['number']*300}, 500);
    } else {
      $('.blockproduct__colwrap').animate({scrollLeft: -checkscroll['number']*300}, 500);
    }

    if (checkscroll['type'] === key) {
      checkscroll['number'] += 1  
    }

    this.setState({checkscroll: checkscroll})
  }

  toggleFiltQuest(key, id) {
    let k = this.state[key]
    this.setState({[key]: !k})
  }

  fetchDataBanner() {
    rxget(config.api_home, this.state.pagingcat, {
      '1': (json) => {
        this.setState({ imgbanners: json.data.imgbanners }) }
    })
  }

  render () {  
    const { t } = this.props
    let language = this.props.i18n.language
    let product = this.state.product
    let catestr = (product && product.appdistobj && product.appdistobj.length > 0 && product.appdistobj[0] && product.appdistobj[0]['name']) ? product.appdistobj[0]['name'] : ''
    let productRelated = this.state.productRelated    
    if (product && product.img_detail && !product.image_list) { product.image_list = product.img_detail }
    if (product && product.image_list && typeof(product.image_list) === 'string') { 
      product.image_list = product.image_list.split(',').filter(Boolean)
    }
    product.image_list = product.image_list && product.image_list.length>0 ? product.image_list : [product.img_landscape]
    let arrStart = Array.apply(null, Array(5)).map((i) => { return {count: 0, width: 0, sum: 0};})
    let arrRatingTemplate = []
    if (this.state.product && typeof(this.state.product.ratings) !== 'undefined') {
      let arrRatings = this.state.product.ratings
      let sumScore = 0
      let countUser = 0
      for (let key in arrRatings) {
        let countstart = arrRatings[key]['score'] - 1
        sumScore += arrRatings[key]['score']
        arrStart[countstart]['count'] += 1
        countUser += 1
      }
      if (sumScore !== 0) {
        for (let key in arrStart) {
          arrStart[key]['count'] = Math.ceil((arrStart[key]['count'] / countUser * 100))
        }
      } 
      arrRatingTemplate = this.state.arrratings.map((perdata,index) => (
        <div className='lk-reviews-line-main' key={index}>
          <div className='lk-reviews-line-main-body'>
            <div className='lk-reviews-main-title-select'>
              <div className='lk-position-img-icon-account'><div className='figure-body-account mlkicon-Profile'></div></div>
              <div className='lk-reviews-line-body-name'>{perdata.name}</div>
              <div className='lk-reviews-line-body-time'>{rxgetdate(perdata.created_at)}</div>
            </div>
            <div className='lk-reviews-line-main-right'>
              <div className='lk-reviews-line-start'>
                <div className='lk-reviews-line-info'>
                  <div className='lk-reviews-line-header-score'>{rxCountStart(perdata.score)}</div>
                  <div className='lk-reviews-line-header-title'>{perdata.title}</div>
                </div>
              </div>
              {(perdata.isCustomer === 1) && <div className='lk-reviews-line-start'>
                <div className='lk-reviews-line-info'>
                  <div className='figure-body-security'></div>
                  <div className='lk-notifi-buyed'>Đã mua sản phẩm</div>
                </div>
              </div>}
              <div className='lk-reviews-line-start'>
                <div className='lk-reviews-line-content'><span>{perdata.content}</span></div>
              </div>
              <div className='lk-reviews-line-start'>
                <div className='lk-reviews-line-like'>
                  {(perdata && perdata.arrlikes && perdata.arrlikes.constructor === Array && this.props.auth && this.props.auth.user && this.props.auth.user.customerid && perdata.arrlikes.indexOf(this.props.auth.user.customerid) !== -1) &&  <div className='lk-reviews-line-like-button' onClick={(e) => this.onClickAddRemoveLike(e, perdata, true, index)}> <span className='iconanna-like'></span>Unlike</div>}
                  {(perdata && perdata.arrlikes && perdata.arrlikes.constructor === Array && this.props.auth && this.props.auth.user && this.props.auth.user.customerid && perdata.arrlikes.indexOf(this.props.auth.user.customerid) === -1) && <div><span className='lk-reviews-line-like-text'>Nhận xét hữu ích với bạn?</span><span className='lk-reviews-line-like-button-start' onClick={(e) => this.onClickAddRemoveLike(e, perdata, false, index)}> <span className='iconanna-like'></span>Like</span></div>}
                </div>
              </div>
            </div>
          </div>
        </div>
      ))
    }
    let arrProcessTemplate = arrStart.reverse().map((perdata, index) => (
      <li key={index} className='lk-list-product-rating-gui clearfix'>
        <span className='lk-list-product-rating-start'>{5 - index} <div className="rx-star"></div></span>
        <span  className='lk-list-rating-progress'>
          <span className='lk-list-rating-progress-bar'>
            <span className='lk-list-rating-progress-bar-choosed' style={{width: perdata.count+'%'}}></span>
          </span>
        </span>
        <span className='lk-list-product-rating-count'>{perdata.count}%</span>
      </li>
    ))

    let tempCartDetail = (<div></div>)
    if (this.props.cart) {
      tempCartDetail = Object.keys(this.props.cart.carts).map(key => {
        let temp = key.split('|')
        let tempOption = {}
        if (this.props.cart && this.props.cart.carts && this.props.cart.carts[key] && this.props.cart.carts[key].data && this.props.cart.carts[key].data.options && this.props.cart.carts[key].data.options[temp[1]]) {
          tempOption = this.props.cart.carts[key].data.options[temp[1]]
        }
        for(let i in tempOption) { if (i.indexOf(tempOption[i]) !== -1) { delete tempOption[i]}}
        // if (tempOption && tempOption['key']) {delete tempOption['key']}
        // if (tempOption && tempOption['data']) {delete tempOption['data']}
        
        return(
        <div key={key} className='clearfix'>
          { (this.props.cart.carts[key] && this.props.cart.carts[key].data) && 
          <div style={{position: 'relative'}}>
            <div className='cart-popup-product-detail clearfix'>
              <div className='clearfix' style={{borderBottom: 'solid 1px #eee', paddingBottom: '11px'}}>
                <div className='cart-popup-product-rm' onClick={() => { this.props.cartDelete(this.props.cart.carts[key].id) }}><span className='mlkicon-Close mlkiconClose'></span></div>
                <Link to={`/product/${this.props.cart.carts[key].data.slug}`}>   
                  <img className='cart-popup-product-img lazyload' alt='ico_default' data-src={config.base_api + '/upload/image/' + (this.props.cart.carts[key].data.img_landscape || 'ico_app_default.png')} />
                </Link>
                
                <div className='cart-popup-detail' style={{float: 'left', width: '75%'}}>
                  <div className='cart-popup-brand rxfigure-body-brand-inlist'></div>
                  <Link to={`/product/${this.props.cart.carts[key].data.slug}`}><div className='cart-popup-product-name'>{this.props.cart.carts[key].data.name}</div></Link>
                  <div className='cart-popup-product-'>
                    <div className={this.props.cart.carts[key].data.price_discount !== 0 ? 'cart-popup-product-price active' : 'cart-popup-product-price'}>{rxcurrencyVnd(this.props.cart.carts[key].data.price)}</div>
                    {this.props.cart.carts[key].data.price_discount ? <div className='cart-popup-product-priceold'>{rxcurrencyVnd(this.props.cart.carts[key].data.price_discount)}</div> : <div></div> }
                  </div>
                  {/*tempOption && tempOption['size'] ? <div className='cart-popup-product-option'>Dung tích: <b> {tempOption['size']['text']}</b></div> : <div></div> }
                  {tempOption && tempOption['mass'] ?<div className='cart-popup-product-option'>Khối lượng: <b>{tempOption['mass']['text']}</b></div> : <div></div> }
                  {tempOption && tempOption['name'] ?<div className='cart-popup-product-option'>Phân loại hàng: <b>{tempOption['name']['text']}</b></div> : <div></div> }
                  {tempOption && tempOption['option'] ?<div className='cart-popup-product-option'>{tempOption['option']['type']}: <b>{tempOption['option']['value']}</b></div> : <div></div> */}
                  
                  <div className='cart-popup-product-option'>{t('Quantity')}: <b>{this.props.cart.carts[key].amount}</b></div>
                  
                </div>
              </div>                
            </div>            
          </div> }
        </div>
        )
      }) 
    }

    let arrfavorite = []
    if (this.props.favorite && this.props.favorite.favorites) {
      for (var key in this.props.favorite.favorites) { arrfavorite.push(key) }
    }
    let keywords = (this.state.product.tags && this.state.product.tags.length > 0 ? helpKeywords(this.state.product.tags) : catestr)
    let description = (this.state.product.desc && this.state.product.desc.length > 0) ? this.state.product.desc : global.SEOdesc

    return (
      <div>{this.state.product.name ? <div className='container--grey' id='container--grey'>
        <Helmet>
          <title>{this.state.product.name}</title>
          <link rel="canonical" href={`/product/${product.slug}`} />
          <meta name="description" content={description} />
          <meta name="keywords" content={keywords} />
        </Helmet>
        <div className='rx-product-buynow-bottom rx-sticky'>
          <div className='container rx-product-buynow row' onClick={(e) => this.onClickScrolltoId('desc')}>
            <div className='col-sm col-md-6'>
              <Link to={`/product/${product.slug}`}>
                <img className='rx-product-figure-header-img lazyload' data-src={config.base_api + '/upload/image/' + product.img_landscape} alt='' title='' />
              </Link>
              <div className='rx-product-buynow-info'>
                <div className='rxname'>{product.name}</div>
                <div className={product.price_discount === 0 ? 'rxprice' : 'rxprice active'}>{rxcurrencyVnd(product.price)} {/*<i>Đã có VAT</i>*/}</div>
                {product.price_discount !== 0 ? <div className='rxprice-discount'><span>{rxcurrencyVnd(product.price_discount)}</span></div> : <div></div>}
              </div>
            </div>
            <div className='col-sm col-md-6 rx-product-buynow-right'>
              
              <div className='rx-quanty-order'>
                <div className='rx-quanty-order-left'>{t('Quantity')}:</div>
                <div className='rx-quanty-order-right'>
                  <div className='rx-quanty-order-right-head'></div>
                  <div className='rx-quanty-order-right-body'>
                    <button onClick={(e) => this.onClickChangeAmount(-1)} className='rx-quanty-order-right-body-minus'>-</button>
                    <input type='number' value={this.state.amount} readOnly='true' className='rx-quanty-input'/>
                    <button onClick={(e) => this.onClickChangeAmount(1)} className='rx-quanty-order-right-body-plus'>+</button>
                  </div>
                </div>
              </div>
              <div className='rx-action-order'>
                <div className='rx-action-addcart-mainbox clearfix'>
                  <div className='rx-action-addcart rx-action-addcart-buynow' onClick={(e) => this.onClickAddCart(e, product)}><span className='rx-cart-buynow mlkicon-Cart'></span>{t('Buy now')}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='container rx-product-detail-page'>
          
          <div className="rx-product-detail-navbar">
            <div className='list-nav'>{t('Home')}</div>
            <div className='list-nav'>{catestr || ''}</div>
            <div className='list-nav active'>{product.name || ''}</div>
          </div>

          <div className='rx-main-page'>        
            { product?
            <div>
            <div className='row row-nopad-col product-content'>
              {/*<div className='product-content-nav-holder'>
                <div className='product-content-nav rx-sticky'>
                  <div onClick={(e) => this.onClickScrolltoId('detail')} className='product-content-nav-item rx-active rxiddetail'>Chi tiết sản phẩm</div>
                  <div onClick={(e) => this.onClickScrolltoId('desc')} className='product-content-nav-item rxiddesc'>Mô tả sản phẩm</div>
                  <div onClick={(e) => this.onClickScrolltoId('review')} className='product-content-nav-item rxidreview'>Đánh giá & nhận xét</div>
                  <div onClick={(e) => this.onClickScrolltoId('faq')} className='product-content-nav-item rxidfaq'>Hỏi đáp</div>              
                </div>
              </div>*/}
              
              <div className='product-detail-content-wrap'>
                <div className='col-sm-lg col-sm-9'>
                  <div className={this.state.thumbZoomin ? 'product-left-img image-active' : 'product-left-img'}>
                  <div className={this.state.thumbZoomin ? 'thumbZomin-active' : 'thumbZomin-hidden'} onClick={(e) => this.thumbZoomin(e)}><span className='mlkicon-Close icon-Close'></span></div>
                  {product && product.image_list !== undefined && 
                    <RxCarouselProduct>
                      {product.image_list && product.image_list.map((per, index) => (
                      <div className='rx-thumb' key={index} onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave}>
                        <Magnifier id={'myimage'+index} className='' src={config.base_api + '/upload/image/' + per} alt='' title={product.name} mgShape='square'/>
                      </div>
                      ))}
                    </RxCarouselProduct>
                  }
                  
                  {/*<div className={(this.state.showzoom) ? '' : 'img-hide-zoom'}><div id='myresult' className='img-zoom-result'></div></div>*/}
                  <div className='product-left-img-zomm'><span className='iconanna-zoom'></span>{t('Hover your mouse over the image to see it enlarge')}</div>
                  </div>
                  <div className='product-left-social'>
                    <div className='product-left-social-left'></div>
                    <div className='product-left-social-right'>Chia sẻ đến <span></span></div>
                  </div>
                  <div className='content product-right'>
                    {/*<div className='rxfigure-body-brand-inlist'>{product.vendor || 'adidas'}</div>*/}
                    {product.code && <div className='rxid'>{t('Product ID')}: {product.code}</div>}
                    <div className='rxname'>{language === 'vi' ? product.name : (product.nameEng ? product.nameEng : product.name)}</div>
                    <div className='clearfix'>
                      <div className='figure-body-rating-star'>
                        {rxCountStart(product.ratingsSumary)}
                        <div className='rx-star-text'>({(typeof(product.ratings) !== 'undefined') && product.ratings.length} {(typeof(product.ratings) === 'undefined') && 0})</div>
                      </div>
                      {/*<div className='figure-body-brand'>Thương hiệu : <b>{product['vendor']}</b></div>*/}
                    </div>

                    <div className='rxprice-container'>
                      <div className={product.price_discount === 0 ? 'rxprice' : 'rxprice active'}>{rxcurrencyVnd(product.price)} {/*<i>Đã có VAT</i>*/}</div>
                      {product.price_discount !== 0 ? <div className='rxprice-discount'><span>{rxcurrencyVnd(product.price_discount)}</span></div> : <div></div>}
                    </div>
                    {/*<div className='rxline'></div>
                    <div className='rx-info-shipping'>
                      <div className='rx-pre-order'>Đặt hàng trước <i>(có hàng sau 25 ngày)</i>.</div>
                      <div className='rx-pre-order'>Miễn Phí Vận Chuyển cho đơn hàng có giá trị từ 99.000₫ (giảm tối đa 40.000₫)</div>              
                    </div>
                    <div className='rxline'></div>*/}
                    { this.state.product.grams && this.state.product.grams>0 ?
                      <div className='rx-quanty'>
                        <div className='rx-quanty-order mass'>
                          <div className='rx-quanty-order-left'>{t('Weight')}:</div>
                          <div className='rx-quanty-order-right'>
                            <div className='rx-quanty-order-right-head'></div>
                            <div className='rx-quanty-order-right-body'>
                              <div className='rx-order-size clearfix'>
                              <span>{this.state.product.grams} gram</span></div>
                            </div>
                          </div>
                        </div> 
                      </div>: <div></div> }

                    <div className='rx-quanty-order'>
                      <div className='rx-quanty-order-left'>{t('Quantity')}:</div>
                      <div className='rx-quanty-order-right'>
                        <div className='rx-quanty-order-right-head'></div>
                        <div className='rx-quanty-order-right-body'>
                          <button onClick={(e) => this.onClickChangeAmount(-1)} className='rx-quanty-order-right-body-minus'>-</button>
                          <input type='number' value={this.state.amount} readOnly='true' className='rx-quanty-input'/>
                          <button onClick={(e) => this.onClickChangeAmount(1)} className='rx-quanty-order-right-body-plus'>+</button>
                        </div>
                      </div>
                      <div className='rx-action-favorite clearfix'>
                        {(arrfavorite.indexOf(product._id) !== -1) && <div><div className='rx-heartfill-medium mlkicon-HeartSolid' onClick={(e) => this.onClickAddRemoveFavorite(e, product, true)}></div> <span className='rx-favorite-text'>{t('Product')+' '}{t('favorite')}</span></div>}
                        {(arrfavorite.indexOf(product._id) === -1) && <div><div className='rx-heart-medium mlkicon-Heart' onClick={(e) => this.onClickAddRemoveFavorite(e, product, false)}></div> <span className='rx-favorite-text'>{t('Product')+' '}{t('favorite')}</span></div>}
                      </div>
                    </div>

                    {/*<div className='rxline'></div>*/}
                    <div className='rx-action-order'>
                      <div className='rx-action-addcart-info'>
                        {this.state.choosenOptionsError && <div className='rx-action-addcart-error'>{this.state.choosenOptionsError}</div> }
                        {this.state.choosenOptionsSuccess && <div className='rx-action-addcart-success'>{this.state.choosenOptionsSuccess}</div> }
                      </div>
                      <div className='rx-action-addcart-mainbox clearfix'>
                        <div className='rx-action-addcart rx-action-addcart-buynow' onClick={(e) => this.onClickAddCart(e, product)}><span className='rx-cart-buynow mlkicon-Cart'></span>{t('Buy now')}</div>
                        <div className='rx-action-addcart rx-action-addcart-lg' onClick={(e) => this.onClickAddCart(e, product,'key')}>{t('Add to cart')}</div>                                      
                      </div>
                    </div>
                  </div>
                  <div className='row row-nopad-col product-detail-block' id='desc'>
                    {/*<div className='product-detail-block-title'>THÔNG TIN CHI TIẾT</div>*/}
                    <div className='row row-nopad-col rx-pad-30'>
                      {/*<div className=' rx-product-desc-col'> */}
                        <div className='rx-product-desc-col-name'><div className='rx-product-desc-col-text'><span className='rx-cart-buynow mlkicon-Cart'></span>{t('Product description')}</div></div>
                        <div dangerouslySetInnerHTML={{__html: product['content']}} />
                      {/*</div>*/}
                    </div>            
                  </div>

                  <div className='row row-nopad-col product-detail-block' id='review'>
                    {/*<div className='product-detail-block-title'>ĐÁNH GIÁ & NHẬN XÉT</div>*/}
                    <div className=' row row-nopad-col rx-pad-30'>
                    <div className='rx-product-desc-col-name'><div className='rx-product-desc-col-text'><span className='rx-cart-buynow mlkicon-Cart'></span>{t('Customer reviews')}</div></div>
                      <div className='col-sm col-md-12'>    
                        <div>
                          <div className='lk-product-reviews-title'></div>
                          <div className='lk-reviews-start-main'>                  
                            <div className='lk-reviews-progress-start-position'>
                              <div className='lk-reviews-start'>
                                <div className='lk-reviews-start-title'>{t('Average rating')}</div>
                                <div className='lk-reviews-start-divice'>{product.ratingsSumary}/5</div>
                                <div className='lk-reviews-start-icon clearfix'>{rxCountStart(product.ratingsSumary, true)}</div>
                                <div className='lk-reviews-start-command'>({(typeof(product.ratings) !== 'undefined') && product.ratings.length} {(typeof(product.ratings) === 'undefined') && 0}  {t('reviews')})</div>
                              </div>
                            </div>

                            <div className='lk-reviews-progress-center-position'>
                              <div className='lk-reviews-progress-start'>
                                <ul>
                                  {arrProcessTemplate}
                                </ul>
                              </div>
                            </div>
                            
                            <div className='lk-reviews-function-command'>
                              <div className='lk-reviews-function-command-title'>{t('Review this product')}</div>
                              <div className='lk-reviews-function-command-width lk-reviews-function-btn' onClick={(e) => this.onClickCheckRating(e, product)}><span className='mlkicon-Write'></span>{t('Write a review')}</div>
                            </div>
                          </div>
                          {this.state.ratings === true && <div>                    
                            <RxRatingbox reviewsrating={this.state.reviewsrating} infodata={product} onChange={(result) => {this.onClickDataUpdateSubmit(result)}} /> 
                          </div>}
                
                        </div>
                        <div>
                          {/*<div className='lk-reviews-main-title'><span>Nhận xét về sản phẩm</span></div>*/}
                          <div>
                            <div className='lk-reviews-main-box clearfix'>
                              <div className='lk-reviews-main-title-select'>{t('Filter reviews')}</div>
                              <div className='lk-reviews-line-start'>
                                <div className='lk-reviews-line-start-score'>
                                  <select className='fullwidth-input' onChange={(e) => this.onBlurDataSelect(e, 'chooseStart')} value={this.state.valueChooseStart}>
                                    <option value={0}>{t('Useful')}</option>
                                    <option value={1}>1 {t('star')}</option>
                                    <option value={2}>2 {t('star')}</option>
                                  </select>
                                </div>
                                <div className='lk-reviews-line-start-score'>
                                  <select className='fullwidth-input' onChange={(e) => this.onBlurDataSelect(e, 'chooseCustomer')} value={this.state.valueChooseCustomer}>
                                    <option value={0}>{t('All customers')}</option>
                                    <option value={1}>{t('Customers have purchased the product')}</option>
                                    <option value={2}>{t('Customers have not purchased products.')}</option>
                                  </select>
                                </div>
                                <div className='lk-reviews-line-start-score'>
                                  <select className='fullwidth-input' onChange={(e) => this.onBlurDataSelect(e, 'chooseStart')} value={this.state.valueChooseStart}>
                                    <option value={0}>{t('All star')}</option>
                                    <option value={1}>1 {t('star')}</option>
                                    <option value={2}>2 {t('star')}</option>
                                    <option value={3}>3 {t('star')}</option>
                                    <option value={4}>4 {t('star')}</option>
                                    <option value={5}>5 {t('star')}</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            {this.state.arrratings && this.state.arrratings.length > 0 ? arrRatingTemplate : <div className='lk-reviews-empty'>Chưa có nhận xét</div>}
                            {this.state.arrratings && this.state.arrratings.length > 0 && <div className='admin-table-pagination admin-pagin-right' style={{marginTop: '15px'}}>
                              {(this.state.chooseSelect.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                              <div className='pagin-curr'>{this.state.chooseSelect.pg_page}</div>
                              {(this.state.chooseSelect.pg_sum >= this.state.chooseSelect.pg_page) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
                            </div>}
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
                <div className='col-sm-lg col-sm-3'>
                  <div className='rx-product-relasted'>
                    <div className='rx-product-relasted-title'>{t('Products of the same type')}</div>
                    <div className='rx-product-relasted-content'>
                    { (this.state.productRelated && this.state.productRelated.length) ?
                      productRelated.slice(5,9).map((perdata, index) => 
                      <div key={index} className={(index % 2) ?'rx-product-relasted-content-post post-background1':'rx-product-relasted-content-post post-background2'}>
                        <div className='rx-product-relasted-content-body'>
                          <Link to={`/product/${perdata.slug}`}>
                            <div className='rx-product-relasted-content-post-left'>
                              <img className='lazyload' data-src={config.base_api + '/upload/image/' + perdata.img_landscape} alt='' title={perdata.name} />              
                            </div>
                          </Link>
                          <div className='rx-product-relasted-content-post-right'>
                            <Link key={index} to={`/product/${perdata.slug}`}>
                              <div className='rx-product-relasted-content-post-title'>{language === 'vi' ? perdata.name : (perdata.nameEng ? perdata.nameEng : perdata.name)}</div>
                              <div className='rx-product-relasted-content-post-price'>{rxcurrencyVnd(perdata.price)} {perdata.price_discount !== 0 ? <div className='post-price-old'>{rxcurrencyVnd(perdata.price_discount)}</div> : <span></span>}</div>
                            </Link>
                            <div className='rx-product-action'>              
                              <div className='rx-product-relasted-content-post-addcart' onClick={(e) => this.onClickAddCart(e, product,'key')}><span className='rx-cart mlkicon-Cart'></span></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      )
                      : <div></div> }
                    </div>
                  </div>
                  <div className='rx-commercial'>
                    {this.state.imgbanners && this.state.imgbanners.length !== 0  && this.state.imgbanners.slice(0,3).map((perdata, index) =>
                      <div key={index}>
                        <Link to={perdata.url}><img className='lazyload' alt='' data-src={config.base_api + '/upload/image/' + perdata.img_landscape } /></Link>
                      </div>
                      )}
                  </div>
                  <div className='rx-product-relasted'>
                    <div className='rx-product-relasted-title'>{t('Related products')}</div>
                    <div className='rx-product-relasted-content'>
                    { (this.state.productRelated && this.state.productRelated.length) ?
                      productRelated.slice(0,4).map((perdata, index) => 
                      <div key={index} className={(index % 2) ?'rx-product-relasted-content-post post-background1':'rx-product-relasted-content-post post-background2'}>
                        <div className='rx-product-relasted-content-body'>
                          <Link to={`/product/${perdata.slug}`}>
                            <div className='rx-product-relasted-content-post-left'>
                              <img className='lazyload' data-src={config.base_api + '/upload/image/' + perdata.img_landscape} alt='' title={perdata.name} />              
                            </div>
                          </Link>
                          <div className='rx-product-relasted-content-post-right'>
                            <Link key={index} to={`/product/${perdata.slug}`}>
                              <div className='rx-product-relasted-content-post-title'>{language === 'vi' ? perdata.name : (perdata.nameEng ? perdata.nameEng : perdata.name)}</div>
                              <div className='rx-product-relasted-content-post-price'>{rxcurrencyVnd(perdata.price)} {perdata.price_discount !== 0 ? <div className='post-price-old'>{rxcurrencyVnd(perdata.price_discount)}</div> : <span></span>}</div>
                            </Link>
                            <div className='rx-product-action'>              
                              <div className='rx-product-relasted-content-post-addcart' onClick={(e) => this.onClickAddCart(e, product,'key')}><span className='rx-cart mlkicon-Cart'></span></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      )
                      : <div></div> }
                    </div>
                  </div>
                  <div className='row row-nopad-col product-detail-block' id='faq'>
                    {/*<div className='product-detail-block-title'>HỎI ĐÁP</div>*/}
                    {/*<div className='row row-nopad-col rx-product-faq'>
                      <div className='rx-product-faq-title'>Câu hỏi thường gặp</div>
                      {questionArr}
                    </div>*/}
                  </div>
                </div>
              </div>
            </div>

            {this.props.rxnav.navs.rxcartpopup && 
              <div className='lk-modal'>
                <div className='lk-modal-content' onClick={(e) => { e.stopPropagation()}}>
                  <div className='lk-modal-header'>
                    <div className='lk-modal-title'>Đặt hàng</div>
                    <div className='lk-modal-close' onClick={(e) => this.props.rxnavClose()}>x</div>
                  </div>
                  <div className='lk-modal-body'>
                    <div className='lk-order-refund'>                                  
                      { this.props.cart && Object.keys(this.props.cart.carts).length !== 0 ?
                        <div>
                          <div className='cart-popup-header'>Giỏ hàng có {Object.keys(this.props.cart.carts).length} sản phẩm</div>
                          <div className='cart-popup-body'>{tempCartDetail}</div>
                          <div className='lk-button-payment-countinuos clearfix'>
                            <div><Link to='/payment' onClick={() => { this.props.rxnavClose() }}><div className='cart-popup-payment'>Đặt hàng</div></Link> </div>
                            <div><div className='cart-popup-continute-order cart-popup-payment' onClick={(e) => this.props.rxnavClose()} >Tiếp tục mua hàng</div></div>
                          </div>
                        </div> : <div>
                          <div className='cart-popup-header'>Giỏ hàng của bạn</div>
                          <div className='cart-popup-emptytext'>Giỏ hàng đang trống, Hãy tiếp tục lựa chọn sản phẩm</div>                            
                        </div>}
                    </div>
                  </div>
                </div>
              </div> }
          </div> : <div><div className='product detil-page'>Không tìm thấy sản phẩm</div></div> }
        </div></div>
        
        <div className='blockfull--product-new'>
          <div className='container'>
            {this.state.products_viewed.length > 0 &&
            <div className='tabs'>
              <div className='tabs-title'>
                <div className='tabs__title'><div className='tabs_title_head'>{t('Product')} <b>{t('watched')}</b></div></div>              
              </div>
              <div className='rx-main-product rx-watched'> 
                <RxProductWatched className='blockproduct' products={this.state.products_viewed} staticContext={this.props.staticContext} language={this.props.i18n.language} t={this.props.t}/>
                <div onClick={() => { this.animateProduct('next')}} className='blockproduct-arr jcarousel-control-prev carousel__arrow'><span className='mlkicon-ArrowNext mlkicon-Arrow'></span></div>
                <div onClick={() => { this.animateProduct('pre')}} className='blockproduct-arr jcarousel-control-next carousel__arrow'><span className='mlkicon-ArrowBack mlkicon-Arrow'></span></div>
              </div>
            </div>}
          </div>
        </div>
      </div> : <div>Sản phẩm không tồn tại</div> }</div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  cart: state.cart,
  auth: state.auth,
  rxnav: state.rxnav,
  viewed: state.viewed,
  favorite: state.favorite,
  like: state.like
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartDelete,
  favoriteAdd,
  favoriteDelete,
  likeAdd,
  likeDelete,
  rxnavToggle,
  rxnavClose,
  viewedAdd
}

const ProductSingle = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductSingle_)

export default withTranslation('translations')(ProductSingle)