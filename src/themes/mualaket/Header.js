import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import $ from 'jquery'
// import ReactDOM from 'react-dom'

const { rxget, rxsetLocal, rxgetLocal } = global.rootRequire('classes/request')
const { cartClear, cartAdd, cartDelete, rxnavToggle, rxnavClose, rxnavChange, rxsearchChange, rxsearchClear } = global.rootRequire('redux')
const { rxcurrencyVnd } = global.rootRequire('classes/ulti')
const config = global.rxu.config
const numberWithCommas = (x) => {
  return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."): '';
}

class Header_ extends Component {

  constructor (props) {
    super(props)
    this.state = global.ultiStaticContext(this.props.staticContext) || { products: [], categories: [], categoryonly:[], menu: []}
    this.state.products = this.state.products || []
    this.state.categories = this.state.categories || []
    this.state.categoryonly = this.state.categoryonly || []
    this.state.menu = this.state.menu || []
    this.state.currency = this.state.currency || [{exchangerate: '1', name: 'AUD', symbol: 'AU$', exchangeratevnd: '16,500'}]
    this.state.mainnavActive = false
    this.state.paging = { st_full: 'created_at:desc', st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 12, search_name: '' }
    this.state.checksearch = false
    this.state.checkshow = false
    this.state.submenu = false
    this.state.intervalId = 0
    this.handleHover = this.handleHover.bind(this)
    this.handleHoverItem = this.handleHoverItem.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.handleClickSubmenu = this.handleClickSubmenu.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.focus = this.focus.bind(this)
    this.state.price_currency = this.state.price_currency || 1
    this.state.currencyvnd = this.state.currencyvnd || 16300
    this.state.currencyname = this.state.currencyname || 'VND'
    this.state.search_name = ''
    this.state.clickNav = true
    this.state.choosenOptionsKey = 0
    this.state.isHoverSearch = false
    this.state.logo = ''
    this.state.pathnamestr = ''
  }

  componentWillReceiveProps() {
    this.scrollDisable()
    this.setState({pathnamestr: (typeof window !== 'undefined') ? window.location.pathname : ''})
  }

  componentDidMount() {
    this.backtotop()
    this.fetchDataHeader()
    // this.fetchDataCatOnly()
    // this.fetchDataConfig()
    this.focus()
    $('.mainhead-cart-boxalert').css('display', 'none')
    this.scrollNavCate()
    this.fetchDataLogo()
    let strcatecurrency = rxgetLocal('currency')
    if (strcatecurrency) {
      let objcurrency = {name: '', exchangerate:'', exchangeratevnd:''}
      try {objcurrency = JSON.parse(strcatecurrency)} catch(e) {}
      if (objcurrency && objcurrency.name) {
        this.setState({ currencyname: objcurrency.name, price_currency: objcurrency.exchangerate })
      }
    }
    this.setState({ pathnamestr: (typeof window !== 'undefined') ? window.location.pathname : '' })
  }

  scrollNavCate(){
    let scope = this
    $(window).scrolled(function() {
      if ($(window).scrollTop() >= 10) {
        // $('.Slider-arrow.Slider-arrow--left').css('left', '0px')
        scope.setState({clickNav: false})
      }
    })
    let pathname = window.location.pathname
    pathname = pathname.substring(pathname.lastIndexOf('/')+1)
    if(pathname && pathname.length > 0) {
      scope.setState({clickNav: false})
    }
  }

  focus() {
    if (this.textInput) {
      this.textInput.focus()
    }
  }

  fetchDataHeader() {
    let currency = this.state.currency
    let currencyvnd = 0
    rxget(config.api_home_header, {}, { '1': (json) => {
      if (json.data && json.data.config && json.data.config.currency && json.data.config.currency.constructor === Array && json.data.config.currency.length > 0) {
        currency = currency.concat(json.data.config.currency)
        for(let i = 0; i < currency.length; i++){
          if (currency[i].name === 'VND') {
            currencyvnd = currency[i].exchangerate
          }
          if(this.state.currencyname === currency[i].name) {
            rxsetLocal('currency', JSON.stringify(currency[i]))
          }
        }
        this.setState({ currency: currency, currencyvnd: currencyvnd })
      }
      this.setState({ categoryonly: json.data.cates, menu: json.data.menu })
    }})
  }

  fetchDataLogo() {
    // rxget(config.api_slide_logo, {}, { '1': (json) => {
    //   if(json.data) { this.setState({logo: json.data}) }
    // }})
  }

  parsecate(arrcate) {
    let arrcatetmp = []
    Object.keys(arrcate).map(catekey => {
      if (catekey === '0') {
        arrcatetmp = arrcate[catekey]
        return arrcatetmp.forEach(objcate => {
          objcate.childArr = this.parsechild(arrcate[objcate._id], arrcate) || []
        })
      } else {
        return arrcatetmp
      }
    })
    console.log(arrcatetmp, 'ppp')
    return arrcatetmp
  }

  parsechild(arrmain, arrcate) {
    if (arrmain && arrmain.length > 0) {
      arrmain.forEach(objcate => {
        objcate.childArr = this.parsechild(arrcate[objcate._id], arrcate) || []
      })
    }
    return arrmain
  }

  scrollDisable() {
    if(this.props.rxnav.navs.rxmainnav) { document.body.classList.add('ff-scroll-disabled')} else { document.body.classList.remove('ff-scroll-disabled')}
  }

  helpShowCartTotal() {
    let tempTotal = 0
    if (this.props.cart) {
      for(let key in this.props.cart.carts) {
        if (this.props.cart.carts.hasOwnProperty(key)) {
          tempTotal += (this.props.cart.carts[key].amount || 0)
        }
      }
    }
    return tempTotal
  }

  onBlurData(e, name) {
    let paging = this.state.paging
    let search_name = this.state.search_name
    if (name === 'search_name') {
      paging['search_name'] = e.target.value
      search_name = e.target.value
    }
    if (paging && paging['search_name'] && paging['search_name'].length > 0) {
      this.setState({ checksearch: true, paging: paging, search_name: search_name })
      this.fetchData()
    } else {
      this.setState({ products: [] })
      this.setState({ checksearch: false, paging: paging, search_name: search_name })
    }
  }
  handleChange(e, name) {
    let paging = this.state.paging
    if(name && name === 'menu') {
      let objcate = this.state.menu.find((obj) => {
        return obj._id === e
      })
      if(objcate && objcate._id) {
        paging['searchid_appdist'] = e
        this.setState({paging: paging, category_id: e })
      }
    } else {
      if (e.target.value === '0') {
        delete(paging['searchid_appdist'])
        delete(paging['searcharr__id'])
        this.setState({ paging: paging, category_id: '0' })
      } else {
        let objcate = this.state.categoryonly.find((obj) => {
          return obj._id === e.target.value
        })
        if(objcate && objcate._id) {
          paging['searchid_appdist'] = e.target.value
          this.setState({paging: paging, category_id: e.target.value })
        }
      }
    }
  }

  handleHover(){
    this.setState({isHovered: !this.state.isHovered })
  }

  handleHoverItem() {
    this.setState({ishoverItem: !this.state.ishoverItem })
  }

  handleClick(e) {
    e.stopPropagation()
    this.props.rxnavToggle('rxmainnav', {})
    this.setState({isClickToggle: !this.state.isClickToggle })
  }
  handleClickSubmenu(e) {
    e.stopPropagation()
    this.setState({isClickSub: !this.state.isClickSub })
  }

  handleSearch() {
    this.setState({isHoverSearch: !this.state.isHoverSearch})
    $('.mainhead-search-box input').focus();
  }

  clearSearchtext() {
    this.setState({search_name: '', products: [], checksearch: false})
    $('.mainhead-search-box input').focus();
  }

  onClickSubmenu(e) {
    e.stopPropagation()
    let submenu = this.state.submenu
    submenu = !submenu
    this.setState({submenu: submenu})
  }

  fetchData() {
    let paging = this.state.paging
    if (this.state.category_id === '0' ) {
      delete paging.searchid_appdist
      delete paging.searcharr__id
    }
    rxget(config.api_home_product, paging, {
      '1': (json) => { this.setState({ products: json.data.products }) }
    })
  }

  showCategory() {
    this.setState({checkshow: !this.state.checkshow})
  }

  onClickChangeCat(e, item) {
    let paging = this.state.paging
    if (typeof(item) !== 'undefined') {
      paging.searchid_appdist = item._id
    } else {
      delete(paging.searchid_appdist)
    }
    paging.price = {min: 100, max: 10000}

    this.props.rxsearchChange('params', { searchid_appdist : paging.searchid_appdist })
    this.setState({paging: paging})
  }

  viewAll(name) {
    let params = {}
    if (typeof(this.state.paging.searchid_appdist) !== 'undefined' && this.state.paging.searchid_appdist &&  this.state.paging.searchid_appdist !== 'undefined' &&  this.state.paging.searchid_appdist !== '0') {
      params.searchid_appdist = this.state.paging.searchid_appdist
    }
    if (typeof(this.state.paging.searcharr__id) !== 'undefined' && this.state.paging.searcharr__id &&  this.state.paging.searcharr__id !== 'undefined') {
      params.searchid_appdist = this.state.paging.searcharr__id
    }
    if (typeof(this.state.paging.search_name) !== 'undefined' && this.state.paging.search_name && this.state.paging.search_name !== 'undefined') {
      params.search_name = this.state.paging.search_name
    }
    if (this.state.category_id && this.state.category_id === '0') {
      delete params.name
    } else {
      params.name = name
    }
    this.props.rxsearchChange('params', params)
  }
  changeLink(e, paramlink, categoryid) {
    window.location = '/search/'+paramlink
  }

  backtotop() {
    let intervalId = setInterval(this.scrollStep.bind(this), 16.66);
    this.setState({ intervalId: intervalId });
  }
  scrollStep() {
    if (window.pageYOffset === 0) {
        clearInterval(this.state.intervalId);
    }
    window.scroll(0, window.pageYOffset - 60);
  }

  handleChangeCurrency(e) {
    let checkcurrency = this.state.currency.find(objcurrency => objcurrency.name === e.target.value )
    if (checkcurrency) {
      rxsetLocal('currency', JSON.stringify(checkcurrency))
      this.setState({currencyname: checkcurrency.name, price_currency: checkcurrency.exchangerate}, () => {
        window.location.reload(false)
      })
    }
  }

  handleClickSelect(e) {
    let paging = this.state.paging
    // let paramlink = ''
    let objcate = this.state.categoryonly.find((obj) => { return obj._id === e.target.value })
    // if (objcate && objcate.name) { paramlink = objcate.slug }
    if (objcate) {
      paging['searchid_appdist'] = e.target.value
      this.setState({ category_id: objcate._id, paging: paging})
    }
    else {
      delete paging.searchid_appdist
      this.setState({ paging: paging})
    }
  }

  rxcurrencyConvert(x) {
    if (x) {
      return x.toString().replace(/,/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
      return x
    }
  }
  changeLanguage(e, lng){
    if(e && e.target.value) {
      lng = e.target.value
    }
    this.props.i18n.changeLanguage(lng);
    window.location.reload(false)
  }
  viewProductDetail(slug){
    window.location = '/product/'+slug
  }
  onClickChangeNav(name) { 
    if (name === 'clickNav') {
      if(!this.state.clickNav) {
        // $('.Slider-arrow.Slider-arrow--left').css('left', '24%')
      } else {
        // $('.Slider-arrow.Slider-arrow--left').css('left', '0px')
      }
      this.setState({
          clickNav: !this.state.clickNav
      })
    }
  }
  showPopupCart(e) {
    e.stopPropagation();
    this.props.rxnavToggle('rxcart', {})
    let cardalert = document.getElementById('cardalert')
    //cardalert.style.display = 'block'
    if(cardalert && cardalert.style.display === 'block') {
      cardalert.style.display = 'none'
    }
  }

  render () {
    const { t } = this.props;
    let windowWidth = (typeof window === 'undefined') ? 0 : window.innerWidth
    let isDesktop = ((typeof navigator !== 'undefined') && ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) || windowWidth < 1035)) ? false : true
    const language = this.props.i18n.language
    const btnClassHover = this.state.isHovered ? 'rx-is-visiable' : ''
    const isClickToggle = this.state.isClickToggle ? 'mlk-wrapper-is-visible' : ''
    const isClickMenuToggle = this.state.isClickSub ? 'mlk-menu-is-visible' : ''
    // const isClickSub    = this.state.isClickSub ? 'mlkicon-Up' : 'mlkicon-Down'
    let width =  (typeof window === 'undefined') ? 0 : window.innerWidth;

    let paramlink = 'all'
    let pathnamestr = this.state.pathnamestr //(typeof window !== 'undefined') ? window.location.pathname : ''
    let category_id_slug = ''
    if (this.state.categoryonly && this.state.categoryonly.constructor === Array && this.state.categoryonly.length > 0) {
      let arrcateslug = pathnamestr.split('/')
      let cateslug = ''
      if (arrcateslug && arrcateslug.length > 1 ) {
        cateslug = arrcateslug[2]
      }
      let objcheckcate = this.state.categoryonly.find((obj) => { return obj.slug === cateslug })
      if (objcheckcate) {
        category_id_slug = objcheckcate._id
      }
    }

    let category_id = (this.state.category_id) ? this.state.category_id : (category_id_slug) ? category_id_slug : ''
    if (category_id && this.state.categoryonly.length > 0) {
      let objcate = this.state.categoryonly.find((obj) => { return obj._id === category_id })
      if (objcate && objcate.name) { paramlink = objcate.slug }
    }
    if (this.state.paging && this.state.paging.search_name && this.state.paging.search_name.length > 0) {
      paramlink += '/' + this.state.paging.search_name
    }

    let tempCartDetail = (<div></div>)
    if (this.props.cart) {
      tempCartDetail = Object.keys(this.props.cart.carts).map(key => {
        let temp = key.split('|')
        let tempOption = {}
        if (this.props.cart && this.props.cart.carts && this.props.cart.carts[key] && this.props.cart.carts[key].data && this.props.cart.carts[key].data.options && this.props.cart.carts[key].data.options[temp[1]]) {
          tempOption = this.props.cart.carts[key].data.options[temp[1]]
        }

        if (tempOption && tempOption['key']) {delete tempOption['key']}
        if (tempOption && tempOption['data']) {delete tempOption['data']}
        return (
        <div key={key} className='clearfix'>
          { (this.props.cart.carts[key] && this.props.cart.carts[key].data) &&
          <div style={{position: 'relative'}}>
            <div className='cart-popup-product-detail clearfix'>
              <div className='clearfix' style={{borderBottom: 'solid 1px #eee', paddingBottom: '11px'}}>
                <div className='cart-popup-product-rm' onClick={() => { this.props.cartDelete(this.props.cart.carts[key].id) }}><span className='mlkicon-Close mlkiconClose'></span></div>
                <Link to={`/product/${this.props.cart.carts[key].data.slug}`}>
                  <img className='cart-popup-product-img lazyload' alt='ico_default' data-src={config.base_api + '/upload/image/' + (this.props.cart.carts[key].data.img_landscape || 'ico_app_default.png')} />
                </Link>

                <div className='cart-popup-detail' style={{float: 'left', width: '65%'}}>
                  <div className='cart-popup-brand rxfigure-body-brand-inlist'></div>
                  <Link to={`/product/${this.props.cart.carts[key].data.slug}`}><div className='cart-popup-product-name'>{this.props.cart.carts[key].data.name}</div></Link>
                  <div className='cart-popup-product-'>
                    <div className={this.props.cart.carts[key].data.price_discount !== 0 ? 'cart-popup-product-price active' : 'cart-popup-product-price'}>{rxcurrencyVnd(this.props.cart.carts[key].data.price)}</div>
                    {this.props.cart.carts[key].data.price_discount ? <div className='cart-popup-product-priceold'>{rxcurrencyVnd(this.props.cart.carts[key].data.price_discount)}</div> : <div></div> }
                  </div>
                  <div className='cart-popup-product-option'>{t('Quantity')}: <b>{this.props.cart.carts[key].amount}</b></div>
                </div>
              </div>
            </div>
          </div> }
        </div>
      )})
    }

    let tempSearchDetail = (<div></div>)
    if (this.state.products) {
      tempSearchDetail = this.state.products.map((perdata, key) => (
        <div key={key} className='clearfix'>
          { perdata &&
          <div style={{position: 'relative'}} onClick={(e) => { this.props.rxnavClose(); this.viewProductDetail(perdata.slug); }}>
            <div className='cart-popup-product-detail clearfix'>
              <div className='clearfix' style={{borderBottom: 'solid 1px #eee', paddingBottom: '11px'}}>
                <Link to={`/product/${perdata.slug}`}>
                  <img className='cart-popup-product-img lazyload'  alt='ico_default' data-src={config.base_api + '/upload/image/' + (perdata.img_landscape || 'ico_app_default.png')} />
                </Link>
                <div className='cart-popup-product-roworder'>
                  <Link to={`/product/${perdata.slug}`}><div className='cart-popup-product-name'>{perdata.name}</div></Link>
                  <div>
                    <div className='cart-popup-product-price'>{rxcurrencyVnd(perdata.price)}</div>
                    { perdata.price_discount ? <div className='cart-popup-product-priceold'>{rxcurrencyVnd(perdata.price_discount)}</div> : ''}
                  </div>
                  <div className='cart-popup-product-action'>
                    <div className='cart-popup-product-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(perdata._id, { amount: 1, data: perdata }) }}><span className='rx-cart mlkicon-Cart'></span></div>
                  </div>
                </div>
              </div>
            </div>
          </div> }
        </div>
      ))
    }
    let arrCategories = []
    if (this.props.category && this.props.category.categories && this.props.category.categories.constructor === Array) {
      arrCategories = this.props.category.categories
    }
    let navCategories = this.state.menu && this.state.menu.slice(0, 8).map(perobj => {
      return (
        // perobj.is_brand === 0 &&
        <li className={'nav-menu-item '+ ((this.state.category_id && this.state.category_id === perobj._id) ? 'rx-active': '')} key={perobj._id}>
          <Link className={'nav-menu-item-hover ' + isClickMenuToggle}  to={{ pathname: '/search/'+perobj.slug, categoryid: perobj._id }} onClick={(e) => {this.setState({category_id: perobj._id});this.handleClick(e)}}>{perobj.name}</Link>
          {isDesktop && (perobj.childArr && perobj.childArr.length > 0 && perobj.childArr.constructor === Array) && <div className= {'dropdown-content ' + isClickMenuToggle }>
            {perobj.childArr.map(perchild => (
              <div key={perchild._id} className='block-child-content'>
                <Link to={{pathname:`/search/${perchild.slug}`, categoryid: perchild._id}} onClick={() => { this.handleChange(perchild._id, 'menu');this.props.rxnavClose(); this.viewAll('Header') }} className='mlk-nav-title'>{perchild.name}</Link>
              </div>))}
          </div>}
        </li>)
    })
    let url = this.state.logo['img_landscape'] ? (config.base_api + '/upload/image/' + this.state.logo['img_landscape']) : 'http://www.annawholesale.com.au/images/static/LOGO-ANNA-WHOLSALE.jpg'
    return (
      <div className='main-header'>
        <div className='main-back-to-top' onClick={() => this.backtotop()}><span className='mlkicon-Up icon-Up'></span></div>

        <div className={'mainhead-container mlk-mainhead-container ' + isClickToggle }>
          <div className='mainhead_top_content'>
            <div className='clearfix'>
              <div className='mainhead_top-left'>Anna Wholesale Chemist | Hotline (AU) +61 450 928 599</div>
              <div className="box-change-language">
                <div className={language === 'vi' ? "form-controlLanguage vn active" : "form-controlLanguage vn"} onClick={() => this.changeLanguage(null, 'vn')}></div>
                <div className={language === 'en' ? "form-controlLanguage eng active" : "form-controlLanguage eng"} onClick={() => this.changeLanguage(null, 'en')}></div>
              </div>
              <div className='nav-menu-boxcurrency'>
                {this.state.currencyname !== 'VND' ?
                <div>
                  <span><b>{t('Exchange rate')+': '}</b></span>
                  <span>1 {this.state.currencyname} = {numberWithCommas(Math.floor(this.state.currencyvnd.toString().replace(/,/g,'') / this.state.price_currency))} đ</span>
                </div>:<div><span><b>{t('Exchange rate')+': '}</b></span>
                  <span>1 AUD = {numberWithCommas(Math.floor(this.state.currencyvnd.toString().replace(/,/g,'')))} đ</span></div>}
                <select className='nav-menu-currency' value={this.state.currencyname || 'VND'} onChange={(e) => {this.handleChangeCurrency(e)}}>
                  {this.state.currency.map((objcurrency, indexcurrency) => (<option key={indexcurrency} value={objcurrency.name}>{objcurrency.name}</option>))}
                </select>
              </div>
            </div>
          </div>
          <div className='mainhead-inner' id='mainhead-inner'>
            <div className='container clearfix mainhead-wrapper'>
              <div className='mainhead'>
                <div className='boxheader'>
                  <Link to='/' className='boxlogo'>
                    <div className='boxheader-logo' style={{ backgroundImage: 'url(' + url +' )'}}></div>
                  </Link>
                  <div className='boxsearch'>
                    <div className='boxsearch-position'>
                      {this.state.isHoverSearch && <div className='fixed_tonggle' onClick={(e) => this.setState({isHoverSearch: false})}></div>}
                      <div className='boxsearch-filter'>
                        <select className='boxsearch-filterselect' onChange={(e) => {this.handleChange(e)}} value={category_id}>
                          <option value='0'>{t('All')}</option>
                          {this.state.categoryonly.map((perdata,index) => <option key={index} value={perdata._id}>{language === 'vi' ? perdata.name : (perdata.nameEng ? perdata.nameEng : perdata.name)}</option>)}
                        </select>
                      </div>
                      <div className='boxsearch-input' onKeyPress={(e) => {if (e.key === 'Enter') { this.changeLink(e, paramlink, category_id) } }}>
                        <input className='boxsearch-inputsearch' autoFocus ref={(input) => { this.textInput = input }} tabIndex='1' type='text' autoComplete='search' name='name' onChange={(e) => {this.onBlurData(e, 'search_name'); this.props.rxnavChange('rxsearchsuggest', {}); } } placeholder={t('Input search')}/>
                        <Link to={{pathname:`/search/${paramlink}`, categoryid: category_id}} onClick={() => { this.props.rxnavClose(); this.viewAll('Header') }} className='boxsearch-inputicon mlkicon-Search'></Link>
                      </div>

                      <div className='mainhead-search-btn'>
                        {/*<div onClick={(e) =>{e.stopPropagation(); this.handleSearch()}} className={this.state.isHoverSearch ? 'rxico-search mlkicon-Close' : 'rxico-search mlkicon-Search'}></div>*/}
                        {this.state.isHoverSearch ? <div onClick={(e) =>{e.stopPropagation(); this.clearSearchtext()}} className='rxico-search mlkicon-Close'></div> : <div onClick={(e) =>{e.stopPropagation(); this.handleSearch()}} className='rxico-search mlkicon-Search'></div>}

                        <div id='mainhead-search' className={this.state.isHoverSearch ? 'mainhead-search input-search' : 'mainhead-search'} onKeyPress={(e) => {if (e.key === 'Enter') { this.changeLink(e, paramlink) } }}>
                          <div className='mainhead-search-box'>
                            <input autoFocus ref={(input) => { this.textInput = input }} tabIndex='1' value={this.state.search_name || ''} type='text' autoComplete='search' name='name' onChange={(e) => {this.onBlurData(e, 'search_name'); this.props.rxnavChange('rxsearchsuggest', {}); } } placeholder={t('Search')+'...'}/>
                            <Link to={`/search/${paramlink}`} onClick={(e) => { this.props.rxnavClose(); this.props.rxsearchChange('params', this.state.paging )}}><div className='mainhead-search-button'><i className='icon-magnifier'></i></div></Link>

                            <div className='mainhead-search-category'>
                              <select className='select-search-category' onChange={(e) => this.handleChange(e)} >
                                <option value='0'>{t('All categories')}</option>
                                {arrCategories.map((perdata,index) => <option key={index} value={perdata._id}>{perdata.name}</option>)}
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className='boxsearch-position'>
                      {(this.props.rxnav.navs.rxsearchsuggest && this.state.checksearch) && <div>
                        { this.state.products && this.state.products.length > 0 ?
                          <div className='mainhead-search-suggestion' onClick={(e) => { e.stopPropagation() }}>
                            <div className='cart-popup-body'>{tempSearchDetail}</div>
                            <div style={{padding: '10px'}} onClick={(e) => { this.changeLink(e, paramlink, category_id) }}> <div className='cart-popup-payment'>{t('View all')}</div> </div>
                          </div> : <div className='mainhead-search-suggestion'>
                            <div className='cart-popup-emptytext'>{t('No match found')}</div>
                          </div>}
                      </div>}
                    </div>
                  </div>

                  <div className='boxregister'>
                    { !(this.props.auth && this.props.auth.user && this.props.auth.user._id) ?
                      <div>
                        <Link to={`/login`}><span className='icon-profile mlkicon-Profile'></span></Link>
                        <div className='boxregister-avatar'>
                          <img data-src='/images/static/path.png' alt='' className='boxregister-imgavatar lazyload'/>
                        </div>
                      </div>
                    : <div>
                      <Link to={`/customer/account`}><span className='icon-profile mlkicon-Profile'></span></Link>
                      <img className='boxregister-avatar lazyload' data-src={config.base_api + '/upload/image/' + (this.props.auth.user.img_landscape || 'path.png')} alt=''/>
                    </div>
                    }

                    <div className='boxregister-text'>
                      { !(this.props.auth && this.props.auth.user && this.props.auth.user._id) ? <div>{t('Hello')}, <div><Link to={`/login`} onClick={(e) => this.backtotop()}>{t('Let Login')}!</Link></div></div>
                          : <div>{t('Hello')}, <div><Link to={`/customer/account`} onClick={(e) => this.backtotop()}>{this.props.auth.user.firstname}</Link></div></div> }
                    </div>
                  </div>
                  <div className='boxshop'>
                    <div className='boxshop-box' onClick={(e) => { e.stopPropagation(); this.props.rxnavToggle('rxcart', {}) }}>
                      <div className='boxshop-img mlkicon-Cart'></div>
                      <div className='boxshop-text'>{t('Cart')}</div>
                      <div className='boxshop-number'>{this.helpShowCartTotal() !== 0 ? this.helpShowCartTotal() : 0 }</div>
                    </div>

                    <div className='mainhead-cart'>
                      { this.props.rxnav.navs.rxcart &&
                        <div className='mainhead-cart-popup' onClick={(e) => { e.stopPropagation() }} >
                        { this.props.cart && Object.keys(this.props.cart.carts).length !== 0 ?
                          <div>
                            <div className='cart-popup-header'>{t('Cart have')} <b>{Object.keys(this.props.cart.carts).length}</b> {t('products')}</div>
                            <div className='cart-popup-body'>{tempCartDetail}</div>
                            <div style={{padding: '15px'}}> <Link to='/payment' onClick={() => { this.props.rxnavClose(); this.backtotop() }}><div className='cart-popup-payment'>{t('Checkout')}</div></Link> </div>
                          </div> : <div>
                            <div className='cart-popup-header'>{t('Your cart')}</div>
                            <div className='cart-popup-emptytext'>{t('Cart empty')}, {t('Please continue to select products')}</div>
                          </div>}
                        </div> }
                    </div>

                    {width > 768 && <div className='mainhead-cart-boxalert' id='cardalert'>
                      <div className='mainhead-cartalert'>
                        <div className='mainhead-cartalert-popup'>
                          <div className='mainhead-cartalert-text'><span className='cartalert-icon iconanna-check'></span>{t('Add to cart successfully')}!</div>
                          <div className='mainhead-cartalert-text'>
                            <Link to='/payment' onClick={() => { this.props.rxnavClose(); this.backtotop() }}>
                              <div className='cart-popup-payment'>{t('View shopping cart and payment')}</div>
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>}

                  </div>
                </div>
              </div>
            </div>
            <div className={this.state.isClickToggle ? 'nav-wrap rxmainnav-wrap rxmainnav-wrap-width' : 'nav-wrap rxmainnav-wrap'}>
              <div onClick={(e) => {e.stopPropagation();this.handleClick(e)}} className={'nav-wrap-hidden ' + isClickToggle}></div>
              <div className={'rxmainnav-toogle-user '+ isClickToggle}>
                <div className='mainhead-account'>
                  <div className='rxico-account icon-user'></div>
                  { !(this.props.auth && this.props.auth.user && this.props.auth.user._id) ? <Link to={`/login`} className='mainhead-account-login'>{t('Login')} <i>{t('Customer')}</i></Link>
                    : <Link to={`/customer/account`} className='mainhead-account-login'>{t('Hello')} {this.props.auth.user.firstname} <i>{t('Account')}</i></Link> }
                </div>
                <div className={'rxmainnav-toggle ' + isClickToggle } onClick={(e) => {e.stopPropagation();this.handleClick(e)}}><span className='mlkicon-Bar nav-icon rx-icon-Bar'></span></div>
              </div>
              <div className={'container mlk-wrapper ' + isClickToggle}>
                <nav className={'nav-menu-nav ' + isClickMenuToggle}>
                  <ul className={'mlk-primary-nav '+btnClassHover +' '+ isClickMenuToggle } onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}>
                    <li className={'nav-menu-item '+ ((this.state.category_id && this.state.category_id === 1) ? 'rx-active': '')}>
                      <Link className={'nav-menu-item-hover ' + isClickMenuToggle}  to='/' >Home</Link>
                    </li>
                    {navCategories}
                    {/*<li className={'nav-menu-item '+ ((this.state.category_id && this.state.category_id === 1) ? 'rx-active': '')}>
                      <Link className={'nav-menu-item-hover ' + isClickMenuToggle}  to='/news' >News</Link>
                    </li>*/}
                    <li className={'nav-menu-item '+ ((this.state.category_id && this.state.category_id === 1) ? 'rx-active': '')}>
                      <Link className={'nav-menu-item-hover ' + isClickMenuToggle}  to='/' >About us</Link>
                    </li>
                  </ul>
                </nav>
                { (this.props.auth && this.props.auth.user && this.props.auth.user._id) ?
                  <div className='rx-mobile-show mobile-nav-block-user'>
                    <div onClick={(e) => {e.stopPropagation();this.handleClick(e)}}><span className='mlkicon-Profile'></span><Link to={`/customer/account`}>{t('Account')}</Link></div>
                    <div onClick={(e) => {e.stopPropagation();this.handleClick(e)}}><span className='mlkicon-Heart'></span><Link to={`/customer/favorite`}>{t('Favorite list')}</Link></div>
                    <div onClick={(e) => {e.stopPropagation();this.handleClick(e)}}><span className='mlkicon-Order'></span><Link to={`/customer/order`}>{t('Order')}</Link></div>
                  </div> :
                  <div className='rx-mobile-show mobile-nav-block-user'>
                    <div onClick={(e) => {e.stopPropagation();this.handleClick(e)}}><span className='mlkicon-Profile'></span><Link to={`/login`}>{t('Login')}</Link> / <Link to={`/register`}>{t('Register')}</Link></div>
                    <div onClick={(e) => {e.stopPropagation();this.handleClick(e)}}><span className='mlkicon-Heart'></span><Link to={`/customer/favorite`}>{t('Favorite list')}</Link></div>
                    <div onClick={(e) => {e.stopPropagation();this.handleClick(e)}}><span className='mlkicon-Order'></span><Link to={`/customer/order`}>{t('Order')}</Link></div>
                  </div>
                }
                <div className="rx-mobile-show box-change-language">{t('Language')}
                  <select value ={this.props.i18n.language}  onChange={(e) => this.changeLanguage(e)}>
                    <option value='vn'>Tiếng Việt</option>
                    <option value='en'>English</option>
                  </select>
                </div>
                <div className={'rx-bg-nav ' + btnClassHover}></div>
              </div>
            </div>
          </div>
        </div>

        <div className='container--shadow'>
          <div className='container'>
            <div className='rx-main-banner-inner'>
              <div className='rx-safe-banner-content row'>
                {/*<div className = 'rx-navbar-cate'>
                  <div className = 'rx-navbar-cate-title' onClick={(e) => this.onClickChangeNav('clickNav')}>{t('PRODUCT PORTFOLIO')} <i className={this.state.clickNav ? 'mlkicon-Up icon-down' : 'mlkicon-Down icon-down'}></i></div>
                  {this.state.clickNav ? <div>
                    <div className = 'rx-navbar-cate-dropdown' id='click_nav_hide'>
                      {cateMenu}
                    </div>
                    <div className='rxnav-fixed' onClick={(e) => this.onClickChangeNav('clickNav')}></div>
                  </div>:<div></div>}
                </div>*/}
                <div><span className='bansafe__icon mlkicon-Icon1'></span><div><b>100% {t('product')}</b></div> <div>{t('Genuine from Australia')}</div></div>
                <div><span className='bansafe__icon mlkicon-Icon2'></span><div><b>{t('Nationwide delivery')}</b></div> <div>{t('fast and convenient')}</div></div>
                <div><span className='bansafe__icon mlkicon-Icon3'></span><div><b>{t('Online payment')}</b></div> <div>{t('Easy and safe')}</div></div>
                <div><span className='bansafe__icon mlkicon-Icon4'></span><div><b>{t('Cash payment')}</b></div> <div>{t('on delivery')}</div></div>
              </div>
            </div>
          </div>
        </div>

        <div className={'rx-overlay-bg ' + btnClassHover}></div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth,
  favorite: state.favorite,
  category: state.category
})

const mapDispatchToProps = {
  cartClear,
  cartAdd,
  cartDelete,
  rxnavToggle,
  rxnavClose,
  rxnavChange,
  rxsearchChange,
  rxsearchClear
}

const Header = connect(
  mapStateToProps,
  mapDispatchToProps
)(Header_)

export default withTranslation('translations')(Header)
