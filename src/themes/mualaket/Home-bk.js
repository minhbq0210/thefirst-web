import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { withTranslation } from 'react-i18next'
import $ from 'jquery'
import ReactGA from 'react-ga'
import {Helmet} from "react-helmet";

const { rxget, rxsetLocal } = global.rootRequire('classes/request')
const Slider = global.rootRequire('components/Slider').default

const RxProductBlock = global.rootRequiretheme('components/Shares/RxProductBlock').default
const RxProductFavoriteBlock = global.rootRequiretheme('components/Shares/RxProductFavorite').default
const RxProductFeaturedBlock = global.rootRequiretheme('components/Shares/RxProductFeatured').default
const RxBrandBlock = global.rootRequiretheme('components/Shares/RxBrandBlock').default
const Loader = global.rootRequire("components/Shares/loader").default

polyfill()

const config = global.rxu.config

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = global.ultiStaticContext(this.props.staticContext) || { cates: [], products_one: [], products_highlights: [], brand: [], imgmobile: [], imgslides: [] }
    this.state.cates   = this.state.cates || []
    this.state.brand   = this.state.brand || []
    this.state.products_one = this.state.products_one || []
    this.state.products_highlights = this.state.products_highlights || []
    this.state.paging     = this.state.paging || {}
    this.state.categories = this.state.categories || []
    this.state.imgslides  = this.state.imgslides || []
    this.state.activeIndex = 0

    this.state.paging.st_full = this.state.paging.st_full || 'created_at:desc'
    this.state.paging.st_col  = this.state.paging.st_col  || 'created_at'
    this.state.paging.st_type = this.state.paging.st_type || -1
    this.state.paging.pg_page = this.state.paging.st_page || 1
    this.state.paging.pg_size = this.state.paging.st_size || 12
    this.state.paging.searchid_appdist = ''
    this.state.checkscroll = {number: 1, type: 'next'}
    this.state.loading = 'false'
  }

  componentDidMount() {
    ReactGA.initialize('UA-157753374-1')
    ReactGA.pageview('/')
    this.setState({loading: true}, () => this.fetchData())
    this.interval = setInterval(() => {
      // this.goToNextSlide() 
    }, 3000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }
  
  animateProduct(key) {
    let checkscroll = this.state.checkscroll
    // let eleDom = document.getElementsByClassName('blockproduct__colwrap')[0]
    // let z = eleDom.scrollWidth - eleDom.clientWidth
    console.log(key)
    if (checkscroll['type'] !== key) {
      checkscroll['number'] = 1
      checkscroll['type'] = key
    }

    if (key === 'next') {
      $('.blockproduct__colwrap').animate({scrollLeft: checkscroll['number']*300}, 500);
    } else {
      $('.blockproduct__colwrap').animate({scrollLeft: -checkscroll['number']*300}, 500);
    }

    if (checkscroll['type'] === key) {
      checkscroll['number'] += 1  
    }

    this.setState({checkscroll: checkscroll})
  }

  fetchData() {
    rxget(config.api_home, this.state.pagingcat, {
      '1': (json) => {
        if (json.data && json.data.imgbrand && json.data.imgbrand.length > 0) {
          rxsetLocal('imgbrand', JSON.stringify(json.data.imgbrand))
        }
        this.setState({ 
          cates: json.data.cates, 
          brand: json.data.brand, 
          imgmobile: json.data.imgmobile, 
          imgslides: json.data.imgslides,
          products_one: json.data.products, 
          products_highlights: json.data.productsbest,
          loading: false }) }
    })
  }
  
  backtotop() {}

  goToSlide(index) {
    this.setState({
      activeIndex: index
    });
  }
  goToPrevSlide(e) {
    e.preventDefault();

    let index = this.state.activeIndex;
    let slides = this.state.imgmobile || {};
    let slidesLength = slides.length;
    if (index < 1) {
      index = slidesLength;
    }
    --index;

    this.setState({
      activeIndex: index
    });
  }
  goToNextSlide(e) {
    // e.preventDefault();
    let index = this.state.activeIndex;
    let slides = this.state.imgmobile || {};
    let slidesLength = slides.length - 1;

    if (index === slidesLength) {
      index = -1;
    }

    ++index;

    this.setState({
      activeIndex: index
    });
  }
  
  helpContentImgslides (data) {
    return (
      <Slider>
        { this.state.imgslides.map((per,i) => ( <div key={i}>
          <a href={per['url']}><img className='imgslide lazyload' alt='' data-src={config.base_api + '/upload/image/' + per['img_landscape']}></img></a>
        </div>))}
      </Slider>
    )
  }

  helpContentImgmobile (data) {
    const { t } = this.props
    return (
      <Slider>
        { this.state.imgmobile.map((per,i) => ( <div key={i}>
          <a href={per['url']}><img alt='' data-src={config.base_api + '/upload/image/' + per['img_landscape']} className='imgslide lazyload'></img></a>
          <div className='mlk-slider-desc'>
            <div className='mlk-bg-overlay'></div>
            <p className='mlk-slider-content'>{per['desc']}</p>
            <a href={per['url']} className='mlk-readmore'>{t('SEE MORE')}</a>
          </div>
        </div>))}
      </Slider>
    )
  }

  render() {
    const { t } = this.props

    let slidehome = (this.state.imgslides && this.state.imgslides.length !== 0) ? this.helpContentImgslides(this.state.imgslides) : <div></div>
    let slidehomeMobile = (this.state.imgmobile && this.state.imgmobile.length !== 0) ? this.helpContentImgmobile(this.state.imgmobile) : <div></div>
      
    return (      
      <div>
        <Helmet>
          <title>{`${global.SEOtitle}`}</title>
          <link rel="canonical" href="/" />
          <meta name="description" content={`${global.SEOdesc}`} />
        </Helmet>
        <div className='rx-header row row-nmar rx-header-desktop'>
          <div className='rx-category'><div className='rx-categories'></div></div>
          <div className='container'>
            <div className='rx-slider'>
              <div className='exampleSliders'>{slidehome}</div>
            </div>
          </div>
        </div>
        <div className='rx-header row row-nmar rx-header-mobile'>
          <div className='rx-slider'>
            <div className='exampleSliders'>{slidehomeMobile}</div>
          </div>
        </div>    
        <div className='blockfull--product'>
          <div className='container'>
            <div className='tabs'>
              <div className='tabs-title'>
                <div className='tabs__title'><div className='tabs_title_head'>{t('Categories')} <b>{t('favorite')}</b></div></div>              
              </div>
              <div className='rx-main-product'>
                <Loader isLoading={this.state.loading}>
                  <RxProductFavoriteBlock className='blockproduct' cates={this.state.cates} staticContext={this.props.staticContext} language = {this.props.i18n.language}/>
                </Loader>
              </div>
            </div>
          </div>
        </div>

        <div className='blockfull--product-new'>
          <div className='container'>
            <div className='tabs'>
              <div className='tabs-title'>
                <div className='tabs__title'><div className='tabs_title_head'>{t('Product')} <b>{t('new')}</b></div></div>              
              </div>
              <Loader isLoading={this.state.loading}>
                <div className='rx-main-product'>
                  <RxProductBlock className='blockproduct' products={this.state.products_one} staticContext={this.props.staticContext} language={this.props.i18n.language}/>
                  <div onClick={() => { this.animateProduct('next')}} className='blockproduct-arr jcarousel-control-prev carousel__arrow'><span className='mlkicon-ArrowNext mlkicon-Arrow'></span></div>
                  <div onClick={() => { this.animateProduct('pre')}} className='blockproduct-arr jcarousel-control-next carousel__arrow'><span className='mlkicon-ArrowBack mlkicon-Arrow'></span></div>
                </div>
              </Loader>
            </div>
          </div>
        </div>

        <div className='blockfull--product-featured'>
          <div className='container'>
            <div className='tabs'>
              <div className='tabs-title'>
                <div className='tabs__title'><div className='tabs_title_head'>{t('Product')} <b>{t('Highlight')}</b></div></div>              
              </div>
              <Loader isLoading={this.state.loading}>
                <div className='rx-main-product'>
                  <RxProductFeaturedBlock className='blockproduct' cates={this.state.products_highlights} staticContext={this.props.staticContext} language={this.props.i18n.language}/>
                </div>
              </Loader>
            </div>
          </div>
        </div>
        <Loader isLoading={this.state.loading}>
          <RxBrandBlock className='blockproduct' brand={this.state.brand} staticContext={this.props.staticContext}/>
        </Loader>
        
      </div>)
  }
}

export default withTranslation('translations')(Home)
