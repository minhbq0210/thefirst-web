import React, { Component } from 'react'
import { polyfill }  from 'es6-promise'
import { withTranslation } from 'react-i18next'
import $ from 'jquery'
import ReactGA from 'react-ga'
import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom'

const { rxget, rxsetLocal } = global.rootRequire('classes/request')
const { rxChunkArray } = global.rootRequire("classes/ulti");
const Slider = global.rootRequire('components/Slider').default

const RxProductBlock = global.rootRequiretheme('components/Shares/RxProductBlock').default
const RxBestCollectionBlock = global.rootRequiretheme('components/Shares/RxBestCollectionBlock').default
const RxProductFavoriteBlock = global.rootRequiretheme('components/Shares/RxProductFavorite').default
// const RxProductFeaturedBlock = global.rootRequiretheme('components/Shares/RxProductFeatured').default
// const RxBrandBlock = global.rootRequiretheme('components/Shares/RxBrandBlock').default
const RxAdvertisementBlock = global.rootRequiretheme('components/Shares/RxAdvertisementBlock').default
const RxContentBlock = global.rootRequiretheme('components/Shares/RxContentBlock').default
const Loader = global.rootRequire("components/Shares/loader").default

polyfill()

const config = global.rxu.config

class Home extends Component {
  constructor(props) {
    super(props)
    let abc = global.ultiStaticContext(this.props.staticContext) || []
    // console.log(abc, 'abc')
    // this.state = global.ultiStaticContext(this.props.staticContext) || { cates: [], products_one: [], products_highlights: [], brand: [], imgmobile: [], imgslides: [], menu: [], banner: [], advertisement: [], video: {}, news: [] }
    this.state = {
      cates         : global.rxu.get(abc, 'cates', []),
      brand         : global.rxu.get(abc, 'brand', []).length > 0 ? rxChunkArray(global.rxu.get(abc, 'brand', []).slice(0, 20), 5) : [],
      menu          : global.rxu.get(abc, 'menu', []),
      video         : global.rxu.get(abc, 'video', {}),
      news          : global.rxu.get(abc, 'news', []),
      advertisement : global.rxu.get(abc, 'advertisement', []),
      products_one  : global.rxu.get(abc, 'products', []).length > 0 ? rxChunkArray(global.rxu.get(abc, 'products', []).slice(0, 24), 12): [],
      products_highlights: global.rxu.get(abc, 'productsbest', []).length > 0 ? rxChunkArray(global.rxu.get(abc, 'productsbest', []).slice(0, 24), 12): [],
      paging        : {st_full: 'created_at:desc', st_col: 'created_at', st_type: -1, st_page: 1, pg_size: 12},
      categories    : [],
      imgslides     : global.rxu.get(abc, 'imgslides', []),
      imgmobile     : global.rxu.get(abc, 'imgmobile', []),
      banner        :  global.rxu.get(abc, 'banner', []),
      activeIndex   :  0,
      searchid_appdist: '',
      checkscroll   : {number: 1, type: 'next'},
      loading       : false
    }
    // console.log(this.state, 'this.state')
    // console.log(this.state.banner, '000')
  }

  componentDidMount() {
    ReactGA.initialize('UA-157753374-1')
    ReactGA.pageview('/')
    this.setState({loading: true}, () => this.fetchData())
    this.interval = setInterval(() => {
      // this.goToNextSlide() 
    }, 3000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }
  
  animateProduct(key) {
    let checkscroll = this.state.checkscroll
    // let eleDom = document.getElementsByClassName('blockproduct__colwrap')[0]
    // let z = eleDom.scrollWidth - eleDom.clientWidth
    // console.log(key)
    if (checkscroll['type'] !== key) {
      checkscroll['number'] = 1
      checkscroll['type'] = key
    }

    if (key === 'next') {
      $('.blockproduct__colwrap').animate({scrollLeft: checkscroll['number']*300}, 500);
    } else {
      $('.blockproduct__colwrap').animate({scrollLeft: -checkscroll['number']*300}, 500);
    }

    if (checkscroll['type'] === key) {
      checkscroll['number'] += 1  
    }

    this.setState({checkscroll: checkscroll})
  }

  fetchData() {
    let windowWidth = (typeof window === 'undefined') ? 0 : window.innerWidth
    rxget(config.api_home, this.state.paging, {
      '1': (json) => {
        if (json.data && json.data.imgbrand && json.data.imgbrand.length > 0) {
          rxsetLocal('imgbrand', JSON.stringify(json.data.imgbrand))
        }
        this.setState({ 
          cates: json.data.cates, 
          brand: windowWidth > 768 ? rxChunkArray(json.data.brand.slice(0, 20), 5) : rxChunkArray(json.data.brand.slice(0, 18), 3), 
          imgmobile: json.data.imgmobile, 
          imgslides: json.data.imgslides,
          products_one: windowWidth > 768 ? rxChunkArray(json.data.products.slice(0, 24), 12) : rxChunkArray(json.data.products.slice(0, 24), 4), 
          products_highlights: windowWidth > 768 ? rxChunkArray(json.data.productsbest.slice(0, 24), 12) : rxChunkArray(json.data.productsbest.slice(0, 24), 4),
          menu: json.data.menu,
          banner: json.data.banner,
          advertisement: json.data.advertisement,
          video: json.data.video,
          news: json.data.news,
          loading: false }) }
    })
  }
  
  backtotop() {}

  goToSlide(index) {
    this.setState({
      activeIndex: index
    });
  }
  goToPrevSlide(e) {
    e.preventDefault();

    let index = this.state.activeIndex;
    let slides = this.state.imgmobile || {};
    let slidesLength = slides.length;
    if (index < 1) {
      index = slidesLength;
    }
    --index;

    this.setState({
      activeIndex: index
    });
  }
  goToNextSlide(e) {
    // e.preventDefault();
    let index = this.state.activeIndex;
    let slides = this.state.imgmobile || {};
    let slidesLength = slides.length - 1;

    if (index === slidesLength) {
      index = -1;
    }

    ++index;

    this.setState({
      activeIndex: index
    });
  }
  
  helpContentImgslides (data) {
    return (
      <Slider>
        { this.state.imgslides.map((per,i) => ( <div key={i}>
          <a href={per['url']}><img className='imgslide' alt='' src={config.base_api + '/upload/image/' + per['img_landscape']}></img></a>
        </div>))}
      </Slider>
    )
  }

  helpContentImgmobile (data) {
    const { t } = this.props
    return (
      <Slider>
        { this.state.imgmobile.map((per,i) => ( <div key={i}>
          <a href={per['url']}><img alt='' src={config.base_api + '/upload/image/' + per['img_landscape']} className='imgslide'></img></a>
          <div className='mlk-slider-desc'>
            <div className='mlk-bg-overlay'></div>
            <p className='mlk-slider-content'>{per['desc']}</p>
            <a href={per['url']} className='mlk-readmore'>{t('SEE MORE')}</a>
          </div>
        </div>))}
      </Slider>
    )
  }

  render() {
    const { t } = this.props
    let windowWidth = (typeof window === 'undefined') ? 0 : window.innerWidth
    let isDesktop = ((typeof navigator !== 'undefined') && ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) || windowWidth < 1035)) ? false : true

    let slidehome = (this.state.imgslides && this.state.imgslides.length !== 0) ? this.helpContentImgslides(this.state.imgslides) : <div></div>
    let slidehomeMobile = (this.state.imgmobile && this.state.imgmobile.length !== 0) ? this.helpContentImgmobile(this.state.imgmobile) : <div></div>
      
    return (      
      <div>
        <Helmet>
          <title>{`${global.SEOtitle}`}</title>
          <link rel="canonical" href="/" />
          <meta name="description" content={`${global.SEOdesc}`} />
        </Helmet>
        <div className='rx-header row row-nmar rx-header-desktop'>
          <div className='rx-category'><div className='rx-categories'></div></div>
          <div className='container'>
            <div className='rx-slider'>
              <div className='exampleSliders'>{slidehome}</div>
            </div>
          </div>
        </div>
        <div className='rx-header row row-nmar rx-header-mobile'>
          <div className='rx-slider'>
            <div className='exampleSliders'>{slidehomeMobile}</div>
          </div>
        </div>  

        {this.state.menu && this.state.menu.length > 0 && <div className='blockfull--product'>
          <div className='container rx-carousel'>
            <div className='tabs'>
              <div className='tabs-title'>
                <div className='tabs__title'><div className='tabs_title_head'><b>ANNA'S BEST COLLECTION</b></div></div>              
              </div>
              <div className='rx-main-product'>
                <Loader isLoading={this.state.loading}>
                  <RxBestCollectionBlock className='blockproduct' cates={this.state.menu} staticContext={this.props.staticContext} language = {this.props.i18n.language}/>
                </Loader>
              </div>
            </div>
          </div>
        </div>}

        {this.state.banner && this.state.banner.length > 0 && <div className='rx-header row row-nmar rx-header-desktop'>
          <div className='rx-category'><div className='rx-categories'></div></div>
          <div className='container rx-banner'>
            <div className='rx-slider'>
              <div className='exampleSliders'>
                <Slider>
                  { this.state.banner && this.state.banner.map((per,i) => ( <div key={i}>
                    <a href={per['url']}><img className='imgslide' alt='' src={config.base_api + '/upload/image/' + per['img_landscape']}></img></a>
                  </div>))}
                </Slider>
              </div>
            </div>
          </div>
        </div>}

        <div className='blockfull--product-featured'>
          <div className='container rx-carousel'>
            <div className=''>
              <div className='tabs-title'>
                <div className='tabs__title'><div className='tabs_title_head'><b>Top Sản Phẩm Bán Chạy Nhất</b></div></div>              
              </div>
              <Loader isLoading={this.state.loading}>
                <Slider>
                  { this.state.products_highlights && this.state.products_highlights.map((per,i) => ( <div key={i}>
                    <RxProductBlock className='blockproduct' products={per} staticContext={this.props.staticContext} language={this.props.i18n.language}/>
                  </div>))}
                </Slider>
              </Loader>
            </div>
          </div>
        </div>

      {/*<div className='blockfull--product-featured'>
        <div className='container rx-carousel'>
          <div className='tabs'>
            <div className='tabs-title'>
              <div className='tabs__title'><div className='tabs_title_head'><b>Top Sản Phẩm Bán Chạy Nhất</b></div></div>              
            </div>
            <Loader isLoading={this.state.loading}>
              <div className='rx-main-product'>
                <RxProductFeaturedBlock className='blockproduct' cates={this.state.products_highlights} staticContext={this.props.staticContext} language={this.props.i18n.language}/>
              </div>
            </Loader>
          </div>
        </div>
      </div>*/}

        {isDesktop && this.state.advertisement && this.state.advertisement.length > 0 && <div className='rx-header row row-nmar rx-header-desktop'>
          <div className='rx-category'><div className='rx-categories'></div></div>
          <div className='container rx-banner'>
            <div className='rx-slider'>
              <div className='exampleSliders'>
                <RxAdvertisementBlock data={this.state.advertisement && this.state.advertisement[0]} staticContext={this.props.staticContext} language = {this.props.i18n.language}/>
              </div>
            </div>
          </div>
        </div>}
        
        <div className='blockfull--product-featured'>
          <div className='container rx-carousel'>
            <div className=''>
              <div className='tabs-title'>
                <div className='tabs__title'><div className='tabs_title_head'><b>Just Arrived - Sản Phẩm Mới</b></div></div>              
              </div>
              <Loader isLoading={this.state.loading}>
                <Slider>
                  { this.state.products_one && this.state.products_one.map((per,i) => ( <div key={i}>
                    <RxProductBlock className='blockproduct' products={per} staticContext={this.props.staticContext} language={this.props.i18n.language}/>
                  </div>))}
                </Slider>
              </Loader>
            </div>
          </div>
        </div>

      {/*<div className='blockfull--product-new'>
        <div className='container rx-carousel'>
          <div className='tabs'>
            <div className='tabs-title'>
              <div className='tabs__title'><div className='tabs_title_head'><b>{t('Product')} {t('new')}</b></div></div>              
            </div>
            <Loader isLoading={this.state.loading}>
              <div className='rx-main-product'>
                <RxProductBlock className='blockproduct' products={this.state.products_one} staticContext={this.props.staticContext} language={this.props.i18n.language}/>
                <div onClick={() => { this.animateProduct('next')}} className='blockproduct-arr jcarousel-control-prev carousel__arrow'><span className='mlkicon-ArrowNext mlkicon-Arrow'></span></div>
                <div onClick={() => { this.animateProduct('pre')}} className='blockproduct-arr jcarousel-control-next carousel__arrow'><span className='mlkicon-ArrowBack mlkicon-Arrow'></span></div>
              </div>
            </Loader>
          </div>
        </div>
      </div>*/}

        {isDesktop && this.state.advertisement && this.state.advertisement.length > 1 && <div className='rx-header row row-nmar rx-header-desktop'>
          <div className='rx-category'><div className='rx-categories'></div></div>
          <div className='container rx-banner'>
            <div className='rx-slider'>
              <div className='exampleSliders'>
                <RxAdvertisementBlock data={this.state.advertisement && this.state.advertisement[1]} staticContext={this.props.staticContext} language = {this.props.i18n.language}/>
              </div>
            </div>
          </div>
        </div>}

        <div className='blockfull--product'>
          <div className='container rx-carousel'>
            <div className='tabs'>
              <div className='tabs-title'>
                <div className='tabs__title'><div className='tabs_title_head'><b>{t('Categories')} {t('favorite')}</b></div></div>              
              </div>
              <div className='rx-main-product'>
                <Loader isLoading={this.state.loading}>
                  <RxProductFavoriteBlock className='blockproduct' cates={this.state.cates} staticContext={this.props.staticContext} language = {this.props.i18n.language}/>
                </Loader>
              </div>
            </div>
          </div>
        </div>

        <div className='blockfull--product'>
          <div className='container rx-carousel-brand'>
            <div className=''>
              <div className='lk-page-brand-title'><b>{t('Brand')} {t('favorite')}</b></div>  
              <div className='lk-page-brand-desc'>{t('brand_desc')}</div>
              <div className='rx-main-product brand'>
                <Loader isLoading={this.state.loading}>
                  <Slider>
                    { this.state.brand && this.state.brand.map((per,i) => ( <div key={i}>
                      {per.map((obj,j) => (
                        <div key={j} style={{ backgroundImage: 'url(' + config.base_api + '/upload/image/' + (obj['img_landscape'] || 'ico_app_default.png') +' )'}} className='footer-brand'><Link to={{ pathname: '/search/'+ obj.slug, brandid: obj._id }}></Link></div>
                      ))}
                    </div>))}
                  </Slider>
                </Loader>
              </div>
            </div>
          </div>
        </div>
        <div className='blockfull--product'>
          <div className='container rx-carousel'>
            <div className='tabs'>
              <div className='rx-main-content'>
                <RxContentBlock video={this.state.video} news={this.state.news} staticContext={this.props.staticContext} language = {this.props.i18n.language}/>
              </div>
            </div>
          </div>
        </div>
        
      </div>)
  }
}

export default withTranslation('translations')(Home)