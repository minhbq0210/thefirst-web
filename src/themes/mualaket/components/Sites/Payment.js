import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import $ from 'jquery'
import {Helmet} from "react-helmet";
const { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose } = global.rootRequire('redux')
const { rxget, rxpost, rxgetLocal, rxsetLocal } = global.rootRequire('classes/request')
const { rxcurrencyVnd, rxChangeAlias } = global.rootRequire('classes/ulti')

const config = global.rxu.config

class Payment_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      editingData: {type_payment: 1, logo: ''},
      msg: '',
      msgerror: '',
      orderData: {},
      success: false,
      chkbox: false,
      checkpayment: false,
      infobank: [],
      total: 0,
      totaltmp: 0,
      weighttotal: 0,
      shiptotal: 0,
      price_discount: 0,
      checkcustomer: false,
    }
    this.state.shipping = this.state.shipping || [{config: {weighttotal: '0', exchangerate: '0'}, name: 'Nhận hàng tại cửa hàng'}]
    this.state.shippingname = this.state.shippingname || 'Nhận hàng tại cửa hàng'
    this.state.config = this.state.config || [{weighttotal: '0', exchangerate: '0'}]
    let strcateshipping = rxgetLocal('shipping')
    if (strcateshipping) {
      let objshipping = {name: ''}
      try {objshipping = JSON.parse(strcateshipping)} catch(e) {}
      if (objshipping && objshipping.name) {
        this.state.shippingname = objshipping.name
        this.state.config = objshipping.config
      }
    }
    this.state.currency = 0
    let strcatecurrency = rxgetLocal('currency')
    if (strcatecurrency) {
      let objcurrency = {name: '', exchangerate:''}
      try {objcurrency = JSON.parse(strcatecurrency)} catch(e) {}
      if (objcurrency && objcurrency.name) {
        this.state.currency = objcurrency
      }
    }
    this.state.dataLocal = global.getDataLocal()
    this.state.editingData['city'] = this.props.auth ? global.rxu.get(this.props.auth.user, ['address', 0, 'city'], 'Thành phố Hồ Chí Minh') : 'Thành phố Hồ Chí Minh'
    this.state.editingData['county'] = this.props.auth ? global.rxu.get(this.props.auth.user, ['address', 0, 'county'], 'Quận 1') : "Quận 1"
    this.state.dataDistrict = []
    if(this.state.dataLocal && this.state.editingData['city']) {
      let checkcity = this.state.dataLocal.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(this.state.editingData['city'])) !== -1)
      if(checkcity && checkcity.length > 0 && checkcity[0].name !== this.state.editingData['city']) {
        let dataDistricts = checkcity[0].districts
        if(dataDistricts && this.state.editingData['county']) {
          let checkcdistrict = Object.keys(dataDistricts).filter(key => rxChangeAlias(dataDistricts[key]).indexOf(rxChangeAlias(this.state.editingData['county'])) !== -1)
          if(checkcdistrict) {
            this.state.editingData['county'] = dataDistricts[checkcdistrict]          }
        }
        this.state.dataDistrict = dataDistricts
        this.state.editingData['city'] = checkcity[0].name
      } else {
        this.state.dataDistrict = checkcity[0].districts
      }
      if(this.state.editingData['city'] === 'Thành phố Hồ Chí Minh') {
        this.state.shippingname = "COD(trong TP.HCM)"
      } else {
        this.state.shippingname = "COD(ngoài TP.HCM)"
      }
    }
  }

  componentDidMount() {
    this.fetchConfig()
    this.fetchDataLogo()
    // If login
    setTimeout(() => { 
      if (this.props.auth && this.props.auth.user && this.props.auth.user._id) {
        let tempUser = this.props.auth.user
        let editingData = this.state.editingData      
        editingData['name'] = tempUser.fullname
        editingData['email'] = tempUser.email 
        editingData['phone'] = tempUser.phone      
        editingData['address'] = tempUser.address
        this.setState({ editingData: editingData })
      }
    }, 20)
  }
  fetchDataLogo(){
    let editingData = this.state.editingData
    rxget(config.api_slide_logo, {}, { '1': (json) => { 
      if(json.data && json.data['img_landscape']) { 
        editingData['logo'] = config.base_api + '/upload/image/' + json.data['img_landscape']
        this.setState({editingData: editingData}) 
      } else {
        editingData['logo'] = 'http://www.annawholesale.com.au/images/static/LOGO-ANNA-WHOLSALE.jpg'
        this.setState({editingData: editingData})
      }
    }})
  }

  fetchConfig() {
    rxget(config.api_setting, {search_type: '2'}, {
      '1': (json) => { 
        let datasetting = json.data
        if (datasetting && datasetting[0] && datasetting[0].info) {
          let infobank = []
          let shipping = []
          if (datasetting[0].info.bank) { infobank = datasetting[0].info.bank}
          this.setState({ infobank: infobank })
          if (datasetting[0].info.shipping) { 
            shipping = datasetting[0].info.shipping
            let checkshipping = shipping.find(objshipping => objshipping.name === this.state.shippingname)
            if(checkshipping) {
              rxsetLocal('shipping', JSON.stringify(checkshipping))
              this.setState({shippingname: checkshipping.name, config: checkshipping.config}, () => {
              })
            }
            this.setState({ shipping: shipping })
          }
        }
      }
    })
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  parseCarts() {
    let editingData = this.state.editingData
    let arrcart = []
    if (this.props.cart && this.props.cart.carts) {
      for(let key in this.props.cart.carts) { arrcart.push(key)}
    }
    editingData['arrcart'] = arrcart
    this.setState({ editingData: editingData })
  }

  postOrder() {
    this.parseCarts()
    rxpost(config.api_order, this.state.editingData, {
      '1': (json) => {
        this.props.cartClear()
        this.setState({ msg: '' })
        this.setState({ success: true})
        this.setState({ orderData: json.data})
      },
      '-2': (json) => {
        this.setState({ msg: this.props.t('Fields must not be empty') })
      }
    }) 
  }

  onClickApplyDiscount() {
    if(this.state.editingData.discountcode !== 'undefined'){
      let params = {'search_code': this.state.editingData.discountcode}
      rxget(config.api_discount_find, params, {
        '1': (json) => {
          if (json.data && json.data.code) {
            this.setState({ msg: '' })
            this.helpCalDiscount(json.data, this.helpCalCart()) 
          } else {
            this.setState({ msg: this.props.t('The discount code is incorrect') , price_discount: 0, total: this.helpCalCart() })
          }
        }
      })
    }
  }

  onClickOrderSubmit(e) {
    let editingData = this.state.editingData
    editingData['textorder'] = this.state.shippingname
    editingData['detail'] = rxgetLocal('rxcart')
    editingData['price'] = this.helpCalCart()
    editingData['fee_ship'] = (this.state.shiptotal === 0) ? this.helpShipTotal() : this.state.shiptotal
    editingData['price_discount'] = Number(this.state.price_discount)
    editingData['totalpay'] = this.helpCalCart()
    editingData['discount_bill'] = 0
    editingData['totalweight'] = (this.state.weighttotal === 0) ? this.helpWeightTotal() : this.state.weighttotal
    if(editingData['street'] && editingData['county'] && editingData['city']) {
      if(this.props.auth && this.props.auth.user && this.props.auth.user._id) {
        editingData['address_deliver'] = [{street: editingData['street'], county: editingData['county'], city: editingData['city']}]
      } else {
        editingData['address'] = [{street: editingData['street'], county: editingData['county'], city: editingData['city']}]
      }
      delete editingData['street']
      delete editingData['county']
      delete editingData['city']
    }
    editingData['currency'] = this.state.currency

    //check fiedls
    if(!editingData['name']) {
      this.setState({msg: this.props.t('Fullname must not be empty')})
    } else if(!editingData['phone']) {
      this.setState({msg: this.props.t('Phone must not be empty')})
    } else if(!editingData['email']) {
      this.setState({msg: this.props.t('Email must not be empty')})
    } else if(!editingData['address']) {
      this.setState({msg: this.props.t('Address must not be empty')})
    } else {
      this.setState({ editingData: editingData }, () => {
        if (!this.state.chkbox) {
          this.postOrder()
        } else {
          let customerinfo = {}
          let arrname = (this.state.editingData['name'] && this.state.editingData['name'].length > 0) ? this.state.editingData['name'].split(" ") : []
          if (arrname.length > 1) {
            customerinfo['firstname'] = arrname[arrname.length - 1]
            customerinfo['lastname'] = arrname.slice(0,-1).join(" ")
          } else if (arrname.length === 1) {
            customerinfo['firstname'] = customerinfo['lastname'] = arrname[arrname.length - 1]
          } else {
            customerinfo['firstname'] = customerinfo['lastname'] = ''
          }
          customerinfo['email'] = this.state.editingData['email']
          customerinfo['password'] = this.state.editingData['password']
          customerinfo['phone'] = this.state.editingData['phone']
          customerinfo['address'] = editingData['address']
          rxpost(config.api_user, customerinfo, {
            '1': (json) => {
              this.setState({ msgerror: '' }, () => {this.postOrder()})
            },
            '-2': (json) => {
              if (json.msg === 'Dupplicate data') {
                this.setState({ msgerror: this.props.t('Email already exists'), msg: '' })
              } else if (json.msg === 'Email invalid format') {
                this.setState({ msgerror: this.props.t('Email invalidate'), msg: '' })
              }else if (json.msg === 'Dupplicate phone') {
                this.setState({ msgerror: this.props.t('Phone number already exists'), msg: '' })
              } else {
                this.setState({ msgerror: this.props.t('Fields must not be empty'), msg: '' })
              }
            }
          })
        }
      }); $('html, body').animate({ scrollTop: 0 }, 200, () => {})
    }
  }

  helpCalCart() {
    let total = 0
    if (this.props.cart && this.props.cart.carts) {
      for (let key in this.props.cart.carts) {      
        if (this.props.cart.carts.hasOwnProperty(key)) {
          if (this.props.cart.carts[key] && this.props.cart.carts[key].data) {
            total += this.props.cart.carts[key].data.price * this.props.cart.carts[key].amount
          }
        }
      }
    }
    return total
  }
  helpWeightTotal() {
    let weighttotal = 0
    if (this.props.cart && this.props.cart.carts) {
      for (let key in this.props.cart.carts) {      
        if (this.props.cart.carts.hasOwnProperty(key)) {
          if (this.props.cart.carts[key] && this.props.cart.carts[key].data) {
            if(this.props.cart.carts[key].data.grams){
              let num = this.props.cart.carts[key].data.grams
              num =Number(num)*this.props.cart.carts[key].amount
              weighttotal += num
            }
          }
        }
      }
    }
    return weighttotal
  }
  helpShipTotal() {
    let shiptotal = 0
    let weighttotal = 0
    if (this.props.cart && this.props.cart.carts) {
      for (let key in this.props.cart.carts) {      
        if (this.props.cart.carts.hasOwnProperty(key)) {
          if (this.props.cart.carts[key] && this.props.cart.carts[key].data) {
            if(this.props.cart.carts[key].data.grams){
              let num = this.props.cart.carts[key].data.grams
              num =Number(num)*this.props.cart.carts[key].amount
              weighttotal += num
            }
          }
        }
      }
    }
    if (this.state.config) {
      for(var i = 0; i < this.state.config.length; i++) {
        if (this.state.config[i].weighttotal <= (weighttotal/1000)) {
          shiptotal = Number(this.state.config[i].exchangerate)
        }        
      }
    }
    return shiptotal
  }

  helpCalDiscount(arrdiscount, priceold) {
    let objmeta = {}
    let pricenew = priceold
    let discount = 0
    if (arrdiscount && arrdiscount.metadiscount){
      objmeta = arrdiscount.metadiscount
      let startdate = arrdiscount.startdate
      let enddate = arrdiscount.enddate
      let nowdate = Date.now()
      if(nowdate <= enddate && nowdate >= startdate){
        if (objmeta['typepromotion'] === 1 ) {
          if (arrdiscount.uselimit === false) {
            if ((arrdiscount.amountdiscount - arrdiscount.amountdiscountused) > 0 ) {
              pricenew = priceold - objmeta['discountvalue']
              discount = objmeta['discountvalue']
            } else {
              this.setState({ msg: this.props.t('The number of discounts has been exhausted') })
              pricenew = priceold
            }
          } else {
            pricenew = priceold - objmeta['discountvalue']
            discount = objmeta['discountvalue']
          }
        } else if (objmeta['typepromotion'] === 2) {
          pricenew = priceold - (priceold * objmeta['discountvalue'] / 100) 
          discount = priceold * objmeta['discountvalue'] / 100
        } else if (objmeta['typepromotion'] ===  3) {
          pricenew = priceold - objmeta['discountvalue']
          discount = objmeta['discountvalue']
        }
      } else {
        pricenew = priceold
        this.setState({ msg: this.props.t('Discount code expired') })
      }
    }
    this.setState({total: pricenew, price_discount: discount})
  }

  handleChangeChk(e, checkbox) {
    this.setState({chkbox: !checkbox})
  }

  toggleOne(e, checkbox) {
    // 1: COD, 2: Payment
    let editingData = this.state.editingData
    editingData['type_payment'] = (checkbox) ? 1 : 2 
    this.setState({checkpayment: !checkbox, editingData: editingData})
  }

  checkCustomerAdress(e, checkbox){
    // 1: Mặc định, 2: ĐC mới
    let editingData = this.state.editingData
    editingData['type_adress'] = (checkbox) ? 1 : 2 
    this.setState({checkcustomer: !checkbox, editingData: editingData})
  }

  getDate(timestamp) {
    let dayarr = [this.props.t('Sunday'), this.props.t('Monday'), this.props.t('Tuesday'), this.props.t('Wednesday'), this.props.t('Thursday'), this.props.t('Friday'), this.props.t('Saturday')]
    timestamp = timestamp * 1000
    let u = new Date(timestamp)  
    let tempstr  = ('0' + u.getUTCDate()).slice(-2) + '-' + ('0' + (u.getUTCMonth() + 1)).slice(-2) + '-' + u.getUTCFullYear()
    let tempstrdate = dayarr[u.getDay()] + ', ' + tempstr
    return tempstrdate
  }
  helpShowCartTotal() {
    let tempTotal = 0

    if (this.props.cart) {
      for(let key in this.props.cart.carts) {
        if (this.props.cart.carts.hasOwnProperty(key)) {        
          tempTotal += (this.props.cart.carts[key].amount || 0)
        }
      }
    }
    return tempTotal
  }
  onClickScrolltoId(id) {
    if (typeof $('#' + id).offset !== 'undefined') {
      $('html, body').animate({ scrollBottom: 0 }, 200, () => {})
    }
  }
  handleChangeShip(e) {
    let checkshipping = this.state.shipping.find(objshipping => objshipping.name === e.target.value)
    if(checkshipping) {
      rxsetLocal('shipping', JSON.stringify(checkshipping))
      this.setState({shippingname: checkshipping.name, config: checkshipping.config}, () => {
      })
    }
  }
  handleChangeCity(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    let dataDistricts = this.state.dataDistrict
    let shippingname = this.state.shippingname
    if(name === 'city') {
      let checkcity = this.state.dataLocal.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(e.target.value)) !== -1)
      if(checkcity && checkcity.length > 0 && checkcity.name !== this.state.editingData['city']) {
        dataDistricts = checkcity[0].districts
      }
      if(e.target.value === 'Thành phố Hồ Chí Minh') {
        shippingname = "COD(trong TP.HCM)"
      } else {
        shippingname = "COD(ngoài TP.HCM)"
      }
      let checkshipping = this.state.shipping.find(objshipping => objshipping.name === shippingname)
      if(checkshipping) {
        rxsetLocal('shipping', JSON.stringify(checkshipping))
        this.setState({config: checkshipping.config})
      }
    }
    this.setState({editingData: editingData, dataDistrict: dataDistricts, shippingname: shippingname})
  }

  render() {
    const { t } = this.props
    let tempCartDetail = []
    if (this.props.cart && this.props.cart.carts) {
      tempCartDetail = Object.keys(this.props.cart.carts).map((key, indexcard) => {
        let tempOption = {}
        if (this.props.cart && this.props.cart.carts && this.props.cart.carts[key] && this.props.cart.carts[key].data && this.props.cart.carts[key].data.options && this.props.cart.carts[key].data.options[0]) {
          tempOption = this.props.cart.carts[key].data.options[0]
        }
        if (tempOption && tempOption['key']) {delete tempOption['key']}
        if (tempOption && tempOption['data']) {delete tempOption['data']}
        return (
        <div key={key} className={(indexcard % 2) ? 'clearfix cart-page-product-row cart-background1' : 'clearfix cart-page-product-row cart-background2'} >
          { (this.props.cart.carts[key] && this.props.cart.carts[key].data) && 
          <div>
            <div className='cart-page-product-name'>
              <img className='cart-page-product-img lazyload' alt='ico_default' data-src={config.base_api + '/upload/image/' + (this.props.cart.carts[key].data.img_landscape || 'ico_app_default.png')} />
              <div className='cart-page-product-detail'>
                <div className='cart-page-product-detail-left'>
                  <div className='figure-body-brand-inlist'>{this.props.cart.carts[key].data.name.vendor ||  ''}</div>
                  <div className='cart-page-product-name-inlist'>{this.props.cart.carts[key].data.name}</div>
                  {this.props.cart.carts[key].data.grams ? <div className='cart-page-product-detail-weight cart-page-product-color'>{t('Weight')}: {this.props.cart.carts[key].data.grams}(grams)</div>:<div></div>}
                  <div className='cart-page-product-price'>{rxcurrencyVnd(this.props.cart.carts[key].data.price)} {this.props.cart.carts[key].data.price_discount !== 0 ? <span className='cart-page-product-priceold'>{rxcurrencyVnd(this.props.cart.carts[key].data.price_discount)}</span> : <span></span>}</div>
                </div>
                <div className='cart-page-product-detail-right'> 
                  <div className='cart-page-product-quantity-inlist'>
                    <div className='cart-page-product-sub' onClick={(e) => { this.props.cartAdd(this.props.cart.carts[key].id, { amount: 1, data: {} }) }}><span className='mlkicon-Plus'></span></div>
                    <div className='cart-page-product-amount'>{this.props.cart.carts[key].amount}</div>
                    <div className='cart-page-product-add' onClick={(e) => { this.props.cartSub(this.props.cart.carts[key].id, { amount: 1, data: {} }) }}><span className='mlkicon-Minus'></span></div>
                  </div>
                  <div className='cart-page-product-rm' onClick={() => { this.props.cartDelete(this.props.cart.carts[key].id) }}>{t('Delete')}</div>
                </div>
              </div>
            </div>
          </div> }
        </div>  
        )    
      })
    }
    let total = (this.state.total === 0) ? this.helpCalCart() : this.state.total
    let totaltmp = (this.state.totaltmp === 0) ? this.helpCalCart() : this.state.totaltmp
    let weighttotal = (this.state.weighttotal === 0) ? this.helpWeightTotal() : this.state.weighttotal
    let shiptotal = (this.state.shiptotal === 0) ? this.helpShipTotal() : this.state.shiptotal
    return (
      <div>
      <Helmet>
        <title>{`${global.SEOtitle}`}</title>
        <link rel="canonical" href={`${this.props.location.pathname}`} />
        <meta name="description" content={global.SEOdesc} />
      </Helmet>
      {this.state.success !== true ? <div className='cart-scroll rx-sticky' onClick={(e) => this.onClickScrolltoId('payment-submit')}>
        <div className='container row'>
          <div className='col-sm col-md-5 cart-scroll-title'>{t('Orders included')}: <span><b>{this.helpShowCartTotal() !== 0 ? this.helpShowCartTotal() : 0 }</b> {t('product')}</span></div>
          <div className='col-sm col-md-4 cart-page-payment-total'>
            <div className='cart-page-payment-subtotal-label'>{t('Total money to pay')}: <br/></div> <div className='cart-page-payment-subtotal'>{rxcurrencyVnd(Number(total)+ Number(shiptotal))}</div>
          </div>
          <div tabIndex='9' className='col-sm col-md-3 cart-page-customer-submit' onClick={(e) => this.onClickOrderSubmit(e)} onKeyPress={(e) => this.onClickOrderSubmit(e)}>{t('Payment now')}</div>
        </div>
      </div>:<div></div>}
      <div className='container'><div className='rx-main-page'>        
          {/*<div className='rx-cat-title'>Trang đặt hàng</div>*/}
          {this.state.success === false && <div className='rx-payment-page'><div className='row '>
            <div className='col-sm col-md-8 cart-page-left'>
              <div className='cart-page-section-title'><div className='cart-page-section-titlefull'><div className='cart-page-section-titletext'>{t('Order')+' '} <span>({this.helpShowCartTotal() !== 0 ? this.helpShowCartTotal() : 0 } {t('product')})</span></div></div></div>
              <div className='cart-page-body'>{tempCartDetail}</div>
          
              <div className='cart-page-shipmoney'>{t('Sub total')}: <b>{rxcurrencyVnd(totaltmp)}</b>
                <span className='cart-page-shipweight'>{t('Weight total')}: <b>{weighttotal} </b>(grams)</span>
                <span className='cart-page-shipweight rx-mobile'>{t('Weight')}: <b>{weighttotal} </b>(grams)</span>
                <div className='cart-page-vat'><i>({t('VAT included')})</i></div>
              </div> 
              <Link to='/'><div className='col-sm col-md-3 cart-page-customer-return' >{t('Shopping continue')}</div></Link>
            </div>
            <div className='col-sm col-md-4 cart-page-customer-info'>
              <div className='cart-page-section-title'><div className='cart-page-section-titlefull'><div className='cart-page-section-titletext'>{t('Customer info')}</div></div></div>
              <div className='clearfix cart-page-customer-inner'>
                
                { !(this.props.auth && this.props.auth.user && this.props.auth.user._id) ?
                  <div>
                    <div>
                      <div className='cart-page-customer-name'>{t('Full name')}</div>
                      <input tabIndex='1' type='text' name='name' autoComplete='name' onChange={(e) => this.onBlurData(e, 'name')} className='fullwidth-input' placeholder={t('Enter')+' '+t('Full name')} />
                    </div>
                    <div>
                      <div className='cart-page-customer-phone'>{t('Phone number')}</div>
                      <input tabIndex='2' type='text' name='phone' autoComplete='tel' onChange={(e) => this.onBlurData(e, 'phone')} className='fullwidth-input' placeholder={t('Enter')+' '+t('Phone number')} />
                    </div>
                    <div>
                      <div className='cart-page-customer-email'>Email</div>
                      <input tabIndex='3' type='email' name='email' autoComplete='email' onChange={(e) => this.onBlurData(e, 'email')} className='fullwidth-input' placeholder={t('Enter')+' Email'} />
                    </div>
                    <div className='cart-page-customer-address'>{t('City')}</div>
                      <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['city'])} onChange={(e) => {this.handleChangeCity(e, 'city')}}>
                        {this.state.dataLocal && this.state.dataLocal.length > 0 ? this.state.dataLocal.map((option, index) => (<option key={index} value={option.name}>{option.name}</option>)) : <option>{global.rxu.get(this.props.auth.user, ['address', 0, 'city'], '')}</option>}
                      </select>
                      <div className='cart-page-customer-address'>{t('District')}</div>
                        <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['county'])} onChange={(e) => {this.handleChangeCity(e, 'county')}}>
                        {this.state.dataDistrict && Object.keys(this.state.dataDistrict).length>0 && Object.keys(this.state.dataDistrict).map(key => (<option key={key} value={this.state.dataDistrict[key]}>{this.state.dataDistrict[key]}</option>))}
                      </select>
                      <div className='cart-page-customer-address'>{t('Address')}</div>
                      <input tabIndex='4' type='text' name='street' autoComplete='street' onChange={(e) => this.onBlurData(e, 'street')} className='fullwidth-input' placeholder={t('Enter')+' '+t('Address')} />
                    </div> : 
                  <div>
                    <div className='cart-page-customer-group'>
                      <div className='input-group payment'>
                        <span className='rs-checkbox-customer-small' onClick={(e) => this.checkCustomerAdress(e, this.state.checkcustomer )}>
                          <span className={!this.state.checkcustomer ? 'active' : ''} ></span>
                        </span>
                        <span className={!this.state.checkcustomer ? 'rs-checkbox-group-nametext active' : 'rs-checkbox-group-nametext'}>{t('Default')} &emsp;&emsp; </span>
                      </div>
                      <div className='input-group payment'>
                        <span className='rs-checkbox-customer-small' onClick={(e) => this.checkCustomerAdress(e, this.state.checkcustomer )}>
                          <span className={this.state.checkcustomer ? 'active' : ''} ></span>
                        </span>
                        <span className={this.state.checkcustomer ? 'rs-checkbox-group-nametext active' : 'rs-checkbox-group-nametext'}>{t('New address')}</span>
                      </div>
                    </div>
                    {!this.state.checkcustomer ?
                      <div>
                        <div className='cart-page-group-info'>
                          <div className='cart-page-customer-name'>{t('Full name')}
                          <span className='cart-page-customer-detail'>{this.state.editingData.name}</span> </div>
                          <div className='cart-page-customer-email'>Email
                          <span className='cart-page-customer-detail'>{this.state.editingData.email}</span></div>
                          <div className='cart-page-customer-phone'>{t('Phone number')}
                          <span className='cart-page-customer-detail'>{global.rxu.get(this.state.editingData, ['phone'], '')}</span></div>
                          <div className='cart-page-customer-adress'>
                            <span className='cart-page-customer-address'>{t('Address')}</span>
                            <span className='cart-page-customer-detail'>
                              {global.rxu.get(this.state.editingData, ['address', 0, 'street'], '')}, 
                              {this.state.editingData['county'] ? this.state.editingData['county'] : global.rxu.get(this.state.editingData, ['address', 0, 'county'], '')}, 
                              {this.state.editingData['city'] ? this.state.editingData['city'] : global.rxu.get(this.state.editingData, ['address', 0, 'city'], '')}
                            </span>
                          </div>
                        </div>
                      </div>:<div>
                        <div className='cart-page-group-info'>
                          <div className='cart-page-customer-name'>{t('Full name')}
                          <span className='cart-page-customer-detail'>{this.state.editingData.name}</span> </div>
                          <div className='cart-page-customer-email'>Email
                          <span className='cart-page-customer-detail'>{this.state.editingData.email}</span></div>
                        </div>
                        <div>
                          <div className='cart-page-customer-phone'>{t('Phone number')}</div>
                          <input tabIndex='2' type='text' name='phone' autoComplete='tel' onChange={(e) => this.onBlurData(e, 'phone_deliver')} className='fullwidth-input' placeholder={t('Enter')+' '+t('Phone number')} />
                        </div>
                        <div className='cart-page-customer-address'>{t('City')}</div>
                        <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['city'])} onChange={(e) => {this.handleChangeCity(e, 'city')}}>
                          {this.state.dataLocal && this.state.dataLocal.length > 0 ? this.state.dataLocal.map((option, index) => (<option key={index} value={option.name}>{option.name}</option>)) : <option>{global.rxu.get(this.props.auth.user, ['address', 0, 'city'], '')}</option>}
                        </select>
                        <div className='cart-page-customer-address'>{t('District')}</div>
                          <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['county'])} onChange={(e) => {this.handleChangeCity(e, 'county')}}>
                          {this.state.dataDistrict && Object.keys(this.state.dataDistrict).length>0 && Object.keys(this.state.dataDistrict).map(key => (<option key={key} value={this.state.dataDistrict[key]}>{this.state.dataDistrict[key]}</option>))}
                        </select>
                        <div className='cart-page-customer-address'>{t('Address')}</div>
                        <input tabIndex='3' type='text' name='street' autoComplete='street' onChange={(e) => this.onBlurData(e, 'street')} className='fullwidth-input' placeholder={t('Enter')+' '+t('Address')} />
                      </div>
                    }
                  </div> }

                <div className='cart-page-customer-address'>{t('Note')}</div>
                <textarea tabIndex='4' className='fullwidth-input' rows='5' placeholder={t('Enter information to note')} onChange={(e) => this.onBlurData(e, 'desc')}/>

                { !(this.props.auth && this.props.auth.user && this.props.auth.user._id) ? <div className='input-group'>
                  <input type='checkbox' id='check1' tabIndex='0' defaultChecked={this.state.chkbox}  onChange={(e) => {this.handleChangeChk(e, this.state.chkbox)}}/>
                  <label htmlFor='check1'>{t('Create a new account')} <div>({t('making ordering in the next time becomes faster')})</div></label>                
                </div> : <div></div> }

                { !(this.props.auth && this.props.auth.user && this.props.auth.user._id) && this.state.chkbox && 
                  <div className='rx-input-group-two'>
                    <div>
                      <div className='cart-page-customer-name'>Email { this.state.msgerror && <div className='cart-page-error' >{this.state.msgerror}</div> }</div>
                      <input value={this.state.editingData.email?this.state.editingData.email:''} tabIndex='3' type='text' autoComplete='email' name='email' onChange={(e) => this.onBlurData(e, 'email')} className='fullwidth-input' />
                    </div>
                    <div>
                      <div className='cart-page-customer-phone'>{t('Password')}</div>
                      <input tabIndex='3' type='password' autoComplete='password' name='password' onChange={(e) => this.onBlurData(e, 'password')} className='fullwidth-input' />
                    </div>
                  </div>}

                <div className='cart-page-shipping-way'><b className='cart-page-shipping-way-title'>{t('Form of transportation')}</b> 
                  <select className='cart-page-shipping-desc' value={this.state.shippingname || 'Nhận hàng tại cửa hàng'} onChange={(e) => {this.handleChangeShip(e)}}>
                  {this.state.shipping.map((objshipping, indexshipping) => (<option key={indexshipping} value={objshipping.name}>{objshipping.name}</option>))}
                </select> 
                  <span className='mlkicon-Plus cart-page-shipping-way-icon'></span>
                </div>
                <div className='cart-page-payment-way'><b className='cart-page-payment-way-title'>{t('Form of payment')}</b> 
                  <div className='input-group'>
                    <span className='rs-checkbox-payment-small' onClick={(e) => this.toggleOne(e, this.state.checkpayment )}>
                      <span className={!this.state.checkpayment ? 'iconanna-check' : ''} ></span>
                    </span>
                    <span className='rs-checkbox-group-nametext'>COD &emsp;&emsp; </span>
                  </div>
                  <div className='input-group'>
                    <span className='rs-checkbox-payment-small' onClick={(e) => this.toggleOne(e, this.state.checkpayment )}>
                      <span className={this.state.checkpayment ? 'iconanna-check' : ''} ></span>
                    </span>
                    <span className='rs-checkbox-group-nametext'>{t('Transfer')}</span>
                  </div>
                  {this.state.checkpayment && <div><br></br>
                    {(this.state.infobank && this.state.infobank.constructor === Array && this.state.infobank.length > 0) && <div className='box-payment'>
                      {this.state.infobank.map((bank, indexbank) => (
                      <div key={indexbank}>
                        <div><b>Chủ tài khoản</b>: {bank.name}</div> 
                        <div><b>Ngân hàng</b>: {bank.namebank}</div> 
                        <div><b>Số tài khoản</b>: {bank.numberbank}</div> 
                        <div><b>Chi nhánh</b>: {bank.bankbranch}</div> 
                      </div>))}
                    </div>}
                  </div>}
                  
                </div>
                { this.state.msg && <div className='cart-page-error' >{this.state.msg}</div> }
                         
                  <div className=''>{/*'payment-fixed'*/}
                    <div className='row rx-row-nomar'>
                      <input tabIndex='8' type='text' autoComplete='discountcode' name='discountcode' placeholder={t('Discount code')} onChange={(e) => this.onBlurData(e, 'discountcode')} className='col-sm col-md-9' style={{margin: '0px'}} />                    
                      <div className='col-sm col-md-3 cart-page-payment-voucher' onClick={(e) => this.onClickApplyDiscount(e)}>{t('Apply')}</div>
                      <div className='cart-page-payment-info'>
                        <div>{t('Sub total')} <span>{rxcurrencyVnd(totaltmp)}</span></div>
                        <div>{t('Shipping fee')}<span>{rxcurrencyVnd(shiptotal)}</span></div>
                        <div>{t('Discount')}<span>{rxcurrencyVnd(this.state.price_discount)}</span></div>
                      </div>
                    </div> 
                    <div className='row rx-row-nomar payment-fixed' id='payment-submit'>
                      <div className='cart-page-payment-total'> 
                        <div className='cart-page-payment-subtotal-label'>{t('Total mount')} <br/></div> <div className='cart-page-payment-subtotal'>{rxcurrencyVnd(Number(total)+ Number(shiptotal))}</div>
                      </div> 
                      <div className='cart-page-payment-total'>
                        <div tabIndex='9' className='cart-page-customer-submit' onClick={(e) => this.onClickOrderSubmit(e)} onKeyPress={(e) => this.onClickOrderSubmit(e)}>{t('Payment now')}</div>                    
                      </div> 
                      <div className='cart-page-payment-total' style={{ textAlign: 'center', fontSize: '13px'}}>
                        <span style={{fontSize: '13px'}}>({t('Please check your order before ordering')})</span>
                      </div>                                 
                    </div>
                  </div>
              </div>            
            </div>
          </div></div> }
          {this.state.success === true && <div className='lk-login-customer-page'>
            <div className='row lk-row-nomar'>
              <div className='rxauthen-left-container lk-contact'>
              </div>
              <div className='lk-page-customer-info lk-page-info-login'>
                <div className='lk-page-line-mainbox clearfix'>
                  <div className='lk-block-cicle-icon-check'>
                    <svg height='1.5em' viewBox='0 0 35 35' className='bock-icon-check-svg'>
                      <g className='lk-icon-check'><path strokeDasharray={`72.7977 72.7977`} strokeDashoffset={`50.2409`} d='M20 6.7L9.3 17.3 4 12c0-4.4 3.6-8 8-8s8 3.6 8 8-3.6 8-8 8-8-3.6-8-8'/></g>
                    </svg>
                  </div> 
                  <span>{t('Thank you for your purchase at the Anna Store')}</span>
                </div>
                
                <div className='lk-box-orderid'><span className='lk-payment-complete-text'>{t('Your order number code')}:</span><span>{this.state.orderData.order_code || ''}</span></div>
                <div>
                  {this.props.auth && this.props.auth.user && this.props.auth.user._id && <div className='lk-payment-complete-text'>{t('You can review your order')} <Link to='/customer/order'>{t('here')}</Link></div>}
                  {/*<div className='lk-payment-complete-text'>{t('Estimated delivery time on')} <span>{this.getDate(this.state.orderData.created_at + 259200)}</span> {t('to')} <span>{this.getDate(this.state.orderData.created_at + 432000)}</span></div>*/}
                  <div className='lk-payment-complete-text'>{t('Order details information has been sent to the email address')}: <i>{global.rxu.get(this.state.editingData, ['email'])}</i> . {t('If not found please check in')} <b>Spam</b> {t('or')} <b>{t('Junk Folder')}.</b></div>
                </div>
                
                <hr></hr>
                <div className='lk-payment-complete-text'><b>{t('Frequently asked questions')}</b></div>
                <ul>
                  <li><div className='lk-payment-complete-text'>{t('How to confirm the order')}?</div></li>
                  <li><div className='lk-payment-complete-text'>{t('Delivery time')}</div></li>
                  <li><div className='lk-payment-complete-text'>{t('Return policy')}</div></li>
                </ul>
              </div>
            </div>
          </div>}        
      </div></div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose
}

const Payment = connect(
  mapStateToProps,
  mapDispatchToProps
)(Payment_)

export default withTranslation('translations')(Payment)