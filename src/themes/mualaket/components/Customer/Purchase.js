import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { withTranslation } from 'react-i18next'
import {Helmet} from "react-helmet";

const { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose, favoriteAdd, favoriteDelete } = global.rootRequire('redux')
const { rxget, rxpost } = global.rootRequire('classes/request')
const { rxcurrencyVnd } = global.rootRequire('classes/ulti')
const { rxCountStart } = global.rootRequire('components/Helpers/RxCountStart')
const RxRatingbox = global.rootRequire('components/Shares/RxRatingbox').default

const config = global.rxu.config
// const numberWithCommas = (x) => {
//   return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// }

class CustomerPurchase_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [],
      editingData: {},
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      msg: '',
      dataarrratings: [0,0,0,0,0],
      reviewsrating: {}
    }
    this.state.intervalId = 0
  }

  onBlurData(e, name) {
    let editingData = (typeof(this.state.reviewsrating) !== 'undefined') ? this.state.reviewsrating : {}
    editingData[name] = e.target.value
    this.setState({ reviewsrating: editingData })    
  }

  componentDidMount() {
    this.fetchAlldata()
    this.backtotop()
  }

  fetchAlldata() {
    this.fetchData()
  }
  backtotop() {
    let intervalId = setInterval(this.scrollStep.bind(this), 16.66);
    this.setState({ intervalId: intervalId });
  }
  scrollStep() {
    if (window.pageYOffset === 0) {
        clearInterval(this.state.intervalId);
    }
    window.scroll(0, window.pageYOffset - 60);
  }

  fetchData() {
    if (this.props.auth && this.props.auth.user && this.props.auth.user.customerid) {
      let tempPaging = this.state.paging
      tempPaging.customerid = this.props.auth.user.customerid
      this.setState({paging: tempPaging})
    }
    
    rxget(config.api_customer_allproduct, this.state.paging, {
      '1': (json) => { 
        this.setState({ data: json.data })
      }
    })
  }

  onClickDataUpdateSubmit(objreviewsrating) {
    let paramsrating = {}
    let reviewsrating = objreviewsrating
    if (reviewsrating && reviewsrating !== 'null' && reviewsrating !== 'undefined') {
      paramsrating['ratings'] = {}
      paramsrating['ratings'] = reviewsrating
      paramsrating['ratings']['customerid'] = this.props.auth.user.customerid
      paramsrating['ratings']['name'] = this.props.auth.user.fullname
      paramsrating['ratings']['isCustomer'] = 1
    }
    paramsrating['_id'] = this.state.editingData._id
    rxpost(config.api_product_rating, paramsrating, {
      '1': (json) => { this.fetchData()}
    })
    this.onClickDataEdit({}, {})
    this.setState({ reviewsrating: {}, dataarrratings: [0,0,0,0,0]}) 
  }

  onClickDataUpdateCancel(e, perdata) {
    this.onClickDataEdit({}, {})
    this.setState({ reviewsrating: {}, dataarrratings: [0,0,0,0,0]}) 
  }

  onClickDataEdit(e, perdata) {
    let clone = JSON.parse(JSON.stringify(perdata))
    let objrating = {}
    if (typeof(perdata._id) !== 'undefined' && typeof(perdata.ratings) !== 'undefined' && perdata.ratings.length > 0) {
      let customerid = this.props.auth.user.customerid
      objrating = perdata.ratings.find(o => o.customerid === customerid )
      this.setState({reviewsrating: objrating})
    }
    this.setState({ editingData: clone})
  }

  onClickAddRemoveFavorite(e, product, checkclick) {
    e.stopPropagation()   
    if (checkclick === false) {
      this.props.favoriteAdd(product._id, { amount: this.state.amount, data: product })  
    } else {
      this.props.favoriteDelete(product._id) 
    }
  }
  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }
  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  render() {
    const { t } = this.props
    let arrfavorite = []
    if (this.props.favorite && this.props.favorite.favorites) {
      for (var key in this.props.favorite.favorites) { arrfavorite.push(key) }
    }
    let dataproduct = []
    if (this.state.data && this.state.data.length > 0) {
      dataproduct = this.state.data.map(perdata => (
        <div className='clearfix lk-customer-product-row'  key={perdata._id}>
          <div className='lk-box-product-img'>
            <Link to={`/product/${perdata.slug}`}><img className='lazyload' data-src={config.base_api + '/upload/image/' + perdata.img_landscape} alt='' title='{perdata.name}' /> </Link>
          </div>
          <div className='lk-box-product-info'>
            <Link to={`/product/${perdata.slug}`}><div className='lk-card-order-lable-name'>{perdata.name} </div></Link>
            <div className='lk-card-order-lable-desc rxprice'>{rxcurrencyVnd(perdata.price)}</div>
          </div>
          <div className='lk-box-product-ratings'>
            
            <div className='clearfix lk-product-positon-start' onClick={(e) => this.onClickDataEdit(e, perdata)}>
              <div className='figure-body-rating-star'>
                {rxCountStart(perdata.ratingsSumary)}
              </div>
            </div>
            <div className='lk-product-command-text'>({perdata.ratings.length} {t('reviews')})</div>
            <div className='clearfix row lk-product-box-addcart '>
              <div className='rx-action-favorite clearfix'>
                {(arrfavorite.indexOf(perdata._id) !== -1) && <div><div className='rx-heartfill-medium mlkicon-HeartSolid' onClick={(e) => this.onClickAddRemoveFavorite(e, perdata, true)}></div></div>}
                {(arrfavorite.indexOf(perdata._id) === -1) && <div><div className='rx-heart-medium mlkicon-Heart' onClick={(e) => this.onClickAddRemoveFavorite(e, perdata, false)}></div></div>}
              </div>
              <div className='rx-action-addcart rx-action-addcart-lg lk-product-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(perdata._id, { amount: 1, data: perdata }) }}>
                <span className="rx-icon rx-icon-cart">
                </span>{t('Add to Cart')}
              </div>
            </div>
          </div>
        </div> 
      ))
    }

    return (
      <div className='container'><div className='rx-main-page'>
        <Helmet>
          <title>{`${global.SEOtitle}`}</title>
          <link rel="canonical" href="/customer/purchased" />
          <meta name="description" content={`${global.SEOdesc}`} />
        </Helmet>
        <div className='lk-customer-page'>
          <div className='row rx-row-nomar'>
            {(this.state.editingData.created_at !== 0) &&
            <div className='col-sm col-md-12'>
              <div className='rxcustomer-title'>{t('Products purchased')}</div>
              <div>{dataproduct}</div>
              <div className='admin-table-pagination'>
                {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => { this.backtotop(); this.onClickPaginBack(e)}}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => { this.backtotop(); this.onClickPaginNext(e) }}><i className='icon-arrow-right'></i></div>}
              </div>
            </div> }

            {(this.state.editingData.created_at === 0) &&
            <div className='col-sm col-md-12'>
              <div className='lk-main-rating-title'>
                <div className='rxcustomer-title'>{t('Product evaluation')} {this.state.editingData.name}</div>
                <div className='lk-reviews-rating-close'><div className='admin-cardblock-formclose'onClick={(e) => this.onClickDataUpdateCancel(e, {})}>x</div></div>
              </div>
              <RxRatingbox reviewsrating={this.state.reviewsrating} infodata={this.state.editingData} onChange={(result) => {this.onClickDataUpdateSubmit(result)}} /> 
            </div> }
          </div>  
        </div>
      </div></div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth,
  favorite: state.favorite
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose,
  favoriteAdd,
  favoriteDelete
}

const CustomerPurchase = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerPurchase_)

export default withTranslation('translations')(CustomerPurchase)
