import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import {Helmet} from "react-helmet";

const { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose } = global.rootRequire('redux')
const { rxget } = global.rootRequire('classes/request')
const { rxgetdate, rxcurrencyVnd } = global.rootRequire('classes/ulti')

const config = global.rxu.config
// const numberWithCommas = (x) => {
//   return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// }

class CustomerOrder_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [],
      editingData: {},
      paging: { st_col: 'created_at', st_type: -1, pg_page: 1, pg_size: 10 },
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  componentDidMount() {
    this.fetchAlldata()
  }

  fetchAlldata() {
    this.fetchData()
  }

  fetchData() {
    let tempPaging = this.state.paging
    if (this.props.auth && this.props.auth.user && this.props.auth.user.customerid) {
      tempPaging.customerid = this.props.auth.user.customerid
    } 
    this.setState({ paging : tempPaging}, () => {
      rxget(config.api_customer_allorder, this.state.paging, {
        '1': (json) => { 
          this.setState({ data: json.data })
        }
      })
    })
  }

  // Pagin
  onClickPaginBack(e) {
    let paging = this.state.paging
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  onClickPaginNext(e) {
    let paging = this.state.paging
    paging.pg_page += 1
    this.setState({ paging: paging }, () => { this.fetchData() })
  }

  render() {
    const { t } = this.props
    let dataorder = []
    if (this.state.data && this.state.data.length > 0) {
      dataorder = this.state.data.map(perdata => (
        <tr key={perdata._id}>         
          <td className='lk-action-edit'><Link to={{ pathname: '/customer/tracking/', id: perdata.order_code }}>{perdata.order_code}</Link></td>
          <td>{rxgetdate(perdata.created_at)}</td>
          {(perdata.status_confirm !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>{t('noconfirm')}</span></td>}
          {(perdata.status_confirm === 1) && <td><span className='lk-status-box lk-status-confirm'>{t('Confirmed')}</span></td>}   
          {(perdata.status_ship !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>{t('noShip')}</span></td>}
          {(perdata.status_ship === 1) && <td><span className='lk-status-box lk-status-confirm'>{t('Shipped')}</span></td>}        
          {(perdata.status_order_payment !== 1) && <td><span className='lk-status-box lk-status-noconfirm'>{t('waitpayment')}</span></td>}
          {(perdata.status_order_payment === 1) && <td><span className='lk-status-box lk-status-confirm'>{t('Paymented')}</span></td>}
          {(perdata.type_payment !== 2) && <td><span className='lk-status-box lk-status-confirm'>{t('Yes')}</span></td>}
          {(perdata.type_payment === 2) && <td><span className='lk-status-box lk-status-noconfirm'>{t('No')}</span></td>}      
          <td><span className='table-total-money'>{rxcurrencyVnd(perdata.price)} </span></td>
          <td>{perdata.type_channel_name}</td>
        </tr>
      ))
    }

    return (
      <div className='container'><div className='rx-main-page'>
        <Helmet>
          <title>{`${global.SEOtitle}`}</title>
          <link rel="canonical" href="/customer/order" />
          <meta name="description" content={`${global.SEOdesc}`} />
        </Helmet>
        <div className='lk-customer-page'>
          <div className='row rx-row-nomar'>
            <div className='col-sm col-md-12'>
              <div className='rxcustomer-title'>{t('Order management')}</div>
                
              <div className='admin-table-wrap'>
                <table className='admin-table-product preset'>
                  <thead>
                    <tr>
                      <th>{t('Code')}</th>
                      <th>{t('Created')}</th>
                      <th>{t('Confirm')}</th>
                      <th>{t('Shipping')}</th>
                      <th>{t('Payment')}</th>                  
                      <th>COD</th>
                      <th>{t('Price')}</th>
                      <th>{t('Channel')}</th>
                    </tr>
                  </thead>
                  <tbody>{dataorder}</tbody>
                </table></div>
                <div className='admin-table-pagination'>
                  {(this.state.paging.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                  <div className='pagin-curr'>{this.state.paging.pg_page}</div>
                  {(this.state.data.length >= this.state.paging.pg_size) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
                </div>

            </div>
          </div>
        </div>
      </div></div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose
}

const CustomerOrder = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerOrder_)

export default withTranslation('translations')(CustomerOrder)