import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import {Helmet} from "react-helmet";

const { favoriteDelete, rxnavToggle, rxnavClose } = global.rootRequire('redux')
const { rxcurrencyVnd } = global.rootRequire('classes/ulti')

const config = global.rxu.config

class CustomerFavorite_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      editingData: {},
      chooseSelect: {pg_page: 1, pg_sum: 1},
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onClickPaginBack(e) {
    let paging = this.state.chooseSelect
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ chooseSelect: paging }, () => { this.parseArrayPagin(Object.keys(this.props.favorite.favorites)) })
  }

  onClickPaginNext(e) {
    let paging = this.state.chooseSelect
    paging.pg_page += 1
    this.setState({ chooseSelect: paging }, () => { this.parseArrayPagin(Object.keys(this.props.favorite.favorites)) })
  }

  parseArrayPagin(favArr) {
    let arraypagin = []
    if (favArr) {
      let tempChooseSelect = this.state.chooseSelect
      tempChooseSelect.pg_sum = Math.ceil(favArr.length / 10)
      arraypagin = favArr.slice((this.state.chooseSelect.pg_page-1)*10, ((this.state.chooseSelect.pg_page-1)*10)+10)  
    }

    return arraypagin
  }

  render() {
    const { t } = this.props
    let tempCartDetail = []
    if (this.props.favorite) {
      tempCartDetail = this.parseArrayPagin(Object.keys(this.props.favorite.favorites)).map(key => (
        <div key={key} className='clearfix cart-page-product-row'>
          { (this.props.favorite.favorites[key] && this.props.favorite.favorites[key].data) && 
          <div>
            <div className='cart-page-product-name'>
              <div className='cart-page-product-rm' onClick={() => { this.props.favoriteDelete(this.props.favorite.favorites[key].id) }}>{t('Delete')}</div>
              <Link to={`/product/${this.props.favorite.favorites[key].data.slug}`}>
                <img className='cart-page-product-img' alt='ico_default' data-src={config.base_api + '/upload/image/' + (this.props.favorite.favorites[key].data.img_landscape || 'ico_app_default.png')} />
              </Link>
              
              <div className='cart-page-product-option'>
                <div className='cart-page-product-titlename'>{this.props.favorite.favorites[key].data.name}<br/></div>  
                <div className='cart-page-product-price'>{rxcurrencyVnd(this.props.favorite.favorites[key].data.price)}{this.props.favorite.favorites[key].data.price_discount?<span className='cart-page-product-priceold'>{rxcurrencyVnd(this.props.favorite.favorites[key].data.price_discount)}</span>:<span className='cart-page-product-priceold'></span>}</div>
              </div>
              
            </div>
          </div> }
        </div>
      ))
    }
    
    return (
      <div className='container'><div className='rx-main-page'>
        <Helmet>
          <title>{`${global.SEOtitle}`}</title>
          <link rel="canonical" href="/customer/favorite" />
          <meta name="description" content={`${global.SEOdesc}`} />
        </Helmet>
        <div className='lk-customer-page'>
          <div className='row rx-row-nomar'>
            <div className='col-sm col-md-12'>
              <div className='rxcustomer-title'>{t('Favorite product')}</div>
              <div className=''>{tempCartDetail}</div>
              <div className='admin-table-pagination admin-pagin-right'>
                {(this.state.chooseSelect.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                <div className='pagin-curr'>{this.state.chooseSelect.pg_page}</div>
                {(this.state.chooseSelect.pg_sum >= this.state.chooseSelect.pg_page) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
              </div>
            </div>
          </div>  
        </div>
      </div></div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  favorite: state.favorite
})

const mapDispatchToProps = {  
  favoriteDelete,
  rxnavToggle,
  rxnavClose
}

const CustomerFavorite = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerFavorite_)

export default withTranslation('translations')(CustomerFavorite)