import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import {Helmet} from "react-helmet";

const { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose } = global.rootRequire('redux')
const { rxget } = global.rootRequire('classes/request')
const { rxgetdate, rxcurrencyVnd } = global.rootRequire('classes/ulti')

const config = global.rxu.config
// const numberWithCommas = (x) => {
//   return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// }

class CustomerOrderTracking_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      data: [],
      editingData: {},
      paging: { st_col: 'created_at', st_type: -1},
      msg: '',
      cardData: [],
      checktracking: false
    }
  }

  onBlurData(e, name) {
    let params = this.state.paging
    params[name] = e.target.value
    this.setState({ paging: params })    
  }

  componentDidMount() {
    let tempPaging = this.state.paging
    if (this.props.location.id) {
      tempPaging.orderid = this.props.location.id 
      this.setState({ paging: tempPaging }, () => {
        this.fetchData()
      })
    }
  }

  fetchData() {
    rxget(config.api_home_order, this.state.paging, {
      '1': (json) => { 
        if (json.data.order) {
          var cardDetail = {}
          if (json.data && json.data.order && json.data.order.detail) {
            cardDetail = JSON.parse(json.data.order.detail)['carts']
          }          
          this.setState({ data: json.data.order, msg: '', checktracking: true, cardData: cardDetail })  
        } else {
          this.setState({ msg: this.props.t('Order code does not exist') })
        }
      },
      '-2': (json) => {
        this.setState({ msg: this.props.t('Incorrect information !') })
      }
    })
  }

  onClickTrackingOrder(e) {
    if (typeof(this.state.paging['orderid'] !== 'undefined' )) {
      this.fetchData()  
    } else {
      this.setState({ msg: this.props.t('Please enter the order code') })
    }
  }

  getDate(timestamp) {
    let dayarr = [this.props.t('Sunday'), this.props.t('Monday'), this.props.t('Tuesday'), this.props.t('Wednesday'), this.props.t('Thursday'), this.props.t('Friday'), this.props.t('Saturday')]
    timestamp = timestamp * 1000
    let u = new Date(timestamp)  
    let tempstr  = ('0' + u.getUTCDate()).slice(-2) + '-' + ('0' + (u.getUTCMonth() + 1)).slice(-2) + '-' + u.getUTCFullYear()
    let tempstrdate = dayarr[u.getDay()] + ', ' + tempstr
    return tempstrdate
  }

  findTime(index, arrdata) {
    if (arrdata.length > 0) {
      var objdata = arrdata.filter(x => x.status === index)[0]
      var timeupdate = (objdata && objdata.time) ? rxgetdate(objdata.time) : ''
      return timeupdate
    } else {
      return ''
    }
  }

  render() {
    const { t } = this.props
    // Data info card in order
    let detailcards = []
    if (this.state.cardData) {
      detailcards = Object.keys(this.state.cardData).map((key) => (
        <tr key={key}>
          <td>            
            <Link to={`/product/${this.state.cardData[key]['data']['slug']}`}>{this.state.cardData[key]['data']['name']}</Link>
          </td>
          <td>
            <div className='lk-text-center'>{this.state.cardData[key]['amount']}</div>
          </td>
        </tr>
      ))   
    }

    let nameProcess = [<span key={0}>{t('ORDER SUCCESSFUL')}</span>]
    if (this.state.data && this.state.data.status_order) {
      switch(this.state.data.status_order) {
        case 1: nameProcess = [<span key={0}>{t('Order successful')}</span>]; break;
        case 2: nameProcess = [<span key={0}>{t('Are packing')}</span>]; break;
        case 3: nameProcess = [<span key={0}>{t('Are packing')}</span>]; break;
        case 4: nameProcess = [<span key={0}>{t('Wait Delivery')}</span>]; break;
        case 5: nameProcess = [<span key={0}>{t('Wait Delivery')}</span>]; break;
        case 6: nameProcess = [<span key={0}>{t('Success delivery')}</span>]; break;
        default: nameProcess = [<span key={0}>{t('Order successful')}</span>]; break;   
      }
    }

    return (
      <div className='container'><div className='rx-main-page'>
        <Helmet>
          <title>{`${global.SEOtitle}`}</title>
          <link rel="canonical" href="/customer/tracking" />
          <meta name="description" content={`${global.SEOdesc}`} />
        </Helmet>
        <div className='lk-customer-page'>
          <div className='row rx-row-nomar'>
            <div className='col-sm col-md-12'>
              <div className='rxcustomer-title'>{t('Look up order status')}</div>
              
              {this.state.checktracking && <div className='admin-table-wrap'>
                <div className='lk-order-tracking-main-block'>
                <div className='lk-header-progress-container'>
                  <ol className='lk-header-progress-list'>
                    <li className='lk-header-progress-item done'><div className='lk-process-item-block'><b>{t('Order successful')}</b><div className='lk-process-item-time'><span>{this.findTime(1, this.state.data['history_order'])}</span></div></div></li>
                    <li className={(typeof(this.state.data) !== 'undefined' && this.state.data['status_order'] < 2) ? 'lk-header-progress-item todo' : 'lk-header-progress-item done' }><div className='lk-process-item-block'><b>{t('The seller received the order')}</b><div className='lk-process-item-time'><span>{this.findTime(2, this.state.data['history_order'])}</span></div></div></li>
                    <li className={(typeof(this.state.data) !== 'undefined' && this.state.data['status_order'] < 2) ? 'lk-header-progress-item todo' : 'lk-header-progress-item done' }><div className='lk-process-item-block'><b>{t('Are packing')}</b><div className='lk-process-item-time'><span>{this.findTime(2, this.state.data['history_order'])}</span></div></div></li>
                    <li className={(typeof(this.state.data) !== 'undefined' && this.state.data['status_order'] < 4) ? 'lk-header-progress-item todo' : 'lk-header-progress-item done' }><div className='lk-process-item-block'><b>{t('Wait Delivery')}</b><div className='lk-process-item-time'><span>{this.findTime(4, this.state.data['history_order'])}</span></div></div></li>
                    <li className={(typeof(this.state.data) !== 'undefined' && this.state.data['status_order'] < 6) ? 'lk-header-progress-item todo' : 'lk-header-progress-item done' }><div className='lk-process-item-block'><b>{t('Success delivery')}</b><div className='lk-process-item-time'><span>{this.findTime(6, this.state.data['history_order'])}</span></div></div></li>
                  </ol>
                </div>
                <div className='lk-order-tracking-main-title'>{t('Order code')}: {this.state.data['order_code']}</div>
                <div className='lk-order-tracking-main-title'>{t('Order status')}: <span>{nameProcess}</span></div>
                </div>

                <div className='lk-order-tracking-main-block'>
                  <div className='row top'>
                    <div className='col-lg-4 col-md-4 item'>
                      <div className='lk-item-padding-box'>
                        <div className='lk-itemtitle-padding-box'>{t("Receiver's address")}</div>
                        <p>{this.state.data['name']}</p>
                        <p>{this.state.data['phone']}</p>
                        <p>{global.rxu.get(this.state.data, ['address', 0, 'street'], '')}, {global.rxu.get(this.state.data, ['address', 0, 'county'], '')}, {global.rxu.get(this.state.data, ['address', 0, 'city'], '')}</p>
                      </div>
                    </div>

                    <div className='col-lg-4 col-md-4 item'>
                      <div className='lk-item-padding-box'>
                        <div className='lk-itemtitle-padding-box'>{t('Delivery time')}</div>
                        <p>{this.state.data['fee_ship'] === 0 ? 'Giao hàng miễn phí' : 'Vận chuyển có phí'} (dự kiến giao hàng vào {this.getDate((Number(this.state.data['created_at']) || 0) + 3*3600*24)} )</p>
                      </div>
                    </div>

                    {/*<div className='col-lg-3 col-md-3 item'>
                      <div className='lk-item-padding-box'>
                        <div className='lk-itemtitle-padding-box'>{t("Receiver's address")}</div>
                        <p>{this.state.data['name']}</p>
                        <p>{this.state.data['phone']}</p>
                        <p>{global.rxu.get(this.state.data, ['address', 0, 'street'])}</p>
                      </div>
                    </div>*/}

                    <div className='col-lg-4 col-md-4 item'>
                      <div className='lk-item-padding-box'>
                        <div className='lk-itemtitle-padding-box'>{t('Form of payment')}</div>
                        <p>{this.state.data['type_payment'] === 1 ? 'Thanh toán khi nhận hàng' : "Chuyển khoản qua ngân hàng"}</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div className='lk-order-tracking-main-block'>
                  <div className='admin-table-wrap'>
                    <div className='lk-order-tracking-main-title'>{t('Orders included')}</div>
                    <table className='admin-table-product preset'>
                      <thead>
                        <tr>
                          <th>{t('Product name')}</th>
                          <th>{t('Quantity')}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {detailcards}
                      </tbody>
                      <tfoot>
                        <tr>
                          <td><strong>{t('Total')}</strong></td>
                          <td><div className='lk-text-center'><strong>{rxcurrencyVnd(this.state.data.price)} </strong></div></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>}
              
              <div>
                <div className='lk-order-tracking-main-title'>{t('Look up by order code')}</div>
                <div className='lk-order-tracking-main-desc'>{t('Fill in the information below to see the brief status of the Order')}</div>
                <div className='lk-order-tracking-main-form clearfix'>
                  <div className='lk-order-tracking-text'><b>{t('Order code')}</b></div>
                  <div className='clearfix'>
                    <div className='lk-order-tracking-input-form'>
                      <input tabIndex='1' type='text' name='orderid' onChange={(e) => this.onBlurData(e, 'orderid')} className='lk-order-tracking-input fullwidth-input' />
                      <div className='lk-order-tracking-text cart-page-error'>{this.state.msg}</div>
                    </div>
                    <div className='lk-order-tracking-main-button' onClick={(e) => this.onClickTrackingOrder(e)}>{t('Look up')}</div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div></div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose
}

const CustomerOrderTracking = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerOrderTracking_)

export default withTranslation('translations')(CustomerOrderTracking)