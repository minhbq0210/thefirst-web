import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import {Helmet} from "react-helmet";

const { cartClear, cartAdd, cartSub, cartDelete, rxnavToggle, rxnavClose, loginAction, logoutAction } = global.rootRequire('redux')
const { rxpost } = global.rootRequire('classes/request')
const { rxChangeAlias } = global.rootRequire('classes/ulti')
const RxUpload = global.rootRequire('components/Shares/RxUpload').default

const config = global.rxu.config

class CustomerOrder_ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      editingData: {img_landscape: ''},
      msg: '',
    }
    this.state.editingData['address'] = this.state.editingData['address'] || []
    this.state.dataLocal = global.getDataLocal()
    this.state.editingData['city'] = global.rxu.get(this.props.auth.user, ['address', 0, 'city'], '')
    this.state.editingData['county'] = global.rxu.get(this.props.auth.user, ['address', 0, 'county'], '')
    this.state.dataDistrict = []
    if(this.state.dataLocal && this.state.editingData['city']) {
      let checkcity = this.state.dataLocal.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(this.state.editingData['city'])) !== -1)
      if(checkcity && checkcity.length > 0 && checkcity.name !== this.state.editingData['city']) {
        let dataDistricts = checkcity[0].districts
        if(dataDistricts && this.state.editingData['county']) {
          let checkcdistrict = Object.keys(dataDistricts).filter(key => rxChangeAlias(dataDistricts[key]).indexOf(rxChangeAlias(this.state.editingData['county'])) !== -1)
          if(checkcdistrict) {
            this.state.editingData['county'] = dataDistricts[checkcdistrict]
          }
        }
        this.state.dataDistrict = dataDistricts
        this.state.editingData['city'] = checkcity[0].name
      }
    }
    if(this.props.auth && this.props.auth.user && this.props.auth.user.img_landscape) {
      this.state.editingData['img_landscape'] = this.props.auth.user.img_landscape
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    if(name === 'street' || name === 'county' || name === 'city') {
      if(editingData['address'] && editingData['address'].length>0) {
        let address = editingData['address'][0]
        address[name] = e.target.value
      } else {
        editingData[name] = e.target.value
      }
    }else {
      editingData[name] = e.target.value
    }
    this.setState({ editingData: editingData })    
  }

  onClickDataUpdateSubmit(e) {
    let editingData = this.state.editingData
    if(editingData['address'].length === 0) {
      editingData['street'] = editingData['street'] ? editingData['street'] : this.props.auth.user.address[0].street
      editingData['county'] = editingData['county'] ? editingData['county'] : this.props.auth.user.address[0].county
      editingData['city'] = editingData['city'] ? editingData['city'] : this.props.auth.user.address[0].city
      editingData['address'].push({street: editingData['street'], county: editingData['county'], city: editingData['city']})
    }
    if (this.props.auth && this.props.auth.user && this.props.auth.user._id) {
      editingData['_id'] = this.props.auth.user._id
      rxpost(config.api_user_edit_customer, editingData, {
        '1': (json) => {
          this.setState({ msg: '' })
          this.props.loginAction(json.data, {data: json.data })
          alert(this.props.t('Update successful'))
        }
      })
    } else {
      alert(this.props.t('Could not update'))
    }
  }
  onClickLogout(e) {
    this.props.history.push('/')
    this.props.logoutAction()
  }
  handleChangeCity(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    let dataDistricts = this.state.dataDistrict
    if(name === 'city') {
      let checkcity = this.state.dataLocal.filter((item, index) => rxChangeAlias(item['name']).indexOf(rxChangeAlias(e.target.value)) !== -1)
      if(checkcity && checkcity.length > 0 && checkcity.name !== this.state.editingData['city']) {
        dataDistricts = checkcity[0].districts
      }
    }
    this.setState({editingData: editingData, dataDistrict: dataDistricts})
  }
  // Callback upload
  callbackUpload(e) {
    this.onBlurData({target: {value: e.images}}, 'img_landscape')
  }

  render() {
    const { t } = this.props
    return (
      <div className='container'><div className='rx-main-page'>
        <Helmet>
          <title>{`${global.SEOtitle}`}</title>
          <link rel="canonical" href="/customer/account" />
          <meta name="description" content={`${global.SEOdesc}`} />
        </Helmet>
        <div className='lk-customer-page'>
          <div className='row rx-row-nomar'>
            <div className='col-sm col-md-12'>
              <div className='rxcustomer-title'>{t('Account information')}</div>
                {this.props.auth && 
                  <div>
                    <div className='lk-customer-img-profile'>
                      <div className='fullwidth-label'>Hình ảnh</div>
                      <RxUpload callback={(e) => this.callbackUpload(e)} images={global.rxu.get(this.state.editingData, 'img_landscape')} scale='img_profile' />
                    </div>
                    <div className='lk-customer-page-info-body clearfix'>
                      <div>
                        <div className='cart-page-customer-name'>{t('Full name')}</div>
                        <input tabIndex='1' type='text' name='fullname' defaultValue={this.props.auth.user.fullname || ''} onChange={(e) => this.onBlurData(e, 'fullname')} className='fullwidth-input' />
                      </div>
                      <div>
                        <div className='cart-page-customer-phone'>Email</div>
                        <input tabIndex='1' type='text' name='email' defaultValue={this.props.auth.user.email || ''} onChange={(e) => this.onBlurData(e, 'email')} className='fullwidth-input' disabled/>
                      </div>
                      <div>
                        <div className='cart-page-customer-name'>{t('Phone number')}</div>
                        <input tabIndex='1' type='text' name='phone' defaultValue={this.props.auth.user.phone || ''} onChange={(e) => this.onBlurData(e, 'phone')} className='fullwidth-input' />
                      </div>
                      <div>
                        <div className='cart-page-customer-name'>{t('City')}</div>
                        <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['city'])} onChange={(e) => {this.handleChangeCity(e, 'city')}}>
                          {this.state.dataLocal && this.state.dataLocal.length > 0 ? this.state.dataLocal.map((option, index) => (<option key={index} value={option.name}>{option.name}</option>)) : <option>{global.rxu.get(this.props.auth.user, ['address', 0, 'city'], '')}</option>}
                        </select>
                      </div>
                      <div>
                        <div className='cart-page-customer-name'>{t('District')} *</div>
                        <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['county'])} onChange={(e) => {this.handleChangeCity(e, 'county')}}>
                          {this.state.dataDistrict && Object.keys(this.state.dataDistrict).length>0 && Object.keys(this.state.dataDistrict).map(key => (<option key={key} value={this.state.dataDistrict[key]}>{this.state.dataDistrict[key]}</option>))}
                        </select>
                      </div>
                      <div>
                        <div className='cart-page-customer-name'>{t('Address')}</div>
                        <input tabIndex='1' type='text' name='address' defaultValue={global.rxu.get(this.props.auth.user, ['address', 0, 'street'], '')} onChange={(e) => this.onBlurData(e, 'street')} className='fullwidth-input' />
                      </div>

                      <div className='admin-cardblock-btns'>
                        <a tabIndex='1' className='btn-edit' onClick={(e) => this.onClickDataUpdateSubmit(e)} onKeyPress={(e) => this.onClickDataUpdateSubmit(e)}>{t('Update')}</a>
                      </div>
                      {(this.props.auth && this.props.auth.user && Object.keys(this.props.auth.user).length !== 0) && <div className='admin-cardblock-logout rx-mobile-show'><a onClick={(e) => this.onClickLogout()}><i className='icon-power nav-icon'></i>{t('Logout')}</a></div>}
                    </div>
                  </div>
                }
            </div>
          </div>
        </div>
      </div></div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  cart: state.cart,
  rxnav: state.rxnav,
  auth: state.auth
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  cartSub,
  cartDelete,
  rxnavToggle,
  rxnavClose,
  loginAction,
  logoutAction
}

const CustomerOrder = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerOrder_)

export default withTranslation('translations')(CustomerOrder)