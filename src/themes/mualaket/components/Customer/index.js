import React, { Component } from 'react'
import { Switch } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'

const { logoutAction } = global.rootRequire('redux')

class Customer_ extends Component {
  
  constructor (props) {
    super(props)
    this.state = {
      chooseSelect: {pg_page: 1, pg_sum: 1}
    }
  }  

  onClickLogout(e) {
    this.props.history.push('/')
    this.props.logoutAction()
  }

  render() {
    const { t } = this.props
    return (
      <div className='container'><div className='rx-main-page'>
        <div className='lk-customer-main'>
          <div className='lk-customer-topnav'></div>
          <div className='lk-customer-mainnav'>
            <div className='lk-customer-mainnav-inner'>        
              { this.props.auth ? <div className='lk-customer-mainnav-name'>{t('Hi')} {(this.props.auth.user.firstname) ? this.props.auth.user.firstname : 'Bạn'}</div> : <div></div>}
              {(this.props.auth && this.props.auth.user && Object.keys(this.props.auth.user).length !== 0) && <div className='lk-customer-mainnav-item'><Link to='/customer/account'><i className='mlkicon-Profile nav-icon'></i>{t('Account information')}</Link></div>}
              <div className='lk-customer-mainnav-item'><Link to='/customer/tracking'><i className='mlkicon-Search nav-icon'></i>{t('Look up orders')}</Link></div>
              {(this.props.auth && this.props.auth.user && Object.keys(this.props.auth.user).length !== 0) && <div className='lk-customer-mainnav-item'><Link to='/customer/order'><i className='mlkicon-Order nav-icon'></i>{t('Order management')}</Link></div>}
              <div className='lk-customer-mainnav-item'><Link to='/customer/favorite'><i className='mlkicon-Heart nav-icon'></i>{t('Favorite product')}</Link></div>
              {(this.props.auth && this.props.auth.user && Object.keys(this.props.auth.user).length !== 0) && <div className='lk-customer-mainnav-item'><Link to='/customer/purchased'><i className='icon-notebook nav-icon'></i>{t('Products purchased')}</Link></div>}
              {(this.props.auth && this.props.auth.user && Object.keys(this.props.auth.user).length !== 0) && <div className='lk-customer-mainnav-item'><a onClick={(e) => this.onClickLogout()}><i className='icon-power nav-icon'></i>{t('Logout')}</a></div>}
            </div>
          </div>

          <div className='lk-customer-container'>
            <div className='admin-container-inner'>
              <Switch>{renderRoutes(this.props.route.routes)}</Switch>
            </div>
          </div>        
        </div>
      </div></div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({  
  auth: state.auth
})

const mapDispatchToProps = {  
  logoutAction
}

const Customer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Customer_)

export default withTranslation('translations')(Customer)