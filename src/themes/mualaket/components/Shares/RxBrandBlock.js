import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import $ from 'jquery'
import { withTranslation } from 'react-i18next'
// const { rxgetLocal } = global.rootRequire('classes/request')
// const { rxconfig } = global.rootRequire('classes/ulti')
const config = global.rxu.config

class RxBrand extends Component { 
  constructor(props) {
    super(props)

    this.state = global.ultiStaticContext(this.props.staticContext) || { brand: [] }
    this.state.brand = this.state.brand || []
    this.state.checkscroll = {number: 1, type: 'next'}
  }

  componentDidMount() {
    if(this.props && this.props.brand && this.props.brand.length > 0) {
      this.setState({brand: this.props.brand})
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.brand && nextProps.brand.length > 0) {
      this.setState({brand: nextProps.brand})
    }
  }

  animateProduct(key) {
    let checkscroll = this.state.checkscroll
    // let eleDom = document.getElementsByClassName('lk-page-brand-body')[0]
    // let z = eleDom.scrollWidth - eleDom.clientWidth
    
    if (checkscroll['type'] !== key) {
      checkscroll['number'] = 1
      checkscroll['type'] = key
    }

    if (key === 'next') {
      $('.lk-page-brand-body').animate({scrollLeft: checkscroll['number']*400}, 500);
    } else {
      $('.lk-page-brand-body').animate({scrollLeft: -checkscroll['number']*400}, 500);
    }

    if (checkscroll['type'] === key) {
      checkscroll['number'] += 1  
    }

    this.setState({checkscroll: checkscroll})
  }
  
  render() {
    const { t } = this.props
    let slidehome = this.state.brand && this.state.brand.length !== 0 ?
    this.state.brand.map((per,i) => (
      <div key={i} style={{ backgroundImage: 'url(' + config.base_api + '/upload/image/' + (per['img_landscape'] || 'ico_app_default.png') +' )'}} className='footer-brand'><Link to={{ pathname: '/search/'+ per.slug, brandid: per._id }}></Link></div>
    )):<div></div>
    return (
    <div className=''>

    <div className='lk-page-brand-wrap rxhidden'>

      <div className='container lk-page-brand'>
        <div className='lk-page-brand-title'>{t('Brand')} <b>{t('favorite')}</b></div>
        <div className='lk-page-brand-desc'>{t('brand_desc')}</div>
        <div className='lk-page-brand-body'>{slidehome}</div>
        <div onClick={(e) => {e.stopPropagation(); this.animateProduct('prev')}} className='Slider-arrow-f footer-brand-prev carousel__arrow'><span className='mlkicon-ArrowBack mlkicon-Arrow-f'></span></div>
        <div onClick={(e) => {e.stopPropagation(); this.animateProduct('next')}} className='Slider-arrow-f footer-brand-next carousel__arrow'><span className='mlkicon-ArrowNext mlkicon-Arrow-f'></span></div>
        <Link to={`/search/all`}><div className='lk-page-brand-more'>{t('SEE MORE')}</div></Link>
      </div>
    </div>
    </div>
    )}
}

export default withTranslation('translations')(RxBrand)