import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { polyfill }  from 'es6-promise'

// const { rxconfig, rxcurrencyVnd } = global.rootRequire('classes/ulti')
const { cartClear, cartAdd } = global.rootRequire('redux')
// const { rxCountStart } = global.rootRequire('components/Helpers/RxCountStart')

polyfill()
const config = global.rxu.config

class ProductFavoriteBlock_ extends Component {
  constructor(props) {
    super(props)
    this.state = {      
      rxpagin: this.props.rxpagin || '',
    }

    this.state = global.ultiStaticContext(this.props.staticContext) || { data: {} }
    this.state.data = this.state.data || {}
  }

  componentDidMount() {
    if(this.props && this.props.data && this.props.data._id && this.props.data.appdist.length > 0) {
      this.setState({data: this.props.data})
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.data && nextProps.data._id && nextProps.data.appdist.length > 0) {
      this.setState({data: nextProps.data})
    } 
  }

  trimContent(values) {
    let result = ''
    if(values) {
      result = values.replace('<p>', '<p class="advertisement-line1">').replace('<p>', '<p class="advertisement-line2">').replace('<p>', '<p class="advertisement-line3">')
      return result
    }
  }

  onClickHeader(e, newtitle) {
    this.props.callback(newtitle)
  }

  render() {
    let language = this.props.language
    let data = this.state.data
    return (
      <div>
        <div className='hide-scroll advertisement-desktop'>
          <div className='container rx-advertisement-body'>
            <div className='rx-advertisement_left'>
              <img className='rx-advertisement_image lazyload' data-src={config.base_api + '/upload/image/' + ((data.img_landscape && data.img_landscape.length > 0) ? data.img_landscape : 'ico_app_default.png')} alt='' />
              {/*<div className='rx-advertisement_image'>
                
              </div>
              <div className='rx-advertisement_title'>
                <div dangerouslySetInnerHTML={{ __html: this.trimContent(data.desc) }}></div>
                <div className='rx-advertisement_title_readmore'>Xem thêm</div>
              </div>*/}
            </div>
            <div className='rx-advertisement_right'>
              {data.appdistobj && data.appdistobj.slice(0,6).map(obj => (
                <div className='boxfavorite-ele' key={obj._id} style={{width: '100%', height: '100%', maxWidth: '100%', margin: '0'}}>
                  <div className='rx-advertisement'>
                    <Link to={{ pathname: '/search/'+ obj.slug, categoryid: obj._id }}>
                      <div className='boxfavorite-elebox rx-advertisement-box'>
                        <img className="boxfavorite-img lazyload" data-src={config.base_api + '/upload/image/' + ((obj.img_landscape && obj.img_landscape.length > 0) ? obj.img_landscape : 'ico_app_default.png')} alt='' title='' />
                      </div>
                      <span className="boxfavorite-text">{language === 'vi' ? obj.name : (obj.nameEng ? obj.nameEng : obj.name)}</span>
                    </Link>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <Link to={{pathname:`/search`, categoryarr: data.appdist}}><div className='rx-advertisement_readmore'>Xem thêm</div></Link>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  cart: state.cart
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd
}

const ProductFavoriteBlock = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductFavoriteBlock_)

export default ProductFavoriteBlock