import React, { Component } from 'react'
import { polyfill } from 'es6-promise'
import { withTranslation } from 'react-i18next'

const { rxcurrencyVnd } = global.rootRequire('classes/ulti')

polyfill() 
const config = global.rxu.config

class RxRatingbox extends Component {
  constructor(props, context) {
    super(props, context)
    let reviewsrating = {}
    if (typeof(this.props.reviewsrating.score) === 'undefined') {
      reviewsrating = {arrlikes:[],score: 5, content: '', title: this.props.t('Very good')}
    } else {
      reviewsrating = this.props.reviewsrating
    }
    
    this.state = {
      reviewsrating: reviewsrating,    
      infodata: this.props.infodata,
      dataarrratings: [0,0,0,0,0],
      results: {},
      checkchangetitle: false,
      type: this.props.type || ''
    }
  }

  onBlurData(e, name) {
    let editingData = (typeof(this.state.reviewsrating) !== 'undefined') ? this.state.reviewsrating : {}
    editingData[name] = e.target.value
    if (name === 'title') {this.setState({ checkchangetitle: true })}
    this.setState({ reviewsrating: editingData })    
  }

  onClickDataUpdateSubmit(e, perdata) {
    this.props.onChange(perdata)
    this.setState({ reviewsrating: {} })    
  }

  getArrayRating(index) {
    let arrRating = [0,0,0,0,0]
    for (var i=0; i < arrRating.length ; i++) {
      arrRating[i] = (i < index) ? 1 : 0
    }
    let reviewArr = [this.props.t('Very good'), this.props.t('Not good'), this.props.t('Normal'), this.props.t('Good'), this.props.t('Relatively good'), this.props.t('Very good')]
    if (this.state.checkchangetitle === false) {
      let tempReviewRating = this.state.reviewsrating
      tempReviewRating.title = (typeof reviewArr[index] !== 'undefined') ? reviewArr[index] : reviewArr[0]
    }
    
    return arrRating
  }

  onClickChooseRatings(e, index, arrlen) {
    let tempReviewRating = this.state.reviewsrating || {}
    tempReviewRating.score = index+1
    let arrtmp = this.getArrayRating(index)
    this.setState({ dataarrratings: arrtmp, reviewsrating: tempReviewRating })
  }
  
  render() {
    const { t } = this.props
    let language = this.props.i18n.language
    let dataarrratingstmp = []
    let arrayratings = [0,0,0,0,0]
    if (typeof(this.state.reviewsrating) !== 'undefined') {
      let score = this.state.reviewsrating.score
      arrayratings = this.getArrayRating(score)
    } 

    dataarrratingstmp = arrayratings.map((perdata,index) => (
      <div key={index}>
      {perdata === 0 && <div className='rx-star-off-big' onClick={(e) => this.onClickChooseRatings(e, index, this.state.dataarrratings.length)} onMouseOver={(e) => this.onClickChooseRatings(e, index, this.state.dataarrratings.length)}></div> }
      {perdata === 1 && <div className='rx-star-big' onClick={(e) => this.onClickChooseRatings(e, index, this.state.dataarrratings.length)} onMouseOver={(e) => this.onClickChooseRatings(e, index, this.state.dataarrratings.length)}></div>}
      </div>
    ))

    return (
      <div className='lk-box-product-reviews-main'>
        <div className='lk-box-product-reviews-title lk-margin-bottom'>{t('SEND YOUR REVIEW')}</div>
        <div className='lk-box-product-reviews-main-rating'>
          <div className='lk-box-product-reviews-img'>
            <img className='lk-img-rating lazyload' data-src={config.base_api + '/upload/image/' + this.state.infodata.img_landscape} alt='' title='{this.state.infodata.name}' /> 
          </div>
          <div className='lk-box-product-reviews-main-inforating'>
            <div className='cart-page-customer-name'>{language === 'vi' ? this.state.infodata.name : global.rxu.get(this.state.infodata, ['nameEng'], this.state.infodata.name)}</div>
            {this.state.type !== 'news' && <div className='cart-page-customer-price-review'>
              <div className='cart-popup-product-price'>{rxcurrencyVnd(this.state.infodata.price)}</div>
              {this.state.infodata.price_discount ? <div className='cart-popup-product-priceold'>{rxcurrencyVnd(this.state.infodata.price_discount)}</div>:<div className='cart-popup-product-priceold'></div>}
            </div>}
          </div>
        </div>

        <div className='lk-box-product-reviews-main-formrating'>
          <div className='clearfix lk-margin-bottom'>
            <div className='figure-body-rating-star'>
              <div className='cart-page-customer-name fontsize-15'>{this.state.type !== 'news' ? t('Your review of this product') : t('Your review of this news')}:</div>
              {dataarrratingstmp}
            </div>
          </div>
          <div className='lk-margin-bottom'>
            <div className='cart-page-customer-name fontsize-15'>{t('Review title')}</div>
            <input tabIndex='1' type='text' name='name' onChange={(e) => this.onBlurData(e, 'title')} className='fullwidth-input' value={this.state.reviewsrating.title || ''} />
          </div>
          <div className='lk-margin-bottom'>
            <div className='cart-page-customer-name fontsize-15'>{t('Write your comment below')}:</div>
            <textarea tabIndex='2' name='name' className='fullwidth-input' rows='5' placeholder={t('Your comment about this product')} onChange={(e) => this.onBlurData(e, 'content')} value={this.state.reviewsrating.content || ''}/>
          </div>
          <div className='lk-box-product-reviews-botton'>
            <div tabIndex='3' className='lk-reviews-product-submit' onClick={(e) => this.onClickDataUpdateSubmit(e, this.state.reviewsrating)}><span className='iconanna-arrival'></span>{t('Submit review')}</div>
          </div>
        </div>
      </div>
    )    
  }
}

RxRatingbox.defaultProps = { onChange: () => {}, reviewsrating: {}, results: {} }
export default withTranslation('translations')(RxRatingbox)