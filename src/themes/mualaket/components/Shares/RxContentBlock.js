import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { polyfill }  from 'es6-promise'

// const { rxconfig, rxcurrencyVnd } = global.rootRequire('classes/ulti')
// const { rxCountStart } = global.rootRequire('components/Helpers/RxCountStart')

polyfill()
const config = global.rxu.config

class ProductFavoriteBlock_ extends Component {
  constructor(props) {
    super(props)
    this.state = {      
      rxpagin: this.props.rxpagin || '',
    }

    this.state = global.ultiStaticContext(this.props.staticContext) || { video: {}, news: [] }
    this.state.video = this.state.video || {}
    this.state.news = this.state.news || []
  }

  componentDidMount() {
    if(this.props && this.props.video && this.props.video._id) {
      this.setState({video: this.props.video})
    }
    if(this.props && this.props.news && this.props.news.length > 0) {
      this.setState({news: this.props.news})
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.video && nextProps.video._id) {
      this.setState({video: nextProps.video})
    } 
    if(nextProps && nextProps.news && nextProps.news.length > 0) {
      this.setState({news: nextProps.news})
    }
  }

  trimContent(values) {
    let result = ''
    if(values) {
      result = values.replace('<p>', '<p class="advertisement-line1">').replace('<p>', '<p class="advertisement-line2">').replace('<p>', '<p class="advertisement-line3">')
      return result
    }
  }

  onClickHeader(e, newtitle) {
    this.props.callback(newtitle)
  }

  onClickContent(e, i) {
    let item = document.getElementById('news__item' + i)
    let item_details = document.getElementById('news__item-details' + i)
    if(item && item_details) {
      item.style.display = 'none'
      item_details.style.display = 'block'
    }
  }

  render() {
    // let language = this.props.language
    let video = this.state.video
    let srcDoc = "<style>*{padding:0;margin:0;overflow:hidden}html,body{height:100%}img,span{position:absolute;width:100%;top:0;bottom:0;margin:auto}span{height:1.5em;text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}</style><a href='"+ 
                  video.url + "'?autoplay=1><img src='" + config.base_api + '/upload/image/' + (video['img_landscape'] || 'ico_app_default.png') 
                  + "' alt='"+ video.name +"'><span>▶</span></a>"
    return (
      <div>
        <div className='rx-main-content__video'>
          <div>Video</div>
          <div className='rx-main__video'>
            <iframe
              src={video.url}
              srcDoc={srcDoc}
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
              title="The Dark Knight Rises: What Went Wrong? – Wisecrack Edition"
            ></iframe>
          </div>
        </div>
        <div className='rx-main-content__news'>
          <div className='rx-main__news-title'>Tin tức - Blogs</div>
          <div className='rx-main__news-body'>
            {this.state.news && this.state.news.map((per, i) => (
              <div className='news__item' key={per._id} onClick={(e) => this.onClickContent(e, i)}>
                <div id={'news__item' + i} className='news__item-demo'>
                  <div className='news__item-title'>{per.name}</div>
                  <div className='news__item-desc'>{per.desc.length > 100 ? (per.desc.slice(0, 100) + '...') : per.desc}</div>
                </div>
                <div id={'news__item-details' + i} className='news__item-details'>
                  <img className='lazyload' data-src={config.base_api + '/upload/image/' + (per.img_landscape || 'ico_app_default.png')} alt='' />
                  <div className='news__item-details-title'>{per.name}</div>
                  <div className='news__item-details-desc'>{per.desc}</div>
                  <Link to={`/news/${per.slug}`}><div className='news__item-readmore'>Read more >></div></Link>
                </div>
              </div>
            )) }
            <Link to='/news'><div className='rx-main__news-readmore'>See all</div></Link>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
})

const mapDispatchToProps = {  
}

const ProductFavoriteBlock = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductFavoriteBlock_)

export default ProductFavoriteBlock