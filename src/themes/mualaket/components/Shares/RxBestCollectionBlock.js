import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { polyfill }  from 'es6-promise'

// const { rxconfig, rxcurrencyVnd } = global.rootRequire('classes/ulti')
const { cartClear, cartAdd } = global.rootRequire('redux')
// const { rxCountStart } = global.rootRequire('components/Helpers/RxCountStart')

polyfill()
const config = global.rxu.config

class ProductFavoriteBlock_ extends Component {
  constructor(props) {
    super(props)
    this.state = {      
      rxsize:  this.props.rxsize  || 4,
      rxpagin: this.props.rxpagin || '',
      loading: this.props.loading || 'false'
    }

    this.state = global.ultiStaticContext(this.props.staticContext) || { cates: [] }
    this.state.cates = this.state.cates || []
  }

  componentDidMount() {
    if(this.props && this.props.cates && this.props.cates.length > 0) {
      this.setState({cates: this.props.cates})
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.cates && nextProps.cates.length > 0) {
      this.setState({cates: nextProps.cates})
    } 
  }

  onClickHeader(e, newtitle) {
    this.props.callback(newtitle)
  }

  render() {
    let language = this.props.language
    return (
      <div>
        <div className='hide-scroll boxfavorite-desktop'>
          <div className='container'>
            <div className='boxfavorite-row best-collect'>
              {this.state.cates.slice(0,4).map(obj => (
                <div className='boxfavorite-ele best-collect' key={obj._id}>
                  <Link to={{ pathname: '/search/'+ obj.slug, categoryid: obj._id }}>
                    <div className='boxfavorite-elebox best-collect'>
                      <img className="boxfavorite-img lazyload" data-src={config.base_api + '/upload/image/' + ((obj.img_landscape && obj.img_landscape.length > 0) ? obj.img_landscape : 'ico_app_default.png')} alt='' title='' />
                    </div>
                    <span className="boxfavorite-text">{language === 'vi' ? obj.name : (obj.nameEng ? obj.nameEng : obj.name)}</span>
                  </Link>
                </div>
              ))}
            </div>
            <div className='boxfavorite-row best-collect'>
              {this.state.cates.slice(4,8).map(obj => (
                <div className='boxfavorite-ele best-collect' key={obj._id}>
                  <Link to={{ pathname: '/search/'+ obj.slug, categoryid: obj._id }}>
                    <div className='boxfavorite-elebox best-collect'>
                      <img className="boxfavorite-img lazyload" data-src={config.base_api + '/upload/image/' + ((obj.img_landscape && obj.img_landscape.length > 0) ? obj.img_landscape : 'ico_app_default.png')} alt='' title='' />
                    </div>
                    <span className="boxfavorite-text">{language === 'vi' ? obj.name : (obj.nameEng ? obj.nameEng : obj.name)}</span>
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  cart: state.cart
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd
}

const ProductFavoriteBlock = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductFavoriteBlock_)

export default ProductFavoriteBlock