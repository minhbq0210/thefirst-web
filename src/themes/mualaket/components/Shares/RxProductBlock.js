import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { polyfill }  from 'es6-promise'
import { withTranslation } from 'react-i18next'

const { rxcurrencyVnd } = global.rootRequire('classes/ulti')
const { cartClear, cartAdd } = global.rootRequire('redux')
// const { rxCountStart } = global.rootRequire('components/Helpers/RxCountStart')

polyfill()
const config = global.rxu.config

class ProductBlock_ extends Component {
  constructor(props) {
    super(props)
    this.state = {      
      rxsize:  this.props.rxsize  || 4,
      rxpagin: this.props.rxpagin || ''
    }

    this.state = global.ultiStaticContext(this.props.staticContext) || { products: [] }
    this.state.products = this.state.products || []
  }

  onClickHeader(e, newtitle) {
    this.props.callback(newtitle)
  }

  render() {
    let language = this.props.i18n.language
    // const { t } = this.props
    let products = this.props.products.map((product, index) => {
      if (index > (this.state.rxsize - 1)) {
        return (<span key={index}></span>)
      } else {
        return (        
          <div className='blockproduct__item' key={product._id}>
            <div className='rx-product-figure'>
              
              <div className='rx-product-figure-header'>
                <img className='rx-product-figure-header-banner lazyload' alt='' data-src={product.is_hot === 0 ? '/images/static/new@3x.jpg': '/images/static/hot@3x.jpg'}/>
                <Link to={`/product/${product.slug}`}>
                  <img className='rx-product-figure-header-img lazyload' data-src={config.base_api + '/upload/image/' + product.img_landscape} alt='' title='' />
                </Link>
              </div>
              <div className='rx-product-figure-body'>
                <Link to={`/product/${product.slug}`}>
                  <div className='figure-body-title'>{language === 'vi' ? product.name : (product.nameEng ? product.nameEng : product.name)}</div>                  
                  <div className='rx-relative'>
                    <div className='figure-body-price'>
                      {rxcurrencyVnd(product.price)} 
                      { product.price_discount ?<div className='price-discount'>{rxcurrencyVnd(product.price_discount)}</div>:<span className='price-discount'></span>}
                    </div>
                    <div className='figure-body-freeship'></div>
                  </div>
                  <div className='clearfix'></div>
                </Link>
                <div className='rx-product-action'>
                  <div className='rx-action-like'></div>                
                  <div className='rx-action-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(product._id, { amount: 1, data: product }) }}><span className='rx-cart mlkicon-Cart'></span></div>
                  <div className='rx-action-card'></div>
                </div>
              </div>
            </div>
          </div>)
      }      
    })
    return (
      <div className='blockproduct__col'>{products}</div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  cart: state.cart
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd
}

const ProductBlock = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductBlock_)

export default withTranslation('translations')(ProductBlock)