import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { polyfill }  from 'es6-promise'
import { withTranslation } from 'react-i18next'

const { rxcurrencyVnd } = global.rootRequire('classes/ulti')
const { cartClear, cartAdd } = global.rootRequire('redux')
// const { rxCountStart } = global.rootRequire('components/Helpers/RxCountStart')

polyfill()
const config = global.rxu.config

class ProductFeaturedBlock_ extends Component {
  constructor(props) {
    super(props)
    this.state = {      
      rxsize:  this.props.rxsize  || 4,
      rxpagin: this.props.rxpagin || ''
    }

    this.state = global.ultiStaticContext(this.props.staticContext) || { cates: [] }
    this.state.cates = this.state.cates || []
  }

  componentDidMount() {
    if(this.props && this.props.cates && this.props.cates.length > 0) {
      this.setState({cates: this.props.cates})
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.cates && nextProps.cates.length > 0) {
      this.setState({cates: nextProps.cates})
    }
  }

  onClickHeader(e, newtitle) {
    this.props.callback(newtitle)
  }

  render() {
    const { t } = this.props
    let language = this.props.language
    return (
      <div>
        <div className='boxfeatured-desktop'>
          <div className='boxfeatured-row'>
            {this.state.cates.slice(0,5).map(obj => (
              <div className='boxfeatured-ele' key={obj._id}>
                { obj.price_discount ? <div className='rx-product-sale-of'><p><span>-{Math.floor((obj.price_discount - obj.price)/obj.price_discount*100)}%</span></p></div> : <div></div>}
                <div className='rx-product-figure-header'>
                  <img className='rx-product-figure-header-banner lazyload' alt='' data-src={obj.is_hot === 0 ? '/images/static/new@3x.jpg': '/images/static/hot@3x.jpg'}/>
                  <Link to={`/product/${obj.slug}`}>
                    <img className='rx-product-figure-header-img lazyload' data-src={config.base_api + '/upload/image/' + obj.img_landscape} alt='' title='' />
                  </Link>
                </div>
                <div className='rx-product-figure-body'>
                  <Link to={`/product/${obj.slug}`}>
                    <div className='figure-body-title'>{language === 'vi' ? obj.name : (obj.nameEng ? obj.nameEng : obj.name)}</div>                  
                    <div className='rx-relative'>
                      <div className='figure-body-price'>
                        {rxcurrencyVnd(obj.price)} 
                        { obj.price_discount ?<div className='price-discount'>{rxcurrencyVnd(obj.price_discount)}</div>:<span className='price-discount'></span>}
                      </div>
                      <div className='figure-body-freeship'></div>
                    </div>
                    <div className='clearfix'></div>
                  </Link>
                  <div className='rx-product-action'>              
                    <div className='rx-action-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(obj._id, { amount: 1, data: obj }) }}><span className='rx-cart mlkicon-Cart'></span></div>
                    <div className='rx-action-card'></div>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className='boxfeatured-row'>
            {this.state.cates.slice(5,10).map(obj => (
              <div className='boxfeatured-ele' key={obj._id}>
                { obj.price_discount ? <div className='rx-product-sale-of'><p><span>-{Math.floor((obj.price_discount - obj.price)/obj.price_discount*100)}</span>%</p></div> : <div></div>}
                <div className='rx-product-figure-header'>
                  <img className='rx-product-figure-header-banner lazyload' alt='' data-src={obj.is_hot === 0 ? '/images/static/new@3x.jpg': '/images/static/hot@3x.jpg'}/>
                  <Link to={`/product/${obj.slug}`}>
                    <img className='rx-product-figure-header-img lazyload' data-src={config.base_api + '/upload/image/' + obj.img_landscape} alt='' title='' />
                  </Link>
                </div>
                <div className='rx-product-figure-body'>
                  <Link to={`/product/${obj.slug}`}>
                    <div className='figure-body-title'>{language === 'vi' ? obj.name : (obj.nameEng ? obj.nameEng : obj.name)}</div>                  
                    <div className='rx-relative'>
                      <div className='figure-body-price'>{rxcurrencyVnd(obj.price)} { obj.price_discount ?<div className='price-discount'>{rxcurrencyVnd(obj.price_discount)}</div>:<span></span>}</div><div className='figure-body-freeship'></div>
                    </div>
                    <div className='clearfix'></div>
                  </Link>
                  <div className='rx-product-action'>              
                    <div className='rx-action-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(obj._id, { amount: 1, data: obj }) }}><span className='rx-cart mlkicon-Cart'></span></div>
                    <div className='rx-action-card'></div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className='boxfeatured-mobile'>
          <div className='boxfeatured-row'>
            {this.state.cates.slice(0,4).map(obj => (
              <div className='boxfeatured-ele' key={obj._id}>
                { obj.price_discount ? <div className='rx-product-sale-of'><p><span>-{Math.floor((obj.price_discount - obj.price)/obj.price_discount*100)}</span>%</p></div> : <div></div>}
                <div className='rx-product-figure-header'>
                  <img className='rx-product-figure-header-banner lazyload' alt='' data-src='/images/static/new@3x.jpg'/>
                  <Link to={`/product/${obj.slug}`}>
                    <img className='rx-product-figure-header-img lazyload' data-src={config.base_api + '/upload/image/' + obj.img_landscape} alt='' title='' />
                  </Link>
                </div>
                <div className='rx-product-figure-body'>
                  <Link to={`/product/${obj.slug}`}>
                    <div className='figure-body-title'>{obj.name}</div>                  
                    <div className='rx-relative'>
                      <div className='figure-body-price'>{rxcurrencyVnd(obj.price)} { obj.price_discount ?<div className='price-discount'>{rxcurrencyVnd(obj.price_discount)}</div>:<span></span>}</div><div className='figure-body-freeship'></div>
                    </div>
                    <div className='clearfix'></div>
                  </Link>
                  <div className='rx-product-action'>              
                    <div className='rx-action-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(obj._id, { amount: 1, data: obj }) }}><span className='rx-cart mlkicon-Cart'></span></div>
                    <div className='rx-action-card'></div>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className='boxfeatured-row'>
            {this.state.cates.slice(4,8).map(obj => (
              <div className='boxfeatured-ele' key={obj._id}>
                { obj.price_discount ? <div className='rx-product-sale-of'><p><span>-{Math.floor((obj.price_discount - obj.price)/obj.price_discount*100)}</span>%</p></div> : <div></div>}
                <div className='rx-product-figure-header'>
                  <img className='rx-product-figure-header-banner lazyload' alt='' data-src='/images/static/new@3x.jpg'/>
                  <Link to={`/product/${obj.slug}`}>
                    <img className='rx-product-figure-header-img lazyload' data-src={config.base_api + '/upload/image/' + obj.img_landscape} alt='' title='' />
                  </Link>
                </div>
                <div className='rx-product-figure-body'>
                  <Link to={`/product/${obj.slug}`}>
                    <div className='figure-body-title'>{obj.name}</div>                  
                    <div className='rx-relative'>
                      <div className='figure-body-price'>{rxcurrencyVnd(obj.price)} { obj.price_discount ?<div className='price-discount'>{rxcurrencyVnd(obj.price_discount)}</div>:<span></span>}</div><div className='figure-body-freeship'></div>
                    </div>
                    <div className='clearfix'></div>
                  </Link>
                  <div className='rx-product-action'>              
                    <div className='rx-action-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(obj._id, { amount: 1, data: obj }) }}><span className='rx-cart mlkicon-Cart'></span></div>
                    <div className='rx-action-card'></div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div>
          <div>{ this.props.rxtitle && <h2>{this.props.rxtitle}</h2> }</div>
          <Link to={`/search/all`}><div className='blockproduct__more'>{t('SEE MORE')}</div></Link>
          <div className='clearfix'></div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  cart: state.cart
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd
}

const ProductFeaturedBlock = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductFeaturedBlock_)

export default withTranslation('translations')(ProductFeaturedBlock)