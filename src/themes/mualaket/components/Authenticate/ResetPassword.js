import React, { Component } from 'react'
import { withTranslation } from 'react-i18next'

const { rxpost } = global.rootRequire('classes/request')
// const { rxconfig } = global.rootRequire('classes/ulti')

const config = global.rxu.config
 
class ResetPassword extends Component {
	
	constructor(props) {
    super(props)
    this.state = { 
      editingData: {},
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    let msg = this.state.msg
    if(name === 'confirm_password') {
      if(!editingData['password'] || e.target.value !== editingData['password']) {
        msg = this.props.t('Passwords must match')
      } else {
        msg = ''
      }
    }
    editingData[name] = e.target.value
    this.setState({ editingData: editingData, msg: msg })    
  }

  onClickSubmit(e) {
    let editingData = this.state.editingData
    let key = this.props.location.pathname ? this.props.location.pathname : ''
    if(key && key.length > 0) {
      editingData['id'] = key.substring(key.lastIndexOf('/')+1)
    }
    if(!editingData['password']) {
      this.setState({msg: this.props.t('Enter password!')})
    } else if(!editingData['confirm_password']) {
      this.setState({msg: this.props.t('Passwords must match')})
    } else if(editingData['password'] && editingData['confirm_password'] && editingData['id']) {
      this.setState({ editingData: editingData, msg: '' }, () => {
        rxpost(config.api_user_reset_password, this.state.editingData, {
          '1': (json) => {
            this.setState({ msg: 'Reset Password Success' })
            alert('Reset Password Success')
            this.props.history.push("/login")
          }
        })
      })
    }
  }

  render() {
    const { t } = this.props
    return (
      <div className='lk-login-customer-page'>
        <div className='row lk-row-nomar'>
          <div className='rxauthen-left-container lk-contact'></div>
          <div className='lk-page-customer-info lk-page-info-register'>
            <div className='lk-login-section-title-wrap clearfix'>
              <div className='lk-assistance-section-title'>{t('Reset Password')}</div>
            </div>

            <div className='lk-register-customer-inner' onKeyPress={(e) => {if (e.key === 'Enter') { this.onClickSubmit(e) } }}>
						  <div className='clearfix'>
                { this.state.msg && <div className='clearfix'><div className='lk-input-error' >{this.state.msg}</div></div> }
						    <div className='lk-input-group-row'>
                  <div>{t('Password')} *</div>
                  <input tabIndex='1' type='password' name='password' onChange={(e) => this.onBlurData(e, 'password')} className='fullwidth-input' />
                </div>
                <div className='lk-input-group-row'>
						      <div>{t('Confirm password')} *</div>
						      <input tabIndex='2' type='password' name='confirm_password' onChange={(e) => this.onBlurData(e, 'confirm_password')} className='fullwidth-input' />
						    </div>
              </div>
						  <div className='lk-login-submit-section'>
						    <div tabIndex='3' className='lk-page-login-customer-submit' onClick={(e) => this.onClickSubmit(e)} onKeyPress={(e) => this.onClickSubmit(e)}>{t('Reset Password')}</div> 
						  </div>
						  <div className='lk-login-submit-section'></div>
						</div> 
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation('translations')(ResetPassword)