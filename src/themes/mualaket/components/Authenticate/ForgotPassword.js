import React, { Component } from 'react'
import { withTranslation } from 'react-i18next'

const { rxget } = global.rootRequire('classes/request')
// const { rxconfig } = global.rootRequire('classes/ulti')

const config = global.rxu.config
 
class ForgotPass extends Component {
	
	constructor(props) {
    super(props)
    this.state = { 
      editingData: {},
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }
  onClickSubmit(){
    let editingData = this.state.editingData
    this.setState({ editingData: editingData }, () => {
      rxget(config.api_user_sentcode, this.state.editingData, {
        '1': (json) => {
          this.setState({ msg: this.props.t('Email has been sent, please check your mailbox for updated information.') })
        },
        '-2': (json) => {
          var strmsg = ''
          if (json.msg === 'Wrong input') {
            strmsg = this.props.t('Information is incorrect')
          } else if(json.msg === 'Email invalid format') {
            strmsg = this.props.t('Email invalid format')
          } else if(json.msg === 'Not Found Account') {
            strmsg = this.props.t('Email is not registered')
          }
          this.setState({ msg: strmsg })
        }
      })
    })
  }

  render() {
    const { t } = this.props
    return (
      <div className='lk-login-customer-page'>
        <div className='row lk-row-nomar'>
          <div className='rxauthen-left-container lk-contact'></div>
          <div className='lk-page-customer-info lk-page-info-login'>
            <div className='lk-login-section-title-wrap clearfix'>
              <div className='lk-assistance-section-title'>{t('Password assistance')}</div>          
            </div>
            <div className='lk-login-customer-inner'>
						  <div onKeyPress={(e) => {if (e.key === 'Enter') { this.onClickSubmit(e) } }}>
                { this.state.msg && <div className='clearfix'><div className='lk-input-error' >{this.state.msg}</div></div> }
                <div className='lk-input-group-row'>
						      <div className='lk-cart-page-customer-name'>{t('Enter the email address associated with your account')}</div>
						      <input tabIndex='1' type='text' autoComplete='email' name='email' onChange={(e) => this.onBlurData(e, 'email')}  className='fullwidth-input' />
						    </div>
  						  <div className='lk-login-submit-section'>
  						    <div tabIndex='3' className='lk-page-login-customer-submit' onClick={(e) => { this.onClickSubmit(e)}} >{t('Continue')}</div>                        
  						  </div>						  
  						</div> 
            </div>
          </div>
        </div>
      </div>
    )
  }
}


export default withTranslation('translations')(ForgotPass)