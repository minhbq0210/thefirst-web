import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { withTranslation } from 'react-i18next'

const { rxpost } = global.rootRequire('classes/request')
// const { rxconfig } = global.rootRequire('classes/ulti')

const config = global.rxu.config
 
class AuthRegister extends Component {
	
	constructor(props) {
    super(props)
    this.state = { 
      editingData: {},
      msg: '',
      msgErr: '',
      msgScc: ''
    }
    this.state.editingData.address = this.state.editingData.address || []
    this.state.dataLocal = global.getDataLocal()
    this.state.editingData['city'] = this.state.editingData['city'] || 'Thành phố Hồ Chí Minh'
    this.state.editingData['county'] = this.state.editingData['county'] || 'Quận 1'
    this.state.dataDistrict = []
    if(this.state.dataLocal && this.state.editingData['city']) {
      let checkcity = this.state.dataLocal.find(obj => obj.name === this.state.editingData['city'] )
      if(checkcity) {
        let districts = []
        Object.keys(checkcity.districts).forEach(function(key) {
          districts.push(checkcity.districts[key])
        })
        this.state.dataDistrict = districts
      }
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onClickRegisterSubmit(e) {
    let editingData = this.state.editingData
    console.log(editingData)
    if ( editingData['street'] && editingData['county'] && editingData['city']) {
      editingData['address'].push({street: editingData['street'], county: editingData['county'], city: editingData['city']})
      delete editingData['street']
      delete editingData['county']
      delete editingData['city']
    }
    this.setState({ editingData: editingData }, () => {
      rxpost(config.api_user, this.state.editingData, {
        '1': (json) => {
          this.setState({ msg: 'Sign Up Success' })
          this.props.history.push("/login")
        },
        '-2': (json) => {
          var strmsg = ''
          if (json.msg === 'Dupplicate data') {
            strmsg = this.props.t('Email already exists')
          } else if (json.msg === 'Email invalid format') {
            strmsg = this.props.t('Email invalidate')
          }else if (json.msg === 'Dupplicate phone') {
            strmsg = this.props.t('Phone already exists')
          } else {
            strmsg = this.props.t('Fields must not be empty')
          }
          this.setState({ msg: strmsg })
        }
      })
    })
  }
  handleChangeCity(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    if(name === 'city') {
      let checkcity = this.state.dataLocal.find(obj => obj.name === e.target.value )
      if (checkcity) {
        let districts = []
        Object.keys(checkcity.districts).forEach(function(key) {
          districts.push(checkcity.districts[key])
        })
        this.setState({dataDistrict: districts})
      }
    }
    this.setState({editingData: editingData})
  }

  render() {
    const { t } = this.props
    return (
      <div className='lk-login-customer-page'>
        <div className='row lk-row-nomar'>
          <div className='rxauthen-left-container lk-contact'></div>
          <div className='lk-page-customer-info lk-page-info-register'>
            <div className='lk-login-section-title-wrap clearfix'>
              <Link className='lk-login-section-title' to='login'>{t('Login')}</Link>
              <div className='lk-login-section-title rx-active'>{t('Register')}</div>
            </div>

            <div className='lk-register-customer-inner' onKeyPress={(e) => {if (e.key === 'Enter') { this.onClickRegisterSubmit(e) } }}>
						  <div className='clearfix'>
                { this.state.msg && <div className='clearfix'><div className='lk-input-error' >{this.state.msg}</div></div> }
						    <div className='lk-input-group-row'>
                  <div>Email *</div>
                  <input tabIndex='1' type='text' name='email' onChange={(e) => this.onBlurData(e, 'email')} className='fullwidth-input' />
                </div>
                <div className='lk-input-group-row'>
                  <div>{t('Password')} *</div>
                  <input tabIndex='2' type='password' name='password' onChange={(e) => this.onBlurData(e, 'password')} className='fullwidth-input' />
                </div>
                <div className='lk-input-group-row'>
						      <div>{t('First name')} *</div>
						      <input tabIndex='3' type='text' name='lastname' onChange={(e) => this.onBlurData(e, 'lastname')} className='fullwidth-input' />
						    </div>
                <div className='lk-input-group-row'>
                  <div>{t('Last name')} *</div>
                  <input tabIndex='4' type='text' name='firstname' onChange={(e) => this.onBlurData(e, 'firstname')} className='fullwidth-input' />
                </div>
						    <div className='lk-input-group-row'>
						      <div>{t('Phone number')} *</div>
						      <input tabIndex='5' type='text' name='phone' onChange={(e) => this.onBlurData(e, 'phone')} className='fullwidth-input' />
						    </div>
                <div className='lk-input-group-row'>
                  <div>{t('City')} *</div>
                  <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['city'])} onChange={(e) => {this.handleChangeCity(e, 'city')}}>
                    {this.state.dataLocal && this.state.dataLocal.length > 0 && this.state.dataLocal.map((option, index) => (<option key={index} value={option.name}>{option.name}</option>))}
                  </select>
                </div>
                <div className='lk-input-group-row'>
                  <div>{t('District')} *</div>
                  <select className='fullwidth-select' value={global.rxu.get(this.state.editingData,['county'])} onChange={(e) => {this.handleChangeCity(e, 'county')}}>
                    {this.state.dataDistrict && this.state.dataDistrict.length>0 && this.state.dataDistrict.map((option, key) => (<option key={key} value={option}>{option}</option>))}
                  </select>
                </div>
                <div className='lk-input-group-row'>
                  <div>{t('Address')} *</div>
                  <input tabIndex='6' type='text' name='address' onChange={(e) => this.onBlurData(e, 'street')} className='fullwidth-input' />
                </div>
						  </div><br/>
						  
						  <div className='lk-login-submit-section'>
						    <div tabIndex='10' className='lk-page-login-customer-submit' onClick={(e) => this.onClickRegisterSubmit(e)} onKeyPress={(e) => this.onClickRegisterSubmit(e)}>{t('Register')}</div> 
						  </div>
						  <div className='lk-login-submit-section'></div>
						</div> 
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation('translations')(AuthRegister)