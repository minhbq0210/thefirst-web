import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { polyfill }  from 'es6-promise'
import { connect } from 'react-redux'
import InputRange from 'react-input-range'
import $ from 'jquery'

const { rxget } = global.rootRequire('classes/request')
// const { rxconfig } = global.rootRequire('classes/ulti')
const { rxCurrency } = global.rootRequire('components/Helpers/RxCurrency')
const { rxCountStart } = global.rootRequire('components/Helpers/RxCountStart')
const { cartClear, cartAdd, rxsearchClear } = global.rootRequire('redux')

polyfill()

const config = global.rxu.config


class Categories_ extends Component {
  constructor(props) {    
    super(props)
    let slug = this.props.match.params.slug
    this.maxPrice = 10000
    this.minPrice = 100

    this.timeout = null
    this.state = global.ultiStaticContext(this.props.staticContext) || {}
    this.state = {slug: slug}
    this.state.categories = this.state.categories || []
    this.state.products = this.state.products || []    
    this.state.paging = this.state.paging || {}
    this.state.option_all = this.state.option_all || []

    this.state.showFilter = false
    this.state.showSorter = false
    this.state.toggleFilCat = false
    this.state.toggleFilSize = false
    this.state.toggleFilColor = false
    this.state.toggleFilPrice = false
    this.state.paging.st_full = this.state.paging.st_full || 'created_at:desc'
    this.state.paging.st_col  = this.state.paging.st_col  || 'created_at'
    this.state.paging.st_type = this.state.paging.st_type || -1
    this.state.paging.pg_page = this.state.paging.st_page || 1
    this.state.paging.pg_size = this.state.paging.st_size || 12
    this.state.paging.price = this.state.paging.price || {min: this.minPrice, max: this.maxPrice}
  }

  componentDidMount() {

    let slug = this.state.slug
    this.initData()
    this.fetchData(slug) 
    this.fetchOption()   
  }

  componentWillReceiveProps(nextProps, nextContext) {    
    if (nextProps.rxsearch && nextProps.rxsearch.params) {     
      let paging = this.state.paging
      if (typeof(nextProps.rxsearch.params['search_name']) !== 'undefined') {
        paging['search_name'] = nextProps.rxsearch.params['search_name']
      } else {
        delete paging['search_name']
      }

      if (typeof(nextProps.rxsearch.params['searchid_appdist']) !== 'undefined') {
        paging['searchid_appdist'] = nextProps.rxsearch.params['searchid_appdist']
      } else {
        delete paging['searchid_appdist']
      }

      this.setState({ paging: paging }, () => {this.fetchData()}) 
    }
  }

  onClickSorting(e, item) {    
    let paging = this.state.paging
    let pagingArr = item.i.split(':')

    paging['st_full'] = item.i
    paging['st_col']  = pagingArr[0]
    paging['st_type'] = (pagingArr[1] === 'desc') ? -1 : 1
    this.setState({ paging: paging }, () => {      
      this.fetchData()
    }) 
  }

  handleChange(e, item) {
    let i = e.target.value
    let paging = this.state.paging
    let pagingArr = i.split(':')

    paging['st_full'] = i
    paging['st_col']  = pagingArr[0]
    paging['st_type'] = (pagingArr[1] === 'desc') ? -1 : 1
    this.setState({ paging: paging }, () => {      
      this.fetchData()
    }) 
  }

  onClickChangepage(e, type, number) {
    let paging = this.state.paging

    // Prevent 0
    type = (paging.pg_page <= 1 && type === 'back') ? 'no' : type
    type = (this.state.products.length === 0 && type === 'next') ? 'no' : type
    switch (type) {      
      case 'back':
        paging.pg_page = paging.pg_page - 1
        break;

      case 'next':
        paging.pg_page = paging.pg_page + 1
        break;

      default:
        break;
    }

    this.setState({ paging: paging }, () => {
      this.fetchData()
    })
  }

  onClickChangeCat(e, item) {
    let paging = this.state.paging    
    if (item) {
      paging.searchid_appdist = item._id
      paging.searchname_appdist = item.name
    } else {
      delete(this.props.location.categoryid)
      delete(paging.searchid_appdist)
      delete(paging.searchname_appdist)
    }
    this.setState({ paging: paging }, () => {
      this.fetchData()
    }) 
  }

  onClickchangeFilter(e, name, item) {
    let paging = this.state.paging
    if (!item || (typeof(paging[name]) !== 'undefined' && item === paging[name])) {
      delete(paging[name])
    } else {
      paging[name] = item 
    }
    if (name === 'searchname_appdist') {
      delete(paging.searchid_appdist)
    }
    this.setState({ paging: paging }, () => {
      this.fetchData()
    })
  }

  onClickchangePriceFilter(e, name, value) {
    let paging = this.state.paging
    if (!paging.price) paging.price = {min: 100, max: 10000}

    paging.price[name] = value
    paging['searchprice_' + name] = value
    this.setState({ paging: paging }, () => {
      clearTimeout(this.timeout)
      this.timeout = setTimeout(() => {
        this.fetchData()
      }, 800)
    })
  }

  onChangeFilterPrice(value) {    
    let paging = this.state.paging    
    paging.price = value
    paging.price.min = value.min < this.minPrice ? this.minPrice : value.min
    paging.price.max = value.max > this.maxPrice ? this.maxPrice : value.max

    // Set up search param for price
    paging.price.min === this.minPrice? delete(paging.searchprice_min): paging.searchprice_min = paging.price.min
    paging.price.max === this.maxPrice? delete(paging.searchprice_max): paging.searchprice_max = paging.price.max

    this.setState({ paging: paging }, () => {
      clearTimeout(this.timeout)      
      this.timeout = setTimeout(() => {
        this.fetchData()
      }, 800)
    })
  }

  onClickClearsearch() {
    this.props.rxsearchClear()
    let paging = this.state.paging
    delete paging.search_name
    this.setState({paging: paging}, () => {
      this.fetchData()
    })
  }
  
  toggleFiltMobile() {
    this.setState({showFilter: !this.state.showFilter})
  }

  toggleFiltDest(key) {
    let k = this.state[key]
    this.setState({[key]: !k})
  }

  toggleSortMobile() {
    this.setState({showSorter: !this.state.showSorter})
  }

  initData() {
    rxget(config.api_category, this.state.pagingcat, {
      '1': (json) => { this.setState({ categories: json.data }) }
    })
  }

  fetchData(slug) {
        
    let paging = this.state.paging
    paging.slug = slug
    let categoryid = this.props.location.categoryid
    if (typeof(categoryid) !== 'undefined' && typeof(paging.searchid_appdist) === 'undefined') {
      paging.searchid_appdist = categoryid
    }

    this.setState({ paging: paging }, () => {
      rxget(config.api_home_product, this.state.paging, {
        '1': (json) => { this.setState({ products: json.data.products }); $('html, body').animate({ scrollTop: 0 }, 200, () => {}) }        
      })
    })    
  }

  fetchOption() {
    rxget(config.api_home_option, {}, {
      '1': (json) => { this.setState({ option_all: json.data })}        
    })
  }

  render() {
    let tempOptionSize = [], tempOptionColor = [], tempOptionName = []
    this.state.option_all.map((option, i) => {
      if (option.type === 'name') {
        option.value.map((temp)=> {
          if (temp.key) { 
            return tempOptionName.push(temp.value) 
          } else {
            return tempOptionName
          }
        })
      } else if (option.type === 'size') {
        option.value.map((temp)=> {
          if (temp !== null && temp.key) { 
            return tempOptionSize.push(temp.value)
          } else {
            return tempOptionSize
          }
        })
      } else if (option.type === 'color') {
        option.value.map((temp)=> {
          if (temp !== null && temp.key) {
            let tempColor = {}
            tempColor['i'] = temp.value
            tempColor['a'] = temp.value
            if (tempOptionColor.length === 0) {
              tempOptionColor.push(tempColor)
            } else {
              var found = tempOptionColor.some(function (el) {
                return el.i === temp.value
              })
              if (!found) { 
                return tempOptionColor.push(tempColor)
              } else {
                return tempOptionColor
              }
            }
            return tempOptionColor
          } else {
            return tempOptionColor
          }
        })
      } else {
        return []
      }
      return []
    })
    
    let categories = this.state.categories.map(category => (
      <div className={'search-categories-item ' + ((this.state.paging && this.state.paging.searchid_appdist === category._id) ? 'rx-active': '')} key={category._id}>
        <a onClick={(e) => this.onClickChangeCat(e, category)}>{category.name}</a>
      </div>
    ))
    
    if (this.state.products)  {}
    let products = this.state.products.map(product => (
      <div className='rx-product-item col-sm-lg col-sm-3 show' key={product._id}>
        <div className='rx-product-figure'>
          <div className='rx-product-figure-header'>
            <img className='rx-product-figure-header-banner lazyload' alt='' data-src='/images/static/new@3x.jpg'/>
            <Link to={`/product/${product.slug}`}>
              <img className='lazyload' data-src={config.base_api + '/upload/image/' + product.img_landscape} alt='' title='' />
            </Link>
          </div>
          <div className='rx-product-figure-body'>
            <div className='figure-body-title'>{product.name}</div>
            <div className='rx-relative'><div className='figure-body-price'>{rxCurrency(product.price)} </div><div className='figure-body-freeship'></div></div>
            <div className='figure-body-rating-star'>
              {rxCountStart(product.ratingsSumary)}
              <div className='rx-star-text'>({(typeof(product.ratings) !== 'undefined') && product.ratings.length}{(typeof(product.ratings) === 'undefined') && 0})</div>
            </div>
            <div className='clearfix'></div>            
          </div>
          <div className='rx-product-action'>
            <div className='rx-action-like'></div>
            <div className='rx-action-card'></div>
            <div className='rx-action-addcart' onClick={(e) => { e.stopPropagation(); this.props.cartAdd(product._id, { amount: 1, data: product }) }}>Mua ngay</div>
          </div>
        </div>
      </div>
    ))
    return (
      <div className='container--grey'><div className='container'><div className='rx-main-page-search'>
        {/*<div className='rx-mobile-show rx-product-filtsort-btnwrap'>
          <div className='rx-product-filt-btn' onClick={(e) => { this.toggleFiltMobile() }}>BỘ LỌC</div>
          <div className='rx-product-sort-btn' onClick={(e) => { this.toggleSortMobile() }}>SẮP XẾP</div>
        </div>*/}
        <div className={'rx-category rx-category-inlist '  + (this.state.showFilter ? 'rx-mobile-show' : '')}>
          <div className={'rx-categories rx-categories-inlist rx-categories-filtered '}>
            <div className='rx-categories-header-inlist' onClick={(e) => { this.toggleFiltMobile() }}>Bộ lọc sản phẩm <span className='rx-mobile-show icon-pencil icons'></span></div>            
            <div className='rx-categories-filtered-content'>
              <div className='rx-search-filtered-number'>Có <b>220</b> sản phẩm được tìm thấy</div>
              <div className='rx-search-name-detail'>{ (this.state.paging && this.state.paging.search_name) ? <span onClick={() => { this.onClickClearsearch() }}>Bắt đầu với : {this.state.paging.search_name}... <i>x</i></span> : <span></span>}</div>
              <div className='rx-filtered-group clearfix'>
                {this.state.paging.searchname_appdist && <span onClick={(e) => this.onClickchangeFilter(e, 'searchname_appdist', this.state.paging.searchname_appdist)} className='rx-active rx-filtered-color rx-filtered-label'>{this.state.paging.searchname_appdist} <span className='iconanna-delete'></span></span>}
                {this.state.paging.searchoption_color && <span onClick={(e) => this.onClickchangeFilter(e, 'searchoption_color', this.state.paging.searchoption_color)} className='rx-active rx-filtered-color rx-filtered-label'>{this.state.paging.searchoption_color} <span className='iconanna-delete'></span></span>}
                {this.state.paging.searchoption_size && <span onClick={(e) => this.onClickchangeFilter(e, 'searchoption_size', this.state.paging.searchoption_size)} className='rx-active rx-filtered-size rx-filtered-label'>{this.state.paging.searchoption_size} <span className='iconanna-delete'></span></span>}                
              </div>
              { (this.state.paging.price && (this.state.paging.price.min || this.state.paging.price.max))? <div className='rx-filtered-group clearfix'>
                {(this.state.paging.price.min && this.state.paging.price.min !== 100 ) ? <span onClick={(e) => this.onClickchangePriceFilter(e, 'min', 100)} className='rx-active rx-filtered-color rx-filtered-label'>&gt; {this.state.paging.price.min} k<span className='iconanna-delete'></span></span>: <div></div>}
                {(this.state.paging.price.max && this.state.paging.price.max !== 10000) ? <span onClick={(e) => this.onClickchangePriceFilter(e, 'max', 10000)} className='rx-active rx-filtered-size rx-filtered-label'>&lt; {this.state.paging.price.max} k<span className='iconanna-delete'></span></span>: <div></div>}                
              </div>: <div></div> }
            </div>
          </div>

          <div className='rx-categories rx-categories-inlist'>
            <div className='rx-categories-header-inlist'>Danh mục sản phẩm <span className={!this.state.toggleFilCat ? 'mlkicon-Plus rxmlkiconPlus' : 'mlkicon-Minus rxmlkiconPlus'} onClick={(e) => { this.toggleFiltDest('toggleFilCat') }}></span></div>
            {this.state.toggleFilCat ? <div className='rx-cat-bg'>
              <div className={'search-categories-item ' + ((!this.state.paging || !this.state.paging.searchid_appdist) ? 'rx-active': '')}><a onClick={(e) => this.onClickChangeCat(e)}>Tất cả danh mục</a></div>
              {categories}
            </div> : <div></div> }
          </div>

          <div className='rx-categories rx-categories-inlist'>
            {/*<div className='rx-categories-header-inlist'>Sản phẩm lọc theo</div>*/}
            <div className='rx-filter-container'>
            
             {/*<div className='rx-filter-choosen'>
                {/*<span>Size L <b>x</b></span>
                <span>500.000 <b>x</b></span> 
                <span>5.000.000 <b>x</b></span> 
                <span>Rothco <b>x</b></span> 
                <span>Alpha industries <b>x</b></span>
                <div className='rx-filter-reset'>Xoá tất cả</div>
              </div>*/}

              <div className='rx-filter-title'>Giá <i>(VNĐ)</i><span className={!this.state.toggleFilPrice ? 'mlkicon-Plus rxmlkiconPlus' : 'mlkicon-Minus rxmlkiconPlus'} onClick={(e) => { this.toggleFiltDest('toggleFilPrice') }}></span></div>              
              {this.state.toggleFilPrice ? <div className='rx-filter-body clearfix rx-filter-price'>
                <InputRange maxValue={this.maxPrice} minValue={this.minPrice} value={this.state.paging.price} onChange={value => this.onChangeFilterPrice(value)} />
              </div> : <div></div> }

              {/*<div className='rx-filter-title'>Nhãn hiệu</div>
              <div className='rx-filter-body clearfix rx-filter-brand'></div>*/}

              <div className='rx-filter-title'>Màu sắc <span className={!this.state.toggleFilColor ? 'mlkicon-Plus rxmlkiconPlus' : 'mlkicon-Minus rxmlkiconPlus'} onClick={(e) => { this.toggleFiltDest('toggleFilColor') }}></span></div>
              {this.state.toggleFilColor ? <div className='rx-filter-body clearfix rx-filter-color'>{
                tempOptionColor.map((value, index) => (
                  <div key={index} onClick={(e) => this.onClickchangeFilter(e, 'searchoption_color', value.i)} className={(this.state.paging && this.state.paging.searchoption_color === value.i? 'rx-filter-option rx-active': 'rx-filter-option')}><div className='rx-filter-option-choose'><span className='filter-input'></span>{value.a}</div></div>
                ))                                
              }</div> : <div></div> }

              <div className='rx-filter-title'>Kích thước <span className={!this.state.toggleFilSize ? 'mlkicon-Plus rxmlkiconPlus' : 'mlkicon-Minus rxmlkiconPlus'} onClick={(e) => { this.toggleFiltDest('toggleFilSize') }}></span></div>
              {this.state.toggleFilSize ?<div className='rx-filter-body clearfix'>{
                tempOptionSize.map((value, index) => (
                  <div key={index} onClick={(e) => this.onClickchangeFilter(e, 'searchoption_size', value)} className={(this.state.paging && this.state.paging.searchoption_size === value? 'rx-filter-option rx-active': 'rx-filter-option')}><div className='rx-filter-option-choose'><span className='filter-input'></span>{value}</div></div>
                ))                                
              }
              </div> : <div></div> }

              
            </div>
          </div>          
        </div>
        <div className='rx-product-list-container'>
          <div className='rx-product-list-inner'>
            <div className="rx-navbar">
              <div className='list-nav'>Home</div>
              <div className='list-nav active'>Chăm sóc sức khỏe</div>
            </div>
            <div className='rx-cat-title'>{this.state.paging.searchname_appdist ? this.state.paging.searchname_appdist : 'Tất cả sản phẩm'}</div>            
            <div className='rx-cat-sorting clearfix'>
              <div className='rx-cat-sorting-name'>
                <i>Sắp xếp theo</i>
                <select className='select-search-category' onChange={(e) => this.handleChange(e, this.state.paging)} >
                  <option value='created_at:desc'>Tất cả</option>
                  {[{i: 'created_at:desc', a: 'Hàng mới về'}, {i: 'sold:desc', a: 'Bán chạy'}, {i: 'price:asc', a: 'Giá tăng dần'}, {i: 'price:desc', a: 'Giá giảm dần'}].map((item,index) => 
                    <option key={index} className={(typeof(this.state.paging) !== 'undefined' && this.state.paging.st_full === item.i) ? 'rx-active' : ''} value={item.i}>{item.a}</option>)}
                </select>
              </div>

              {/*}
              { [{i: 'created_at:desc', a: 'Hàng mới về'}, {i: 'sold:desc', a: 'Bán chạy'}, {i: 'price:asc', a: 'Giá tăng dần'}, {i: 'price:desc', a: 'Giá giảm dần'}].map((item, index) => (
                  <span key={index} className={(typeof(this.state.paging) !== 'undefined' && this.state.paging.st_full === item.i) ? 'rx-active' : ''}
                    onClick={(e) => this.onClickSorting(e, item)}>{item.a}</span>
                )) }
              */}
              { typeof(this.state.paging) !== 'undefined' &&
                <div className='rx-cat-pagin rxhidden'>                
                  <span onClick={(e) => this.onClickChangepage(e, 'back')} className='mlkicon-Left btn-backs'></span> 
                  { this.state.paging.pg_page > 1 && 
                  <span onClick={(e) => this.onClickChangepage(e, 'back')}>{ this.state.paging.pg_page - 1 }</span> }
                  <span className='rx-active'>{this.state.paging.pg_page}</span> 
                  <span onClick={(e) => this.onClickChangepage(e, 'next')}>{this.state.paging.pg_page + 1}</span> 
                  <span onClick={(e) => this.onClickChangepage(e, 'next')} className='mlkicon-Right btn-nexts' ></span>
                </div> }              
            </div>
            <div className='row rx-cat-product'>{products}</div>
            <div className='clearfix'></div>
            <div className='rx-cat-sorting clearfix martop20'>              
              { typeof(this.state.paging) !== 'undefined' &&
                <div className='rx-cat-pagin'>                
                  <span onClick={(e) => this.onClickChangepage(e, 'back')} className='mlkicon-Left btn-backs'></span> 
                  { this.state.paging.pg_page > 1 && 
                  <span onClick={(e) => this.onClickChangepage(e, 'back')}>{ this.state.paging.pg_page - 1 }</span> }
                  <span className='rx-active'>{this.state.paging.pg_page}</span> 
                  <span onClick={(e) => this.onClickChangepage(e, 'next')}>{this.state.paging.pg_page + 1}</span> 
                  <span onClick={(e) => this.onClickChangepage(e, 'next')} className='mlkicon-Right btn-nexts' ></span>
                </div> }
            </div>
          </div>
        </div>
      </div></div></div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  cart: state.cart,
  rxsearch: state.rxSearch
})

const mapDispatchToProps = {  
  cartClear,
  cartAdd,
  rxsearchClear
}

const Categories = connect(
  mapStateToProps,
  mapDispatchToProps
)(Categories_)

export default Categories