// Share component
const Payment = global.rootRequiretheme('components/Sites/Payment').default
const AuthLogin = global.rootRequiretheme('components/Authenticate/Login').default
const AuthRegister = global.rootRequiretheme('components/Authenticate/Register').default
const AuthForgotPass = global.rootRequiretheme('components/Authenticate/ForgotPassword').default
const AuthResetPassword = global.rootRequiretheme('components/Authenticate/ResetPassword').default

const Customer = global.rootRequiretheme('components/Customer').default
const CustomerOrder = global.rootRequiretheme('components/Customer/Order').default
const CustomerAccount = global.rootRequiretheme('components/Customer/Account').default
const CustomerAddress = global.rootRequiretheme('components/Customer/Address').default
const CustomerPurchase = global.rootRequiretheme('components/Customer/Purchase').default
const CustomerFavorite = global.rootRequiretheme('components/Customer/Favorite').default
const CustomerOrderTracking = global.rootRequiretheme('components/Customer/Tracking').default

// Theme component
const Home = global.rootRequiretheme('Home').default
const About = global.rootRequiretheme('About').default
const Contact = global.rootRequiretheme('Contact').default
const RedirectWithStatus = global.rootRequiretheme('RedirectWithStatus').default
const Categories = global.rootRequiretheme('Categories').default

const News = global.rootRequiretheme('News').default
const NewsAll = global.rootRequiretheme('News/All').default
const NewsSingle = global.rootRequiretheme('News/Single').default

const Product = global.rootRequiretheme('Product').default
const ProductAll = global.rootRequiretheme('Product/All').default
const ProductSingle = global.rootRequiretheme('Product/Single').default

const NotFound = global.rootRequiretheme('NotFound').default

let Themeroutes = [
  { path: '/',
    exact: true,
    component: Home,
    loadData: 'home_get'
  },
  { path: '/category/:slug',
    exact: true,
    component: Categories,
    loadData: 'category'
  },
  {
    path: '/product',
    component: Product,
    routes: [
      { path: '/product',
        exact: true,
        component: ProductAll,
      },
      { path: '/product/:slug',
        component: ProductSingle,
        loadData: 'home_get_productdetail'
      }
    ]
  },
  {
    path: '/search',
    component: Product,
    routes: [
      { path: '/search',
        exact: true,
        component: ProductAll,
      },
      { path: '/search/:cat/:search',
        component: ProductAll,
      },
      { path: '/search/:cat',
        component: ProductAll,
      }
    ]
  },
  {
    path: '/payment',
    component: Payment
  },
  { path: '/about',
    component: About
  },
  { path: '/contact',
    component: Contact
  },
  { path: '/movie',
    component: RedirectWithStatus,
    status: 301,
    to: '/book'
  },
  {
    path: '/login',
    component: AuthLogin
  },
  {
    path: '/register',
    component: AuthRegister
  },
  {
    path: '/forgotpassword',
    component: AuthForgotPass
  },
  { path: '/reset_password/:slug',
    component: AuthResetPassword,
    loadData: 'user_get_userdetail'
  },
  { path: '/customer',    
    component: Customer,
    routes: [       
      {
        path: '/customer',
        exact: true,
        component: Customer
      },
      {
        path: '/customer/account',
        component: CustomerAccount
      },
      {
        path: '/customer/order',
        component: CustomerOrder
      },
      {
        path: '/customer/address',
        component: CustomerAddress
      },
      {
        path: '/customer/purchased',
        component: CustomerPurchase
      },
      {
        path: '/customer/favorite',
        component: CustomerFavorite
      },
      {
        path: '/customer/tracking',
        component: CustomerOrderTracking
      }
    ]
  },
  {
    path: '/news',
    component: News,
    routes: [
      { path: '/news',
        exact: true,
        component: NewsAll
      },
      { path: '/news/:slug',
        component: NewsSingle,
        loadData: 'home_get_newsdetail'
      }
    ]
  },
  { path: '*',
    component: NotFound
  }]

export default Themeroutes