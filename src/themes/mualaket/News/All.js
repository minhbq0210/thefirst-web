import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { polyfill }  from 'es6-promise'
// import fetch from 'isomorphic-fetch'
import { connect } from 'react-redux'
import {Helmet} from "react-helmet";

const { rxget } = global.rootRequire('classes/request')
const { inDays } = global.rootRequire('classes/ulti')

polyfill()

// const WAIT_INTERVAL = 500
const config = global.rxu.config

class NewsAll_ extends Component {
  constructor(props) {
    super(props)
    this.state = { 
      data: [], categories: [], hot: [],
      allcate: {_id: 0, name: 'Tất cả'}, 
      curobj: {categories: 0}, 
      paging: { st_col: 'created_at', st_type: -1, pg_size: 5, pg_page: 1 },
    }        
    this.state = this.props.staticContext || { hot: [], categories: [], data: [], paging: {} }        
    this.state.categories = this.state.categories || []
    this.state.data = this.state.data || []
    this.state.hot = this.state.hot || []
    this.state.paging = { st_col: 'created_at', st_type: -1, pg_size: 5, pg_page: 1 }
  }

  componentDidMount() {
    window.scrollTo({top: 0})
    this.fetchData()
  }

  onClickCat(e, categoryId) {

    let tempPaging = this.state.paging    
    if (categoryId) {
      tempPaging.searchid_appdist = categoryId
    } else {
      delete(tempPaging.searchid_appdist)
    }

    this.setState({paging: tempPaging}, () => {
      this.fetchData()
    })
  }

  fetchData() {
    rxget(config.api_home_news, this.state.paging, {
      '1': (json) => { this.setState({ categories: json.data.categories, data: json.data.contents, hot: json.data.contentshot }) }
    })
  }

  render() {
    let categories = this.state.categories.map(category => (
      <div key={category._id} onClick={(e) => this.onClickCat(e, category._id)} className={(this.state.paging.searchid_appdist === category._id)? 'rx-active': ''}>{category.name}</div>
    ))
    
    let posts = this.state.data.map(obj => {      
      return (
        <div className='rxnews-post' key={obj._id}>
          <div className='rxnews-post-pic'><img className='lazyload' data-src={config.base_api + '/upload/image/' + obj.img_landscape} alt='' title='{obj.name}' /></div>
          <div className='rxnews-post-body'>
            { obj.appdistobj && <div className='rxnews-post-cat clearfix'>{obj.appdistobj.map((cat) => (<div key={cat._id}>{cat.name}</div>))}</div> }
            <div className='rxnewsex-date'> {inDays(obj.updated_at)}</div>
            <div><Link className='rxnews-post-title' to={`/news/${obj.slug}`}>{obj.name}</Link></div>
            <div className='rxnews-post-desc'>{obj.desc}</div>
          </div>
        </div>
      )
    })

    let hot = this.state.hot.map(obj => {
      return (<Link to={`/news/${obj.slug}`} key={obj._id}>{obj.name}</Link>)      
    })
    
    return (
      <div className='container rxnews-pagewrap'><div className='rx-main-page'>
        <Helmet>
          <title>{ `${global.SEOtitle}` }</title>
          <link rel="canonical" href={`${this.props.location.pathname}`} />
          <meta name="description" content={global.SEOdesc} />
        </Helmet>
        <div className='rxnews-cat clearfix row row-nopad-col'>
          <div onClick={(e) => this.onClickCat(e, false)} className={!(this.state.paging && this.state.paging.searchid_appdist)? 'rx-active': ''}>Xem tất cả</div>
          {categories}
        </div>
        <div className='rxnews-wrapper row row-nopad-col'>
          <div className='col-sm-lg col-sm-8 rx-full-small'>
            <div className='rxnews-contents'>{posts}</div>
          </div>

          <div className='col-sm-lg col-sm-4 rx-hide-small'>
            <div className='rxnews-hot'>
              <div className='rxnews-hothead'>LATEST POSTS</div>
              <div className='rxnews-hotbody'>{hot}</div>
            </div>
          </div>
        </div>
      </div></div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
})

const mapDispatchToProps = {
}

const NewsAll = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsAll_)

export default NewsAll