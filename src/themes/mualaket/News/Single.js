import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { polyfill }  from 'es6-promise'
// import fetch from 'isomorphic-fetch'
import { connect } from 'react-redux'
import {Helmet} from "react-helmet";

import { withTranslation } from 'react-i18next'
const { rxget, rxpost } = global.rootRequire('classes/request')
const { rxCountStart } = global.rootRequire('components/Helpers/RxCountStart')
const RxRatingbox = global.rootRequiretheme('components/Shares/RxRatingbox').default
const { rxgetdate } = global.rootRequire('classes/ulti') //rxcurrencyVnd
// const { rxCurrency } = global.rootRequire('components/Helpers/RxCurrency')
// const { cartClear, cartAdd } = global.rootRequire('redux')

polyfill()
const config = global.rxu.config

class NewsSingle_ extends Component {
  constructor (props) {
    super(props)
    let slug = this.props.match.params.slug

    this.state = this.props.staticContext || { hot: [], categories: [], data: [], paging: {}, news: {} }
    this.state.slug = this.state.slug || slug
    this.state.news = this.state.news || {}
    this.state.categories = this.state.categories || []    
    this.state.hot = this.state.hot || []
    this.state.paging = { st_col: 'created_at', st_type: -1 } 
    this.state.chooseSelect = this.state.chooseSelect || {chooseCustomer: 0, chooseStart: 0, pg_page: 1, pg_sum: 1} 
    this.state.reviewsrating = this.state.reviewsrating || {} 
    this.state.arrratings = this.state.arrratings || []
  }

  componentDidMount () {
    window.scrollTo({top: 0})
    let slug = this.state.slug
    this.setState({slug: slug}, () => { this.fetchData() })    
  }

  componentWillReceiveProps (nextProps, nextContext) {
    if (nextProps.match.params.slug !== this.state.slug) {  
      let slug = nextProps.match.params.slug
      this.setState({slug: slug}, () => { this.fetchData() })
    }
  }

  fetchData() {
    rxget(config.api_home_newsdetail, {slug: this.state.slug}, {
      '1': (json) => {
        this.setState({ categories: json.data.categories, hot: json.data.contentshot, news: json.data.news }) 
        if (typeof(json.data.news.ratings) !== 'undefined') {
          let arrlen = json.data.news.ratings.length
          let tempChooseSelect = this.state.chooseSelect
          tempChooseSelect.pg_sum = Math.ceil(arrlen / 10)
          this.setState({chooseSelect: tempChooseSelect}, () => {
            this.parseRatings(this.state.chooseSelect)
          })                  
        }
        
      }
    })
  }

  parseRatings(chooseSelect) {
    let arrayratings = this.state.news.ratings
    let arraytmp = []
    if (typeof(arrayratings) !== 'undefined' && arrayratings.length > 0) {
      for(var i = 0; i < arrayratings.length; i++)
      {       
        if (chooseSelect.chooseStart !== 0 && chooseSelect.chooseCustomer !== 0) {
          if (arrayratings[i].score === chooseSelect.chooseStart && arrayratings[i].isCustomer === chooseSelect.chooseCustomer) arraytmp.push(arrayratings[i])
        } else if (chooseSelect.chooseStart === 0 && chooseSelect.chooseCustomer !== 0) {
          if (arrayratings[i].isCustomer === chooseSelect.chooseCustomer) arraytmp.push(arrayratings[i])
        } else if (chooseSelect.chooseStart !== 0 && chooseSelect.chooseCustomer === 0) {
          if (arrayratings[i].score === chooseSelect.chooseStart) arraytmp.push(arrayratings[i])
        } else {
          arraytmp.push(arrayratings[i])
        }
      }
    }

    let tempChooseSelect = this.state.chooseSelect
    tempChooseSelect.pg_sum = Math.ceil(arraytmp.length/10)
    this.setState({chooseSelect: tempChooseSelect})
    let arraypagin = arraytmp.slice((chooseSelect.pg_page-1)*5, ((chooseSelect.pg_page-1)*5)+5)
    this.setState({ arrratings: arraypagin}) ///.reverse()
  }

  onClickCat() {
    this.props.history.push("/news")
  }

  onClickCheckRating(e, news) {
    let objrating = {}
    if (typeof(news._id) !== 'undefined' && typeof(news.ratings) !== 'undefined' && news.ratings.length > 0) {
      let customerid = (this.props.auth && this.props.auth.user && this.props.auth.user.customerid) ? this.props.auth.user.customerid : ''
      objrating = news.ratings.find(o => o.customerid === customerid )
      this.setState({reviewsrating: objrating})
    }
    this.setState({ratings: !this.state.ratings})
  }

  onClickDataUpdateSubmit(objreviewsrating) {
    let paramsrating = {}
    let reviewsrating = objreviewsrating
    let randomid = Math.random().toString(36).substr(2) + Math.floor((Math.random() * 90000) + 10000);
    if (reviewsrating && reviewsrating !== 'null' && reviewsrating !== 'undefined') {
      paramsrating['ratings'] = {}
      paramsrating['ratings']['arrlikes'] = []
      paramsrating['ratings'] = reviewsrating
      paramsrating['ratings']['customerid'] = (this.props.auth && this.props.auth.user && this.props.auth.user.customerid) ? this.props.auth.user.customerid : randomid
      paramsrating['ratings']['name'] = (this.props.auth && this.props.auth.user && this.props.auth.user.fullname) ? this.props.auth.user.fullname : 'Anonymous'
      paramsrating['ratings']['isCustomer'] = (this.state.reviewsrating && this.state.reviewsrating.isCustomer === 1) ? 1 : 2
    }
    paramsrating['_id'] = this.state.news._id
    rxpost(config.api_content_rating, paramsrating, {
      '1': (json) => {this.fetchData(); this.setState({ratings: false, reviewsrating: {}})}
    })
  }

  onBlurDataSelect(e, name) {
    let tempChooseSelect = this.state.chooseSelect    
    if (name === 'chooseCustomer') {
      tempChooseSelect.chooseCustomer = Number(e.target.value)
    }
    if (name === 'chooseStart') {
      tempChooseSelect.chooseStart = Number(e.target.value)
    }

    this.setState({chooseSelect: tempChooseSelect}, () => {
      this.parseRatings(this.state.chooseSelect)
    })    
  }

  onClickPaginBack(e) {
    let paging = this.state.chooseSelect
    paging.pg_page = (paging.pg_page > 1) ? (paging.pg_page - 1) :  paging.pg_page
    this.setState({ chooseSelect: paging }, () => { this.parseRatings(paging) })
  }

  onClickPaginNext(e) {
    let paging = this.state.chooseSelect
    paging.pg_page += 1
    this.setState({ chooseSelect: paging }, () => { this.parseRatings(paging) })
  }

  onClickAddRemoveLike(e, perdata, checkclick, index) {
    let arrayratings = this.state.arrratings
    e.stopPropagation() 
    if (this.props.auth.user.customerid) {
      if (checkclick === false) {
      if (perdata.arrlikes.constructor === String) {
        perdata.arrlikes = perdata.arrlikes.split(',').filter(Boolean)
      }
      perdata.arrlikes.push(this.props.auth.user.customerid)
      arrayratings[index] = perdata
      let paramsrating = {}
      let reviewsrating = perdata
      paramsrating['ratings'] = {}
      paramsrating['ratings'] = reviewsrating
      paramsrating['ratings']['arrlikes'] = perdata.arrlikes
      paramsrating['ratings']['customerid'] =  perdata.customerid 
      paramsrating['ratings']['name'] = perdata.name
      paramsrating['ratings']['isCustomer'] = perdata.isCustomer
      paramsrating['_id'] = this.state.news._id
      rxpost(config.api_content_rating, paramsrating, {
        '1': (json) => {this.fetchData(); this.setState({ratings: false, reviewsrating: {}, arrratings: arrayratings})}
      })
      } else {
        if (perdata.arrlikes.constructor === String) {
          perdata.arrlikes = perdata.arrlikes.split(',').filter(Boolean)
        }
        let indexuser = perdata.arrlikes.indexOf(this.props.auth.user.customerid)
        if (indexuser > -1) {
          perdata.arrlikes.splice(indexuser, 1)
        }
        arrayratings[index] = perdata
        let paramsrating = {}
        let reviewsrating = perdata
        paramsrating['ratings'] = {}
        paramsrating['ratings'] = reviewsrating
        paramsrating['ratings']['arrlikes'] = perdata.arrlikes
        paramsrating['ratings']['customerid'] =  perdata.customerid 
        paramsrating['ratings']['name'] = perdata.name
        paramsrating['ratings']['isCustomer'] = perdata.isCustomer
        paramsrating['_id'] = this.state.news._id
        rxpost(config.api_content_rating, paramsrating, {
          '1': (json) => {this.fetchData(); this.setState({ratings: false, reviewsrating: {}, arrratings: arrayratings})}
        })
      }
    } else {
      alert('Vui lòng đăng nhập để like')
    }
  }

  render () {
    // let posts = (<div></div>)
    
    // let categories = this.state.categories.map(category => (
    //   <div key={category._id} onClick={(e) => this.onClickCat(e)}>{category.name}</div>
    // ))

    const { t } = this.props
    // let language = this.props.i18n.language

    let hot = this.state.hot.map(obj => {
      return (<Link to={`/news/${obj.slug}`} key={obj._id}>{obj.name}</Link>)
    }) 

    let SEOdesc = global.rxu.get(this.state.news, 'desc', global.SEOdesc)

    let news = this.state.news

    let arrStart = Array.apply(null, Array(5)).map((i) => { return {count: 0, width: 0, sum: 0};})
    let arrRatingTemplate = []
    if (this.state.news && typeof(this.state.news.ratings) !== 'undefined') {
      let arrRatings = this.state.news.ratings
      let sumScore = 0
      let countUser = 0
      for (let key in arrRatings) {
        let countstart = arrRatings[key]['score'] - 1
        sumScore += arrRatings[key]['score']
        arrStart[countstart]['count'] += 1
        countUser += 1
      }
      if (sumScore !== 0) {
        for (let key in arrStart) {
          arrStart[key]['count'] = Math.ceil((arrStart[key]['count'] / countUser * 100))
        }
      } 
      arrRatingTemplate = this.state.arrratings.map((perdata,index) => (
        <div className='lk-reviews-line-main' key={index}>
          <div className='lk-reviews-line-main-body'>
            <div className='lk-reviews-main-title-select'>
              <div className='lk-position-img-icon-account'><div className='figure-body-account mlkicon-Profile'></div></div>
              <div className='lk-reviews-line-body-name'>{perdata.name}</div>
              <div className='lk-reviews-line-body-time'>{rxgetdate(perdata.created_at)}</div>
            </div>
            <div className='lk-reviews-line-main-right'>
              <div className='lk-reviews-line-start'>
                <div className='lk-reviews-line-info'>
                  <div className='lk-reviews-line-header-score'>{rxCountStart(perdata.score)}</div>
                  <div className='lk-reviews-line-header-title'>{perdata.title}</div>
                </div>
              </div>
              <div className='lk-reviews-line-start'>
                <div className='lk-reviews-line-content'><span>{perdata.content}</span></div>
              </div>
              <div className='lk-reviews-line-start'>
                <div className='lk-reviews-line-like'>
                  {(perdata && perdata.arrlikes && perdata.arrlikes.constructor === Array && this.props.auth && this.props.auth.user && this.props.auth.user.customerid && perdata.arrlikes.indexOf(this.props.auth.user.customerid) !== -1) &&  <div className='lk-reviews-line-like-button' onClick={(e) => this.onClickAddRemoveLike(e, perdata, true, index)}> <span className='iconanna-like'></span>Unlike</div>}
                  {(perdata && perdata.arrlikes && perdata.arrlikes.constructor === Array && this.props.auth && this.props.auth.user && this.props.auth.user.customerid && perdata.arrlikes.indexOf(this.props.auth.user.customerid) === -1) && <div><span className='lk-reviews-line-like-text'>Nhận xét hữu ích với bạn?</span><span className='lk-reviews-line-like-button-start' onClick={(e) => this.onClickAddRemoveLike(e, perdata, false, index)}> <span className='iconanna-like'></span>Like</span></div>}
                </div>
              </div>
            </div>
          </div>
        </div>
      ))
    }
    let arrProcessTemplate = arrStart.reverse().map((perdata, index) => (
      <li key={index} className='lk-list-product-rating-gui clearfix'>
        <span className='lk-list-product-rating-start'>{5 - index} <div className="rx-star"></div></span>
        <span  className='lk-list-rating-progress'>
          <span className='lk-list-rating-progress-bar'>
            <span className='lk-list-rating-progress-bar-choosed' style={{width: perdata.count+'%'}}></span>
          </span>
        </span>
        <span className='lk-list-product-rating-count'>{perdata.count}%</span>
      </li>
    ))

    return (
       <div className='container rxnews-pagewrap'><div className='rx-main-page'>
        <Helmet>
          <title>{ `${global.rxu.get(this.state.news, 'name', global.SEOtitle)}` }</title>
          <link rel="canonical" href={`${this.props.location.pathname}`} />
          <meta name="description" content={SEOdesc} />
        </Helmet>
        <div className='rxnewsex-wrapper row row-nopad-col'>
          <div className='col-sm-lg col-sm-8 rx-full-small'>
            <div className='rxnewsex-title'>{this.state.news.name}</div>
            <hr/>
            {/* this.state.news.appdistobj && <div className='rxnews-post-cat clearfix'>{this.state.news.appdistobj.map((cat) => (<div key={cat._id}>{cat.name}</div>))}</div> }
            <div className='rxnewsex-date'>Lúc {rxgetdate(this.state.news.created_at)}</div>
            <div className='rxnewsex-desc'>{this.state.news.desc}</div>
            <div className='rxnewsex-image'><img className='' src={config.base_api + '/upload/image/' + this.state.news.img_landscape} alt='' title='{this.state.news.name}' /></div>*/}
            <div className='rxnewsex-content' dangerouslySetInnerHTML={{__html: global.rxu.get(this.state.news, 'content', this.state.news.desc)}} />
          </div>

          <div className='col-sm-lg col-sm-4 rx-hide-small'>
            <div className='rxnews-hot'>
              <div className='rxnews-hothead'>Nóng bỏng tay</div>
              <div className='rxnews-hotbody'>{hot}</div>
            </div>
          </div>

          <div className='row row-nopad-col product-detail-block' id='review'>
            <div className=' row row-nopad-col rx-pad-30'>
            <div className='rx-product-desc-col-name'><div className='rx-product-desc-col-text'><span className='rx-cart-buynow'></span>{t('Customer reviews')}</div></div>
              <div className='col-sm col-md-12'>    
                <div>
                  <div className='lk-product-reviews-title'></div>
                  <div className='lk-reviews-start-main'>                  
                    <div className='lk-reviews-progress-start-position'>
                      <div className='lk-reviews-start'>
                        <div className='lk-reviews-start-title'>{t('Average rating')}</div>
                        <div className='lk-reviews-start-divice'>{news.ratingsSumary || 0}/5</div>
                        <div className='lk-reviews-start-icon clearfix'>{rxCountStart(news.ratingsSumary, true)}</div>
                        <div className='lk-reviews-start-command'>({(typeof(news.ratings) !== 'undefined') && news.ratings.length} {(typeof(news.ratings) === 'undefined') && 0}  {t('reviews')})</div>
                      </div>
                    </div>

                    <div className='lk-reviews-progress-center-position'>
                      <div className='lk-reviews-progress-start'>
                        <ul>
                          {arrProcessTemplate}
                        </ul>
                      </div>
                    </div>
                    
                    <div className='lk-reviews-function-command'>
                      <div className='lk-reviews-function-command-title'>{t('Review this product')}</div>
                      <div className='lk-reviews-function-command-width lk-reviews-function-btn' onClick={(e) => this.onClickCheckRating(e, news)}><span className='mlkicon-Write'></span>{t('Write a review')}</div>
                    </div>
                  </div>
                  {this.state.ratings === true && <div>                    
                    <RxRatingbox reviewsrating={this.state.reviewsrating} infodata={news} onChange={(result) => {this.onClickDataUpdateSubmit(result)}} type='news' /> 
                  </div>}
        
                </div>
                <div>
                  {/*<div className='lk-reviews-main-title'><span>Nhận xét về sản phẩm</span></div>*/}
                  <div>
                    <div className='lk-reviews-main-box clearfix'>
                      <div className='lk-reviews-main-title-select'>{t('Filter reviews')}</div>
                      <div className='lk-reviews-line-start'>
                        <div className='lk-reviews-line-start-score'>
                          <select className='fullwidth-input' onChange={(e) => this.onBlurDataSelect(e, 'chooseStart')} value={this.state.valueChooseStart}>
                            <option value={0}>{t('All star')}</option>
                            <option value={1}>1 {t('star')}</option>
                            <option value={2}>2 {t('star')}</option>
                            <option value={3}>3 {t('star')}</option>
                            <option value={4}>4 {t('star')}</option>
                            <option value={5}>5 {t('star')}</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    {this.state.arrratings && this.state.arrratings.length > 0 ? arrRatingTemplate : <div className='lk-reviews-empty'>Chưa có nhận xét</div>}
                    {this.state.arrratings && this.state.arrratings.length > 0 && <div className='admin-table-pagination admin-pagin-right' style={{marginTop: '15px'}}>
                      {(this.state.chooseSelect.pg_page !== 1) && <div className='pagin-back' onClick={(e) => this.onClickPaginBack(e)}><i className='icon-arrow-left'></i></div>}
                      <div className='pagin-curr'>{this.state.chooseSelect.pg_page}</div>
                      {(this.state.chooseSelect.pg_sum >= this.state.chooseSelect.pg_page) && <div className='pagin-next' onClick={(e) => this.onClickPaginNext(e)}><i className='icon-arrow-right'></i></div>}
                    </div>}
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div></div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  auth: state.auth
})

const mapDispatchToProps = {  
  
}

const NewsSingle = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsSingle_)


export default withTranslation('translations')(NewsSingle)
