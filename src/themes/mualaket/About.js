import React, { Component } from 'react'

class About extends Component {
	
	constructor(props) {
    super(props) 
    this.state = { 
      editingData: {},
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onClickLoginSubmit(e) {  
  }

  render() {
    return (
    <div className='container'>
      {/*<div className='rx-main-page'>
        <div className='row lk-page-custom-about-head'>
        	<div className='lk-page-custom-about-head- col-md-6'>
        		<div className='lk-page-custom-about-head-title'>ĐẲNG CẤP CỦA DÒNG THỜI TRANG LÍNH</div>
        		<div className='lk-page-custom-about-head-desc'>Bạn là một tín đồ của dòng thời trang Lính? Đã bao lâu nay bạn săn tìm nguồn hàng thời trang Lính hàng xịn, nhập khẩu chính hãng từ các hãng thời trang Lính danh tiếng của Mỹ? Bạn là người luôn khát khao sở hữu những món đồ độc, đẵng cấp để thể thiện cái chất riêng của mình? Từ nay trở đi các cửa hàng Hàng Độc Từ Mỹ sẽ là địa điểm quen thuộc của bạn! Tự tin thể hiện bản sắc riêng của mình với những món đồ thời trang Lính cực độc từ những hãng thời trang Lính danh tiếng của Mỹ: Rothco, Propper, Alpha Industries do chúng tôi thửa riêng mang về cho bạn!</div>
        	</div>
        	<div className='lk-page-custom-about-head- col-md-6'>
        		<div className='lk-page-custom-about-head-image'></div>
        	</div>
        </div>
        <div className='row lk-page-custom-about-body'>
        	<div className='lk-page-custom-about-body-title'>Hệ thống cửa hàng</div>
        	<div className='lk-page-custom-about-body-desc'>Tự tin thể hiện bản sắc riêng của mình với những món đồ thời trang Lính cực độc từ những hãng thời trang Lính danh tiếng của Mỹ: Rothco, Propper, Alpha Industries do chúng tôi thửa riêng mang về cho bạn!</div>
        	<div className='lk-page-custom-about-body- col-md-4'>
        		<div className='lk-page-custom-about-body--'>
	        		<div className='lk-page-custom-about-body-img img1'></div>
	        		<div className='lk-page-custom-about-body-text'>
	        			<div className='about-body-text-left'>TPHCM</div>
	        			<div className='about-body-text-right'>
	        				<div><span className='ab-icon'></span><span>97 Đường số 1, Cư Xá Đô Thành Phường 4, Quận 3, TP.HCM</span></div>
	        				<div><span className='ab-icon'></span><span>090.888.1212</span></div>
	        			</div>
	        		</div>
        		</div>
        	</div>
        	<div className='lk-page-custom-about-body- col-md-4'>
        		<div className='lk-page-custom-about-body--'>
	        		<div className='lk-page-custom-about-body-img img2'></div>
	        		<div className='lk-page-custom-about-body-text'>
	        			<div className='about-body-text-left'>HÀ NỘI</div>
	        			<div className='about-body-text-right'>
	        				<div><span className='ab-icon'></span><span>629 Đê La Thành, Quận Ba Đình, Hà Nội</span></div>
	        				<div><span className='ab-icon'></span><span>090.888.6388</span></div>
	        			</div>
	        		</div>
        		</div>
        	</div>
        	<div className='lk-page-custom-about-body- col-md-4'>
        		<div className='lk-page-custom-about-body--'>
	        		<div className='lk-page-custom-about-body-img img3'></div>
	        		<div className='lk-page-custom-about-body-text'>
	        			<div className='about-body-text-left'>ĐÀ LẠT</div>
	        			<div className='about-body-text-right'>
	        				<div><span className='ab-icon'></span><span>Tầng trệt, Thương xá Latupipe 1 Nguyễn Thị Minh Khai, Đà Lạt</span></div>
	        				<div><span className='ab-icon'></span><span>090.888.0866</span></div>
	        			</div>
	        		</div>
        		</div>
        	</div>
        </div>
        <div className='row lk-page-custom-about-footer'>
        	<div className='lk-page-custom-about-footer- col-md-4'>
           <div className='about-footer-title'>
             <div className='about-footer-title-'>GIỜ MỞ CỬA</div>
             <div className='about-footer-title--'>
               <span className='footer-day'>Thứ hai - Thứ Sáu<b></b></span><span className='footer-hour'>9.30 to 20.30</span>
               <span className='footer-day'>Thứ bảy<b></b></span><span className='footer-hour'>9.30 to 20.30</span>
               <span className='footer-day'>Chủ nhật<b></b></span><span className='footer-hour'>Nghỉ</span>
             </div>
           </div> 
          </div>
        	<div className='lk-page-custom-about-footer- col-md-4'>
           <div className='about-footer-title'>
             <div className='about-footer-title---'>Cảm ơn anh em đã gắn bó, chia sẻ và ủng hộ suốt hơn 2 năm qua. Hàng Độc Từ Mỹ sẽ cố gắn hết sức mở thêm các cửa hàng ở các tỉnh trong thời gian sớm nhất có thể để đến gần hơn với anh em mọi miền đất nước!</div>
           </div> 
          </div>
        	<div className='lk-page-custom-about-footer- col-md-4'>
           <div className='about-footer-title'>
             <div className='about-footer-title---'>Hàng Độc từ Mỹ là nhà phân phối chính hãng được Rothco và Alpha industries công nhận. Hàng Độc Từ Mỹ chỉ kinh doanh các sản phẩm thời trang lính nhập khẩu chính hãng từ các hãng thời trang lính danh tiếng nhất tại Mỹ!</div>
           </div> 
          </div>
        </div>
      </div>*/}
      
    </div>
    )
  }
}
export default About