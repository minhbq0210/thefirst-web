import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import $ from 'jquery'
import { withTranslation } from 'react-i18next'
const { rxget } = global.rootRequire('classes/request')
// const { rxconfig } = global.rootRequire('classes/ulti')
const config = global.rxu.config
class Footer extends Component { 
  constructor (props) {
    super(props)
    this.state = global.ultiStaticContext(this.props.staticContext) || {}
    this.state.email = this.state.email || ''
  }

  animateProduct(key) {
    let z = $('.lk-page-brand-body').get(0).scrollWidth - $('.lk-page-brand-body').get(0).clientWidth
    if (key === 'next') {
      $('.lk-page-brand-body').animate({scrollLeft: z}, 1000);
    } else {
      $('.lk-page-brand-body').animate({scrollLeft: -z}, 1000);
    }
  }

  validateEmail (email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
  }

  onBlurData(e) {
    this.setState({email: e.target.value})
  }  

  sentemail(e) {
    if (this.validateEmail(this.state.email)) {
      rxget(config.api_sentemail, {email: this.state.email}, { '1': (json) => { 
        if (json && json.success && json.success === 1) {
          alert('Một email vừa được gửi đến email của bạn')    
        } else {
          alert('Email không gửi được, vui lòng thử lại sau') 
        }
      }})  
    } else {
      alert('Vui lòng nhập đúng định dạng email')
    }
  }
  
  render() {
    const { t } = this.props
    return (
  	<div className='mainfooter-container'>	
       <div className='rx-footer-subcribe'>
        <div className='rx-footer'>
          <div className='rx-footer-subcribe_content'>
            <b className='rx-footer-mail-title'>Đăng ký nhận tin AnnaWhole sale Big saving up to 70% OFF</b>
            <span>Thông tin của bạn được Anna Wholesale Australia bảo mật tuyệt đối và bạn có thể hủy đăng ký</span>
          </div>
          <div className='rx-footer-main-col-address rx-footer-mail-desc'>
            <div className='lk-page-custome-register-mail-input'>
              <input type='text' autoComplete='email' placeholder='nhập địa chỉ Email...' onChange={(e) => {this.onBlurData(e)}}/>
              <button onClick={() => { this.sentemail() }}></button>
            </div>
            {/*<div className='rx-footer-social-contain'>
              <span className='rx-footer-social-ico mlkicon-Facebook'/>
              <span className='rx-footer-social-ico mlkicon-Google'/>
              <span className='rx-footer-social-ico mlkicon-Youtube'/>
              <span className='rx-footer-social-ico mlkicon-Zalo'/>
              <span className='rx-footer-social-ico mlkicon-Shopee'/>
            </div>*/}
          </div>
        </div>
      </div>	
    	<div className='rx-footer'>
        <div className='rx-footer-main clearfix'>
          <div className='rx-footer-main-col'>
            <b>{t('Support')}</b>
            {/*<a className='rx-footer-time'>{t('Business hours')}: 9h30 - 20h30 {t('from monday to saturday')}<br/><div>{t('Off sunday, holidays')}</div></a>*/}
            <div className='rx-footer-hotline'><b>Hotline:</b>  +61 450 928 599</div>
            <div className='rx-footer-hotline'><b>Email:</b>  info@annawholesale.com.au  </div>
            <div className='rx-footer-helplink'> 
              <div><span className='mlkicon-Return rx-footer-help-ico' /><Link to='/'>{t('Return policy')}</Link></div>
              <div><span className='mlkicon-Info rx-footer-help-ico' /><Link to='/'>{t('Shopping guide')}</Link></div>
              <div><span className='icon-book-open rx-footer-help-ico' /><Link to='/'>Quy định giao hàng</Link></div>
            </div>
          </div>

          <div className='rx-footer-main-col'>
            <b>{t('Store')}</b>
            <div>{t('Store')} 1: <b>Ozlux Healthfood</b></div>
            <div className='rx-footer-address'>Shop QG 12/ 8 Quay Street, Haymarket Sydney 2000</div>          
            <div>{t('Store')} 2: <b>AnnaStore Wholesale</b></div>
            <div className='rx-footer-address'>Shop 3 / 12-26 Little Regent Street, Chippendale, Sydney , 2008</div>
            <div>{t('Store')} 3: <b>AnnaStore Wholesale</b></div>
            <div className='rx-footer-address'>{t('Address Store')}</div>
          </div>
          <div className='rx-footer-main-col'>
            <b>Follow us</b><br/>
            <a className='rx-footer-inter' href='https://www.instagram.com/annastorechemist/' target="_blank" rel="noopener noreferrer"><span className='rx-footer-social-ico iconanna-instagram'/></a>
            <a className='rx-footer-inter' href='https://www.facebook.com/AnnaStore-Wholesale-106136010946512' target="_blank" rel="noopener noreferrer"><span className='rx-footer-social-ico mlkicon-Facebook'/></a>
          </div>

          {/*<div className='rx-footer-main-col rxfooter-none'>
            <b>LIÊN KẾT NHANH</b>
            <a className='rx-footer-fastlink-item' href='/home'>Trang chủ</a>
            <a className='rx-footer-fastlink-item' href='/search/all'>Cửa hàng</a>
            <a className='rx-footer-fastlink-item' href='/pages/chung-nhan-chinh-hang'>Chứng nhận chính hãng</a>
            <a className='rx-footer-fastlink-item' href='/pages/chinh-sach-khach-hang'>Chính sách khách hàng</a>
            <a className='rx-footer-fastlink-item' href='/news'>Tin tức</a>
            <a className='rx-footer-fastlink-item' href='/contact'>Liên hệ</a>
          </div> */}      

          {/*<div className='rx-footer-main-col'>
            <b>Phương thức thanh toán</b>
            <div className='rx-footer-main-col-address'>
              <div className='bank-footer-img' style={{ backgroundImage: 'url(/images/static/Footer/Bank/bitmap@2x1.png)' }}></div>
              <div className='bank-footer-img' style={{ backgroundImage: 'url(/images/static/Footer/Bank/bitmap@2x2.png)' }}></div>
              <div className='bank-footer-img' style={{ backgroundImage: 'url(/images/static/Footer/Bank/bitmap@2x5.png)' }}></div>
              <div className='bank-footer-img' style={{ backgroundImage: 'url(/images/static/Footer/Bank/bitmap@2x4.png)' }}></div>
              <div className='bank-footer-img' style={{ backgroundImage: 'url(/images/static/Footer/Bank/bitmap@2x3.png)' }}></div>
              <div className='bank-footer-img' style={{ backgroundImage: 'url(/images/static/Footer/Bank/bitmap@2x6.png)' }}></div>
            </div>
          </div>*/}

          {/*<div className='rx-footer-main-col'>
            <b>Thông tin cửa hàng</b>
            <div className='rx-footer-main-col-address'>
              <b>TPHCM</b>
              <p>97 Đường số 1, Cư Xá Đô Thành Phường 4, Quận 3, TP.HCM</p>

              <b>Hà Nội</b>
              <p>629 Đê La Thành, Quận Ba Đình, Hà Nội</p>

              <b>Đà Lạt</b>
              <p>Tầng trệt, Thương xá Latupipe 1 Nguyễn Thị Minh Khai, Đà Lạt</p>
            </div>
          </div>

          <div className='rx-footer-main-col'>
            <b>P.Thức thanh toán</b> 
            <a className='rx-footer-img'></a>         
          </div>

          <div className='rx-footer-main-col'>
            <b>Kết nối chúng tôi</b>
            <a className='rx-footer-img-connect'></a>
          </div>
        </div>*/}
      </div>
      </div>
      <div className='rx-footer-end'>
        <div className='rx-footer-end-inner'>
          <div className='rx-footer-ecdc-privacy'>© 2019 <b>Anna Wholesale</b> Terms  |  Privacy</div>
          <div className='rx-footer-ecdc-copyright'>
            <div className='rx-footer-ecdc-paymentway'>
              <span className='rx-footer-ecdc-paymentway-item'><span className='mlkicon-Visa'><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span><span className="path6"></span><span className="path7"></span><span className="path8"></span><span className="path9"></span><span className="path10"></span><span className="path11"></span><span className="path12"></span><span className="path13"></span><span className="path14"></span><span className="path15"></span></span></span>
              <span className='rx-footer-ecdc-paymentway-item'><span className='mlkicon-Master'>
                <span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span><span className="path6"></span><span className="path7"></span><span className="path8"></span><span className="path9"></span><span className="path10"></span><span className="path11"></span><span className="path12"></span><span className="path13"></span><span className="path14"></span><span className="path15"></span>
                <span className="path16"></span><span className="path17"></span><span className="path18"></span><span className="path19"></span><span className="path20"></span><span className="path21"></span><span className="pat22"></span><span className="pat23"></span>
              </span></span> 
              <span className='rx-footer-ecdc-paymentway-item'><span className='mlkicon-JCB'><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span><span className="path6"></span><span className="path7"></span><span className="path8"></span><span className="path9"></span><span className="path10"></span><span className="path11"></span><span className="path12"></span><span className="path13"></span><span className="path14"></span><span className="path15"></span></span></span>
              <span className='rx-footer-ecdc-paymentway-item'><span className='mlkicon-Napas'><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span><span className="path6"></span><span className="path7"></span><span className="path8"></span><span className="path9"></span><span className="path10"></span><span className="path11"></span><span className="path12"></span><span className="path13"></span><span className="path14"></span><span className="path15"></span></span></span>
              <span className='rx-footer-ecdc-paymentway-item'><span className='mlkicon-Cash'><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span><span className="path6"></span><span className="path7"></span><span className="path8"></span><span className="path9"></span><span className="path10"></span><span className="path11"></span><span className="path12"></span><span className="path13"></span><span className="path14"></span><span className="path15"></span></span></span>
              <span className='rx-footer-ecdc-paymentway-item'><span className='mlkicon-Banking'><span className="path1"></span><span className="path2"></span></span></span>
            </div>
          </div>        
        </div>
      </div>
    </div>
    )}
}

export default withTranslation('translations')(Footer)