import React, { Component } from 'react'

class Contact extends Component {
	
	constructor(props) {    
    super(props) 
    this.state = { 
      editingData: {},
      msg: ''
    }
  }

  onBlurData(e, name) {
    let editingData = this.state.editingData
    editingData[name] = e.target.value
    this.setState({ editingData: editingData })    
  }

  onClickLoginSubmit(e) {  
  }

  render() {
    return (
    <div className='container'>
      <div className='lk-login-customer-page'>
        <div className='row lk-row-nomar'>
          <div className='rxauthen-left-container lk-contact'>
          	<div className='lk-contact-title'>Liên hệ</div>
			      <div className='lk-contact-desc'>Đến với chúng tôi để khẳng định chính mình, để thể hiện cái khí chất của đàn ông!</div>          	
          </div>
          <div className='lk-page-customer-info lk-page-info-login'>
            <div className='lk-login-section-title-wrap clearfix'>
              <div className='lk-login-section-title-contact rx-active'>Liên hệ</div>              
            </div>
            <div className='lk-register-customer-inner'>
      			  <div className='clearfix'>
                      { this.state.msg && <div className='clearfix'><div className='lk-input-error' >{this.state.msg}</div></div> }
      				<div className='lk-input-group-row'>
                <div>Họ tên </div>
                <input tabIndex='1' type='text' name='username' onChange={(e) => this.onBlurData(e, 'name')} className='fullwidth-input' />
              </div>
              <div className='lk-input-group-row'>
                <div>Điện thoại di động </div>
                <input tabIndex='2' type='text' name='phone' onChange={(e) => this.onBlurData(e, 'phone')} className='fullwidth-input' />
              </div>
              <div className='lk-input-group-row'>
      		      <div>Địa chỉ </div>
      		      <input tabIndex='3' type='text' name='address' onChange={(e) => this.onBlurData(e, 'address')} className='fullwidth-input' />
      		    </div>
              <div className='lk-input-group-row'>
                <div>Email </div>
                <input tabIndex='4' type='text' name='email' onChange={(e) => this.onBlurData(e, 'email')} className='fullwidth-input' />
              </div>
      				<div className='lk-input-group-row lk-contact'>
                <div>Nội dung </div>
                <textarea tabIndex='4' type='text' name='content' onChange={(e) => this.onBlurData(e, 'content')} className='fullwidth-input' />
              </div>    
      			  </div>
      			  <div className='lk-login-submit-section lk-contact'>
      			    <div tabIndex='10' className='lk-page-login-customer-submit' onClick={(e) => this.onClickRegisterSubmit(e)} onKeyPress={(e) => this.onClickRegisterSubmit(e)}>Gởi ngay</div> 
      			  </div>
      			  <div className='lk-login-submit-section'></div>
      			</div> 

          </div>
        </div>
      </div>
    </div>
    )
  }
}
export default Contact