import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { activateGeod, closeGeod, rxnavClose } from './redux'

import Main from './main'

const Header = global.rootRequiretheme('Header').default
const Footer = global.rootRequiretheme('Footer').default

class App extends Component {
  render() {
    let linkCss = '/static/css/main_' + global.currentTheme + '.css?ver=8.5'
    return (
    	<div onClick={() => { this.props.rxnavClose() }}>
        <link rel="preload" href={linkCss} as="style"/>
    		<Header staticContext={this.props.staticContext} />
        <Main />
        <Footer />
    	</div>
    )
  }
}

// AppContainer.js
const mapStateToProps = (state, ownProps) => ({
  geod: state.geod,
})

const mapDispatchToProps = {
  activateGeod,
  closeGeod,
  rxnavClose
}

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

export default withRouter(AppContainer)
