import { polyfill }  from 'es6-promise'

import {} from './global'

import Admin from './components/Admin'
import AdminLogin from './components/Admin/Login'
import AdminDashboard from './components/Admin/Dashboard'
import AdminProduct from './components/Admin/Product'
import AdminContent from './components/Admin/Content'
import AdminPage from './components/Admin/Page'
import notPermission from './components/Authenticate/notPermission'

import AdminCateProduct from './components/Admin/Cateproduct'
import AdminCateBrand from './components/Admin/Catebrand'
import AdminBestCollection from './components/Admin/BestCollection'
import AdminAdvertisement from './components/Admin/Advertisement'
import AdminAttribute from './components/Admin/Attribute'
import AdminOption from './components/Admin/Option'
import AdminNavigation from './components/Admin/Navigation'
import AdminUser from './components/Admin/User'
import AdminRole from './components/Admin/Role'
import AdminPermission from './components/Admin/Permission'
import AdminOrder from './components/Admin/Order'
import AdminOrderWait from './components/Admin/OrderWait'
import AdminOrderShip from './components/Admin/OrderShip'
import AdminOrderPayment from './components/Admin/OrderPayment'
import AdminConfig from './components/Admin/Config'
import AdminCustomer from './components/Admin/Customer'
import AdminDiscount from './components/Admin/Discount'
import AdminQuestion from './components/Admin/Question'
import AdminInventory from './components/Admin/Inventory'
import AdminReport from './components/Admin/Report'

import AdminSlide from './components/Admin/Slide'
import AdminSettingFilter from './components/Admin/Settingfilter'

// Themes
const Themeroutes = global.rootRequiretheme('Themeroutes').default

polyfill()

// SERVER SIDE LOAD DATA
const routes = [	
	{	path: '/admin',		
		component: Admin,
		routes: [				
			{ path: '/admin',
				exact: true,
				component: AdminDashboard,
				loadData: 'admin_product_get'
			},
			{	path: '/admin/login',
				component: AdminLogin,
			},
			{ path: '/admin/product',
				component: AdminProduct,				
			},
			{ path: '/admin/category-product',
				component: AdminCateProduct,				
			},
			{ path: '/admin/category-brand',
				component: AdminCateBrand,				
			},
			{ path: '/admin/best-collection',
				component: AdminBestCollection,				
			},
			{ path: '/admin/advertisement',
				component: AdminAdvertisement,				
			},
			{ path: '/admin/product_attribute',
				component: AdminAttribute,
			},
			{ path: '/admin/product_option',
				component: AdminOption,
			},			
			{ path: '/admin/content',
				component: AdminContent,				
			},
			{ path: '/admin/page',
				component: AdminPage,				
			},
			{ path: '/admin/navigation',
				component: AdminNavigation,				
			},
			{ path: '/admin/slide',
				component: AdminSlide,
			},
			{ path: '/admin/notPermission',
				component: notPermission,
			},
      { path: '/admin/settingfilter',
        component: AdminSettingFilter,
      },
			{ path: '/admin/config',
				component: AdminConfig,
			},
			{ path: '/admin/user',
				component: AdminUser,
			},
			{ path: '/admin/user_role',
				component: AdminRole,
			},
			{ path: '/admin/order',
				component: AdminOrder,
			},
			{ path: '/admin/order_wait',
				component: AdminOrderWait,
			},
			{ path: '/admin/order_ship',
				component: AdminOrderShip,
			},
			{ path: '/admin/order_payment',
				component: AdminOrderPayment,
			},
			{ path: '/admin/customer',
				component: AdminCustomer,
			},
			{ path: '/admin/permission',
				component: AdminPermission,
			},
			{ path: '/admin/discount',
				component: AdminDiscount,
			},
			{ path: '/admin/question',
				component: AdminQuestion,
			},
			{ path: '/admin/inventory',
				component: AdminInventory,
			},
			{ path: '/admin/report',
				component: AdminReport,
			}
		]
	}	
].concat(Themeroutes)

export default routes