/* global localStorage */
// Website - rxshop
// src/classes/ulti.js
import { rxsecret } from './secret'
let rxu = require('./../universal/rxUtil.js')
let secret = rxsecret()

export function rxgetdate(timestamp) {
  timestamp = timestamp * 1000
  let u = new Date(timestamp)
  let tempstr  = ('0' + u.getUTCDate()).slice(-2) + '-' + ('0' + (u.getUTCMonth() + 1)).slice(-2) + '-' + u.getUTCFullYear() + ' '
                 + ('0' + u.getHours()).slice(-2) + ':' + ('0' + u.getMinutes()).slice(-2) + ':' + ('0' + u.getSeconds()).slice(-2)
  return tempstr
}

export function rxgetdatetime(timestamp) {
  let u = new Date(timestamp)
  let tempstr  = u.getFullYear() + '-' + ('0' + (u.getMonth() + 1)).slice(-2) + '-' + ('0' + u.getDate()).slice(-2) + ' '
                 + ('0' + u.getHours()).slice(-2) + ':' + ('0' + u.getMinutes()).slice(-2) + ':' + ('0' + u.getSeconds()).slice(-2)
  return tempstr
}
export function rxgetdatetimeprint (timestamp) {
  timestamp = (timestamp + 25200) * 1000
  let u = new Date(timestamp)
  let tempstr = ('0' + u.getUTCDate()).slice(-2) + '/' + ('0' + (u.getUTCMonth() + 1)).slice(-2) + '/' + u.getUTCFullYear()
  return tempstr
}
export function rxsget(arr, index1, default1) {
  default1 = default1 || ''
  return (typeof arr[index1] !== 'undefined') ? arr[index1] : default1
}

export function rxgetLocalStore(cname, cdefault) {
  if (typeof(Storage) !== 'undefined') {
      console.log('Trình duyệt của bạn hỗ trợ Storage');
  } else {
      console.log('Trình duyệt của bạn không hỗ trợ Storage');
  }

  if (typeof window !== 'undefined' && typeof(Storage) !== 'undefined')  {
    return localStorage.getItem(cname)
  } else {
    return cdefault
  }
}

export function helpKeywords(cats) {
  let result = ''
  if (cats) {
    for(let i = 0; i < cats.length; i++) {
      result += cats[i].text + ', '
    }
    return result.slice(0, -2)
  }
}

export function rxChunkArray(array, size) {
  const chunked_arr = [];
  for (let i = 0; i < array.length; i++) {
    const last = chunked_arr[chunked_arr.length - 1];
    if (!last || last.length === size) {
      chunked_arr.push([array[i]]);
    } else {
      last.push(array[i]);
    }
  }
  return chunked_arr;
}

export function rxcurrencyVnd(x) {
  x = (Math.round(Number(x) * 100) / 100) || 0
  let strcatecurrency = rxgetLocalStore('currency')
  let typecurrency = 'VND'
  let symbolcurrency = 'VND'
  if (strcatecurrency) {
    let objcurrency = {name: ''}
    try {
      objcurrency = JSON.parse(strcatecurrency)
      if (objcurrency && objcurrency.exchangerate) {
        let exchangerate = Number(objcurrency.exchangerate.replace(/,/g,''))
        symbolcurrency = objcurrency.symbol
        typecurrency = objcurrency.name
        x = (typecurrency !== 'VND') ? (x * exchangerate).toFixed(2) : (x * exchangerate).toFixed(0)
      }
    } catch(e) {console.log(e)}
  }
  let strresult = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
  if (typecurrency === 'VND') {
    strresult = strresult + ' đ'
  } else if (typecurrency === 'AUD') {
    strresult = '$' + strresult + ' AUD'
  } else {
    strresult = symbolcurrency + ' ' + strresult
  }
  return strresult;
}

export function inDays(date) {
  const NOW = new Date()
  const times = [['giây', 1], ['phút', 60], ['giờ', 3600], ['ngày', 86400], ['tuần', 604800], ['tháng', 2592000], ['năm', 31536000]]

  date = new Date(date * 1000) || new Date()
  var diff = Math.round((NOW - date) / 1000)
  for (var t = 0; t < times.length; t++) {
    if (diff < times[t][1]) {
      if (t === 0) {
        return 'Cách đây vài giây'
      } else {
        diff = Math.round(diff / times[t - 1][1])
        return diff + ' ' + times[t - 1][0] + ' trước'
      }
    }
  }
}

// export function rxcurrencyVnd(x) {
//   x = (Math.round(Number(x) * 100) / 100) || 0
//   let strcatecurrency = rxgetLocalStore('currency')
//   let typecurrency = 'AUD'
//   let symbolcurrency = 'AUD'
//   if (strcatecurrency) {
//     let objcurrency = {name: ''}
//     try {
//       objcurrency = JSON.parse(strcatecurrency)
//       if (objcurrency && objcurrency.exchangerate) {
//         let exchangerate = Number(objcurrency.exchangerate.replace(/,/g,''))
//         symbolcurrency = objcurrency.symbol
//         typecurrency = objcurrency.name
//         x = (typecurrency !== 'VND') ? (x * exchangerate).toFixed(2) : (x * exchangerate).toFixed(0)
//       }
//     } catch(e) {console.log(e)}
//   }
//   let strresult = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
//   if (typecurrency === 'VND') {
//     strresult = strresult + ' đ'
//   } else if (typecurrency === 'AUD') {
//     strresult = '$' + strresult + ' AUD'
//   } else {
//     strresult = symbolcurrency + ' ' + strresult
//   }
//   return strresult;
// }

export function rxChangeAlias(x) {
  let str = x;
  if (str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g,'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,'y');
    str = str.replace(/đ/g,'d');
    str = str.replace(/ + /g,' ');
    str = str.replace(/ /g,'-');
    str = str.trim();
  }

  return str;
}

export const rxconfig = (function () {
  let config = []

  // Web
  config.web_port = secret.web_port

  // Domain
  config.web_domain = secret.base_domain;

  // Server
  config.base_api = secret.base_api

  // Authorize
  config.api_authorize = config.base_api + '/authorize'
  config.api_authcutomer = config.api_authorize + '/authcustomer'

  // Homepage
  config.api_home = config.base_api + '/site'
  config.api_home_product = config.api_home + '/product'
  config.api_home_productdetail = config.api_home + '/productdetail'
  config.api_home_news = config.api_home + '/news'
  config.api_home_newsdetail = config.api_home + '/newsdetail'
  config.api_home_order = config.api_home + '/order'

  config.api_home_option = config.api_home + '/option'
  config.api_home_config = config.api_home + '/config'
  config.api_home_header = config.api_home + '/header'

  config.api_home_pages  = config.api_home + '/pages'
  config.api_home_pagesdetail = config.api_home + '/pagesdetail'

  // Product & productcate
  config.api_category = config.base_api + '/productcate'
  config.api_category_brand = config.api_category + '/brand'
  config.api_category_option_all = config.api_category + '/option_all'
  config.api_category_option = config.api_category + '/option'
  config.api_category_edit = config.api_category + '/edit'
  config.api_category_delete = config.api_category + '/delete'
  config.api_category_restore = config.api_category + '/restore'
  config.api_category_all = config.api_category + '/cate_all'
  config.api_category_only = config.api_category + '/cate_only'
  config.api_category_brand_all = config.api_category + '/brand_all'
  config.api_category_cate_brand = config.api_category + '/cate_brand'
  
  // Best Collection & best collection cate
  config.api_category_collection = config.api_category + '/collection'
  config.api_category_collection_edit = config.api_category + '/edit'
  config.api_category_collection_delete = config.api_category + '/delete'
  config.api_category_collection_restore = config.api_category + '/restore'
  config.api_category_collectioncate = config.api_category + '/collectioncate'
  config.api_category_collectioncate_edit = config.api_category + '/edit'
  config.api_category_collectioncate_delete = config.api_category + '/delete'
  config.api_category_collectioncate_restore = config.api_category + '/restore'
  
  // Advertisement && advertisement cate
  config.api_category_advertisement = config.api_category + '/advertisement'
  config.api_category_advertisementcate = config.api_category + '/advertisementcate'
  config.api_category_advertisement_edit = config.api_category + '/edit'
  config.api_category_advertisementcate_edit = config.api_category + '/edit'
  config.api_category_advertisementcate_delete = config.api_category + '/delete'
  config.api_category_advertisementcate_restore = config.api_category + '/restore'

  config.api_product = config.base_api + '/product'
  config.api_product_updateprice = config.api_product + '/updateprice'
  config.api_product_updatebrandobj = config.api_product + '/updatebrandobj'
  config.api_product_edit = config.api_product + '/edit'
  config.api_product_delete = config.api_product + '/delete'
  config.api_product_restore = config.api_product + '/restore'
  config.api_product_rating = config.api_product + '/rating'
  config.api_product_inventory = config.api_product + '/inventory'
  config.api_product_importexcel = config.api_product + '/importexcel'
  config.api_product_sentMail = config.api_product + '/sentMail'
  config.api_product_getSale = config.api_product + '/getSale'

  // Content & contentcate
  config.api_contentcate = config.base_api + '/contentcate'
  config.api_contentcate_edit = config.api_contentcate + '/edit'
  config.api_contentcate_delete = config.api_contentcate + '/delete'
  config.api_contentcate_restore = config.api_contentcate + '/restore'

  config.api_content = config.base_api + '/content'
  config.api_content_edit = config.api_content + '/edit'
  config.api_content_delete = config.api_content + '/delete'
  config.api_content_restore = config.api_content + '/restore'
  config.api_content_rating = config.api_content + '/rating'

  config.api_contentpage = config.base_api + '/page'
  config.api_contentpage_edit = config.api_contentpage + '/edit'
  config.api_contentpage_delete = config.api_contentpage + '/delete'
  config.api_contentpage_restore = config.api_contentpage + '/restore'

  // Slide Baner Home
  config.api_slide = config.base_api + '/slide'
  config.api_slide_logo = config.api_slide + '/logo'
  config.api_slide_edit = config.api_slide + '/edit'
  config.api_slide_delete = config.api_slide + '/delete'
  config.api_slide_restore = config.api_slide + '/restore'

  // advertisement cate
  config.api_advertisement = config.base_api + '/advertisement'
  config.api_advertisement_edit = config.api_advertisement + '/edit'
  config.api_advertisement_delete = config.api_advertisement + '/delete'
  config.api_advertisement_restore = config.api_advertisement + '/restore'

  // Setting Filter Option
  config.api_setoption = config.base_api + '/option'
  config.api_setoption_edit = config.api_setoption + '/edit'
  config.api_setoption_delete = config.api_setoption + '/delete'
  config.api_setoption_restore = config.api_setoption + '/restore'

  // Customer
  config.api_customer = config.base_api + '/customer'
  config.api_customer_edit = config.api_customer + '/edit'
  config.api_customer_delete = config.api_customer + '/delete'
  config.api_customer_restore = config.api_customer + '/restore'
  config.api_customer_allorder = config.api_customer + '/allorder'
  config.api_customer_allproduct = config.api_customer + '/allproduct'

  // User
  config.api_user = config.base_api + '/user'
  config.api_user_edit = config.api_user + '/edit'
  config.api_user_delete = config.api_user + '/delete'
  config.api_user_restore = config.api_user + '/restore'
  config.api_user_admin = config.api_user + '/admin'
  config.api_user_edit_customer = config.api_user + '/editcustomer'
  config.api_user_sentcode = config.api_user + '/sentcode'
  config.api_user_reset_password = config.api_user + '/reset_password'

  // Order
  config.api_order = config.base_api + '/order'
  config.api_order_edit = config.api_order + '/edit'
  config.api_order_delete = config.api_order + '/delete'
  config.api_order_restore = config.api_order + '/restore'
  config.api_order_optionproduct = config.api_order + '/optionproduct'
  config.api_order_all = config.api_order + '/all'

  // Permission
  config.api_permission = config.base_api + '/permission'
  config.api_permissionall = config.api_permission + '/all'
  config.api_permission_edit = config.api_permission + '/edit'
  config.api_permission_delete = config.api_permission + '/delete'
  config.api_permission_restore = config.api_permission + '/restore'
  config.api_permission_genpermission = config.api_permission + '/generation'

  // Role
  config.api_role = config.base_api + '/role'
  config.api_role_edit = config.api_role + '/edit'
  config.api_role_delete = config.api_role + '/delete'
  config.api_role_restore = config.api_role + '/restore'
  config.api_role_all = config.api_role + '/all'

  // Config Setting
  config.api_setting = config.base_api + '/config'
  config.api_config_setting = config.api_setting + '/setting'
  config.api_setting_edit = config.api_setting + '/edit'
  config.api_setting_delete = config.api_setting + '/delete'

  // Discount
  config.api_discount = config.base_api + '/discount'
  config.api_discount_edit = config.api_discount + '/edit'
  config.api_discount_find = config.api_discount + '/find'
  config.api_discount_delete = config.api_discount + '/delete'
  config.api_discount_restore = config.api_discount + '/restore'
  config.api_discount_active = config.api_discount + '/active'
  config.api_discount_inactive = config.api_discount + '/inactive'

  // Dashboard
  config.api_dashboard = config.base_api + '/dashboard'

  config.api_sentemail = config.api_home + '/sentemail'

  config.api_sentemail = config.base_api + '/sentemail'
  config.api_sentemail_orderdetail = config.api_sentemail + '/orderdetail'
  config.api_sentemail_test = config.api_sentemail + '/test'

  return config
}())

rxu.config = rxconfig
export default rxu
