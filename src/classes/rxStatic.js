let path = require('path')
let fs = require('fs')

let serverStaticFiles = function (request, response) {
  response.setHeader('Cache-Control', 'public, max-age=2592000')
  response.setHeader('Expires', new Date(Date.now() + 2592000000).toUTCString())
  response.setHeader('x-content-type-options', 'nosniff')
  response.setHeader('x-frame-options', 'SAMEORIGIN')
  response.setHeader('x-xss-protection', '1; mode=block')
  response.setHeader('strict-transport-security', 'max-age=31536000; includeSubDomains; preload')
  response.removeHeader("X-Powered-By");

  let filePath = './build' + request.url
  if (filePath === './') filePath = './index.html'
  let isStatic = (filePath.indexOf('upload/image') !== -1)
  let extname = path.extname(filePath)
  let contentType = 'text/html'

  switch (extname) {
    case '.xml':
      isStatic = true
      contentType = 'text/xml'
      break
    case '.txt':
      isStatic = true
      contentType = 'text/html'
      break
    case '.xsl':
      isStatic = true
      contentType = 'application/xslt+xml'
    break
    case '.ico':
      isStatic = true
      contentType = 'image/x-icon'
      break
    case '.js':
      isStatic = true
      contentType = 'text/javascript'
      break
    case '.css':
      isStatic = true
      contentType = 'text/css'
      break
    case '.json':
      isStatic = true
      contentType = 'application/json'
      break
    case '.png':
      isStatic = true
      contentType = 'image/png'
      break
    case '.jpg':
      isStatic = true
      contentType = 'image/jpg'
      break
    case '.svg':
      isStatic = true
      contentType = 'image/svg+xml'
      break
    case '.gif':
      isStatic = true
      contentType = 'image/gif'
      break
    case '.ttf':
      isStatic = true
      contentType = 'font/ttf'
      break
    case '.woff':
      isStatic = true
      contentType = 'font/woff'
      break
    case '.wav':
      isStatic = true
      contentType = 'audio/wav'
      break
    case '.gz':
      isStatic = true
      contentType = 'application/gzip'
    break

    default:
      if (isStatic && !extname) {
        filePath += 'ico_app_default1.png'
        contentType = 'image/png'
      }
      break
  }

  if (isStatic) {
    fs.readFile(filePath, function (error, content) {
      if (error) {
        if (error.code === 'ENOENT') {
          /* fs.readFile('./404.html', function (error, content) {
            error ? console.log(error) : error = null
            response.writeHead(200, { 'Content-Type': contentType })
            response.end(content, 'utf-8')
          }) */
          response.writeHead(404)
          response.end()
        } else {
          response.writeHead(500)
          response.end('Sorry, check with the site admin for error: ' + error.code + ' ..\n')
          response.end()
        }
      } else {
        response.writeHead(200, { 'Content-Type': contentType })
        response.end(content, 'utf-8')
      }
    })

    return true
  }

  return false
}

module.exports = {
  serverStaticFiles: serverStaticFiles
}
