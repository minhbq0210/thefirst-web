import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { I18nextProvider } from 'react-i18next'

import i18n from './i18n'
import AppContainer from './app'
import { store } from './redux'

global.rootRequiretheme('Styles/style')

render(
  <Provider store={store}>
  	<BrowserRouter>
  		<I18nextProvider i18n={ i18n }>
  			<AppContainer />
  		</I18nextProvider>
  	</BrowserRouter>
  </Provider>
, document.getElementById('root'))