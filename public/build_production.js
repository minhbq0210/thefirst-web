/**
 * Custom Node script to generate index.ejs based on the index.html file.
 */

const path = require('path');
const fs = require('fs');
const htmlPath = path.resolve('build/index.html');
const ejsPath = path.resolve('build/index.ejs');

// PROGRESS start
console.log('Start generating an index.ejs based on the index.html...');

// READ index.html
let html = fs.readFileSync(htmlPath, 'utf8');

let matchCss = html.match(/(main.*?\.css)/g)
let maincss = (matchCss && Array.isArray(matchCss) && matchCss[0]) ? matchCss[0] : ''

let matchJs = html.match(/<body.*?>(.+?)<\/body>/g)
let mainjs = (matchJs && Array.isArray(matchJs) && matchJs[0]) ? matchJs[0].replace('<body>','').replace('</body>','').replace('<noscript>You need to enable JavaScript to run this app.</noscript><div id="root"></div><script src="/lazysizes.min.js" async></script>', '') : ''

let htmlejs = fs.readFileSync(ejsPath, 'utf8');
let ejs = htmlejs.replace(/<!--maincss-->/g, maincss).replace('<!--mainjs-->', mainjs);

console.log('Reading the index.html file completed! Proceeding to writing...')

// WRITE index.ejs
fs.writeFileSync(ejsPath, ejs, 'utf8');
console.log('The index.ejs has been saved!');

// DELETE index.html
fs.unlinkSync(htmlPath);
console.log("The Process completed!\r\n");